export type UiVariant =
  | 'success'
  | 'primary'
  | 'dark'
  | 'danger'
  | 'light'
  | 'warning'
  | 'bg'

export type UiSize = 'small' | 'medium' | 'large'
