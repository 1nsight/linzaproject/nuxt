import type { InjectionKey, Ref } from '@nuxtjs/composition-api'
import { PageWithLocalesFragment } from '~/sdk/types'

export const AdminPagesProvider: InjectionKey<Ref<
  PageWithLocalesFragment[]
>> = Symbol('AdminPagesProvider')
