import type { Ref } from '@nuxtjs/composition-api'

export { Auth_Roles_Enum as Role } from '~/sdk/types'

export enum AppEvents {
  LocaleChanged = 'localeChanged',
  Connected = 'connected',
  ConnectedAsUser = 'connectedAsUser'
}

export enum AuthEvents {
  Login = 'login',
  Logout = 'logout',
  TokenUpdated = 'tokenUpdated'
}

export type ImageItem = {
  blurhash: string
  url: string
}

export enum ImageItemAction {
  add = 'add',
  delete = 'delete'
}

export type ToRefs<T extends Record<string, any>> = {
  [P in keyof T]: Ref<T[P]>
}
