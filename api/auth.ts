import type { Context } from '@nuxt/types'
import EventEmitter from 'eventemitter3'
import type { IncomingHttpHeaders } from 'http'
import jwtDecode from 'jwt-decode'
import { AuthEvents } from '~/types'
import { ssrRef } from '@nuxtjs/composition-api'

type JwtResponse = {
  jwt_token: string
  jwt_expires_in: string
}

type JwtClaims = {
  'x-hasura-user-id'?: string
  'x-hasura-allowed-roles'?: string[]
  'x-hasura-default-role'?: string
}

const claimsNamespace = 'https://hasura.io/jwt/claims'

export default class Auth extends EventEmitter {
  private _jwtExpiresIn = 90000
  private _refreshInterval?: number
  private _claims: JwtClaims = {}
  private _isAuthenticated = ssrRef(false)
  private _token = ssrRef<string | null>(null)

  constructor(private _context: Context) {
    super()
    if (process.client) {
      this.on(AuthEvents.Login, () => {
        if (this._refreshInterval) window.clearInterval(this._refreshInterval)
        this._refreshInterval = window.setInterval(async () => {
          if (this.isAuthenticated) {
            try {
              // silent refresh token
              await this.refreshToken()
            } catch {}
          }
        }, this._jwtExpiresIn - 10000)
      })

      this.on(AuthEvents.Logout, () => {
        if (this._refreshInterval) window.clearInterval(this._refreshInterval)
      })
    }
  }

  get isAuthenticated() {
    return this._isAuthenticated.value
  }

  get token() {
    return this._token.value
  }

  get userId(): string | null {
    return (this.getClaim('x-hasura-user-id') as string) || null
  }

  private _onLogout() {
    if (this._isAuthenticated.value) {
      this._jwtExpiresIn = 90000
      this._token.value = null
      this._claims = {}
      this._isAuthenticated.value = false
      this.emit(AuthEvents.Logout)
    }
    this.emit(AuthEvents.TokenUpdated)
  }

  private _onLogin() {
    if (!this._isAuthenticated.value) {
      this._isAuthenticated.value = true
      this.emit(AuthEvents.Login)
    }
    this.emit(AuthEvents.TokenUpdated)
  }

  private parseJwtResponse({ jwt_token, jwt_expires_in }: JwtResponse) {
    try {
      this._token.value = jwt_token
      this._jwtExpiresIn = parseInt(jwt_expires_in)
      const decoded: {
        [claimsNamespace]: JwtClaims
      } = jwtDecode(jwt_token)

      this._claims = decoded[claimsNamespace]
      this._onLogin()
    } catch (e) {
      this._onLogout()
      throw e
    }
  }

  getClaim(key: keyof JwtClaims) {
    return this._claims[key]
  }

  async checkRequest() {
    const { headers } = this._context.req
    if (headers?.['cookie']) {
      try {
        if (
          headers.cookie
            .split(';')
            .map(c => c.split('=')[0].trim())
            .includes('refresh_token')
        ) {
          const data = await this.refreshToken()
          if (data?.['set-cookie']) {
            // set cookies for browser
            this._context.res.setHeader('set-cookie', data['set-cookie'])
          }
        }
      } catch {} // error 401, token expired or not valid
    }
  }

  async register(email: string, password: string) {
    await this._context.$axios.$post('/api/auth/register', {
      email,
      password
    })
  }

  async login(email: string, password: string) {
    const data = await this._context.$axios.$post<JwtResponse>(
      '/api/auth/login',
      {
        email,
        password,
        cookie: true
      }
    )
    this.parseJwtResponse(data)
  }

  async logout() {
    this._onLogout()
    await this._context.$axios.$post('/api/auth/logout')
  }

  async refreshToken(): Promise<IncomingHttpHeaders | null> {
    try {
      const { data, headers } = await this._context.$axios.get<JwtResponse>(
        '/api/auth/token/refresh',
        { params: { cookie: true }, progress: false }
      )
      this.parseJwtResponse(data)
      return process.server ? headers : null
    } catch (err) {
      this._onLogout()
      throw err
    }
  }
}
