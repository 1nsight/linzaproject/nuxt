import type { Context } from '@nuxt/types'

type UploadParams = {
  type?: 'avatar' | 'album' | 'event' | 'portfolio' | 'folder' | 'store'
  with_blurhash?: boolean
  object_id?: string
  private?: boolean
}

export default class Storage {
  constructor(private _context: Context) {}

  async upload(
    path: string,
    file: File | Blob,
    params: UploadParams = {},
    headers: { [key: string]: string } = {}
  ) {
    const formData = new FormData()

    Object.keys(params).forEach(key => {
      formData.append(key, params[key as keyof UploadParams] as string)
    })

    formData.append('file', file)

    return await this._context.$axios.$post<UploadedFile>(
      `/api/storage/o/${path}`,
      formData,
      {
        headers: {
          'Content-Type': 'multipart/form-data',
          ...headers
        }
      }
    )
  }

  async delete(path: string, headers: { [key: string]: string } = {}) {
    return await this._context.$axios.$delete(`/api/storage/o/public/${path}`, {
      headers
    })
  }

  getUrl(path: string) {
    return path.replace('public/', '')
  }
}

export interface UploadedFile {
  key: string
  blurhash: string
  AcceptRanges: string
  LastModified: string
  ContentLength: number
  ETag: string
  ContentType: string
  Metadata: {
    token: string
  }
}
