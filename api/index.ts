import type { Context } from '@nuxt/types'
import Auth from './auth'
import Storage from './storage'

export default class Api {
  readonly auth: Auth
  readonly storage: Storage

  constructor(context: Context) {
    this.auth = new Auth(context)
    this.storage = new Storage(context)
  }
}
