import type { NuxtConfig } from '@nuxt/types'

const config: NuxtConfig = {
  modern: true,
  telemetry: false,
  pageTransition: {
    name: 'page',
    mode: 'out-in'
  },
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/composition-api',
    'nuxt-i18n',
    '@nuxtjs/svg-sprite',
    'nuxt-typed-vuex',
    '@nuxtjs/pwa'
  ],
  modules: [
    'v-wave/nuxt',
    '@nuxtjs/axios',
    '@nuxtjs/apollo',
    'portal-vue/nuxt',
    ['vue-scrollto/nuxt', { duration: 300 }],
    [
      '@nuxtjs/yandex-metrika',
      {
        id: '74930137',
        webvisor: true,
        // clickmap:true,
        useCDN: true
        // trackLinks:true,
        // accurateTrackBounce:true,
      }
    ]
  ],
  plugins: [
    '~plugins/global-setup',
    { src: '~/plugins/v-click-outside', ssr: false },
    { src: '~/plugins/blur-hash-image', ssr: false },
    { src: '~/plugins/inputmask', ssr: false },
    { src: '~/plugins/map', mode: 'client' },
    '~/plugins/element-ui'
  ],
  css: [
    '~/assets/scss/_reboot.scss',
    '~/assets/scss/_variables.scss',
    '~/assets/scss/_globals.scss'
  ],
  apollo: {
    errorHandler: '~/plugins/apollo-error-handler',
    clientConfigs: {
      default: '~/plugins/apollo-client'
    },
    defaultOptions: {
      $query: {
        loadingKey: 'loading',
        fetchPolicy: 'cache-and-network'
      },
      $mutation: {
        loadingKey: 'loading'
      }
    },
    watchLoading: '~/plugins/apollo-watch-loading-handler'
  },
  env: {},
  head: {
    titleTemplate: title => (title ? `${title} | ULU: photo & video events` : 'ULU: photo & video events'),
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        name: 'msapplication-TileColor',
        content: '#2b5797'
      },
      {
        name: 'msapplication-config',
        content: '/favicon/browserconfig.xml'
      },
      {
        name: 'theme-color',
        content: '#2b5797'
      }
    ],
    link: [
      {
        rel: 'apple-touch-icon',
        sizes: '180x180',
        href: '/favicon/apple-touch-icon.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '32x32',
        href: '/favicon/favicon-32x32.png'
      },
      {
        rel: 'icon',
        type: 'image/png',
        sizes: '16x16',
        href: '/favicon/favicon-16x16.png'
      },
      {
        rel: 'manifest',
        href: '/favicon/site.webmanifest'
      },
      {
        rel: 'mask-icon',
        href: '/favicon/safari-pinned-tab.svg',
        color: '#5bbad5'
      },
      {
        rel: 'shortcut icon',
        href: '/favicon/favicon.ico',
        type: 'image/x-icon'
      }
    ]
  },
  loading: { color: '#00ffb3' },
  axios: {
    proxy: true,
    credentials: true
  },
  proxy: {
    '/api/': {
      target: process.env.NUXT_ENV_API_ENDPOINT,
      pathRewrite: { '^/api/': '' }
    },
    '/storage/': {
      target: process.env.NUXT_ENV_STORAGE_BASE_URL,
      pathRewrite: { '^/storage/': '' }
    }
  },
  svgSprite: {
    input: '~/assets/icons/'
  },
  i18n: {
    defaultLocale: 'en',
    strategy: 'prefix_except_default',
    locales: [
      {
        code: 'en',
        file: 'en-US.ts'
      },
      {
        code: 'ru',
        file: 'ru-RU.ts'
      }
    ],
    lazy: true,
    langDir: 'lang/',
    detectBrowserLanguage: {
      useCookie: true,
      cookieKey: 'i18n_redirected'
    }
  },
  router: {
    linkExactActiveClass: 'is-active'
  },
  pwa: {
    meta: false
  },
  build: {
    extend(config, ctx) {
      if (
        ctx &&
        ctx.isClient &&
        config.optimization &&
        config.optimization.splitChunks
      ) {
        config.optimization.splitChunks.maxSize = 51200
      }
    }
  }
}

export default config
