import type { ApolloProvider } from 'vue-apollo'
import type { ScrollOptions } from 'vue-scrollto/vue-scrollto'
import { accessorType } from '@/store'
import Api from '~/api'
import { getSdk } from '~/sdk/wrapper'

type ElementDescriptor = Element | string

type ScrollToFunction = {
  (options: ScrollOptions): () => void
  (element: ElementDescriptor, options?: ScrollOptions): () => void
  (
    element: ElementDescriptor,
    duration: number,
    options?: ScrollOptions
  ): () => void
}

declare module 'vue/types/vue' {
  interface Vue {
    $accessor: typeof accessorType
    $scrollTo: ScrollToFunction
  }
}

declare module '@nuxt/types' {
  interface NuxtAppOptions {
    $accessor: typeof accessorType
    $api: Api
    $sdk: ReturnType<typeof getSdk>
  }

  interface NuxtApp {
    $scrollTo: ScrollToFunction
  }

  interface Context {
    apolloProvider: ApolloProvider
    app: NuxtAppOptions
  }
}

declare global {
  interface Window {
    BuyWithCrypto: any
  }
}
