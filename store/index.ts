import { getAccessorType } from 'typed-vuex'
import * as auth from './auth'
import * as layout from './layout'
import * as wallet from './wallet'
import * as seo from './seo'
import * as connection from './connection'

const state = () => ({})

const modules = {
  auth,
  layout,
  wallet,
  seo,
  connection
}

export const accessorType = getAccessorType({
  state,
  modules
})
