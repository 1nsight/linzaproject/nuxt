import { mutationTree } from 'nuxt-typed-vuex'
import { WalletFragment } from '~/sdk/types'

export const namespaced = true

export const state = () => ({
  wallet: null as WalletFragment | null
})

export const mutations = mutationTree(state, {
  setWallet(state, payload: WalletFragment | null) {
    state.wallet = payload
  }
})
