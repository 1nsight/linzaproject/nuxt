import { actionTree, getAccessorType, mutationTree } from 'typed-vuex'

export const state = () => ({
  sidebarMenu: false,
  homeTab: 'map',
  homeFilter: false,
  filterResult: [] as any,
  filterData: {
    styles: [] as any,
    payments: [] as any,
    types: '' as String,
    sortBy: 'Popular' as string,
    location: null
  } as any
})

export const mutations = mutationTree(state, {
  setSidebarMenu(state, payload: boolean) {
    state.sidebarMenu = payload
  },
  setHomeTab(state, value: string) {
    state.homeTab = value
  },
  setHomeFilter(state, payload: boolean) {
    state.homeFilter = payload
  },
  setFilterResult(state, payload: any) {
    state.filterResult = payload
  },
  setFilterData(state, payload: Array<any>) {
    state.filterData = payload
  }
})

export const actions = actionTree(
  { state, mutations },
  {
    toggleSidebarMenu({ state, commit }) {
      commit('setSidebarMenu', !state.sidebarMenu)
    },
    toggleHomeFilter({ state, commit }) {
      commit('setHomeFilter', !state.homeFilter)
    }
  }
)

export const accessorType = getAccessorType({
  state,
  actions,
  mutations
})
