import { getterTree, mutationTree } from 'nuxt-typed-vuex'
import { CurrentUserFragment } from '~/sdk/types'
import type { Role } from '~/types'

export const namespaced = true

export const state = () => ({
  user: null as CurrentUserFragment | null,
  urlAfterAuth: null as string | null
})

export const getters = getterTree(state, {
  userId: state => state.user?.id || null,
  isAuthenticated: state => !!state.user,

  withRole: state => (role: Role) => {
    if (!state.user?.account) return false
    if (state.user.account.default_role === role) return true
    return (
      state.user.account.account_roles?.findIndex(
        accountRole => accountRole?.role === role
      ) > -1
    )
  }
})

export const mutations = mutationTree(state, {
  setUser(state, payload: CurrentUserFragment | null) {
    state.user = payload
  },

  setUrlAfterAuth(state, payload: string | null) {
    state.urlAfterAuth = payload
  }
})
