import { mutationTree } from 'nuxt-typed-vuex'
import { SeoFragment } from '~/sdk/types'

export const namespaced = true

export const state = () => ({
  default: {} as SeoFragment
})

export const mutations = mutationTree(state, {
  setDefaultSeo(state, payload: SeoFragment) {
    state.default = payload
  }
})
