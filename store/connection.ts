import { getAccessorType, mutationTree } from 'typed-vuex'

export const state = () => ({
  connected: false,
  connectedAsUser: false
})

export const mutations = mutationTree(state, {
  setConnectionState(
    state,
    payload: { connected: boolean; connectedAsUser: boolean }
  ) {
    state.connected = payload.connected
    state.connectedAsUser = payload.connectedAsUser
  }
})

export const accessorType = getAccessorType({
  state,
  mutations
})
