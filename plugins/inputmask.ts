import { defineNuxtPlugin } from '@nuxtjs/composition-api'
import Vue from 'vue'
import Inputmask from 'inputmask'

export default defineNuxtPlugin(() => {
  Vue.directive('mask', {
    bind(el, binding) {
      const input =
        el.tagName === 'INPUT'
          ? el
          : (el.getElementsByTagName('INPUT')[0] as HTMLElement)
      if (input) Inputmask(binding.value).mask(input)
    }
  })
})
