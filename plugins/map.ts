/* eslint-disable import/no-extraneous-dependencies */
// import Vue from 'vue'
// const { MglMap, MglMarker, MglGeolocateControl } = require('vue-mapbox')
// const Mapbox = require('mapbox-gl')
// require('@/assets/scss/mapbox-gl.scss')

// Vue.component('MglMap', MglMap)
// Vue.component('MglMarker', MglMarker)
// Vue.component('MglGeolocateControl', MglGeolocateControl)

// Vue.prototype.$mapbox = Mapbox

import Vue from 'vue'
// @ts-ignore
import VueMapbox from '@studiometa/vue-mapbox-gl'
import '@/assets/scss/mapbox-gl.scss'

Vue.use(VueMapbox)
