import { defineNuxtPlugin } from '@nuxtjs/composition-api'
import Vue, { PluginFunction } from 'vue'
const vClickOutside: PluginFunction<unknown> = require('v-click-outside')

export default defineNuxtPlugin(() => {
  Vue.use(vClickOutside)
})
