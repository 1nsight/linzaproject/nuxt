import { defineNuxtPlugin } from '@nuxtjs/composition-api'
import { Loading } from 'element-ui'
import Vue from 'vue'

import '~/assets/scss/element/loading.scss'

const locale = require('element-ui/lib/locale')
const en = require('element-ui/lib/locale/lang/en')
const ru = require('element-ui/lib/locale/lang/ru-RU')

const switchLocale = (newLocale: string) => {
  switch (newLocale) {
    case 'ru':
      locale.use(ru)
      break

    default:
      locale.use(en)
      break
  }
}

export default defineNuxtPlugin(context => {
  Vue.use(Loading)
  switchLocale(context.app.i18n.locale)

  context.app.i18n.beforeLanguageSwitch = (_, newLocale) => {
    switchLocale(newLocale)
  }
})
