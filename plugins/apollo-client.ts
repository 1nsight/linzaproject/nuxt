import { defineNuxtPlugin } from '@nuxtjs/composition-api'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { ApolloLink, split } from 'apollo-link'
import { setContext } from 'apollo-link-context'
import { ErrorLink } from 'apollo-link-error'
import { createHttpLink } from 'apollo-link-http'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import { Message } from 'element-ui'
import { GraphQLClient } from 'graphql-request'
import { SubscriptionClient } from 'subscriptions-transport-ws'
import Api from '~/api'
import { getSdk } from '~/sdk/wrapper'
import { AppEvents, AuthEvents } from '~/types'

export default defineNuxtPlugin(context => {
  context.app.$api = new Api(context)

  const sdkClient = new GraphQLClient(context.env.NUXT_ENV_GRAPHQL_ENDPOINT)
  context.app.$sdk = getSdk(sdkClient)

  context.app.$api.auth.on(AuthEvents.TokenUpdated, () => {
    const headers = makeHeaders() as Headers
    sdkClient.setHeaders(headers)
    if (process.client) sendHeaders()
  })

  if (process.server) {
    return {
      httpEndpoint: context.env.NUXT_ENV_GRAPHQL_ENDPOINT,
      getAuth: () =>
        context.app.$api.auth.isAuthenticated
          ? `Bearer ${context.app.$api.auth.token}`
          : ''
    }
  }

  function makeHeaders() {
    return context.app.$api.auth.isAuthenticated
      ? {
          authorization: `Bearer ${context.app.$api.auth.token}`
        }
      : {}
  }

  const subscriptionClient = new SubscriptionClient(
    context.env.NUXT_ENV_GRAPHQL_SOCKET_ENDPOINT || '',
    {
      reconnect: true,
      lazy: false,
      connectionParams: () => ({
        headers: makeHeaders()
      })
    }
  )

  const onConnected = () => {
    const connectedAsUser = context.app.$api.auth.isAuthenticated
    context.app.$accessor.connection.setConnectionState({
      connected: true,
      connectedAsUser
    })

    if (process.client) {
      window.$nuxt.$root.$emit(AppEvents.Connected, true)
      window.$nuxt.$root.$emit(AppEvents.ConnectedAsUser, connectedAsUser)
    }
  }

  subscriptionClient.onConnected(onConnected)
  subscriptionClient.onReconnected(onConnected)

  subscriptionClient.onDisconnected(() => {
    context.app.$accessor.connection.setConnectionState({
      connected: false,
      connectedAsUser: false
    })
  })

  const sendHeaders = () => {
    const { client } = subscriptionClient
    if (client instanceof WebSocket) {
      const send = () =>
        client.send(
          JSON.stringify({
            type: 'connection_init',
            payload: { headers: makeHeaders() }
          })
        )

      if (client.readyState === WebSocket.OPEN) {
        send()
      } else {
        client.onopen = send
      }
    }
  }

  const wsLink = new WebSocketLink(subscriptionClient)

  const customFetch = async (uri: string, options: any) => {
    if (process.client) window.$nuxt.$loading.start()
    const response = await fetch(uri, options)
    if (process.client) window.$nuxt.$loading.finish()
    return response
  }

  const httpLink = createHttpLink({
    uri: context.env.NUXT_ENV_GRAPHQL_ENDPOINT,
    fetch: customFetch
  })

  const splitLink = split(
    ({ query }) => {
      const definition = getMainDefinition(query)
      return (
        definition.kind === 'OperationDefinition' &&
        definition.operation === 'subscription'
      )
    },
    wsLink,
    httpLink
  )

  const authLink = setContext((_, { headers }) => {
    return {
      headers: {
        ...headers,
        ...makeHeaders()
      }
    }
  })

  const errorLink = new ErrorLink(error => {
    console.error('errorLink', error)
    if (process.client) {
      Message.error(
        error?.graphQLErrors?.[0].message ||
          error?.networkError?.message ||
          'error'
      )
    }
  })

  const link = ApolloLink.from([errorLink, authLink, splitLink])

  return {
    link,
    cache: new InMemoryCache(),
    defaultHttpLink: false
  } as any
})
