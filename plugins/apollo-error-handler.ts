// import type { Context } from '@nuxt/types'
import { ErrorResponse } from 'apollo-link-error'

export default ({
  graphQLErrors,
  networkError,
  operation,
  forward
}: ErrorResponse) =>
  // context: Context
  {
    console.error('Global error handler')
    console.error(graphQLErrors, networkError, operation, forward)
  }
