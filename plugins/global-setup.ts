import useGetLocales from '@/hooks/get-locales'
import {
  defineNuxtPlugin,
  onGlobalSetup,
  provide,
  ref
} from '@nuxtjs/composition-api'
import { DefaultApolloClient } from '@vue/apollo-composable'
import { useCurrentUserSubscription, useWalletSubscription } from '~/sdk/hooks'
import { AppEvents, AuthEvents } from '~/types'

export default defineNuxtPlugin(async ({ app, redirect }) => {
  async function loadDefaultSeo() {
    try {
      const { pages } = await app.$sdk.getDefaultSeo({
        locales: useGetLocales(app.i18n)
      })
      const seo = pages[0]?.pages_contents[0]?.seo
      if (seo) {
        app.$accessor.seo.setDefaultSeo(seo)
      } else {
        throw new Error('Page not found')
      }
    } catch (err) {
      console.error('Unable to load default seo')
      console.error(err)
    }
  }

  if (process.server) {
    await app.$api.auth.checkRequest()
    if (app.$api.auth.isAuthenticated && !!app.$api.auth.userId) {
      const userId = app.$api.auth.userId

      // load current user
      try {
        const data = await app.$sdk.getCurrentUser({
          userId
        })
        app.$accessor.auth.setUser(data.user)
      } catch (err) {
        console.error('Unable to load current user')
        console.error(err)
      }

      // load wallet of current user
      try {
        const { wallets } = await app.$sdk.getWallet({
          userId
        })
        if (wallets.length > 0) app.$accessor.wallet.setWallet(wallets[0])
      } catch (err) {
        console.error('Unable to load wallet of current user')
        console.error(err)
      }

      // load default seo
      await loadDefaultSeo()
    }
  }

  onGlobalSetup(() => {
    provide(DefaultApolloClient, app.apolloProvider?.defaultClient)
    const userId = app.$accessor.auth.userId || ''

    if (process.client) {
      // CLIENT

      if (app.$accessor.auth.isAuthenticated) {
        app.$api.auth.refreshToken()
      }

      app.i18n.onLanguageSwitched = () => {
        loadDefaultSeo()
        window.$nuxt.$emit(AppEvents.LocaleChanged)
      }

      app.router?.beforeEach((_, __, next) => {
        app.$accessor.layout.setSidebarMenu(false)
        next()
      })

      const queryOptions = ref({
        enabled: false
      })

      // user updated
      const {
        onResult: onUserResult,
        variables: userVariables
      } = useCurrentUserSubscription({ userId }, queryOptions)
      onUserResult(result => {
        const navigate = app.$accessor.auth.user == null
        app.$accessor.auth.setUser(result?.data?.user || null)
        if (navigate) {
          if (app.$accessor.auth.urlAfterAuth) {
            redirect(app.localePath(app.$accessor.auth.urlAfterAuth))
            app.$accessor.auth.setUrlAfterAuth(null)
          } else {
            redirect(app.localePath('/profile'))
          }
        }
      })

      // wallet updated
      const {
        onResult: walletOnResult,
        variables: walletVariables
      } = useWalletSubscription({ userId }, queryOptions)
      walletOnResult(result => {
        if (result?.data?.wallets && result.data.wallets.length > 0)
          app.$accessor.wallet.setWallet(result.data.wallets[0])
      })

      app.$api.auth.on(AuthEvents.Login, () => {
        const userId = app.$api.auth.userId
        if (userId) {
          userVariables.value.userId = userId
          walletVariables.value.userId = userId
          queryOptions.value.enabled = true
        }
      })

      app.$api.auth.on(AuthEvents.Logout, () => {
        queryOptions.value.enabled = false
        app.$accessor.auth.setUser(null)
        app.router?.push(app.localePath('/'))
      })
    } else {
      // SERVER
    }
  })
})
