import { defineNuxtPlugin } from '@nuxtjs/composition-api'
import Vue from 'vue'

// @ts-ignore
import VueBlurHash from 'vue-blurhash'
export default defineNuxtPlugin(() => {
  Vue.use(VueBlurHash)
})
