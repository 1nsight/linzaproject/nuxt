#!/bin/sh
eval $(egrep -v '^#' .env | xargs)
apollo client:download-schema schema.graphql --endpoint ${NUXT_ENV_GRAPHQL_ENDPOINT} --header "x-hasura-admin-secret: $HASURA_GRAPHQL_ADMIN_SECRET"
