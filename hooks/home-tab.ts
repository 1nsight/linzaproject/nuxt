import { useContext, computed } from '@nuxtjs/composition-api'

export default function () {
  const { app } = useContext()
  const homeTab = computed(() => app.$accessor.layout.homeTab)
  const setHomeTab = app.$accessor.layout.setHomeTab

  const homeFilter = computed(() => app.$accessor.layout.homeFilter)
  const toggleHomeFilter = app.$accessor.layout.toggleHomeFilter

  const filterData = computed(() => app.$accessor.layout.filterData)
  const filterResult = computed(() => app.$accessor.layout.filterResult)
  const setFilterData = app.$accessor.layout.setFilterData
  const setFilterResult = app.$accessor.layout.setFilterResult

  return {
    homeTab,
    setHomeTab,

    homeFilter,
    toggleHomeFilter,

    filterData,
    filterResult,
    setFilterData,
    setFilterResult
  }
}
