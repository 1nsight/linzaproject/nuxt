import { useContext, computed } from '@nuxtjs/composition-api'

export default function () {
  const { app } = useContext()
  const sidebarMenu = computed(() => app.$accessor.layout.sidebarMenu)
  const setSidebarMenu = app.$accessor.layout.setSidebarMenu
  const toggleSidebarMenu = app.$accessor.layout.toggleSidebarMenu

  return {
    sidebarMenu,
    toggleSidebarMenu,
    setSidebarMenu
  }
}
