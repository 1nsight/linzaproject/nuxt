import { useContext } from '@nuxtjs/composition-api'
export default function (value: any) {
  const { app } = useContext()
  switch (value) {
    case 'PER_FILE':
      return app.i18n.t('payment_type.per_file')
      break
    case 'DONATION':
      return app.i18n.t('payment_type.donation')
      break
    case 'FREE':
      return app.i18n.t('payment_type.free')
      break
    case 'FIXED':
      return app.i18n.t('payment_type.fixed')
      break

    case 'INFO_PER_FILE':
      return app.i18n.t('payment_info.per_file')
      break
    case 'INFO_DONATION':
      return app.i18n.t('payment_info.donation')
      break
    case 'INFO_FREE':
      return app.i18n.t('payment_info.free')
      break
    case 'INFO_FIXED':
      return app.i18n.t('payment_info.fixed')
      break
  }
}
