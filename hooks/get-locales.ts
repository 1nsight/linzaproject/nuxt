import VueI18n from 'vue-i18n'
import { Locales_Enum } from '@/sdk/types'

export default (i18n: VueI18n) => {
  return i18n.locale === 'en'
    ? [Locales_Enum.En]
    : [Locales_Enum.En, i18n.locale.toUpperCase() as Locales_Enum]
}
