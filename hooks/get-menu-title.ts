import { MenuItemWithLocalizationFragment } from '@/sdk/types'

export default function (item: MenuItemWithLocalizationFragment): string {
  return (
    item.localizations[0]?.value || item?.localizations[1]?.value || item.id
  )
}
