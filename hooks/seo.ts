import type { MetaInfo } from 'vue-meta'
import { FullPageFragment, SeoFragment } from '~/sdk/types'
import { ToRefs } from '~/types'

declare type MetaInfoMapper<T> = {
  [P in keyof T]: P extends 'base'
    ? T[P] | undefined
    : T[P] extends () => any
    ? T[P] | undefined
    : T[P] extends Array<any> | Record<string, unknown>
    ? T[P]
    : T[P] | undefined
}

// export const seoSearchKeys: Array<
//   keyof Omit<SeoFragment, '__typename' | 'id'>
// > = ['description', 'title', 'image', 'keywords']

export default function (
  page: FullPageFragment,
  defaultSeo: SeoFragment,
  meta: ToRefs<MetaInfoMapper<Required<MetaInfo>> & MetaInfo>,
  lang: string
) {
  const seo = page.pages_contents[0].seo

  meta.title.value = seo.title || defaultSeo.title || undefined
  meta.htmlAttrs.value = {
    lang
  }

  function addMetaItem(name: keyof SeoFragment, og = false) {
    const content = seo[name]
    if (content) {
      const key = og ? `og:${name}` : name
      meta.meta.value.push({
        [og ? 'property' : 'name']: key,
        content,
        charset: 'utf8',
        vmid: key
      })
    }
  }

  addMetaItem('description')
  addMetaItem('keywords')

  addMetaItem('description', true)
  addMetaItem('title', true)
  addMetaItem('image', true)
}
