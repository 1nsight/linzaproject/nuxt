module.exports = {
  parser: 'vue-eslint-parser',
  extends: [
    '@nuxtjs',
    '@nuxtjs/eslint-config-typescript',
    'prettier',
    'prettier/vue',
    'plugin:prettier/recommended',
    'plugin:nuxt/recommended'
  ],
  rules: {
    camelcase: 'off',
    'no-console': ['warn', { allow: ['error'] }],
    'vue/no-v-html': 'off',
    'vue/attributes-order': 'off',
    'import/order': 'off'
  }
}
