FROM node:14-alpine as build

WORKDIR /usr/src/app

COPY package*.json .
COPY yarn.lock .

RUN yarn

COPY . .
RUN yarn build

ENV HOST 0.0.0.0
EXPOSE 3000

CMD ["yarn", "start"]