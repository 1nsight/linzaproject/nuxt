module.exports = {
  singleQuote: true,
  semi: false,
  spaces: 2,
  tabs: false,
  trailingComma: 'none',
  endOfLine: 'lf',
  arrowParens: 'avoid'
}
