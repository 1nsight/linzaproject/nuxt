import { defineNuxtMiddleware } from '@nuxtjs/composition-api'
import { Role } from '@/types'

export default defineNuxtMiddleware(context => {
  if (!context.app.$accessor.auth.withRole(Role.Administrator)) {
    context.error({
      statusCode: 403
    })
  }
})
