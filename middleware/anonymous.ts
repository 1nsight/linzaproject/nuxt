import { defineNuxtMiddleware } from '@nuxtjs/composition-api'

export default defineNuxtMiddleware(context => {
  if (context.app.$accessor.auth.isAuthenticated) {
    context.redirect(context.app.localePath('/profile'))
  }
})
