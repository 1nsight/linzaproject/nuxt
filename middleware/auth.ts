import { defineNuxtMiddleware } from '@nuxtjs/composition-api'

export default defineNuxtMiddleware(context => {
  if (!context.app.$accessor.auth.isAuthenticated) {
    context.app.$accessor.auth.setUrlAfterAuth(context.route.fullPath)
    context.redirect(context.app.localePath('/auth'))
    // context.error({
    //   statusCode: 403
    // })
  }
})
