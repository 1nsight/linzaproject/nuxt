export default {
  language: 'Русский',
  login_reg: 'Войти',

  email: 'Email',
  password: 'Пароль',
  finish: 'Готово',
  next_step: 'Далее',

  cookie: 'Мы используем куки чтобы улучшать проект для вас!',
  cookie_more: 'Подробнее',

  balance: 'Баланс',
  ico_price: 'Цена токена ICO',
  exchange_rate: 'Обменный курс',
  buy: 'Купить',
  send: 'Отправить',
  withdrawal: 'Вывод',
  tokens: 'Токены',
  to_buy: 'для покупки',
  exchange_rate_tooltip: 'Live update from coinmarketcap.com',
  map_input: 'Введите адрес',
  share_link: 'Поделиться ссылкой',

  sidebar_title: {
    project: 'Проект',
    social: 'Социальные сети'
  },

  alert: {
    success_upload: 'Успешная загрузка',
    error_upload: 'Ой, попробуйте еще раз, пожалуйста',
    delete_media: 'Изображения удалены'
  },

  errors: {
    email_not_valid: 'Email введен неверно',
    password_not_valid: 'Password must be 8 characters long',
    password_again: 'Пароли не совпадают',
    oops: 'Ой, попробуйте еще раз, пожалуйста',
    account_does_not_exist: 'Пользователь не существует',
    account_already_exists: 'Аккаунт уже существует'
  },

  auth: {
    welcome: 'Добро пожаловать!',
    description:
      'Это уникальное приложение позволяет вам находить лучших фотографов со всей планеты, заказывать фотосессии, выбирать и загружать фотографии, просматривать отличные коллекции стоковых фотографий.',
    login: 'Войти',
    loginBtn: 'Войти',
    register: 'Регистрация',
    forgot: 'Забыли?',
    password_again: 'Повторите пароль',
    login_with_google: 'Войти через Google',
    easy_way: 'Простой способ'
  },

  profile: {
    menu: {
      profile: 'Профиль',
      photography: 'Фотографии',
      settings: 'Настройки',
      notifications: 'Оповещения',
      chat: 'Чат',
      wallet: 'Кошелёк',
      search: 'Поиск',
      logout: 'Выход'
    }
  },
  footer: {
    terms: 'Условия использования',
    accept: 'Принимаем',
    copyright: '© United Love Universe. All rights reserved.'
  },

  main: {
    h_tokens: 'Купить токены',
    h_more: 'Подробнее',
    copyright: '2020 © Сделано с любовью для фотографов',
    terms: 'Условия использования',
    more: 'подробнее',
    token: 'О Токене ULUS',
    download: 'Скачать'
  },

  app: {
    text: 'Приложение Ulu для смартфонов',
    download: 'Скачать мобильное приложение',
    soon: 'Скоро'
  },

  settings: {
    add: 'Добавить',
    name: 'Имя',
    username: 'Логин',
    slogan: 'Лозунг',
    location: 'Местоположение',
    styles: 'Стили',
    bio: 'Обо мне',
    payment: 'Оплата',
    storage: 'Облачное хранилище',
    statistics: 'Статистика',
    time: 'Время загрузки',
    phone: 'Телефон',
    email: 'Email',
    update: 'Сохранить настройки',
    save: 'Сохранить',
    subscription: 'Месячная подписка'
  },

  pagenav: {
    feed: 'Лента',
    albums: 'Альбомы',
    location: 'Местонахождение',
    payment: 'Типы оплаты'
  },

  buttons: {
    back: 'Назад',
    save: 'Сохранить',
    delete: 'Удалить',
    cancel: 'Отменить',
    private: 'Приватный',
    open: 'Открытый',
    share: 'Поделиться',
    create: 'Создать',
    upload: 'Загрузить',
    edit_settings: 'Настройки',
    photo_session: 'Фотосессия',
    create_album: 'Создать альбом',
    create_event: 'Создать событие',
    copy_link: 'Скопировать ссылку',
    add_payment: 'Добавить тип оплаты',
    set_cover: 'Установить как обложку'
  },

  upload: {
    title: 'Загрузка файлов',
    drag: 'Перетащите или выберите'
  },

  payment_type: {
    per_file: 'За файл',
    donation: 'Донат',
    fixed: 'За файлы',
    free: 'Бесплатно',
    marketing: 'Маркетинг'
  },
  payment_info: {
    per_file: 'Покупайте только те файлы, которые вам нравятся',
    fixed: 'Единая цена за все файлы',
    donation: 'Платите столько, сколько считаете нужным',
    free: 'Дружба это все',
    marketing: 'Что-то лучше денег'
  },

  events: {
    add_event: 'Создать событие',
    edit_event: 'Редактировать событие',
    add_album: 'Создать альбом',
    edit_album: 'Редактировать альбом',
    client: 'Клиент',
    note: 'Комментарий',
    date: 'Дата',
    time: 'Время',
    name: 'Название',
    description: 'Описание',
    location: 'Местоположение',
    tags: 'Теги',
    payment: 'Типы оплаты',
    price: 'Цена',
    income: 'Доход',
    downloads: 'Загрузки',
    visits: 'Просмотры'
  }
}
