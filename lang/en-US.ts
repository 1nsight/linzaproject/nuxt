const translation = {
  language: 'English',
  login_reg: 'Login or register',

  cookie: 'We use cookies to improve user experience for you!',
  cookie_more: 'Read more',

  email: 'Email',
  password: 'Password',
  finish: "Let's go",
  next_step: 'Next step',

  balance: 'Balance',
  ico_price: 'ICO Token price',
  exchange_rate: 'Exchange rate',
  buy: 'Buy',
  to_buy: 'to buy',
  to_send: 'to send',
  to_account: 'To account',
  send: 'Send',
  withdrawal: 'Withdraw',
  tokens: 'Tokens',
  exchange_rate_tooltip: 'Live update from coinmarketcap.com',
  success: 'Success',
  failure: 'Failure',
  processing: 'Processing...',
  waiting_confirmation: 'Waiting for network confirmation...',
  map_input: 'Enter the address',
  share_link: 'Share link',

  sidebar_title: {
    project: 'Project',
    social: 'Social'
  },

  alert: {
    success_upload: 'Success upload',
    error_upload: 'Oops, try again please',
    delete_media: 'Delete done'
  },

  errors: {
    email_not_valid: 'Email is not valid',
    password_not_valid: 'Password must be 8 characters long',
    password_again: 'Password and confirmation is not equal',
    oops: 'Oops, try again please',
    account_does_not_exist: 'Account does not exist',
    account_already_exists: 'Account already exists'
  },

  auth: {
    welcome: 'Welcome!',
    description:
      'This unique app lets you – find great photographers all around the planet, order photoshoots, choose and download photos, browse great collections of stock photography.',
    login: 'Sign in',
    loginBtn: 'Log in',
    register: 'Register now',
    forgot: 'Forgot?',
    password_again: 'Password again',
    login_with_google: 'Login with Google',
    easy_way: 'Easy way'
  },

  profile: {
    menu: {
      profile: 'Profile',
      photography: 'Photography',
      settings: 'Settings',
      notifications: 'Notifications',
      chat: 'Chat',
      wallet: 'Wallet',
      search: 'Search',
      logout: 'Log out'
    }
  },
  footer: {
    terms: 'Terms of usage',
    accept: 'WE ACCEPT',
    copyright: '© United Love Universe. All rights reserved.'
  },

  main: {
    h_tokens: 'Buy tokens',
    h_more: 'Read more',
    copyright: '2020 (c) Made with love to share love through photography.',
    terms: 'Terms of usage',
    more: 'read more',
    token: 'About ULUS Token',
    download: 'About Download'
  },

  app: {
    text: 'Cool right? Download for free',
    download: 'Download mobile app',
    soon: 'Soon'
  },

  settings: {
    add: 'Add',
    name: 'Name',
    username: 'Username',
    slogan: 'Slogan',
    location: 'Location',
    styles: 'Styles',
    bio: 'About',
    payment: 'Payment',
    storage: 'Cloud storage',
    statistics: 'Statistics',
    time: 'Upload time',
    phone: 'Phone',
    email: 'Email',
    update: 'Save settings',
    save: 'Save',
    subscription: 'Monthly subscription'
  },

  pagenav: {
    feed: 'Feed',
    albums: 'Albums',
    location: 'Location',
    payment: 'Payment options'
  },

  buttons: {
    back: 'Back',
    save: 'Save',
    delete: 'Delete',
    cancel: 'Cancel',
    private: 'Private',
    open: 'Open',
    share: 'Share',
    create: 'Create',
    upload: 'Upload',
    edit_settings: 'Edit settings',
    photo_session: 'Photo session',
    create_album: 'Create album',
    create_event: 'Create event',
    copy_link: 'Copy link',
    add_payment: 'Add payment type',
    set_cover: 'Установить как обложку'
  },

  upload: {
    title: 'Upload files',
    drag: 'Drag & drop or choose'
  },

  payment_type: {
    per_file: 'Per file',
    donation: 'Donation',
    fixed: 'All files',
    free: 'Free',
    marketing: 'Marketing'
  },
  payment_info: {
    per_file: 'Buy the only files you get in loved with',
    fixed: 'One price for all files',
    donation: 'Pay as much as they feel',
    free: 'Friendship is everything',
    marketing: 'Something better then money'
  },

  events: {
    add_event: 'Create event',
    edit_event: 'Edit event',
    add_album: 'Create album',
    edit_album: 'Edit album',
    client: 'Client',
    note: 'Add note',
    date: 'Date',
    time: 'Time',
    name: 'Name',
    description: 'Description',
    location: 'Location',
    tags: 'Tags',
    payment: 'Payment type',
    price: 'Price',
    income: 'Income',
    downloads: 'Downloads',
    visits: 'Visits'
  }
}

export default translation
export type ErrorKey = keyof typeof translation.errors
