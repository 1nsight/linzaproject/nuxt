import gql from 'graphql-tag'
export const PageFragment = gql`
  fragment Page on pages {
    __typename
    id
    slug
    name
    active
  }
`
export const PageContentFragment = gql`
  fragment PageContent on pages_contents {
    __typename
    id
    locale
    name_in_menu
    page_id
    title
    content
  }
`
export const SeoFragment = gql`
  fragment SEO on seo {
    __typename
    id
    image
    keywords
    description
    title
  }
`
export const PageContentBlockFragment = gql`
  fragment PageContentBlock on pages_contents_blocks {
    __typename
    id
    page_content_id
    title
    hash
    content
    order
    menu_label
    menu_link
    active
  }
`
export const FullPageFragment = gql`
  fragment FullPage on pages {
    ...Page
    pages_contents {
      ...PageContent
      seo {
        ...SEO
      }
      pages_contents_blocks {
        ...PageContentBlock
      }
    }
  }
  ${PageFragment}
  ${PageContentFragment}
  ${SeoFragment}
  ${PageContentBlockFragment}
`
export const PageWithLocalesFragment = gql`
  fragment PageWithLocales on pages {
    ...Page
    pages_contents {
      __typename
      id
      locale
    }
  }
  ${PageFragment}
`
export const MenuPageFragment = gql`
  fragment MenuPage on menu_pages {
    __typename
    id
    order
    page_id
  }
`
export const MenuItemFragment = gql`
  fragment MenuItem on menu_items {
    __typename
    id
    order
    url
    external
    type
  }
`
export const LocalizationFragment = gql`
  fragment Localization on localizations {
    __typename
    id
    locale
    value
  }
`
export const MenuItemWithLocalizationFragment = gql`
  fragment MenuItemWithLocalization on menu_items {
    ...MenuItem
    localizations {
      ...Localization
    }
  }
  ${MenuItemFragment}
  ${LocalizationFragment}
`
export const WalletFragment = gql`
  fragment Wallet on wallets {
    __typename
    user_id
    id
    created_at
    updated_at
    balance
  }
`
export const UserFragment = gql`
  fragment User on users {
    __typename
    id
    display_name
    username
    slogan
    bio
    avatar_url
    avatar_blurhash
    created_at
    is_photographer
    is_favorite
    waves_address
  }
`
export const TransactionFragment = gql`
  fragment Transaction on transactions {
    __typename
    album_id
    amount
    comment
    created_at
    data
    event_id
    from_wallet_id
    id
    status
    to_wallet_id
    type
    updated_at
    from_wallet {
      ...Wallet
      user {
        ...User
      }
    }
    to_wallet {
      ...Wallet
      user {
        ...User
      }
    }
  }
  ${WalletFragment}
  ${UserFragment}
`
export const NetworkTransactionFragment = gql`
  fragment NetworkTransaction on network_transactions {
    __typename
    tx_hash
    transaction_id
    created_at
    status
    crypto_amount
    usd_amount
    data
  }
`
export const ConfigFragment = gql`
  fragment Config on config {
    key
    value
  }
`
export const FavoriteFragment = gql`
  fragment Favorite on favorites {
    __typename
    id
    owner_id
    user_id
    album_id
    media_file_id
  }
`
export const StorageFragment = gql`
  fragment Storage on storages_view {
    __typename
    id
    user_id
    used_bytes
    available_bytes
  }
`
export const UserTariffFragment = gql`
  fragment UserTariff on users_tariffs {
    __typename
    tariff_id
    id
    auto_renewal
    created_at
    end
    length
    start
    user_id
  }
`
export const TariffFragment = gql`
  fragment Tariff on tariffs {
    __typename
    id
    active
    created_at
    name
    price
    space
    updated_at
  }
`
export const GeocodingFragment = gql`
  fragment Geocoding on GeoOutput {
    city
    name
    display_name
    address_line1
    address_line2
    postcode
    region
    result_type
    state
    country
    country_code
    county
    geo
  }
`
export const CoverImageFragment = gql`
  fragment CoverImage on cover_images {
    __typename
    id
    url
    blurhash
    desktop_active
    mobile_active
  }
`
export const StatisticsFragment = gql`
  fragment Statistics on albums_statistics_view {
    __typename
    album_id
    downloads
    income
    visits
    new_visits
    new_downloads
  }
`
export const LocationFragment = gql`
  fragment Location on locations {
    __typename
    id
    name
    geo
    display_name
    city
    country
    user_id
  }
`
export const MediaFileFragment = gql`
  fragment MediaFile on media_files {
    __typename
    id
    preview_url
    blurhash
  }
`
export const AlbumTagFragment = gql`
  fragment AlbumTag on albums_tags {
    __typename
    album_id
    tag_id
  }
`
export const TagFragment = gql`
  fragment Tag on tags {
    __typename
    id
    tag
  }
`
export const AlbumPaymentTypeFragment = gql`
  fragment AlbumPaymentType on albums_payment_types {
    __typename
    id
    album_id
    price
    payment_type
    data
  }
`
export const AlbumFragment = gql`
  fragment Album on albums {
    __typename
    id
    name
    comment
    user_id
    user {
      ...User
    }
    statistics {
      ...Statistics
    }
    location_id
    location {
      ...Location
    }
    total_media_files
    cover {
      ...MediaFile
    }
    created_at
    private
    location {
      ...Location
    }
    tags {
      ...AlbumTag
      tag {
        ...Tag
      }
    }
    payment_types {
      ...AlbumPaymentType
    }
  }
  ${UserFragment}
  ${StatisticsFragment}
  ${LocationFragment}
  ${MediaFileFragment}
  ${AlbumTagFragment}
  ${TagFragment}
  ${AlbumPaymentTypeFragment}
`
export const EventPaymentTypeFragment = gql`
  fragment EventPaymentType on events_payment_types {
    __typename
    id
    event_id
    price
    payment_type
    data
  }
`
export const EventFragment = gql`
  fragment Event on events {
    __typename
    id
    private
    name
    comment
    note
    created_at
    updated_at
    date
    retouching
    time_for_upload
    total_media_files
    uploaded_time
    cover_media_file_id
    cover {
      ...MediaFile
    }
    location_id
    location {
      ...Location
    }
    user_id
    user {
      ...User
    }
    customer_id
    customer {
      ...User
    }
    payment_types {
      ...EventPaymentType
    }
  }
  ${MediaFileFragment}
  ${LocationFragment}
  ${UserFragment}
  ${EventPaymentTypeFragment}
`
export const StoreFragment = gql`
  fragment Store on store {
    __typename
    id
    category
    type
    total_media_files
    name
    description
    nft
    phone
    quantity
    duration
    date_start
    date_end
    price
    currency
    user_id
    user {
      ...User
    }
    cover_media_file_id
    cover {
      ...MediaFile
    }
    location_id
    location {
      ...Location
    }
    created_at
    updated_at
  }
  ${UserFragment}
  ${MediaFileFragment}
  ${LocationFragment}
`
export const FullUserFragment = gql`
  fragment FullUser on users {
    ...User
    blocked_for_search
    hide_in_search
  }
  ${UserFragment}
`
export const AccountFragment = gql`
  fragment Account on auth_accounts {
    __typename
    id
    email
    active
    default_role
    account_roles {
      role
    }
  }
`
export const UserStyleFragment = gql`
  fragment UserStyle on users_styles {
    __typename
    id
    style_id
    user_id
  }
`
export const StyleFragment = gql`
  fragment Style on styles {
    __typename
    id
    name
  }
`
export const UserPaymentTypeFragment = gql`
  fragment UserPaymentType on users_payment_types {
    __typename
    id
    payment_type
    price
  }
`
export const UserSearchMediaFileFragment = gql`
  fragment UserSearchMediaFile on users_search_media_files {
    __typename
    order
    id
    media_file_id
    media_file {
      ...MediaFile
    }
  }
  ${MediaFileFragment}
`
export const CurrentUserFragment = gql`
  fragment CurrentUser on users {
    ...FullUser
    phone
    telegram
    whatsapp
    account {
      ...Account
    }
    location_id
    location {
      ...Location
    }
    styles {
      ...UserStyle
      style {
        ...Style
      }
    }
    payment_types {
      ...UserPaymentType
    }
    storage {
      available_bytes
      used_bytes
    }
    favorites_aggregate {
      aggregate {
        count
      }
    }
    in_favorites_aggregate {
      aggregate {
        count
      }
    }
    media_search: media_files_in_search {
      ...UserSearchMediaFile
    }
    media_search_aggregate: media_files_in_search_aggregate {
      aggregate {
        count
      }
    }
  }
  ${FullUserFragment}
  ${AccountFragment}
  ${LocationFragment}
  ${UserStyleFragment}
  ${StyleFragment}
  ${UserPaymentTypeFragment}
  ${UserSearchMediaFileFragment}
`
export const SearchUserFragment = gql`
  fragment SearchUser on users {
    ...FullUser
    location {
      ...Location
    }
    in_favorites_aggregate {
      aggregate {
        count
      }
    }
    media_files_in_search {
      media_file {
        ...MediaFile
      }
    }
  }
  ${FullUserFragment}
  ${LocationFragment}
  ${MediaFileFragment}
`
export const UserLocationFragment = gql`
  fragment UserLocation on users {
    location_id
    location {
      ...Location
    }
  }
  ${LocationFragment}
`
export const UserSearchMediaFilesFragment = gql`
  fragment UserSearchMediaFiles on users {
    media_files_in_search {
      ...UserSearchMediaFile
    }
  }
  ${UserSearchMediaFileFragment}
`
export const AdminPagesDocument = gql`
  subscription adminPages {
    pages {
      ...PageWithLocales
    }
  }
  ${PageWithLocalesFragment}
`
export const AdminGetPagesDocument = gql`
  query adminGetPages {
    pages {
      ...PageWithLocales
    }
  }
  ${PageWithLocalesFragment}
`
export const AdminGetPageDocument = gql`
  query adminGetPage($id: uuid!) {
    page: pages_by_pk(id: $id) {
      ...FullPage
    }
  }
  ${FullPageFragment}
`
export const AdminUpdatePageDocument = gql`
  mutation adminUpdatePage(
    $object: pages_insert_input!
    $on_conflict: pages_on_conflict!
  ) {
    page: insert_pages_one(object: $object, on_conflict: $on_conflict) {
      ...FullPage
    }
  }
  ${FullPageFragment}
`
export const AdminDeletePageDocument = gql`
  mutation adminDeletePage($id: uuid!) {
    delete_pages_by_pk(id: $id) {
      id
    }
  }
`
export const AdminAddMenuPageDocument = gql`
  mutation adminAddMenuPage($pageId: uuid!, $order: Int!) {
    insert_menu_pages_one(object: { page_id: $pageId, order: $order }) {
      ...MenuPage
    }
  }
  ${MenuPageFragment}
`
export const AdminDeleteMenuPageDocument = gql`
  mutation adminDeleteMenuPage($id: uuid!) {
    delete_menu_pages_by_pk(id: $id) {
      id
    }
  }
`
export const AdminGetMenuPagesDocument = gql`
  query adminGetMenuPages {
    menu: menu_pages(order_by: { order: asc }) {
      id
      order
      page_id
      page {
        name
        id
        active
      }
    }
  }
`
export const AdminUpdateOrderMenuPagesDocument = gql`
  mutation adminUpdateOrderMenuPages($objects: [menu_pages_insert_input!]!) {
    insert_menu_pages(
      objects: $objects
      on_conflict: { constraint: menu_pages_pkey, update_columns: [order] }
    ) {
      affected_rows
    }
  }
`
export const AdminMenuItemsDocument = gql`
  query adminMenuItems($type: menu_type_enum!) {
    menu: menu_items(
      where: { type: { _eq: $type } }
      order_by: { order: asc }
    ) {
      ...MenuItemWithLocalization
    }
  }
  ${MenuItemWithLocalizationFragment}
`
export const AdminAddMenuItemDocument = gql`
  mutation adminAddMenuItem($item: menu_items_insert_input!) {
    insert_menu_items_one(
      object: $item
      on_conflict: { constraint: menu_pkey, update_columns: [external, url] }
    ) {
      ...MenuItemWithLocalization
    }
  }
  ${MenuItemWithLocalizationFragment}
`
export const AdminUpdateOrderMenuItemsDocument = gql`
  mutation adminUpdateOrderMenuItems($objects: [menu_items_insert_input!]!) {
    insert_menu_items(
      objects: $objects
      on_conflict: { constraint: menu_pkey, update_columns: [order] }
    ) {
      affected_rows
    }
  }
`
export const AdminDeleteMenuItemDocument = gql`
  mutation adminDeleteMenuItem($id: uuid!) {
    delete_menu_items_by_pk(id: $id) {
      id
    }
  }
`
export const AdminDeletePageContentBlockDocument = gql`
  mutation adminDeletePageContentBlock($id: uuid!) {
    delete_pages_contents_blocks_by_pk(id: $id) {
      id
    }
  }
`
export const AdminGetCoverImagesDocument = gql`
  query adminGetCoverImages {
    coverImages: cover_images {
      id
      url
      blurhash
      desktop_active
      mobile_active
    }
  }
`
export const AdminAddCoverImagesDocument = gql`
  mutation adminAddCoverImages($item: cover_images_insert_input!) {
    insert_cover_images_one(
      object: $item
      on_conflict: {
        constraint: cover_images_pkey
        update_columns: [blurhash, desktop_active, mobile_active, url]
      }
    ) {
      ...CoverImage
    }
  }
  ${CoverImageFragment}
`
export const AdminDeleteCoverImagesDocument = gql`
  mutation adminDeleteCoverImages($id: uuid!) {
    delete_cover_images_by_pk(id: $id) {
      id
    }
  }
`
export const AdminEditUserDocument = gql`
  mutation adminEditUser($id: uuid!, $blocked: Boolean!) {
    update_users_by_pk(
      pk_columns: { id: $id }
      _set: { blocked_for_search: $blocked }
    ) {
      ...FullUser
    }
  }
  ${FullUserFragment}
`
export const AddToFavoriteDocument = gql`
  mutation addToFavorite($object: favorites_insert_input!) {
    insert_favorites_one(object: $object) {
      user {
        __typename
        id
        is_favorite
      }
      media_file {
        __typename
        id
        is_favorite
      }
      album {
        __typename
        id
        is_favorite
      }
    }
  }
`
export const CreateLocationDocument = gql`
  mutation CreateLocation($object: locations_insert_input!) {
    location: insert_locations_one(object: $object) {
      ...Location
    }
  }
  ${LocationFragment}
`
export const CreateStoreDocument = gql`
  mutation createStore($object: store_insert_input!) {
    store: insert_store_one(object: $object) {
      ...Store
    }
  }
  ${StoreFragment}
`
export const CreateTagDocument = gql`
  mutation createTag($objects: [tags_insert_input!]!) {
    insert_tags(
      objects: $objects
      on_conflict: { constraint: tags_tag_key, update_columns: [id] }
    ) {
      affected_rows
      returning {
        tag
        id
      }
    }
  }
`
export const DeleteMediaDocument = gql`
  mutation deleteMedia($id: uuid!) {
    delete_media_files(where: { id: { _eq: $id } }) {
      returning {
        ...MediaFile
      }
    }
  }
  ${MediaFileFragment}
`
export const CreateAlbumDocument = gql`
  mutation createAlbum(
    $object: albums_insert_input!
    $payment: [albums_payment_types_insert_input!]!
  ) {
    album: insert_albums_one(object: $object) {
      ...Album
    }
    payment: insert_albums_payment_types(objects: $payment) {
      returning {
        ...AlbumPaymentType
      }
    }
  }
  ${AlbumFragment}
  ${AlbumPaymentTypeFragment}
`
export const CreateEventDocument = gql`
  mutation createEvent(
    $object: events_insert_input!
    $payment: [events_payment_types_insert_input!]!
  ) {
    event: insert_events_one(
      object: $object
      on_conflict: { constraint: events_pkey, update_columns: location_id }
    ) {
      ...Event
    }
    payment: insert_events_payment_types(objects: $payment) {
      returning {
        ...EventPaymentType
      }
    }
  }
  ${EventFragment}
  ${EventPaymentTypeFragment}
`
export const DeleteAlbumDocument = gql`
  mutation deleteAlbum($id: uuid!) {
    delete_albums_by_pk(id: $id) {
      id
    }
  }
`
export const DeleteEventDocument = gql`
  mutation deleteEvent($id: uuid!) {
    delete_events_by_pk(id: $id) {
      id
    }
  }
`
export const UpdateAlbumDocument = gql`
  mutation updateAlbum(
    $id: uuid!
    $object: albums_set_input!
    $payment: [albums_payment_types_insert_input!]!
    $tags: [albums_tags_insert_input!]!
  ) {
    album: update_albums_by_pk(pk_columns: { id: $id }, _set: $object) {
      ...Album
    }
    delete_albums_payment_types(where: { album_id: { _eq: $id } }) {
      affected_rows
    }
    insert_albums_payment_types(
      objects: $payment
      on_conflict: {
        constraint: albums_payment_types_album_id_payment_type_key
        update_columns: [payment_type, price]
      }
    ) {
      returning {
        ...AlbumPaymentType
      }
    }
  }
  ${AlbumFragment}
  ${AlbumPaymentTypeFragment}
`
export const UpdateCoverEventDocument = gql`
  mutation updateCoverEvent($id: uuid!, $object: events_set_input!) {
    event: update_events_by_pk(pk_columns: { id: $id }, _set: $object) {
      ...Event
    }
  }
  ${EventFragment}
`
export const UpdateCoverAlbumDocument = gql`
  mutation updateCoverAlbum($id: uuid!, $object: albums_set_input!) {
    album: update_albums_by_pk(pk_columns: { id: $id }, _set: $object) {
      ...Album
    }
  }
  ${AlbumFragment}
`
export const UpdateEventDocument = gql`
  mutation updateEvent(
    $id: uuid!
    $object: events_set_input!
    $payment: [events_payment_types_insert_input!]!
  ) {
    event: update_events_by_pk(pk_columns: { id: $id }, _set: $object) {
      ...Event
    }
    delete_events_payment_types(where: { event_id: { _eq: $id } }) {
      affected_rows
    }
    insert_events_payment_types(
      objects: $payment
      on_conflict: {
        constraint: events_payment_types_event_id_payment_type_key
        update_columns: [payment_type, price]
      }
    ) {
      returning {
        ...EventPaymentType
      }
    }
  }
  ${EventFragment}
  ${EventPaymentTypeFragment}
`
export const RemoveFromFavoriteDocument = gql`
  mutation removeFromFavorite($where: favorites_bool_exp!) {
    delete_favorites(where: $where) {
      affected_rows
      returning {
        user {
          __typename
          id
          is_favorite
        }
        media_file {
          __typename
          id
          is_favorite
        }
        album {
          __typename
          id
          is_favorite
        }
      }
    }
  }
`
export const UpdateSettingsPaymentTypesDocument = gql`
  mutation UpdateSettingsPaymentTypes(
    $objects: [users_payment_types_insert_input!]!
    $forDelete: [uuid!]!
    $delete: Boolean!
  ) {
    insert_users_payment_types(
      objects: $objects
      on_conflict: {
        constraint: users_payment_types_user_id_payment_type_key
        update_columns: [price]
      }
    ) {
      affected_rows
    }
    delete_users_payment_types(where: { id: { _in: $forDelete } })
      @include(if: $delete) {
      affected_rows
    }
  }
`
export const UpdateSettingsSearchPhotosDocument = gql`
  mutation UpdateSettingsSearchPhotos(
    $userId: uuid!
    $objects: [users_search_media_files_insert_input!]!
  ) {
    delete_users_search_media_files(where: { user_id: { _eq: $userId } }) {
      affected_rows
    }
    insert_users_search_media_files(
      objects: $objects
      on_conflict: { constraint: search_photos_pkey, update_columns: [order] }
    ) {
      returning {
        ...UserSearchMediaFile
      }
    }
  }
  ${UserSearchMediaFileFragment}
`
export const UpdateSettingsStylesDocument = gql`
  mutation UpdateSettingsStyles(
    $objects: [users_styles_insert_input!]!
    $forDelete: [uuid!]!
    $insert: Boolean!
    $delete: Boolean!
  ) {
    insert_users_styles(objects: $objects) @include(if: $insert) {
      affected_rows
    }
    delete_users_styles(where: { id: { _in: $forDelete } })
      @include(if: $delete) {
      affected_rows
    }
  }
`
export const UpdateCurrentUserDocument = gql`
  mutation updateCurrentUser(
    $userId: uuid!
    $user: users_set_input!
    $email: citext!
    $styles: [users_styles_insert_input!]!
  ) {
    update_users_by_pk(pk_columns: { id: $userId }, _set: $user) {
      ...CurrentUser
    }
    update_auth_accounts(
      where: { user_id: { _eq: $userId } }
      _set: { email: $email }
    ) {
      returning {
        ...Account
      }
    }
    delete_users_styles(where: { user_id: { _eq: $userId } }) {
      affected_rows
    }
    insert_users_styles(objects: $styles) {
      returning {
        ...UserStyle
        style {
          ...Style
        }
      }
    }
    insert_users_styles(objects: $styles) {
      returning {
        ...UserStyle
        style {
          ...Style
        }
      }
    }
  }
  ${CurrentUserFragment}
  ${AccountFragment}
  ${UserStyleFragment}
  ${StyleFragment}
`
export const UpdateUserWavesTokenDocument = gql`
  mutation updateUserWavesToken($userId: uuid!, $waves_address: String!) {
    update_users_by_pk(
      pk_columns: { id: $userId }
      _set: { waves_address: $waves_address }
    ) {
      ...CurrentUser
    }
  }
  ${CurrentUserFragment}
`
export const GetWalletDocument = gql`
  query getWallet($userId: uuid!) {
    wallets(where: { user_id: { _eq: $userId } }) {
      ...Wallet
    }
  }
  ${WalletFragment}
`
export const GetCurrentUserDocument = gql`
  query getCurrentUser($userId: uuid!) {
    user: users_by_pk(id: $userId) {
      ...CurrentUser
    }
  }
  ${CurrentUserFragment}
`
export const GetPageDocument = gql`
  query getPage($locales: [locales_enum!]!, $slug: String!) {
    pages(where: { active: { _eq: true }, slug: { _eq: $slug } }) {
      ...Page
      pages_contents(
        where: { locale: { _in: $locales } }
        order_by: { locale: desc }
        limit: 1
      ) {
        ...PageContent
        seo {
          ...SEO
        }
        pages_contents_blocks(
          where: { active: { _eq: true } }
          order_by: { order: asc }
        ) {
          ...PageContentBlock
        }
      }
    }
  }
  ${PageFragment}
  ${PageContentFragment}
  ${SeoFragment}
  ${PageContentBlockFragment}
`
export const GetMenuPagesDocument = gql`
  query getMenuPages($locales: [locales_enum!]!) {
    menu: menu_pages(
      order_by: { order: asc }
      where: { page: { active: { _eq: true } } }
    ) {
      ...MenuPage
      page {
        ...Page
        pages_contents(
          where: { locale: { _in: $locales } }
          order_by: { locale: desc }
          limit: 1
        ) {
          __typename
          id
          name_in_menu
          pages_contents_blocks(
            where: { active: { _eq: true } }
            order_by: { order: asc }
          ) {
            __typename
            id
            hash
            menu_label
            menu_link
            active
          }
        }
      }
    }
  }
  ${MenuPageFragment}
  ${PageFragment}
`
export const GetMenuItemsDocument = gql`
  query getMenuItems($types: [menu_type_enum!]!, $locales: [locales_enum!]!) {
    menu: menu_items(
      where: { type: { _in: $types } }
      order_by: { order: asc }
    ) {
      ...MenuItem
      localizations(
        where: { locale: { _in: $locales } }
        order_by: { locale: desc }
        limit: 2
      ) {
        ...Localization
      }
    }
  }
  ${MenuItemFragment}
  ${LocalizationFragment}
`
export const GetTransactionsDocument = gql`
  query getTransactions(
    $where: transactions_bool_exp = {}
    $order_by: [transactions_order_by!] = {}
    $limit: Int!
    $offset: Int!
  ) {
    transactions: transactions(
      where: $where
      order_by: $order_by
      limit: $limit
      offset: $offset
    ) {
      ...Transaction
    }
  }
  ${TransactionFragment}
`
export const GetConfigsDocument = gql`
  query getConfigs($in: [config_keys_enum!]!) {
    config(where: { key: { _in: $in } }) {
      ...Config
    }
  }
  ${ConfigFragment}
`
export const GetDefaultSeoDocument = gql`
  query getDefaultSeo($locales: [locales_enum!]!) {
    pages(where: { slug: { _eq: "/" } }) {
      pages_contents(
        where: { locale: { _in: $locales } }
        order_by: { locale: desc }
        limit: 1
      ) {
        seo {
          ...SEO
        }
      }
    }
  }
  ${SeoFragment}
`
export const CoverImagesDocument = gql`
  query coverImages {
    cover_images(where: { desktop_active: { _eq: true } }) {
      ...CoverImage
    }
  }
  ${CoverImageFragment}
`
export const GetUsersListDocument = gql`
  query getUsersList {
    users {
      id
      username
    }
  }
`
export const GetAlbumDocument = gql`
  query getAlbum($id: uuid!) {
    album: albums_by_pk(id: $id) {
      ...Album
    }
  }
  ${AlbumFragment}
`
export const GetEventDocument = gql`
  query getEvent($id: uuid!) {
    event: events_by_pk(id: $id) {
      ...Event
    }
  }
  ${EventFragment}
`
export const GetUserFavoritesDocument = gql`
  query getUserFavorites($userId: uuid!) {
    favorites(where: { owner_id: { _eq: $userId } }) {
      user {
        ...User
        ...UserLocation
        ...UserSearchMediaFiles
      }
    }
  }
  ${UserFragment}
  ${UserLocationFragment}
  ${UserSearchMediaFilesFragment}
`
export const GeoReverseDocument = gql`
  query GeoReverse($lat: Float!, $lng: Float!, $lang: String!, $type: String) {
    geo_reverse(lat: $lat, lng: $lng, type: $type, lang: $lang) {
      ...Geocoding
    }
  }
  ${GeocodingFragment}
`
export const GeoAutocompleteDocument = gql`
  query GeoAutocomplete($text: String!, $lang: String!, $type: String) {
    geo_autocomplete(text: $text, type: $type, lang: $lang) {
      ...Geocoding
    }
  }
  ${GeocodingFragment}
`
export const GetMediaDocument = gql`
  query getMedia(
    $where: media_files_bool_exp!
    $limit: Int!
    $offset: Int!
    $order_by: [media_files_order_by!] = { created_at: desc }
  ) {
    media_files(
      where: $where
      limit: $limit
      offset: $offset
      order_by: $order_by
    ) {
      ...MediaFile
    }
    count: media_files_aggregate(where: $where) {
      aggregate {
        count
      }
    }
  }
  ${MediaFileFragment}
`
export const GetUserPaymentDocument = gql`
  query getUserPayment($id: uuid!) {
    users_payment_types(where: { user_id: { _eq: $id } }) {
      price
      payment_type
      id
    }
  }
`
export const GetProfileDocument = gql`
  query GetProfile($username: String! = "") {
    user: users(where: { username: { _eq: $username } }) {
      ...User
      ...UserLocation
      ...UserSearchMediaFiles
    }
  }
  ${UserFragment}
  ${UserLocationFragment}
  ${UserSearchMediaFilesFragment}
`
export const GetSearchPhotographersDocument = gql`
  query getSearchPhotographers(
    $where: users_bool_exp!
    $limit: Int!
    $offset: Int!
    $orderBy: [users_order_by!] = { created_at: desc }
  ) {
    search_photographers(
      where: $where
      limit: $limit
      offset: $offset
      order_by: $orderBy
    ) {
      ...SearchUser
    }
    count: search_photographers_aggregate(where: $where) {
      aggregate {
        count
      }
    }
  }
  ${SearchUserFragment}
`
export const GetSearchPhotographersLocationDocument = gql`
  query getSearchPhotographersLocation(
    $args: search_photographers_by_location_args!
  ) {
    search_photographers_by_location(args: $args) {
      id
      display_name
    }
  }
`
export const GetStoreDocument = gql`
  query GetStore($id: uuid!) {
    store: store_by_pk(id: $id) {
      ...Store
    }
  }
  ${StoreFragment}
`
export const GetUserAlbumsDocument = gql`
  query getUserAlbums($userId: uuid!) {
    albums(where: { user_id: { _eq: $userId } }) {
      ...Album
    }
  }
  ${AlbumFragment}
`
export const GetUserStyleDocument = gql`
  query getUserStyle($userId: uuid!) {
    users_styles(where: { user_id: { _eq: $userId } }) {
      ...UserStyle
      style {
        ...Style
      }
    }
  }
  ${UserStyleFragment}
  ${StyleFragment}
`
export const GetStyleDocument = gql`
  query getStyle {
    styles {
      ...Style
    }
  }
  ${StyleFragment}
`
export const GetClientsDocument = gql`
  query getClients($userId: uuid!) {
    users(where: { _not: { id: { _eq: $userId } } }) {
      ...User
    }
  }
  ${UserFragment}
`
export const GetUsersDocument = gql`
  query getUsers($where: users_bool_exp! = {}, $offset: Int!, $limit: Int!) {
    users(
      where: $where
      offset: $offset
      limit: $limit
      order_by: { created_at: asc }
    ) {
      ...CurrentUser
    }
    users_aggregate {
      aggregate {
        count
      }
    }
  }
  ${CurrentUserFragment}
`
export const GetUserPublicDocument = gql`
  query getUserPublic($where: users_bool_exp! = {}) {
    users(where: $where) {
      ...SearchUser
    }
  }
  ${SearchUserFragment}
`
export const AlbumsDocument = gql`
  subscription albums(
    $limit: Int!
    $offset: Int!
    $userId: uuid!
    $order_by: [albums_order_by!] = { created_at: desc }
  ) {
    albums(
      limit: $limit
      offset: $offset
      where: { user_id: { _eq: $userId } }
      order_by: $order_by
    ) {
      ...Album
    }
  }
  ${AlbumFragment}
`
export const CurrentUserDocument = gql`
  subscription currentUser($userId: uuid!) {
    user: users_by_pk(id: $userId) {
      ...CurrentUser
    }
  }
  ${CurrentUserFragment}
`
export const EventsDocument = gql`
  subscription events(
    $limit: Int!
    $offset: Int!
    $userId: uuid!
    $order_by: [events_order_by!] = { created_at: desc }
  ) {
    events(
      limit: $limit
      offset: $offset
      where: {
        _or: [{ user_id: { _eq: $userId } }, { customer_id: { _eq: $userId } }]
      }
      order_by: $order_by
    ) {
      ...Event
    }
  }
  ${EventFragment}
`
export const MediaDocument = gql`
  subscription media(
    $where: media_files_bool_exp!
    $limit: Int!
    $offset: Int!
    $order_by: [media_files_order_by!] = { created_at: desc }
  ) {
    media_files(
      where: $where
      limit: $limit
      offset: $offset
      order_by: $order_by
    ) {
      ...MediaFile
      users_search_photos {
        id
        order
      }
    }
  }
  ${MediaFileFragment}
`
export const StoreDocument = gql`
  subscription store(
    $limit: Int!
    $offset: Int!
    $userId: uuid!
    $type: store_types_enum_comparison_exp = { _eq: null }
    $order_by: [store_order_by!] = { created_at: desc }
  ) {
    store(
      limit: $limit
      offset: $offset
      where: { user_id: { _eq: $userId }, type: $type }
      order_by: $order_by
    ) {
      ...Store
    }
  }
  ${StoreFragment}
`
export const TransactionsDocument = gql`
  subscription transactions($offset: Int!, $where: transactions_bool_exp!) {
    transactions(
      limit: 20
      offset: $offset
      order_by: { created_at: desc }
      where: $where
    ) {
      ...Transaction
      network_transactions {
        ...NetworkTransaction
      }
    }
  }
  ${TransactionFragment}
  ${NetworkTransactionFragment}
`
export const WalletDocument = gql`
  subscription wallet($userId: uuid!) {
    wallets(where: { user_id: { _eq: $userId } }) {
      ...Wallet
    }
  }
  ${WalletFragment}
`
