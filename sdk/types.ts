export type Maybe<T> = T | null
export type Exact<T extends { [key: string]: unknown }> = {
  [K in keyof T]: T[K]
}
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: string
  String: string
  Boolean: boolean
  Int: number
  Float: number
  uuid: string
  timestamptz: Date
  jsonb: { [key: string]: any } | null
  citext: any
  date: Date
  numeric: number
  geometry: { type: 'Point'; coordinates: number[] }
  geography: any
  bigint: string
  timestamp: any
  name: any
  float8: any
  /** The `Upload` scalar type represents a file upload. */
  Upload: any
}

/** columns and relationships of "albums" */
export type Albums = {
  __typename?: 'albums'
  archived: Scalars['Boolean']
  comment: Scalars['String']
  /** An object relationship */
  cover?: Maybe<Media_Files>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at: Scalars['timestamptz']
  /** An array relationship */
  favorites: Array<Favorites>
  /** An aggregated array relationship */
  favorites_aggregate: Favorites_Aggregate
  id: Scalars['uuid']
  /** A computed field, executes function "album_is_favorite" */
  is_favorite?: Maybe<Scalars['Boolean']>
  /** An object relationship */
  location?: Maybe<Locations>
  location_id?: Maybe<Scalars['uuid']>
  /** An array relationship */
  media_files: Array<Media_Files>
  /** An aggregated array relationship */
  media_files_aggregate: Media_Files_Aggregate
  name: Scalars['String']
  /** An array relationship */
  payment_types: Array<Albums_Payment_Types>
  /** An aggregated array relationship */
  payment_types_aggregate: Albums_Payment_Types_Aggregate
  private: Scalars['Boolean']
  /** An object relationship */
  statistics?: Maybe<Albums_Statistics_View>
  /** An array relationship */
  tags: Array<Albums_Tags>
  /** An aggregated array relationship */
  tags_aggregate: Albums_Tags_Aggregate
  total_media_files: Scalars['Int']
  /** An array relationship */
  transactions: Array<Transactions>
  /** An aggregated array relationship */
  transactions_aggregate: Transactions_Aggregate
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
  /** An array relationship */
  visits: Array<Albums_Visits>
  /** An aggregated array relationship */
  visits_aggregate: Albums_Visits_Aggregate
}

/** columns and relationships of "albums" */
export type AlbumsFavoritesArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsFavorites_AggregateArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsMedia_FilesArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsMedia_Files_AggregateArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsPayment_TypesArgs = {
  distinct_on?: Maybe<Array<Albums_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Payment_Types_Order_By>>
  where?: Maybe<Albums_Payment_Types_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsPayment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Payment_Types_Order_By>>
  where?: Maybe<Albums_Payment_Types_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsTagsArgs = {
  distinct_on?: Maybe<Array<Albums_Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Tags_Order_By>>
  where?: Maybe<Albums_Tags_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsTags_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Tags_Order_By>>
  where?: Maybe<Albums_Tags_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsTransactionsArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsTransactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsVisitsArgs = {
  distinct_on?: Maybe<Array<Albums_Visits_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Visits_Order_By>>
  where?: Maybe<Albums_Visits_Bool_Exp>
}

/** columns and relationships of "albums" */
export type AlbumsVisits_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Visits_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Visits_Order_By>>
  where?: Maybe<Albums_Visits_Bool_Exp>
}

/** aggregated selection of "albums" */
export type Albums_Aggregate = {
  __typename?: 'albums_aggregate'
  aggregate?: Maybe<Albums_Aggregate_Fields>
  nodes: Array<Albums>
}

/** aggregate fields of "albums" */
export type Albums_Aggregate_Fields = {
  __typename?: 'albums_aggregate_fields'
  avg?: Maybe<Albums_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Albums_Max_Fields>
  min?: Maybe<Albums_Min_Fields>
  stddev?: Maybe<Albums_Stddev_Fields>
  stddev_pop?: Maybe<Albums_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Albums_Stddev_Samp_Fields>
  sum?: Maybe<Albums_Sum_Fields>
  var_pop?: Maybe<Albums_Var_Pop_Fields>
  var_samp?: Maybe<Albums_Var_Samp_Fields>
  variance?: Maybe<Albums_Variance_Fields>
}

/** aggregate fields of "albums" */
export type Albums_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Albums_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "albums" */
export type Albums_Aggregate_Order_By = {
  avg?: Maybe<Albums_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Albums_Max_Order_By>
  min?: Maybe<Albums_Min_Order_By>
  stddev?: Maybe<Albums_Stddev_Order_By>
  stddev_pop?: Maybe<Albums_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Albums_Stddev_Samp_Order_By>
  sum?: Maybe<Albums_Sum_Order_By>
  var_pop?: Maybe<Albums_Var_Pop_Order_By>
  var_samp?: Maybe<Albums_Var_Samp_Order_By>
  variance?: Maybe<Albums_Variance_Order_By>
}

/** input type for inserting array relation for remote table "albums" */
export type Albums_Arr_Rel_Insert_Input = {
  data: Array<Albums_Insert_Input>
  on_conflict?: Maybe<Albums_On_Conflict>
}

/** aggregate avg on columns */
export type Albums_Avg_Fields = {
  __typename?: 'albums_avg_fields'
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "albums" */
export type Albums_Avg_Order_By = {
  total_media_files?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "albums". All fields are combined with a logical 'AND'. */
export type Albums_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Albums_Bool_Exp>>>
  _not?: Maybe<Albums_Bool_Exp>
  _or?: Maybe<Array<Maybe<Albums_Bool_Exp>>>
  archived?: Maybe<Boolean_Comparison_Exp>
  comment?: Maybe<String_Comparison_Exp>
  cover?: Maybe<Media_Files_Bool_Exp>
  cover_media_file_id?: Maybe<Uuid_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  favorites?: Maybe<Favorites_Bool_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  location?: Maybe<Locations_Bool_Exp>
  location_id?: Maybe<Uuid_Comparison_Exp>
  media_files?: Maybe<Media_Files_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
  payment_types?: Maybe<Albums_Payment_Types_Bool_Exp>
  private?: Maybe<Boolean_Comparison_Exp>
  statistics?: Maybe<Albums_Statistics_View_Bool_Exp>
  tags?: Maybe<Albums_Tags_Bool_Exp>
  total_media_files?: Maybe<Int_Comparison_Exp>
  transactions?: Maybe<Transactions_Bool_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
  visits?: Maybe<Albums_Visits_Bool_Exp>
}

/** unique or primary key constraints on table "albums" */
export enum Albums_Constraint {
  /** unique or primary key constraint */
  AlbumsPkey = 'albums_pkey'
}

/** input type for incrementing integer column in table "albums" */
export type Albums_Inc_Input = {
  total_media_files?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "albums" */
export type Albums_Insert_Input = {
  archived?: Maybe<Scalars['Boolean']>
  comment?: Maybe<Scalars['String']>
  cover?: Maybe<Media_Files_Obj_Rel_Insert_Input>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  favorites?: Maybe<Favorites_Arr_Rel_Insert_Input>
  id?: Maybe<Scalars['uuid']>
  location?: Maybe<Locations_Obj_Rel_Insert_Input>
  location_id?: Maybe<Scalars['uuid']>
  media_files?: Maybe<Media_Files_Arr_Rel_Insert_Input>
  name?: Maybe<Scalars['String']>
  payment_types?: Maybe<Albums_Payment_Types_Arr_Rel_Insert_Input>
  private?: Maybe<Scalars['Boolean']>
  statistics?: Maybe<Albums_Statistics_View_Obj_Rel_Insert_Input>
  tags?: Maybe<Albums_Tags_Arr_Rel_Insert_Input>
  total_media_files?: Maybe<Scalars['Int']>
  transactions?: Maybe<Transactions_Arr_Rel_Insert_Input>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
  visits?: Maybe<Albums_Visits_Arr_Rel_Insert_Input>
}

/** aggregate max on columns */
export type Albums_Max_Fields = {
  __typename?: 'albums_max_fields'
  comment?: Maybe<Scalars['String']>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  location_id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  total_media_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "albums" */
export type Albums_Max_Order_By = {
  comment?: Maybe<Order_By>
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  location_id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Albums_Min_Fields = {
  __typename?: 'albums_min_fields'
  comment?: Maybe<Scalars['String']>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  location_id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  total_media_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "albums" */
export type Albums_Min_Order_By = {
  comment?: Maybe<Order_By>
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  location_id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "albums" */
export type Albums_Mutation_Response = {
  __typename?: 'albums_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Albums>
}

/** input type for inserting object relation for remote table "albums" */
export type Albums_Obj_Rel_Insert_Input = {
  data: Albums_Insert_Input
  on_conflict?: Maybe<Albums_On_Conflict>
}

/** on conflict condition type for table "albums" */
export type Albums_On_Conflict = {
  constraint: Albums_Constraint
  update_columns: Array<Albums_Update_Column>
  where?: Maybe<Albums_Bool_Exp>
}

/** ordering options when selecting data from "albums" */
export type Albums_Order_By = {
  archived?: Maybe<Order_By>
  comment?: Maybe<Order_By>
  cover?: Maybe<Media_Files_Order_By>
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  favorites_aggregate?: Maybe<Favorites_Aggregate_Order_By>
  id?: Maybe<Order_By>
  location?: Maybe<Locations_Order_By>
  location_id?: Maybe<Order_By>
  media_files_aggregate?: Maybe<Media_Files_Aggregate_Order_By>
  name?: Maybe<Order_By>
  payment_types_aggregate?: Maybe<Albums_Payment_Types_Aggregate_Order_By>
  private?: Maybe<Order_By>
  statistics?: Maybe<Albums_Statistics_View_Order_By>
  tags_aggregate?: Maybe<Albums_Tags_Aggregate_Order_By>
  total_media_files?: Maybe<Order_By>
  transactions_aggregate?: Maybe<Transactions_Aggregate_Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
  visits_aggregate?: Maybe<Albums_Visits_Aggregate_Order_By>
}

/** columns and relationships of "albums_payment_types" */
export type Albums_Payment_Types = {
  __typename?: 'albums_payment_types'
  /** An object relationship */
  album: Albums
  album_id: Scalars['uuid']
  created_at: Scalars['timestamptz']
  data?: Maybe<Scalars['jsonb']>
  id: Scalars['uuid']
  payment_type: Payment_Types_Enum
  price: Scalars['numeric']
  /** An object relationship */
  type: Payment_Types
  updated_at: Scalars['timestamptz']
}

/** columns and relationships of "albums_payment_types" */
export type Albums_Payment_TypesDataArgs = {
  path?: Maybe<Scalars['String']>
}

/** aggregated selection of "albums_payment_types" */
export type Albums_Payment_Types_Aggregate = {
  __typename?: 'albums_payment_types_aggregate'
  aggregate?: Maybe<Albums_Payment_Types_Aggregate_Fields>
  nodes: Array<Albums_Payment_Types>
}

/** aggregate fields of "albums_payment_types" */
export type Albums_Payment_Types_Aggregate_Fields = {
  __typename?: 'albums_payment_types_aggregate_fields'
  avg?: Maybe<Albums_Payment_Types_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Albums_Payment_Types_Max_Fields>
  min?: Maybe<Albums_Payment_Types_Min_Fields>
  stddev?: Maybe<Albums_Payment_Types_Stddev_Fields>
  stddev_pop?: Maybe<Albums_Payment_Types_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Albums_Payment_Types_Stddev_Samp_Fields>
  sum?: Maybe<Albums_Payment_Types_Sum_Fields>
  var_pop?: Maybe<Albums_Payment_Types_Var_Pop_Fields>
  var_samp?: Maybe<Albums_Payment_Types_Var_Samp_Fields>
  variance?: Maybe<Albums_Payment_Types_Variance_Fields>
}

/** aggregate fields of "albums_payment_types" */
export type Albums_Payment_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Albums_Payment_Types_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "albums_payment_types" */
export type Albums_Payment_Types_Aggregate_Order_By = {
  avg?: Maybe<Albums_Payment_Types_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Albums_Payment_Types_Max_Order_By>
  min?: Maybe<Albums_Payment_Types_Min_Order_By>
  stddev?: Maybe<Albums_Payment_Types_Stddev_Order_By>
  stddev_pop?: Maybe<Albums_Payment_Types_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Albums_Payment_Types_Stddev_Samp_Order_By>
  sum?: Maybe<Albums_Payment_Types_Sum_Order_By>
  var_pop?: Maybe<Albums_Payment_Types_Var_Pop_Order_By>
  var_samp?: Maybe<Albums_Payment_Types_Var_Samp_Order_By>
  variance?: Maybe<Albums_Payment_Types_Variance_Order_By>
}

/** append existing jsonb value of filtered columns with new jsonb value */
export type Albums_Payment_Types_Append_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** input type for inserting array relation for remote table "albums_payment_types" */
export type Albums_Payment_Types_Arr_Rel_Insert_Input = {
  data: Array<Albums_Payment_Types_Insert_Input>
  on_conflict?: Maybe<Albums_Payment_Types_On_Conflict>
}

/** aggregate avg on columns */
export type Albums_Payment_Types_Avg_Fields = {
  __typename?: 'albums_payment_types_avg_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "albums_payment_types" */
export type Albums_Payment_Types_Avg_Order_By = {
  price?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "albums_payment_types". All fields are combined with a logical 'AND'. */
export type Albums_Payment_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Albums_Payment_Types_Bool_Exp>>>
  _not?: Maybe<Albums_Payment_Types_Bool_Exp>
  _or?: Maybe<Array<Maybe<Albums_Payment_Types_Bool_Exp>>>
  album?: Maybe<Albums_Bool_Exp>
  album_id?: Maybe<Uuid_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  data?: Maybe<Jsonb_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  payment_type?: Maybe<Payment_Types_Enum_Comparison_Exp>
  price?: Maybe<Numeric_Comparison_Exp>
  type?: Maybe<Payment_Types_Bool_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "albums_payment_types" */
export enum Albums_Payment_Types_Constraint {
  /** unique or primary key constraint */
  AlbumsPaymentTypesAlbumIdPaymentTypeKey = 'albums_payment_types_album_id_payment_type_key',
  /** unique or primary key constraint */
  AlbumsPaymentTypesPkey = 'albums_payment_types_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Albums_Payment_Types_Delete_At_Path_Input = {
  data?: Maybe<Array<Maybe<Scalars['String']>>>
}

/**
 * delete the array element with specified index (negative integers count from the
 * end). throws an error if top level container is not an array
 */
export type Albums_Payment_Types_Delete_Elem_Input = {
  data?: Maybe<Scalars['Int']>
}

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Albums_Payment_Types_Delete_Key_Input = {
  data?: Maybe<Scalars['String']>
}

/** input type for incrementing integer column in table "albums_payment_types" */
export type Albums_Payment_Types_Inc_Input = {
  price?: Maybe<Scalars['numeric']>
}

/** input type for inserting data into table "albums_payment_types" */
export type Albums_Payment_Types_Insert_Input = {
  album?: Maybe<Albums_Obj_Rel_Insert_Input>
  album_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  data?: Maybe<Scalars['jsonb']>
  id?: Maybe<Scalars['uuid']>
  payment_type?: Maybe<Payment_Types_Enum>
  price?: Maybe<Scalars['numeric']>
  type?: Maybe<Payment_Types_Obj_Rel_Insert_Input>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Albums_Payment_Types_Max_Fields = {
  __typename?: 'albums_payment_types_max_fields'
  album_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  price?: Maybe<Scalars['numeric']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "albums_payment_types" */
export type Albums_Payment_Types_Max_Order_By = {
  album_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  price?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Albums_Payment_Types_Min_Fields = {
  __typename?: 'albums_payment_types_min_fields'
  album_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  price?: Maybe<Scalars['numeric']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "albums_payment_types" */
export type Albums_Payment_Types_Min_Order_By = {
  album_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  price?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "albums_payment_types" */
export type Albums_Payment_Types_Mutation_Response = {
  __typename?: 'albums_payment_types_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Albums_Payment_Types>
}

/** input type for inserting object relation for remote table "albums_payment_types" */
export type Albums_Payment_Types_Obj_Rel_Insert_Input = {
  data: Albums_Payment_Types_Insert_Input
  on_conflict?: Maybe<Albums_Payment_Types_On_Conflict>
}

/** on conflict condition type for table "albums_payment_types" */
export type Albums_Payment_Types_On_Conflict = {
  constraint: Albums_Payment_Types_Constraint
  update_columns: Array<Albums_Payment_Types_Update_Column>
  where?: Maybe<Albums_Payment_Types_Bool_Exp>
}

/** ordering options when selecting data from "albums_payment_types" */
export type Albums_Payment_Types_Order_By = {
  album?: Maybe<Albums_Order_By>
  album_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  data?: Maybe<Order_By>
  id?: Maybe<Order_By>
  payment_type?: Maybe<Order_By>
  price?: Maybe<Order_By>
  type?: Maybe<Payment_Types_Order_By>
  updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "albums_payment_types" */
export type Albums_Payment_Types_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Albums_Payment_Types_Prepend_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** select columns of table "albums_payment_types" */
export enum Albums_Payment_Types_Select_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Data = 'data',
  /** column name */
  Id = 'id',
  /** column name */
  PaymentType = 'payment_type',
  /** column name */
  Price = 'price',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "albums_payment_types" */
export type Albums_Payment_Types_Set_Input = {
  album_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  data?: Maybe<Scalars['jsonb']>
  id?: Maybe<Scalars['uuid']>
  payment_type?: Maybe<Payment_Types_Enum>
  price?: Maybe<Scalars['numeric']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate stddev on columns */
export type Albums_Payment_Types_Stddev_Fields = {
  __typename?: 'albums_payment_types_stddev_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "albums_payment_types" */
export type Albums_Payment_Types_Stddev_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Albums_Payment_Types_Stddev_Pop_Fields = {
  __typename?: 'albums_payment_types_stddev_pop_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "albums_payment_types" */
export type Albums_Payment_Types_Stddev_Pop_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Albums_Payment_Types_Stddev_Samp_Fields = {
  __typename?: 'albums_payment_types_stddev_samp_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "albums_payment_types" */
export type Albums_Payment_Types_Stddev_Samp_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Albums_Payment_Types_Sum_Fields = {
  __typename?: 'albums_payment_types_sum_fields'
  price?: Maybe<Scalars['numeric']>
}

/** order by sum() on columns of table "albums_payment_types" */
export type Albums_Payment_Types_Sum_Order_By = {
  price?: Maybe<Order_By>
}

/** update columns of table "albums_payment_types" */
export enum Albums_Payment_Types_Update_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Data = 'data',
  /** column name */
  Id = 'id',
  /** column name */
  PaymentType = 'payment_type',
  /** column name */
  Price = 'price',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate var_pop on columns */
export type Albums_Payment_Types_Var_Pop_Fields = {
  __typename?: 'albums_payment_types_var_pop_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "albums_payment_types" */
export type Albums_Payment_Types_Var_Pop_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Albums_Payment_Types_Var_Samp_Fields = {
  __typename?: 'albums_payment_types_var_samp_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "albums_payment_types" */
export type Albums_Payment_Types_Var_Samp_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Albums_Payment_Types_Variance_Fields = {
  __typename?: 'albums_payment_types_variance_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "albums_payment_types" */
export type Albums_Payment_Types_Variance_Order_By = {
  price?: Maybe<Order_By>
}

/** primary key columns input for table: "albums" */
export type Albums_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "albums" */
export enum Albums_Select_Column {
  /** column name */
  Archived = 'archived',
  /** column name */
  Comment = 'comment',
  /** column name */
  CoverMediaFileId = 'cover_media_file_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Name = 'name',
  /** column name */
  Private = 'private',
  /** column name */
  TotalMediaFiles = 'total_media_files',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "albums" */
export type Albums_Set_Input = {
  archived?: Maybe<Scalars['Boolean']>
  comment?: Maybe<Scalars['String']>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  location_id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  private?: Maybe<Scalars['Boolean']>
  total_media_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** columns and relationships of "albums_statistics_view" */
export type Albums_Statistics_View = {
  __typename?: 'albums_statistics_view'
  /** An object relationship */
  album?: Maybe<Albums>
  album_id?: Maybe<Scalars['uuid']>
  downloads?: Maybe<Scalars['bigint']>
  income?: Maybe<Scalars['numeric']>
  new_downloads?: Maybe<Scalars['bigint']>
  new_visits?: Maybe<Scalars['bigint']>
  visits?: Maybe<Scalars['bigint']>
}

/** aggregated selection of "albums_statistics_view" */
export type Albums_Statistics_View_Aggregate = {
  __typename?: 'albums_statistics_view_aggregate'
  aggregate?: Maybe<Albums_Statistics_View_Aggregate_Fields>
  nodes: Array<Albums_Statistics_View>
}

/** aggregate fields of "albums_statistics_view" */
export type Albums_Statistics_View_Aggregate_Fields = {
  __typename?: 'albums_statistics_view_aggregate_fields'
  avg?: Maybe<Albums_Statistics_View_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Albums_Statistics_View_Max_Fields>
  min?: Maybe<Albums_Statistics_View_Min_Fields>
  stddev?: Maybe<Albums_Statistics_View_Stddev_Fields>
  stddev_pop?: Maybe<Albums_Statistics_View_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Albums_Statistics_View_Stddev_Samp_Fields>
  sum?: Maybe<Albums_Statistics_View_Sum_Fields>
  var_pop?: Maybe<Albums_Statistics_View_Var_Pop_Fields>
  var_samp?: Maybe<Albums_Statistics_View_Var_Samp_Fields>
  variance?: Maybe<Albums_Statistics_View_Variance_Fields>
}

/** aggregate fields of "albums_statistics_view" */
export type Albums_Statistics_View_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Albums_Statistics_View_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "albums_statistics_view" */
export type Albums_Statistics_View_Aggregate_Order_By = {
  avg?: Maybe<Albums_Statistics_View_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Albums_Statistics_View_Max_Order_By>
  min?: Maybe<Albums_Statistics_View_Min_Order_By>
  stddev?: Maybe<Albums_Statistics_View_Stddev_Order_By>
  stddev_pop?: Maybe<Albums_Statistics_View_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Albums_Statistics_View_Stddev_Samp_Order_By>
  sum?: Maybe<Albums_Statistics_View_Sum_Order_By>
  var_pop?: Maybe<Albums_Statistics_View_Var_Pop_Order_By>
  var_samp?: Maybe<Albums_Statistics_View_Var_Samp_Order_By>
  variance?: Maybe<Albums_Statistics_View_Variance_Order_By>
}

/** input type for inserting array relation for remote table "albums_statistics_view" */
export type Albums_Statistics_View_Arr_Rel_Insert_Input = {
  data: Array<Albums_Statistics_View_Insert_Input>
}

/** aggregate avg on columns */
export type Albums_Statistics_View_Avg_Fields = {
  __typename?: 'albums_statistics_view_avg_fields'
  downloads?: Maybe<Scalars['Float']>
  income?: Maybe<Scalars['Float']>
  new_downloads?: Maybe<Scalars['Float']>
  new_visits?: Maybe<Scalars['Float']>
  visits?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "albums_statistics_view" */
export type Albums_Statistics_View_Avg_Order_By = {
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "albums_statistics_view". All fields are combined with a logical 'AND'. */
export type Albums_Statistics_View_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Albums_Statistics_View_Bool_Exp>>>
  _not?: Maybe<Albums_Statistics_View_Bool_Exp>
  _or?: Maybe<Array<Maybe<Albums_Statistics_View_Bool_Exp>>>
  album?: Maybe<Albums_Bool_Exp>
  album_id?: Maybe<Uuid_Comparison_Exp>
  downloads?: Maybe<Bigint_Comparison_Exp>
  income?: Maybe<Numeric_Comparison_Exp>
  new_downloads?: Maybe<Bigint_Comparison_Exp>
  new_visits?: Maybe<Bigint_Comparison_Exp>
  visits?: Maybe<Bigint_Comparison_Exp>
}

/** input type for incrementing integer column in table "albums_statistics_view" */
export type Albums_Statistics_View_Inc_Input = {
  downloads?: Maybe<Scalars['bigint']>
  income?: Maybe<Scalars['numeric']>
  new_downloads?: Maybe<Scalars['bigint']>
  new_visits?: Maybe<Scalars['bigint']>
  visits?: Maybe<Scalars['bigint']>
}

/** input type for inserting data into table "albums_statistics_view" */
export type Albums_Statistics_View_Insert_Input = {
  album?: Maybe<Albums_Obj_Rel_Insert_Input>
  album_id?: Maybe<Scalars['uuid']>
  downloads?: Maybe<Scalars['bigint']>
  income?: Maybe<Scalars['numeric']>
  new_downloads?: Maybe<Scalars['bigint']>
  new_visits?: Maybe<Scalars['bigint']>
  visits?: Maybe<Scalars['bigint']>
}

/** aggregate max on columns */
export type Albums_Statistics_View_Max_Fields = {
  __typename?: 'albums_statistics_view_max_fields'
  album_id?: Maybe<Scalars['uuid']>
  downloads?: Maybe<Scalars['bigint']>
  income?: Maybe<Scalars['numeric']>
  new_downloads?: Maybe<Scalars['bigint']>
  new_visits?: Maybe<Scalars['bigint']>
  visits?: Maybe<Scalars['bigint']>
}

/** order by max() on columns of table "albums_statistics_view" */
export type Albums_Statistics_View_Max_Order_By = {
  album_id?: Maybe<Order_By>
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Albums_Statistics_View_Min_Fields = {
  __typename?: 'albums_statistics_view_min_fields'
  album_id?: Maybe<Scalars['uuid']>
  downloads?: Maybe<Scalars['bigint']>
  income?: Maybe<Scalars['numeric']>
  new_downloads?: Maybe<Scalars['bigint']>
  new_visits?: Maybe<Scalars['bigint']>
  visits?: Maybe<Scalars['bigint']>
}

/** order by min() on columns of table "albums_statistics_view" */
export type Albums_Statistics_View_Min_Order_By = {
  album_id?: Maybe<Order_By>
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** response of any mutation on the table "albums_statistics_view" */
export type Albums_Statistics_View_Mutation_Response = {
  __typename?: 'albums_statistics_view_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Albums_Statistics_View>
}

/** input type for inserting object relation for remote table "albums_statistics_view" */
export type Albums_Statistics_View_Obj_Rel_Insert_Input = {
  data: Albums_Statistics_View_Insert_Input
}

/** ordering options when selecting data from "albums_statistics_view" */
export type Albums_Statistics_View_Order_By = {
  album?: Maybe<Albums_Order_By>
  album_id?: Maybe<Order_By>
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** select columns of table "albums_statistics_view" */
export enum Albums_Statistics_View_Select_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Downloads = 'downloads',
  /** column name */
  Income = 'income',
  /** column name */
  NewDownloads = 'new_downloads',
  /** column name */
  NewVisits = 'new_visits',
  /** column name */
  Visits = 'visits'
}

/** input type for updating data in table "albums_statistics_view" */
export type Albums_Statistics_View_Set_Input = {
  album_id?: Maybe<Scalars['uuid']>
  downloads?: Maybe<Scalars['bigint']>
  income?: Maybe<Scalars['numeric']>
  new_downloads?: Maybe<Scalars['bigint']>
  new_visits?: Maybe<Scalars['bigint']>
  visits?: Maybe<Scalars['bigint']>
}

/** aggregate stddev on columns */
export type Albums_Statistics_View_Stddev_Fields = {
  __typename?: 'albums_statistics_view_stddev_fields'
  downloads?: Maybe<Scalars['Float']>
  income?: Maybe<Scalars['Float']>
  new_downloads?: Maybe<Scalars['Float']>
  new_visits?: Maybe<Scalars['Float']>
  visits?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "albums_statistics_view" */
export type Albums_Statistics_View_Stddev_Order_By = {
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Albums_Statistics_View_Stddev_Pop_Fields = {
  __typename?: 'albums_statistics_view_stddev_pop_fields'
  downloads?: Maybe<Scalars['Float']>
  income?: Maybe<Scalars['Float']>
  new_downloads?: Maybe<Scalars['Float']>
  new_visits?: Maybe<Scalars['Float']>
  visits?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "albums_statistics_view" */
export type Albums_Statistics_View_Stddev_Pop_Order_By = {
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Albums_Statistics_View_Stddev_Samp_Fields = {
  __typename?: 'albums_statistics_view_stddev_samp_fields'
  downloads?: Maybe<Scalars['Float']>
  income?: Maybe<Scalars['Float']>
  new_downloads?: Maybe<Scalars['Float']>
  new_visits?: Maybe<Scalars['Float']>
  visits?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "albums_statistics_view" */
export type Albums_Statistics_View_Stddev_Samp_Order_By = {
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Albums_Statistics_View_Sum_Fields = {
  __typename?: 'albums_statistics_view_sum_fields'
  downloads?: Maybe<Scalars['bigint']>
  income?: Maybe<Scalars['numeric']>
  new_downloads?: Maybe<Scalars['bigint']>
  new_visits?: Maybe<Scalars['bigint']>
  visits?: Maybe<Scalars['bigint']>
}

/** order by sum() on columns of table "albums_statistics_view" */
export type Albums_Statistics_View_Sum_Order_By = {
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** aggregate var_pop on columns */
export type Albums_Statistics_View_Var_Pop_Fields = {
  __typename?: 'albums_statistics_view_var_pop_fields'
  downloads?: Maybe<Scalars['Float']>
  income?: Maybe<Scalars['Float']>
  new_downloads?: Maybe<Scalars['Float']>
  new_visits?: Maybe<Scalars['Float']>
  visits?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "albums_statistics_view" */
export type Albums_Statistics_View_Var_Pop_Order_By = {
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Albums_Statistics_View_Var_Samp_Fields = {
  __typename?: 'albums_statistics_view_var_samp_fields'
  downloads?: Maybe<Scalars['Float']>
  income?: Maybe<Scalars['Float']>
  new_downloads?: Maybe<Scalars['Float']>
  new_visits?: Maybe<Scalars['Float']>
  visits?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "albums_statistics_view" */
export type Albums_Statistics_View_Var_Samp_Order_By = {
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Albums_Statistics_View_Variance_Fields = {
  __typename?: 'albums_statistics_view_variance_fields'
  downloads?: Maybe<Scalars['Float']>
  income?: Maybe<Scalars['Float']>
  new_downloads?: Maybe<Scalars['Float']>
  new_visits?: Maybe<Scalars['Float']>
  visits?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "albums_statistics_view" */
export type Albums_Statistics_View_Variance_Order_By = {
  downloads?: Maybe<Order_By>
  income?: Maybe<Order_By>
  new_downloads?: Maybe<Order_By>
  new_visits?: Maybe<Order_By>
  visits?: Maybe<Order_By>
}

/** aggregate stddev on columns */
export type Albums_Stddev_Fields = {
  __typename?: 'albums_stddev_fields'
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "albums" */
export type Albums_Stddev_Order_By = {
  total_media_files?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Albums_Stddev_Pop_Fields = {
  __typename?: 'albums_stddev_pop_fields'
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "albums" */
export type Albums_Stddev_Pop_Order_By = {
  total_media_files?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Albums_Stddev_Samp_Fields = {
  __typename?: 'albums_stddev_samp_fields'
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "albums" */
export type Albums_Stddev_Samp_Order_By = {
  total_media_files?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Albums_Sum_Fields = {
  __typename?: 'albums_sum_fields'
  total_media_files?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "albums" */
export type Albums_Sum_Order_By = {
  total_media_files?: Maybe<Order_By>
}

/** columns and relationships of "albums_tags" */
export type Albums_Tags = {
  __typename?: 'albums_tags'
  /** An object relationship */
  album: Albums
  album_id: Scalars['uuid']
  id: Scalars['uuid']
  /** An object relationship */
  tag: Tags
  tag_id: Scalars['uuid']
}

/** aggregated selection of "albums_tags" */
export type Albums_Tags_Aggregate = {
  __typename?: 'albums_tags_aggregate'
  aggregate?: Maybe<Albums_Tags_Aggregate_Fields>
  nodes: Array<Albums_Tags>
}

/** aggregate fields of "albums_tags" */
export type Albums_Tags_Aggregate_Fields = {
  __typename?: 'albums_tags_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Albums_Tags_Max_Fields>
  min?: Maybe<Albums_Tags_Min_Fields>
}

/** aggregate fields of "albums_tags" */
export type Albums_Tags_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Albums_Tags_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "albums_tags" */
export type Albums_Tags_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Albums_Tags_Max_Order_By>
  min?: Maybe<Albums_Tags_Min_Order_By>
}

/** input type for inserting array relation for remote table "albums_tags" */
export type Albums_Tags_Arr_Rel_Insert_Input = {
  data: Array<Albums_Tags_Insert_Input>
  on_conflict?: Maybe<Albums_Tags_On_Conflict>
}

/** Boolean expression to filter rows from the table "albums_tags". All fields are combined with a logical 'AND'. */
export type Albums_Tags_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Albums_Tags_Bool_Exp>>>
  _not?: Maybe<Albums_Tags_Bool_Exp>
  _or?: Maybe<Array<Maybe<Albums_Tags_Bool_Exp>>>
  album?: Maybe<Albums_Bool_Exp>
  album_id?: Maybe<Uuid_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  tag?: Maybe<Tags_Bool_Exp>
  tag_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "albums_tags" */
export enum Albums_Tags_Constraint {
  /** unique or primary key constraint */
  AlbumsTagsPkey = 'albums_tags_pkey',
  /** unique or primary key constraint */
  AlbumsTagsTagIdAlbumIdKey = 'albums_tags_tag_id_album_id_key'
}

/** input type for inserting data into table "albums_tags" */
export type Albums_Tags_Insert_Input = {
  album?: Maybe<Albums_Obj_Rel_Insert_Input>
  album_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  tag?: Maybe<Tags_Obj_Rel_Insert_Input>
  tag_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Albums_Tags_Max_Fields = {
  __typename?: 'albums_tags_max_fields'
  album_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  tag_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "albums_tags" */
export type Albums_Tags_Max_Order_By = {
  album_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  tag_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Albums_Tags_Min_Fields = {
  __typename?: 'albums_tags_min_fields'
  album_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  tag_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "albums_tags" */
export type Albums_Tags_Min_Order_By = {
  album_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  tag_id?: Maybe<Order_By>
}

/** response of any mutation on the table "albums_tags" */
export type Albums_Tags_Mutation_Response = {
  __typename?: 'albums_tags_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Albums_Tags>
}

/** input type for inserting object relation for remote table "albums_tags" */
export type Albums_Tags_Obj_Rel_Insert_Input = {
  data: Albums_Tags_Insert_Input
  on_conflict?: Maybe<Albums_Tags_On_Conflict>
}

/** on conflict condition type for table "albums_tags" */
export type Albums_Tags_On_Conflict = {
  constraint: Albums_Tags_Constraint
  update_columns: Array<Albums_Tags_Update_Column>
  where?: Maybe<Albums_Tags_Bool_Exp>
}

/** ordering options when selecting data from "albums_tags" */
export type Albums_Tags_Order_By = {
  album?: Maybe<Albums_Order_By>
  album_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  tag?: Maybe<Tags_Order_By>
  tag_id?: Maybe<Order_By>
}

/** primary key columns input for table: "albums_tags" */
export type Albums_Tags_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "albums_tags" */
export enum Albums_Tags_Select_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Id = 'id',
  /** column name */
  TagId = 'tag_id'
}

/** input type for updating data in table "albums_tags" */
export type Albums_Tags_Set_Input = {
  album_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  tag_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "albums_tags" */
export enum Albums_Tags_Update_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Id = 'id',
  /** column name */
  TagId = 'tag_id'
}

/** update columns of table "albums" */
export enum Albums_Update_Column {
  /** column name */
  Archived = 'archived',
  /** column name */
  Comment = 'comment',
  /** column name */
  CoverMediaFileId = 'cover_media_file_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Name = 'name',
  /** column name */
  Private = 'private',
  /** column name */
  TotalMediaFiles = 'total_media_files',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** aggregate var_pop on columns */
export type Albums_Var_Pop_Fields = {
  __typename?: 'albums_var_pop_fields'
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "albums" */
export type Albums_Var_Pop_Order_By = {
  total_media_files?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Albums_Var_Samp_Fields = {
  __typename?: 'albums_var_samp_fields'
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "albums" */
export type Albums_Var_Samp_Order_By = {
  total_media_files?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Albums_Variance_Fields = {
  __typename?: 'albums_variance_fields'
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "albums" */
export type Albums_Variance_Order_By = {
  total_media_files?: Maybe<Order_By>
}

/** columns and relationships of "albums_visits" */
export type Albums_Visits = {
  __typename?: 'albums_visits'
  /** An object relationship */
  album: Albums
  album_id: Scalars['uuid']
  date: Scalars['timestamptz']
  id: Scalars['uuid']
  /** An object relationship */
  user?: Maybe<Users>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregated selection of "albums_visits" */
export type Albums_Visits_Aggregate = {
  __typename?: 'albums_visits_aggregate'
  aggregate?: Maybe<Albums_Visits_Aggregate_Fields>
  nodes: Array<Albums_Visits>
}

/** aggregate fields of "albums_visits" */
export type Albums_Visits_Aggregate_Fields = {
  __typename?: 'albums_visits_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Albums_Visits_Max_Fields>
  min?: Maybe<Albums_Visits_Min_Fields>
}

/** aggregate fields of "albums_visits" */
export type Albums_Visits_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Albums_Visits_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "albums_visits" */
export type Albums_Visits_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Albums_Visits_Max_Order_By>
  min?: Maybe<Albums_Visits_Min_Order_By>
}

/** input type for inserting array relation for remote table "albums_visits" */
export type Albums_Visits_Arr_Rel_Insert_Input = {
  data: Array<Albums_Visits_Insert_Input>
  on_conflict?: Maybe<Albums_Visits_On_Conflict>
}

/** Boolean expression to filter rows from the table "albums_visits". All fields are combined with a logical 'AND'. */
export type Albums_Visits_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Albums_Visits_Bool_Exp>>>
  _not?: Maybe<Albums_Visits_Bool_Exp>
  _or?: Maybe<Array<Maybe<Albums_Visits_Bool_Exp>>>
  album?: Maybe<Albums_Bool_Exp>
  album_id?: Maybe<Uuid_Comparison_Exp>
  date?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "albums_visits" */
export enum Albums_Visits_Constraint {
  /** unique or primary key constraint */
  AlbumsVisitsPkey = 'albums_visits_pkey'
}

/** input type for inserting data into table "albums_visits" */
export type Albums_Visits_Insert_Input = {
  album?: Maybe<Albums_Obj_Rel_Insert_Input>
  album_id?: Maybe<Scalars['uuid']>
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Albums_Visits_Max_Fields = {
  __typename?: 'albums_visits_max_fields'
  album_id?: Maybe<Scalars['uuid']>
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "albums_visits" */
export type Albums_Visits_Max_Order_By = {
  album_id?: Maybe<Order_By>
  date?: Maybe<Order_By>
  id?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Albums_Visits_Min_Fields = {
  __typename?: 'albums_visits_min_fields'
  album_id?: Maybe<Scalars['uuid']>
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "albums_visits" */
export type Albums_Visits_Min_Order_By = {
  album_id?: Maybe<Order_By>
  date?: Maybe<Order_By>
  id?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "albums_visits" */
export type Albums_Visits_Mutation_Response = {
  __typename?: 'albums_visits_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Albums_Visits>
}

/** input type for inserting object relation for remote table "albums_visits" */
export type Albums_Visits_Obj_Rel_Insert_Input = {
  data: Albums_Visits_Insert_Input
  on_conflict?: Maybe<Albums_Visits_On_Conflict>
}

/** on conflict condition type for table "albums_visits" */
export type Albums_Visits_On_Conflict = {
  constraint: Albums_Visits_Constraint
  update_columns: Array<Albums_Visits_Update_Column>
  where?: Maybe<Albums_Visits_Bool_Exp>
}

/** ordering options when selecting data from "albums_visits" */
export type Albums_Visits_Order_By = {
  album?: Maybe<Albums_Order_By>
  album_id?: Maybe<Order_By>
  date?: Maybe<Order_By>
  id?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "albums_visits" */
export type Albums_Visits_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "albums_visits" */
export enum Albums_Visits_Select_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Date = 'date',
  /** column name */
  Id = 'id',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "albums_visits" */
export type Albums_Visits_Set_Input = {
  album_id?: Maybe<Scalars['uuid']>
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "albums_visits" */
export enum Albums_Visits_Update_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Date = 'date',
  /** column name */
  Id = 'id',
  /** column name */
  UserId = 'user_id'
}

/** columns and relationships of "auth.account_providers" */
export type Auth_Account_Providers = {
  __typename?: 'auth_account_providers'
  /** An object relationship */
  account: Auth_Accounts
  account_id: Scalars['uuid']
  auth_provider: Scalars['String']
  auth_provider_unique_id: Scalars['String']
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  /** An object relationship */
  provider: Auth_Providers
  updated_at: Scalars['timestamptz']
}

/** aggregated selection of "auth.account_providers" */
export type Auth_Account_Providers_Aggregate = {
  __typename?: 'auth_account_providers_aggregate'
  aggregate?: Maybe<Auth_Account_Providers_Aggregate_Fields>
  nodes: Array<Auth_Account_Providers>
}

/** aggregate fields of "auth.account_providers" */
export type Auth_Account_Providers_Aggregate_Fields = {
  __typename?: 'auth_account_providers_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Auth_Account_Providers_Max_Fields>
  min?: Maybe<Auth_Account_Providers_Min_Fields>
}

/** aggregate fields of "auth.account_providers" */
export type Auth_Account_Providers_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Account_Providers_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "auth.account_providers" */
export type Auth_Account_Providers_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Auth_Account_Providers_Max_Order_By>
  min?: Maybe<Auth_Account_Providers_Min_Order_By>
}

/** input type for inserting array relation for remote table "auth.account_providers" */
export type Auth_Account_Providers_Arr_Rel_Insert_Input = {
  data: Array<Auth_Account_Providers_Insert_Input>
  on_conflict?: Maybe<Auth_Account_Providers_On_Conflict>
}

/** Boolean expression to filter rows from the table "auth.account_providers". All fields are combined with a logical 'AND'. */
export type Auth_Account_Providers_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Account_Providers_Bool_Exp>>>
  _not?: Maybe<Auth_Account_Providers_Bool_Exp>
  _or?: Maybe<Array<Maybe<Auth_Account_Providers_Bool_Exp>>>
  account?: Maybe<Auth_Accounts_Bool_Exp>
  account_id?: Maybe<Uuid_Comparison_Exp>
  auth_provider?: Maybe<String_Comparison_Exp>
  auth_provider_unique_id?: Maybe<String_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  provider?: Maybe<Auth_Providers_Bool_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "auth.account_providers" */
export enum Auth_Account_Providers_Constraint {
  /** unique or primary key constraint */
  AccountProvidersAccountIdAuthProviderKey = 'account_providers_account_id_auth_provider_key',
  /** unique or primary key constraint */
  AccountProvidersAuthProviderAuthProviderUniqueIdKey = 'account_providers_auth_provider_auth_provider_unique_id_key',
  /** unique or primary key constraint */
  AccountProvidersPkey = 'account_providers_pkey'
}

/** input type for inserting data into table "auth.account_providers" */
export type Auth_Account_Providers_Insert_Input = {
  account?: Maybe<Auth_Accounts_Obj_Rel_Insert_Input>
  account_id?: Maybe<Scalars['uuid']>
  auth_provider?: Maybe<Scalars['String']>
  auth_provider_unique_id?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  provider?: Maybe<Auth_Providers_Obj_Rel_Insert_Input>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Auth_Account_Providers_Max_Fields = {
  __typename?: 'auth_account_providers_max_fields'
  account_id?: Maybe<Scalars['uuid']>
  auth_provider?: Maybe<Scalars['String']>
  auth_provider_unique_id?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "auth.account_providers" */
export type Auth_Account_Providers_Max_Order_By = {
  account_id?: Maybe<Order_By>
  auth_provider?: Maybe<Order_By>
  auth_provider_unique_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Auth_Account_Providers_Min_Fields = {
  __typename?: 'auth_account_providers_min_fields'
  account_id?: Maybe<Scalars['uuid']>
  auth_provider?: Maybe<Scalars['String']>
  auth_provider_unique_id?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "auth.account_providers" */
export type Auth_Account_Providers_Min_Order_By = {
  account_id?: Maybe<Order_By>
  auth_provider?: Maybe<Order_By>
  auth_provider_unique_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "auth.account_providers" */
export type Auth_Account_Providers_Mutation_Response = {
  __typename?: 'auth_account_providers_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Account_Providers>
}

/** input type for inserting object relation for remote table "auth.account_providers" */
export type Auth_Account_Providers_Obj_Rel_Insert_Input = {
  data: Auth_Account_Providers_Insert_Input
  on_conflict?: Maybe<Auth_Account_Providers_On_Conflict>
}

/** on conflict condition type for table "auth.account_providers" */
export type Auth_Account_Providers_On_Conflict = {
  constraint: Auth_Account_Providers_Constraint
  update_columns: Array<Auth_Account_Providers_Update_Column>
  where?: Maybe<Auth_Account_Providers_Bool_Exp>
}

/** ordering options when selecting data from "auth.account_providers" */
export type Auth_Account_Providers_Order_By = {
  account?: Maybe<Auth_Accounts_Order_By>
  account_id?: Maybe<Order_By>
  auth_provider?: Maybe<Order_By>
  auth_provider_unique_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  provider?: Maybe<Auth_Providers_Order_By>
  updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "auth.account_providers" */
export type Auth_Account_Providers_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "auth.account_providers" */
export enum Auth_Account_Providers_Select_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  AuthProvider = 'auth_provider',
  /** column name */
  AuthProviderUniqueId = 'auth_provider_unique_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "auth.account_providers" */
export type Auth_Account_Providers_Set_Input = {
  account_id?: Maybe<Scalars['uuid']>
  auth_provider?: Maybe<Scalars['String']>
  auth_provider_unique_id?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** update columns of table "auth.account_providers" */
export enum Auth_Account_Providers_Update_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  AuthProvider = 'auth_provider',
  /** column name */
  AuthProviderUniqueId = 'auth_provider_unique_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** columns and relationships of "auth.account_roles" */
export type Auth_Account_Roles = {
  __typename?: 'auth_account_roles'
  /** An object relationship */
  account: Auth_Accounts
  account_id: Scalars['uuid']
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  role: Auth_Roles_Enum
  /** An object relationship */
  roleByRole: Auth_Roles
}

/** aggregated selection of "auth.account_roles" */
export type Auth_Account_Roles_Aggregate = {
  __typename?: 'auth_account_roles_aggregate'
  aggregate?: Maybe<Auth_Account_Roles_Aggregate_Fields>
  nodes: Array<Auth_Account_Roles>
}

/** aggregate fields of "auth.account_roles" */
export type Auth_Account_Roles_Aggregate_Fields = {
  __typename?: 'auth_account_roles_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Auth_Account_Roles_Max_Fields>
  min?: Maybe<Auth_Account_Roles_Min_Fields>
}

/** aggregate fields of "auth.account_roles" */
export type Auth_Account_Roles_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Account_Roles_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "auth.account_roles" */
export type Auth_Account_Roles_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Auth_Account_Roles_Max_Order_By>
  min?: Maybe<Auth_Account_Roles_Min_Order_By>
}

/** input type for inserting array relation for remote table "auth.account_roles" */
export type Auth_Account_Roles_Arr_Rel_Insert_Input = {
  data: Array<Auth_Account_Roles_Insert_Input>
  on_conflict?: Maybe<Auth_Account_Roles_On_Conflict>
}

/** Boolean expression to filter rows from the table "auth.account_roles". All fields are combined with a logical 'AND'. */
export type Auth_Account_Roles_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Account_Roles_Bool_Exp>>>
  _not?: Maybe<Auth_Account_Roles_Bool_Exp>
  _or?: Maybe<Array<Maybe<Auth_Account_Roles_Bool_Exp>>>
  account?: Maybe<Auth_Accounts_Bool_Exp>
  account_id?: Maybe<Uuid_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  role?: Maybe<Auth_Roles_Enum_Comparison_Exp>
  roleByRole?: Maybe<Auth_Roles_Bool_Exp>
}

/** unique or primary key constraints on table "auth.account_roles" */
export enum Auth_Account_Roles_Constraint {
  /** unique or primary key constraint */
  AccountRolesPkey = 'account_roles_pkey',
  /** unique or primary key constraint */
  UserRolesAccountIdRoleKey = 'user_roles_account_id_role_key'
}

/** input type for inserting data into table "auth.account_roles" */
export type Auth_Account_Roles_Insert_Input = {
  account?: Maybe<Auth_Accounts_Obj_Rel_Insert_Input>
  account_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  role?: Maybe<Auth_Roles_Enum>
  roleByRole?: Maybe<Auth_Roles_Obj_Rel_Insert_Input>
}

/** aggregate max on columns */
export type Auth_Account_Roles_Max_Fields = {
  __typename?: 'auth_account_roles_max_fields'
  account_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "auth.account_roles" */
export type Auth_Account_Roles_Max_Order_By = {
  account_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Auth_Account_Roles_Min_Fields = {
  __typename?: 'auth_account_roles_min_fields'
  account_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "auth.account_roles" */
export type Auth_Account_Roles_Min_Order_By = {
  account_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
}

/** response of any mutation on the table "auth.account_roles" */
export type Auth_Account_Roles_Mutation_Response = {
  __typename?: 'auth_account_roles_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Account_Roles>
}

/** input type for inserting object relation for remote table "auth.account_roles" */
export type Auth_Account_Roles_Obj_Rel_Insert_Input = {
  data: Auth_Account_Roles_Insert_Input
  on_conflict?: Maybe<Auth_Account_Roles_On_Conflict>
}

/** on conflict condition type for table "auth.account_roles" */
export type Auth_Account_Roles_On_Conflict = {
  constraint: Auth_Account_Roles_Constraint
  update_columns: Array<Auth_Account_Roles_Update_Column>
  where?: Maybe<Auth_Account_Roles_Bool_Exp>
}

/** ordering options when selecting data from "auth.account_roles" */
export type Auth_Account_Roles_Order_By = {
  account?: Maybe<Auth_Accounts_Order_By>
  account_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  role?: Maybe<Order_By>
  roleByRole?: Maybe<Auth_Roles_Order_By>
}

/** primary key columns input for table: "auth.account_roles" */
export type Auth_Account_Roles_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "auth.account_roles" */
export enum Auth_Account_Roles_Select_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Role = 'role'
}

/** input type for updating data in table "auth.account_roles" */
export type Auth_Account_Roles_Set_Input = {
  account_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  role?: Maybe<Auth_Roles_Enum>
}

/** update columns of table "auth.account_roles" */
export enum Auth_Account_Roles_Update_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Role = 'role'
}

/** columns and relationships of "auth.accounts" */
export type Auth_Accounts = {
  __typename?: 'auth_accounts'
  /** An array relationship */
  account_providers: Array<Auth_Account_Providers>
  /** An aggregated array relationship */
  account_providers_aggregate: Auth_Account_Providers_Aggregate
  /** An array relationship */
  account_roles: Array<Auth_Account_Roles>
  /** An aggregated array relationship */
  account_roles_aggregate: Auth_Account_Roles_Aggregate
  active: Scalars['Boolean']
  created_at: Scalars['timestamptz']
  custom_register_data?: Maybe<Scalars['jsonb']>
  default_role: Auth_Roles_Enum
  email?: Maybe<Scalars['citext']>
  id: Scalars['uuid']
  is_anonymous: Scalars['Boolean']
  mfa_enabled: Scalars['Boolean']
  new_email?: Maybe<Scalars['citext']>
  otp_secret?: Maybe<Scalars['String']>
  password_hash?: Maybe<Scalars['String']>
  /** An array relationship */
  refresh_tokens: Array<Auth_Refresh_Tokens>
  /** An aggregated array relationship */
  refresh_tokens_aggregate: Auth_Refresh_Tokens_Aggregate
  /** An object relationship */
  role: Auth_Roles
  ticket: Scalars['uuid']
  ticket_expires_at: Scalars['timestamptz']
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** columns and relationships of "auth.accounts" */
export type Auth_AccountsAccount_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>
  where?: Maybe<Auth_Account_Providers_Bool_Exp>
}

/** columns and relationships of "auth.accounts" */
export type Auth_AccountsAccount_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>
  where?: Maybe<Auth_Account_Providers_Bool_Exp>
}

/** columns and relationships of "auth.accounts" */
export type Auth_AccountsAccount_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>
  where?: Maybe<Auth_Account_Roles_Bool_Exp>
}

/** columns and relationships of "auth.accounts" */
export type Auth_AccountsAccount_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>
  where?: Maybe<Auth_Account_Roles_Bool_Exp>
}

/** columns and relationships of "auth.accounts" */
export type Auth_AccountsCustom_Register_DataArgs = {
  path?: Maybe<Scalars['String']>
}

/** columns and relationships of "auth.accounts" */
export type Auth_AccountsRefresh_TokensArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>
}

/** columns and relationships of "auth.accounts" */
export type Auth_AccountsRefresh_Tokens_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>
}

/** aggregated selection of "auth.accounts" */
export type Auth_Accounts_Aggregate = {
  __typename?: 'auth_accounts_aggregate'
  aggregate?: Maybe<Auth_Accounts_Aggregate_Fields>
  nodes: Array<Auth_Accounts>
}

/** aggregate fields of "auth.accounts" */
export type Auth_Accounts_Aggregate_Fields = {
  __typename?: 'auth_accounts_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Auth_Accounts_Max_Fields>
  min?: Maybe<Auth_Accounts_Min_Fields>
}

/** aggregate fields of "auth.accounts" */
export type Auth_Accounts_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Accounts_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "auth.accounts" */
export type Auth_Accounts_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Auth_Accounts_Max_Order_By>
  min?: Maybe<Auth_Accounts_Min_Order_By>
}

/** append existing jsonb value of filtered columns with new jsonb value */
export type Auth_Accounts_Append_Input = {
  custom_register_data?: Maybe<Scalars['jsonb']>
}

/** input type for inserting array relation for remote table "auth.accounts" */
export type Auth_Accounts_Arr_Rel_Insert_Input = {
  data: Array<Auth_Accounts_Insert_Input>
  on_conflict?: Maybe<Auth_Accounts_On_Conflict>
}

/** Boolean expression to filter rows from the table "auth.accounts". All fields are combined with a logical 'AND'. */
export type Auth_Accounts_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Accounts_Bool_Exp>>>
  _not?: Maybe<Auth_Accounts_Bool_Exp>
  _or?: Maybe<Array<Maybe<Auth_Accounts_Bool_Exp>>>
  account_providers?: Maybe<Auth_Account_Providers_Bool_Exp>
  account_roles?: Maybe<Auth_Account_Roles_Bool_Exp>
  active?: Maybe<Boolean_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  custom_register_data?: Maybe<Jsonb_Comparison_Exp>
  default_role?: Maybe<Auth_Roles_Enum_Comparison_Exp>
  email?: Maybe<Citext_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  is_anonymous?: Maybe<Boolean_Comparison_Exp>
  mfa_enabled?: Maybe<Boolean_Comparison_Exp>
  new_email?: Maybe<Citext_Comparison_Exp>
  otp_secret?: Maybe<String_Comparison_Exp>
  password_hash?: Maybe<String_Comparison_Exp>
  refresh_tokens?: Maybe<Auth_Refresh_Tokens_Bool_Exp>
  role?: Maybe<Auth_Roles_Bool_Exp>
  ticket?: Maybe<Uuid_Comparison_Exp>
  ticket_expires_at?: Maybe<Timestamptz_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "auth.accounts" */
export enum Auth_Accounts_Constraint {
  /** unique or primary key constraint */
  AccountsEmailKey = 'accounts_email_key',
  /** unique or primary key constraint */
  AccountsNewEmailKey = 'accounts_new_email_key',
  /** unique or primary key constraint */
  AccountsPkey = 'accounts_pkey',
  /** unique or primary key constraint */
  AccountsUserIdKey = 'accounts_user_id_key'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Auth_Accounts_Delete_At_Path_Input = {
  custom_register_data?: Maybe<Array<Maybe<Scalars['String']>>>
}

/**
 * delete the array element with specified index (negative integers count from the
 * end). throws an error if top level container is not an array
 */
export type Auth_Accounts_Delete_Elem_Input = {
  custom_register_data?: Maybe<Scalars['Int']>
}

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Auth_Accounts_Delete_Key_Input = {
  custom_register_data?: Maybe<Scalars['String']>
}

/** input type for inserting data into table "auth.accounts" */
export type Auth_Accounts_Insert_Input = {
  account_providers?: Maybe<Auth_Account_Providers_Arr_Rel_Insert_Input>
  account_roles?: Maybe<Auth_Account_Roles_Arr_Rel_Insert_Input>
  active?: Maybe<Scalars['Boolean']>
  created_at?: Maybe<Scalars['timestamptz']>
  custom_register_data?: Maybe<Scalars['jsonb']>
  default_role?: Maybe<Auth_Roles_Enum>
  email?: Maybe<Scalars['citext']>
  id?: Maybe<Scalars['uuid']>
  is_anonymous?: Maybe<Scalars['Boolean']>
  mfa_enabled?: Maybe<Scalars['Boolean']>
  new_email?: Maybe<Scalars['citext']>
  otp_secret?: Maybe<Scalars['String']>
  password_hash?: Maybe<Scalars['String']>
  refresh_tokens?: Maybe<Auth_Refresh_Tokens_Arr_Rel_Insert_Input>
  role?: Maybe<Auth_Roles_Obj_Rel_Insert_Input>
  ticket?: Maybe<Scalars['uuid']>
  ticket_expires_at?: Maybe<Scalars['timestamptz']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Auth_Accounts_Max_Fields = {
  __typename?: 'auth_accounts_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  email?: Maybe<Scalars['citext']>
  id?: Maybe<Scalars['uuid']>
  new_email?: Maybe<Scalars['citext']>
  otp_secret?: Maybe<Scalars['String']>
  password_hash?: Maybe<Scalars['String']>
  ticket?: Maybe<Scalars['uuid']>
  ticket_expires_at?: Maybe<Scalars['timestamptz']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "auth.accounts" */
export type Auth_Accounts_Max_Order_By = {
  created_at?: Maybe<Order_By>
  email?: Maybe<Order_By>
  id?: Maybe<Order_By>
  new_email?: Maybe<Order_By>
  otp_secret?: Maybe<Order_By>
  password_hash?: Maybe<Order_By>
  ticket?: Maybe<Order_By>
  ticket_expires_at?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Auth_Accounts_Min_Fields = {
  __typename?: 'auth_accounts_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  email?: Maybe<Scalars['citext']>
  id?: Maybe<Scalars['uuid']>
  new_email?: Maybe<Scalars['citext']>
  otp_secret?: Maybe<Scalars['String']>
  password_hash?: Maybe<Scalars['String']>
  ticket?: Maybe<Scalars['uuid']>
  ticket_expires_at?: Maybe<Scalars['timestamptz']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "auth.accounts" */
export type Auth_Accounts_Min_Order_By = {
  created_at?: Maybe<Order_By>
  email?: Maybe<Order_By>
  id?: Maybe<Order_By>
  new_email?: Maybe<Order_By>
  otp_secret?: Maybe<Order_By>
  password_hash?: Maybe<Order_By>
  ticket?: Maybe<Order_By>
  ticket_expires_at?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "auth.accounts" */
export type Auth_Accounts_Mutation_Response = {
  __typename?: 'auth_accounts_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Accounts>
}

/** input type for inserting object relation for remote table "auth.accounts" */
export type Auth_Accounts_Obj_Rel_Insert_Input = {
  data: Auth_Accounts_Insert_Input
  on_conflict?: Maybe<Auth_Accounts_On_Conflict>
}

/** on conflict condition type for table "auth.accounts" */
export type Auth_Accounts_On_Conflict = {
  constraint: Auth_Accounts_Constraint
  update_columns: Array<Auth_Accounts_Update_Column>
  where?: Maybe<Auth_Accounts_Bool_Exp>
}

/** ordering options when selecting data from "auth.accounts" */
export type Auth_Accounts_Order_By = {
  account_providers_aggregate?: Maybe<Auth_Account_Providers_Aggregate_Order_By>
  account_roles_aggregate?: Maybe<Auth_Account_Roles_Aggregate_Order_By>
  active?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  custom_register_data?: Maybe<Order_By>
  default_role?: Maybe<Order_By>
  email?: Maybe<Order_By>
  id?: Maybe<Order_By>
  is_anonymous?: Maybe<Order_By>
  mfa_enabled?: Maybe<Order_By>
  new_email?: Maybe<Order_By>
  otp_secret?: Maybe<Order_By>
  password_hash?: Maybe<Order_By>
  refresh_tokens_aggregate?: Maybe<Auth_Refresh_Tokens_Aggregate_Order_By>
  role?: Maybe<Auth_Roles_Order_By>
  ticket?: Maybe<Order_By>
  ticket_expires_at?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "auth.accounts" */
export type Auth_Accounts_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Auth_Accounts_Prepend_Input = {
  custom_register_data?: Maybe<Scalars['jsonb']>
}

/** select columns of table "auth.accounts" */
export enum Auth_Accounts_Select_Column {
  /** column name */
  Active = 'active',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CustomRegisterData = 'custom_register_data',
  /** column name */
  DefaultRole = 'default_role',
  /** column name */
  Email = 'email',
  /** column name */
  Id = 'id',
  /** column name */
  IsAnonymous = 'is_anonymous',
  /** column name */
  MfaEnabled = 'mfa_enabled',
  /** column name */
  NewEmail = 'new_email',
  /** column name */
  OtpSecret = 'otp_secret',
  /** column name */
  PasswordHash = 'password_hash',
  /** column name */
  Ticket = 'ticket',
  /** column name */
  TicketExpiresAt = 'ticket_expires_at',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "auth.accounts" */
export type Auth_Accounts_Set_Input = {
  active?: Maybe<Scalars['Boolean']>
  created_at?: Maybe<Scalars['timestamptz']>
  custom_register_data?: Maybe<Scalars['jsonb']>
  default_role?: Maybe<Auth_Roles_Enum>
  email?: Maybe<Scalars['citext']>
  id?: Maybe<Scalars['uuid']>
  is_anonymous?: Maybe<Scalars['Boolean']>
  mfa_enabled?: Maybe<Scalars['Boolean']>
  new_email?: Maybe<Scalars['citext']>
  otp_secret?: Maybe<Scalars['String']>
  password_hash?: Maybe<Scalars['String']>
  ticket?: Maybe<Scalars['uuid']>
  ticket_expires_at?: Maybe<Scalars['timestamptz']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "auth.accounts" */
export enum Auth_Accounts_Update_Column {
  /** column name */
  Active = 'active',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CustomRegisterData = 'custom_register_data',
  /** column name */
  DefaultRole = 'default_role',
  /** column name */
  Email = 'email',
  /** column name */
  Id = 'id',
  /** column name */
  IsAnonymous = 'is_anonymous',
  /** column name */
  MfaEnabled = 'mfa_enabled',
  /** column name */
  NewEmail = 'new_email',
  /** column name */
  OtpSecret = 'otp_secret',
  /** column name */
  PasswordHash = 'password_hash',
  /** column name */
  Ticket = 'ticket',
  /** column name */
  TicketExpiresAt = 'ticket_expires_at',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** columns and relationships of "auth.providers" */
export type Auth_Providers = {
  __typename?: 'auth_providers'
  /** An array relationship */
  account_providers: Array<Auth_Account_Providers>
  /** An aggregated array relationship */
  account_providers_aggregate: Auth_Account_Providers_Aggregate
  provider: Scalars['String']
}

/** columns and relationships of "auth.providers" */
export type Auth_ProvidersAccount_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>
  where?: Maybe<Auth_Account_Providers_Bool_Exp>
}

/** columns and relationships of "auth.providers" */
export type Auth_ProvidersAccount_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>
  where?: Maybe<Auth_Account_Providers_Bool_Exp>
}

/** aggregated selection of "auth.providers" */
export type Auth_Providers_Aggregate = {
  __typename?: 'auth_providers_aggregate'
  aggregate?: Maybe<Auth_Providers_Aggregate_Fields>
  nodes: Array<Auth_Providers>
}

/** aggregate fields of "auth.providers" */
export type Auth_Providers_Aggregate_Fields = {
  __typename?: 'auth_providers_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Auth_Providers_Max_Fields>
  min?: Maybe<Auth_Providers_Min_Fields>
}

/** aggregate fields of "auth.providers" */
export type Auth_Providers_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Providers_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "auth.providers" */
export type Auth_Providers_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Auth_Providers_Max_Order_By>
  min?: Maybe<Auth_Providers_Min_Order_By>
}

/** input type for inserting array relation for remote table "auth.providers" */
export type Auth_Providers_Arr_Rel_Insert_Input = {
  data: Array<Auth_Providers_Insert_Input>
  on_conflict?: Maybe<Auth_Providers_On_Conflict>
}

/** Boolean expression to filter rows from the table "auth.providers". All fields are combined with a logical 'AND'. */
export type Auth_Providers_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Providers_Bool_Exp>>>
  _not?: Maybe<Auth_Providers_Bool_Exp>
  _or?: Maybe<Array<Maybe<Auth_Providers_Bool_Exp>>>
  account_providers?: Maybe<Auth_Account_Providers_Bool_Exp>
  provider?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "auth.providers" */
export enum Auth_Providers_Constraint {
  /** unique or primary key constraint */
  ProvidersPkey = 'providers_pkey'
}

/** input type for inserting data into table "auth.providers" */
export type Auth_Providers_Insert_Input = {
  account_providers?: Maybe<Auth_Account_Providers_Arr_Rel_Insert_Input>
  provider?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Auth_Providers_Max_Fields = {
  __typename?: 'auth_providers_max_fields'
  provider?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "auth.providers" */
export type Auth_Providers_Max_Order_By = {
  provider?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Auth_Providers_Min_Fields = {
  __typename?: 'auth_providers_min_fields'
  provider?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "auth.providers" */
export type Auth_Providers_Min_Order_By = {
  provider?: Maybe<Order_By>
}

/** response of any mutation on the table "auth.providers" */
export type Auth_Providers_Mutation_Response = {
  __typename?: 'auth_providers_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Providers>
}

/** input type for inserting object relation for remote table "auth.providers" */
export type Auth_Providers_Obj_Rel_Insert_Input = {
  data: Auth_Providers_Insert_Input
  on_conflict?: Maybe<Auth_Providers_On_Conflict>
}

/** on conflict condition type for table "auth.providers" */
export type Auth_Providers_On_Conflict = {
  constraint: Auth_Providers_Constraint
  update_columns: Array<Auth_Providers_Update_Column>
  where?: Maybe<Auth_Providers_Bool_Exp>
}

/** ordering options when selecting data from "auth.providers" */
export type Auth_Providers_Order_By = {
  account_providers_aggregate?: Maybe<Auth_Account_Providers_Aggregate_Order_By>
  provider?: Maybe<Order_By>
}

/** primary key columns input for table: "auth.providers" */
export type Auth_Providers_Pk_Columns_Input = {
  provider: Scalars['String']
}

/** select columns of table "auth.providers" */
export enum Auth_Providers_Select_Column {
  /** column name */
  Provider = 'provider'
}

/** input type for updating data in table "auth.providers" */
export type Auth_Providers_Set_Input = {
  provider?: Maybe<Scalars['String']>
}

/** update columns of table "auth.providers" */
export enum Auth_Providers_Update_Column {
  /** column name */
  Provider = 'provider'
}

/** columns and relationships of "auth.refresh_tokens" */
export type Auth_Refresh_Tokens = {
  __typename?: 'auth_refresh_tokens'
  /** An object relationship */
  account: Auth_Accounts
  account_id: Scalars['uuid']
  created_at: Scalars['timestamptz']
  expires_at: Scalars['timestamptz']
  refresh_token: Scalars['uuid']
}

/** aggregated selection of "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Aggregate = {
  __typename?: 'auth_refresh_tokens_aggregate'
  aggregate?: Maybe<Auth_Refresh_Tokens_Aggregate_Fields>
  nodes: Array<Auth_Refresh_Tokens>
}

/** aggregate fields of "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Aggregate_Fields = {
  __typename?: 'auth_refresh_tokens_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Auth_Refresh_Tokens_Max_Fields>
  min?: Maybe<Auth_Refresh_Tokens_Min_Fields>
}

/** aggregate fields of "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Auth_Refresh_Tokens_Max_Order_By>
  min?: Maybe<Auth_Refresh_Tokens_Min_Order_By>
}

/** input type for inserting array relation for remote table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Arr_Rel_Insert_Input = {
  data: Array<Auth_Refresh_Tokens_Insert_Input>
  on_conflict?: Maybe<Auth_Refresh_Tokens_On_Conflict>
}

/** Boolean expression to filter rows from the table "auth.refresh_tokens". All fields are combined with a logical 'AND'. */
export type Auth_Refresh_Tokens_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Refresh_Tokens_Bool_Exp>>>
  _not?: Maybe<Auth_Refresh_Tokens_Bool_Exp>
  _or?: Maybe<Array<Maybe<Auth_Refresh_Tokens_Bool_Exp>>>
  account?: Maybe<Auth_Accounts_Bool_Exp>
  account_id?: Maybe<Uuid_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  expires_at?: Maybe<Timestamptz_Comparison_Exp>
  refresh_token?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "auth.refresh_tokens" */
export enum Auth_Refresh_Tokens_Constraint {
  /** unique or primary key constraint */
  RefreshTokensPkey = 'refresh_tokens_pkey'
}

/** input type for inserting data into table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Insert_Input = {
  account?: Maybe<Auth_Accounts_Obj_Rel_Insert_Input>
  account_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  expires_at?: Maybe<Scalars['timestamptz']>
  refresh_token?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Auth_Refresh_Tokens_Max_Fields = {
  __typename?: 'auth_refresh_tokens_max_fields'
  account_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  expires_at?: Maybe<Scalars['timestamptz']>
  refresh_token?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Max_Order_By = {
  account_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  expires_at?: Maybe<Order_By>
  refresh_token?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Auth_Refresh_Tokens_Min_Fields = {
  __typename?: 'auth_refresh_tokens_min_fields'
  account_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  expires_at?: Maybe<Scalars['timestamptz']>
  refresh_token?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Min_Order_By = {
  account_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  expires_at?: Maybe<Order_By>
  refresh_token?: Maybe<Order_By>
}

/** response of any mutation on the table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Mutation_Response = {
  __typename?: 'auth_refresh_tokens_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Refresh_Tokens>
}

/** input type for inserting object relation for remote table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Obj_Rel_Insert_Input = {
  data: Auth_Refresh_Tokens_Insert_Input
  on_conflict?: Maybe<Auth_Refresh_Tokens_On_Conflict>
}

/** on conflict condition type for table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_On_Conflict = {
  constraint: Auth_Refresh_Tokens_Constraint
  update_columns: Array<Auth_Refresh_Tokens_Update_Column>
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>
}

/** ordering options when selecting data from "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Order_By = {
  account?: Maybe<Auth_Accounts_Order_By>
  account_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  expires_at?: Maybe<Order_By>
  refresh_token?: Maybe<Order_By>
}

/** primary key columns input for table: "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Pk_Columns_Input = {
  refresh_token: Scalars['uuid']
}

/** select columns of table "auth.refresh_tokens" */
export enum Auth_Refresh_Tokens_Select_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  ExpiresAt = 'expires_at',
  /** column name */
  RefreshToken = 'refresh_token'
}

/** input type for updating data in table "auth.refresh_tokens" */
export type Auth_Refresh_Tokens_Set_Input = {
  account_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  expires_at?: Maybe<Scalars['timestamptz']>
  refresh_token?: Maybe<Scalars['uuid']>
}

/** update columns of table "auth.refresh_tokens" */
export enum Auth_Refresh_Tokens_Update_Column {
  /** column name */
  AccountId = 'account_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  ExpiresAt = 'expires_at',
  /** column name */
  RefreshToken = 'refresh_token'
}

/** columns and relationships of "auth.roles" */
export type Auth_Roles = {
  __typename?: 'auth_roles'
  /** An array relationship */
  account_roles: Array<Auth_Account_Roles>
  /** An aggregated array relationship */
  account_roles_aggregate: Auth_Account_Roles_Aggregate
  /** An array relationship */
  accounts: Array<Auth_Accounts>
  /** An aggregated array relationship */
  accounts_aggregate: Auth_Accounts_Aggregate
  role: Scalars['String']
}

/** columns and relationships of "auth.roles" */
export type Auth_RolesAccount_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>
  where?: Maybe<Auth_Account_Roles_Bool_Exp>
}

/** columns and relationships of "auth.roles" */
export type Auth_RolesAccount_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>
  where?: Maybe<Auth_Account_Roles_Bool_Exp>
}

/** columns and relationships of "auth.roles" */
export type Auth_RolesAccountsArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>
  where?: Maybe<Auth_Accounts_Bool_Exp>
}

/** columns and relationships of "auth.roles" */
export type Auth_RolesAccounts_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>
  where?: Maybe<Auth_Accounts_Bool_Exp>
}

/** aggregated selection of "auth.roles" */
export type Auth_Roles_Aggregate = {
  __typename?: 'auth_roles_aggregate'
  aggregate?: Maybe<Auth_Roles_Aggregate_Fields>
  nodes: Array<Auth_Roles>
}

/** aggregate fields of "auth.roles" */
export type Auth_Roles_Aggregate_Fields = {
  __typename?: 'auth_roles_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Auth_Roles_Max_Fields>
  min?: Maybe<Auth_Roles_Min_Fields>
}

/** aggregate fields of "auth.roles" */
export type Auth_Roles_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Auth_Roles_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "auth.roles" */
export type Auth_Roles_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Auth_Roles_Max_Order_By>
  min?: Maybe<Auth_Roles_Min_Order_By>
}

/** input type for inserting array relation for remote table "auth.roles" */
export type Auth_Roles_Arr_Rel_Insert_Input = {
  data: Array<Auth_Roles_Insert_Input>
  on_conflict?: Maybe<Auth_Roles_On_Conflict>
}

/** Boolean expression to filter rows from the table "auth.roles". All fields are combined with a logical 'AND'. */
export type Auth_Roles_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Auth_Roles_Bool_Exp>>>
  _not?: Maybe<Auth_Roles_Bool_Exp>
  _or?: Maybe<Array<Maybe<Auth_Roles_Bool_Exp>>>
  account_roles?: Maybe<Auth_Account_Roles_Bool_Exp>
  accounts?: Maybe<Auth_Accounts_Bool_Exp>
  role?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "auth.roles" */
export enum Auth_Roles_Constraint {
  /** unique or primary key constraint */
  RolesPkey = 'roles_pkey'
}

export enum Auth_Roles_Enum {
  Administrator = 'administrator',
  Anonymous = 'anonymous',
  Me = 'me',
  User = 'user'
}

/** expression to compare columns of type auth_roles_enum. All fields are combined with logical 'AND'. */
export type Auth_Roles_Enum_Comparison_Exp = {
  _eq?: Maybe<Auth_Roles_Enum>
  _in?: Maybe<Array<Auth_Roles_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Auth_Roles_Enum>
  _nin?: Maybe<Array<Auth_Roles_Enum>>
}

/** input type for inserting data into table "auth.roles" */
export type Auth_Roles_Insert_Input = {
  account_roles?: Maybe<Auth_Account_Roles_Arr_Rel_Insert_Input>
  accounts?: Maybe<Auth_Accounts_Arr_Rel_Insert_Input>
  role?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Auth_Roles_Max_Fields = {
  __typename?: 'auth_roles_max_fields'
  role?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "auth.roles" */
export type Auth_Roles_Max_Order_By = {
  role?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Auth_Roles_Min_Fields = {
  __typename?: 'auth_roles_min_fields'
  role?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "auth.roles" */
export type Auth_Roles_Min_Order_By = {
  role?: Maybe<Order_By>
}

/** response of any mutation on the table "auth.roles" */
export type Auth_Roles_Mutation_Response = {
  __typename?: 'auth_roles_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Auth_Roles>
}

/** input type for inserting object relation for remote table "auth.roles" */
export type Auth_Roles_Obj_Rel_Insert_Input = {
  data: Auth_Roles_Insert_Input
  on_conflict?: Maybe<Auth_Roles_On_Conflict>
}

/** on conflict condition type for table "auth.roles" */
export type Auth_Roles_On_Conflict = {
  constraint: Auth_Roles_Constraint
  update_columns: Array<Auth_Roles_Update_Column>
  where?: Maybe<Auth_Roles_Bool_Exp>
}

/** ordering options when selecting data from "auth.roles" */
export type Auth_Roles_Order_By = {
  account_roles_aggregate?: Maybe<Auth_Account_Roles_Aggregate_Order_By>
  accounts_aggregate?: Maybe<Auth_Accounts_Aggregate_Order_By>
  role?: Maybe<Order_By>
}

/** primary key columns input for table: "auth.roles" */
export type Auth_Roles_Pk_Columns_Input = {
  role: Scalars['String']
}

/** select columns of table "auth.roles" */
export enum Auth_Roles_Select_Column {
  /** column name */
  Role = 'role'
}

/** input type for updating data in table "auth.roles" */
export type Auth_Roles_Set_Input = {
  role?: Maybe<Scalars['String']>
}

/** update columns of table "auth.roles" */
export enum Auth_Roles_Update_Column {
  /** column name */
  Role = 'role'
}

/** expression to compare columns of type bigint. All fields are combined with logical 'AND'. */
export type Bigint_Comparison_Exp = {
  _eq?: Maybe<Scalars['bigint']>
  _gt?: Maybe<Scalars['bigint']>
  _gte?: Maybe<Scalars['bigint']>
  _in?: Maybe<Array<Scalars['bigint']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['bigint']>
  _lte?: Maybe<Scalars['bigint']>
  _neq?: Maybe<Scalars['bigint']>
  _nin?: Maybe<Array<Scalars['bigint']>>
}

/** expression to compare columns of type Boolean. All fields are combined with logical 'AND'. */
export type Boolean_Comparison_Exp = {
  _eq?: Maybe<Scalars['Boolean']>
  _gt?: Maybe<Scalars['Boolean']>
  _gte?: Maybe<Scalars['Boolean']>
  _in?: Maybe<Array<Scalars['Boolean']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['Boolean']>
  _lte?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Scalars['Boolean']>
  _nin?: Maybe<Array<Scalars['Boolean']>>
}

export enum CacheControlScope {
  Private = 'PRIVATE',
  Public = 'PUBLIC'
}

/** expression to compare columns of type citext. All fields are combined with logical 'AND'. */
export type Citext_Comparison_Exp = {
  _eq?: Maybe<Scalars['citext']>
  _gt?: Maybe<Scalars['citext']>
  _gte?: Maybe<Scalars['citext']>
  _ilike?: Maybe<Scalars['String']>
  _in?: Maybe<Array<Scalars['citext']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _like?: Maybe<Scalars['String']>
  _lt?: Maybe<Scalars['citext']>
  _lte?: Maybe<Scalars['citext']>
  _neq?: Maybe<Scalars['citext']>
  _nilike?: Maybe<Scalars['String']>
  _nin?: Maybe<Array<Scalars['citext']>>
  _nlike?: Maybe<Scalars['String']>
  _nsimilar?: Maybe<Scalars['String']>
  _similar?: Maybe<Scalars['String']>
}

/** columns and relationships of "config" */
export type Config = {
  __typename?: 'config'
  /** An object relationship */
  config_key: Config_Keys
  data?: Maybe<Scalars['jsonb']>
  key: Config_Keys_Enum
  public: Scalars['Boolean']
  value?: Maybe<Scalars['String']>
}

/** columns and relationships of "config" */
export type ConfigDataArgs = {
  path?: Maybe<Scalars['String']>
}

/** aggregated selection of "config" */
export type Config_Aggregate = {
  __typename?: 'config_aggregate'
  aggregate?: Maybe<Config_Aggregate_Fields>
  nodes: Array<Config>
}

/** aggregate fields of "config" */
export type Config_Aggregate_Fields = {
  __typename?: 'config_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Config_Max_Fields>
  min?: Maybe<Config_Min_Fields>
}

/** aggregate fields of "config" */
export type Config_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Config_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "config" */
export type Config_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Config_Max_Order_By>
  min?: Maybe<Config_Min_Order_By>
}

/** append existing jsonb value of filtered columns with new jsonb value */
export type Config_Append_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** input type for inserting array relation for remote table "config" */
export type Config_Arr_Rel_Insert_Input = {
  data: Array<Config_Insert_Input>
  on_conflict?: Maybe<Config_On_Conflict>
}

/** Boolean expression to filter rows from the table "config". All fields are combined with a logical 'AND'. */
export type Config_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Config_Bool_Exp>>>
  _not?: Maybe<Config_Bool_Exp>
  _or?: Maybe<Array<Maybe<Config_Bool_Exp>>>
  config_key?: Maybe<Config_Keys_Bool_Exp>
  data?: Maybe<Jsonb_Comparison_Exp>
  key?: Maybe<Config_Keys_Enum_Comparison_Exp>
  public?: Maybe<Boolean_Comparison_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "config" */
export enum Config_Constraint {
  /** unique or primary key constraint */
  ConfigKeyKey = 'config_key_key',
  /** unique or primary key constraint */
  ConfigPkey = 'config_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Config_Delete_At_Path_Input = {
  data?: Maybe<Array<Maybe<Scalars['String']>>>
}

/**
 * delete the array element with specified index (negative integers count from the
 * end). throws an error if top level container is not an array
 */
export type Config_Delete_Elem_Input = {
  data?: Maybe<Scalars['Int']>
}

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Config_Delete_Key_Input = {
  data?: Maybe<Scalars['String']>
}

/** input type for inserting data into table "config" */
export type Config_Insert_Input = {
  config_key?: Maybe<Config_Keys_Obj_Rel_Insert_Input>
  data?: Maybe<Scalars['jsonb']>
  key?: Maybe<Config_Keys_Enum>
  public?: Maybe<Scalars['Boolean']>
  value?: Maybe<Scalars['String']>
}

/** columns and relationships of "config_keys" */
export type Config_Keys = {
  __typename?: 'config_keys'
  /** An object relationship */
  config?: Maybe<Config>
  value: Scalars['String']
}

/** aggregated selection of "config_keys" */
export type Config_Keys_Aggregate = {
  __typename?: 'config_keys_aggregate'
  aggregate?: Maybe<Config_Keys_Aggregate_Fields>
  nodes: Array<Config_Keys>
}

/** aggregate fields of "config_keys" */
export type Config_Keys_Aggregate_Fields = {
  __typename?: 'config_keys_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Config_Keys_Max_Fields>
  min?: Maybe<Config_Keys_Min_Fields>
}

/** aggregate fields of "config_keys" */
export type Config_Keys_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Config_Keys_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "config_keys" */
export type Config_Keys_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Config_Keys_Max_Order_By>
  min?: Maybe<Config_Keys_Min_Order_By>
}

/** input type for inserting array relation for remote table "config_keys" */
export type Config_Keys_Arr_Rel_Insert_Input = {
  data: Array<Config_Keys_Insert_Input>
  on_conflict?: Maybe<Config_Keys_On_Conflict>
}

/** Boolean expression to filter rows from the table "config_keys". All fields are combined with a logical 'AND'. */
export type Config_Keys_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Config_Keys_Bool_Exp>>>
  _not?: Maybe<Config_Keys_Bool_Exp>
  _or?: Maybe<Array<Maybe<Config_Keys_Bool_Exp>>>
  config?: Maybe<Config_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "config_keys" */
export enum Config_Keys_Constraint {
  /** unique or primary key constraint */
  ConfigKeysPkey = 'config_keys_pkey'
}

export enum Config_Keys_Enum {
  ChargeDescription = 'CHARGE_DESCRIPTION',
  ChargeName = 'CHARGE_NAME',
  FreeTariffId = 'FREE_TARIFF_ID',
  LnaPriceInUsd = 'LNA_PRICE_IN_USD'
}

/** expression to compare columns of type config_keys_enum. All fields are combined with logical 'AND'. */
export type Config_Keys_Enum_Comparison_Exp = {
  _eq?: Maybe<Config_Keys_Enum>
  _in?: Maybe<Array<Config_Keys_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Config_Keys_Enum>
  _nin?: Maybe<Array<Config_Keys_Enum>>
}

/** input type for inserting data into table "config_keys" */
export type Config_Keys_Insert_Input = {
  config?: Maybe<Config_Obj_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Config_Keys_Max_Fields = {
  __typename?: 'config_keys_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "config_keys" */
export type Config_Keys_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Config_Keys_Min_Fields = {
  __typename?: 'config_keys_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "config_keys" */
export type Config_Keys_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "config_keys" */
export type Config_Keys_Mutation_Response = {
  __typename?: 'config_keys_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Config_Keys>
}

/** input type for inserting object relation for remote table "config_keys" */
export type Config_Keys_Obj_Rel_Insert_Input = {
  data: Config_Keys_Insert_Input
  on_conflict?: Maybe<Config_Keys_On_Conflict>
}

/** on conflict condition type for table "config_keys" */
export type Config_Keys_On_Conflict = {
  constraint: Config_Keys_Constraint
  update_columns: Array<Config_Keys_Update_Column>
  where?: Maybe<Config_Keys_Bool_Exp>
}

/** ordering options when selecting data from "config_keys" */
export type Config_Keys_Order_By = {
  config?: Maybe<Config_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "config_keys" */
export type Config_Keys_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "config_keys" */
export enum Config_Keys_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "config_keys" */
export type Config_Keys_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "config_keys" */
export enum Config_Keys_Update_Column {
  /** column name */
  Value = 'value'
}

/** aggregate max on columns */
export type Config_Max_Fields = {
  __typename?: 'config_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "config" */
export type Config_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Config_Min_Fields = {
  __typename?: 'config_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "config" */
export type Config_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "config" */
export type Config_Mutation_Response = {
  __typename?: 'config_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Config>
}

/** input type for inserting object relation for remote table "config" */
export type Config_Obj_Rel_Insert_Input = {
  data: Config_Insert_Input
  on_conflict?: Maybe<Config_On_Conflict>
}

/** on conflict condition type for table "config" */
export type Config_On_Conflict = {
  constraint: Config_Constraint
  update_columns: Array<Config_Update_Column>
  where?: Maybe<Config_Bool_Exp>
}

/** ordering options when selecting data from "config" */
export type Config_Order_By = {
  config_key?: Maybe<Config_Keys_Order_By>
  data?: Maybe<Order_By>
  key?: Maybe<Order_By>
  public?: Maybe<Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "config" */
export type Config_Pk_Columns_Input = {
  key: Config_Keys_Enum
}

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Config_Prepend_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** select columns of table "config" */
export enum Config_Select_Column {
  /** column name */
  Data = 'data',
  /** column name */
  Key = 'key',
  /** column name */
  Public = 'public',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "config" */
export type Config_Set_Input = {
  data?: Maybe<Scalars['jsonb']>
  key?: Maybe<Config_Keys_Enum>
  public?: Maybe<Scalars['Boolean']>
  value?: Maybe<Scalars['String']>
}

/** update columns of table "config" */
export enum Config_Update_Column {
  /** column name */
  Data = 'data',
  /** column name */
  Key = 'key',
  /** column name */
  Public = 'public',
  /** column name */
  Value = 'value'
}

/** columns and relationships of "contacts" */
export type Contacts = {
  __typename?: 'contacts'
  blocked: Scalars['Boolean']
  /** An object relationship */
  contact: Users
  contact_id: Scalars['uuid']
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  name?: Maybe<Scalars['String']>
  /** A computed field, executes function "get_contact_new_messages_count" */
  new_messages?: Maybe<Scalars['Int']>
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** aggregated selection of "contacts" */
export type Contacts_Aggregate = {
  __typename?: 'contacts_aggregate'
  aggregate?: Maybe<Contacts_Aggregate_Fields>
  nodes: Array<Contacts>
}

/** aggregate fields of "contacts" */
export type Contacts_Aggregate_Fields = {
  __typename?: 'contacts_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Contacts_Max_Fields>
  min?: Maybe<Contacts_Min_Fields>
}

/** aggregate fields of "contacts" */
export type Contacts_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Contacts_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "contacts" */
export type Contacts_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Contacts_Max_Order_By>
  min?: Maybe<Contacts_Min_Order_By>
}

/** input type for inserting array relation for remote table "contacts" */
export type Contacts_Arr_Rel_Insert_Input = {
  data: Array<Contacts_Insert_Input>
  on_conflict?: Maybe<Contacts_On_Conflict>
}

/** Boolean expression to filter rows from the table "contacts". All fields are combined with a logical 'AND'. */
export type Contacts_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Contacts_Bool_Exp>>>
  _not?: Maybe<Contacts_Bool_Exp>
  _or?: Maybe<Array<Maybe<Contacts_Bool_Exp>>>
  blocked?: Maybe<Boolean_Comparison_Exp>
  contact?: Maybe<Users_Bool_Exp>
  contact_id?: Maybe<Uuid_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  name?: Maybe<String_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "contacts" */
export enum Contacts_Constraint {
  /** unique or primary key constraint */
  ContactsPkey = 'contacts_pkey',
  /** unique or primary key constraint */
  ContactsUserIdContactIdKey = 'contacts_user_id_contact_id_key',
  /** unique or primary key constraint */
  ContactsUserIdNameKey = 'contacts_user_id_name_key'
}

/** input type for inserting data into table "contacts" */
export type Contacts_Insert_Input = {
  blocked?: Maybe<Scalars['Boolean']>
  contact?: Maybe<Users_Obj_Rel_Insert_Input>
  contact_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Contacts_Max_Fields = {
  __typename?: 'contacts_max_fields'
  contact_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "contacts" */
export type Contacts_Max_Order_By = {
  contact_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Contacts_Min_Fields = {
  __typename?: 'contacts_min_fields'
  contact_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "contacts" */
export type Contacts_Min_Order_By = {
  contact_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "contacts" */
export type Contacts_Mutation_Response = {
  __typename?: 'contacts_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Contacts>
}

/** input type for inserting object relation for remote table "contacts" */
export type Contacts_Obj_Rel_Insert_Input = {
  data: Contacts_Insert_Input
  on_conflict?: Maybe<Contacts_On_Conflict>
}

/** on conflict condition type for table "contacts" */
export type Contacts_On_Conflict = {
  constraint: Contacts_Constraint
  update_columns: Array<Contacts_Update_Column>
  where?: Maybe<Contacts_Bool_Exp>
}

/** ordering options when selecting data from "contacts" */
export type Contacts_Order_By = {
  blocked?: Maybe<Order_By>
  contact?: Maybe<Users_Order_By>
  contact_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "contacts" */
export type Contacts_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "contacts" */
export enum Contacts_Select_Column {
  /** column name */
  Blocked = 'blocked',
  /** column name */
  ContactId = 'contact_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "contacts" */
export type Contacts_Set_Input = {
  blocked?: Maybe<Scalars['Boolean']>
  contact_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "contacts" */
export enum Contacts_Update_Column {
  /** column name */
  Blocked = 'blocked',
  /** column name */
  ContactId = 'contact_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** columns and relationships of "cover_images" */
export type Cover_Images = {
  __typename?: 'cover_images'
  blurhash: Scalars['String']
  created_at: Scalars['timestamptz']
  desktop_active: Scalars['Boolean']
  id: Scalars['uuid']
  mobile_active: Scalars['Boolean']
  thumb_url?: Maybe<Scalars['String']>
  updated_at: Scalars['timestamptz']
  url: Scalars['String']
}

/** aggregated selection of "cover_images" */
export type Cover_Images_Aggregate = {
  __typename?: 'cover_images_aggregate'
  aggregate?: Maybe<Cover_Images_Aggregate_Fields>
  nodes: Array<Cover_Images>
}

/** aggregate fields of "cover_images" */
export type Cover_Images_Aggregate_Fields = {
  __typename?: 'cover_images_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Cover_Images_Max_Fields>
  min?: Maybe<Cover_Images_Min_Fields>
}

/** aggregate fields of "cover_images" */
export type Cover_Images_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Cover_Images_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "cover_images" */
export type Cover_Images_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Cover_Images_Max_Order_By>
  min?: Maybe<Cover_Images_Min_Order_By>
}

/** input type for inserting array relation for remote table "cover_images" */
export type Cover_Images_Arr_Rel_Insert_Input = {
  data: Array<Cover_Images_Insert_Input>
  on_conflict?: Maybe<Cover_Images_On_Conflict>
}

/** Boolean expression to filter rows from the table "cover_images". All fields are combined with a logical 'AND'. */
export type Cover_Images_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Cover_Images_Bool_Exp>>>
  _not?: Maybe<Cover_Images_Bool_Exp>
  _or?: Maybe<Array<Maybe<Cover_Images_Bool_Exp>>>
  blurhash?: Maybe<String_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  desktop_active?: Maybe<Boolean_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  mobile_active?: Maybe<Boolean_Comparison_Exp>
  thumb_url?: Maybe<String_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  url?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "cover_images" */
export enum Cover_Images_Constraint {
  /** unique or primary key constraint */
  CoverImagesPkey = 'cover_images_pkey'
}

/** input type for inserting data into table "cover_images" */
export type Cover_Images_Insert_Input = {
  blurhash?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  desktop_active?: Maybe<Scalars['Boolean']>
  id?: Maybe<Scalars['uuid']>
  mobile_active?: Maybe<Scalars['Boolean']>
  thumb_url?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  url?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Cover_Images_Max_Fields = {
  __typename?: 'cover_images_max_fields'
  blurhash?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  thumb_url?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  url?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "cover_images" */
export type Cover_Images_Max_Order_By = {
  blurhash?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  thumb_url?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  url?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Cover_Images_Min_Fields = {
  __typename?: 'cover_images_min_fields'
  blurhash?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  thumb_url?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  url?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "cover_images" */
export type Cover_Images_Min_Order_By = {
  blurhash?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  thumb_url?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  url?: Maybe<Order_By>
}

/** response of any mutation on the table "cover_images" */
export type Cover_Images_Mutation_Response = {
  __typename?: 'cover_images_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Cover_Images>
}

/** input type for inserting object relation for remote table "cover_images" */
export type Cover_Images_Obj_Rel_Insert_Input = {
  data: Cover_Images_Insert_Input
  on_conflict?: Maybe<Cover_Images_On_Conflict>
}

/** on conflict condition type for table "cover_images" */
export type Cover_Images_On_Conflict = {
  constraint: Cover_Images_Constraint
  update_columns: Array<Cover_Images_Update_Column>
  where?: Maybe<Cover_Images_Bool_Exp>
}

/** ordering options when selecting data from "cover_images" */
export type Cover_Images_Order_By = {
  blurhash?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  desktop_active?: Maybe<Order_By>
  id?: Maybe<Order_By>
  mobile_active?: Maybe<Order_By>
  thumb_url?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  url?: Maybe<Order_By>
}

/** primary key columns input for table: "cover_images" */
export type Cover_Images_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "cover_images" */
export enum Cover_Images_Select_Column {
  /** column name */
  Blurhash = 'blurhash',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DesktopActive = 'desktop_active',
  /** column name */
  Id = 'id',
  /** column name */
  MobileActive = 'mobile_active',
  /** column name */
  ThumbUrl = 'thumb_url',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Url = 'url'
}

/** input type for updating data in table "cover_images" */
export type Cover_Images_Set_Input = {
  blurhash?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  desktop_active?: Maybe<Scalars['Boolean']>
  id?: Maybe<Scalars['uuid']>
  mobile_active?: Maybe<Scalars['Boolean']>
  thumb_url?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  url?: Maybe<Scalars['String']>
}

/** update columns of table "cover_images" */
export enum Cover_Images_Update_Column {
  /** column name */
  Blurhash = 'blurhash',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DesktopActive = 'desktop_active',
  /** column name */
  Id = 'id',
  /** column name */
  MobileActive = 'mobile_active',
  /** column name */
  ThumbUrl = 'thumb_url',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Url = 'url'
}

/** columns and relationships of "credit_cards" */
export type Credit_Cards = {
  __typename?: 'credit_cards'
  created_at?: Maybe<Scalars['timestamptz']>
  cvc?: Maybe<Scalars['String']>
  expires: Scalars['date']
  id: Scalars['uuid']
  name: Scalars['String']
  number: Scalars['String']
  updated_at?: Maybe<Scalars['timestamptz']>
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
  /** An object relationship */
  wallet: Wallets
  wallet_id: Scalars['uuid']
}

/** aggregated selection of "credit_cards" */
export type Credit_Cards_Aggregate = {
  __typename?: 'credit_cards_aggregate'
  aggregate?: Maybe<Credit_Cards_Aggregate_Fields>
  nodes: Array<Credit_Cards>
}

/** aggregate fields of "credit_cards" */
export type Credit_Cards_Aggregate_Fields = {
  __typename?: 'credit_cards_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Credit_Cards_Max_Fields>
  min?: Maybe<Credit_Cards_Min_Fields>
}

/** aggregate fields of "credit_cards" */
export type Credit_Cards_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Credit_Cards_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "credit_cards" */
export type Credit_Cards_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Credit_Cards_Max_Order_By>
  min?: Maybe<Credit_Cards_Min_Order_By>
}

/** input type for inserting array relation for remote table "credit_cards" */
export type Credit_Cards_Arr_Rel_Insert_Input = {
  data: Array<Credit_Cards_Insert_Input>
  on_conflict?: Maybe<Credit_Cards_On_Conflict>
}

/** Boolean expression to filter rows from the table "credit_cards". All fields are combined with a logical 'AND'. */
export type Credit_Cards_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Credit_Cards_Bool_Exp>>>
  _not?: Maybe<Credit_Cards_Bool_Exp>
  _or?: Maybe<Array<Maybe<Credit_Cards_Bool_Exp>>>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  cvc?: Maybe<String_Comparison_Exp>
  expires?: Maybe<Date_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  name?: Maybe<String_Comparison_Exp>
  number?: Maybe<String_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
  wallet?: Maybe<Wallets_Bool_Exp>
  wallet_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "credit_cards" */
export enum Credit_Cards_Constraint {
  /** unique or primary key constraint */
  CreditCardsPkey = 'credit_cards_pkey'
}

/** input type for inserting data into table "credit_cards" */
export type Credit_Cards_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  cvc?: Maybe<Scalars['String']>
  expires?: Maybe<Scalars['date']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  number?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
  wallet?: Maybe<Wallets_Obj_Rel_Insert_Input>
  wallet_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Credit_Cards_Max_Fields = {
  __typename?: 'credit_cards_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  cvc?: Maybe<Scalars['String']>
  expires?: Maybe<Scalars['date']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  number?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
  wallet_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "credit_cards" */
export type Credit_Cards_Max_Order_By = {
  created_at?: Maybe<Order_By>
  cvc?: Maybe<Order_By>
  expires?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  number?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
  wallet_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Credit_Cards_Min_Fields = {
  __typename?: 'credit_cards_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  cvc?: Maybe<Scalars['String']>
  expires?: Maybe<Scalars['date']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  number?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
  wallet_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "credit_cards" */
export type Credit_Cards_Min_Order_By = {
  created_at?: Maybe<Order_By>
  cvc?: Maybe<Order_By>
  expires?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  number?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
  wallet_id?: Maybe<Order_By>
}

/** response of any mutation on the table "credit_cards" */
export type Credit_Cards_Mutation_Response = {
  __typename?: 'credit_cards_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Credit_Cards>
}

/** input type for inserting object relation for remote table "credit_cards" */
export type Credit_Cards_Obj_Rel_Insert_Input = {
  data: Credit_Cards_Insert_Input
  on_conflict?: Maybe<Credit_Cards_On_Conflict>
}

/** on conflict condition type for table "credit_cards" */
export type Credit_Cards_On_Conflict = {
  constraint: Credit_Cards_Constraint
  update_columns: Array<Credit_Cards_Update_Column>
  where?: Maybe<Credit_Cards_Bool_Exp>
}

/** ordering options when selecting data from "credit_cards" */
export type Credit_Cards_Order_By = {
  created_at?: Maybe<Order_By>
  cvc?: Maybe<Order_By>
  expires?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  number?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
  wallet?: Maybe<Wallets_Order_By>
  wallet_id?: Maybe<Order_By>
}

/** primary key columns input for table: "credit_cards" */
export type Credit_Cards_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "credit_cards" */
export enum Credit_Cards_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Cvc = 'cvc',
  /** column name */
  Expires = 'expires',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Number = 'number',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id',
  /** column name */
  WalletId = 'wallet_id'
}

/** input type for updating data in table "credit_cards" */
export type Credit_Cards_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  cvc?: Maybe<Scalars['String']>
  expires?: Maybe<Scalars['date']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  number?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
  wallet_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "credit_cards" */
export enum Credit_Cards_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Cvc = 'cvc',
  /** column name */
  Expires = 'expires',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Number = 'number',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id',
  /** column name */
  WalletId = 'wallet_id'
}

/** expression to compare columns of type date. All fields are combined with logical 'AND'. */
export type Date_Comparison_Exp = {
  _eq?: Maybe<Scalars['date']>
  _gt?: Maybe<Scalars['date']>
  _gte?: Maybe<Scalars['date']>
  _in?: Maybe<Array<Scalars['date']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['date']>
  _lte?: Maybe<Scalars['date']>
  _neq?: Maybe<Scalars['date']>
  _nin?: Maybe<Array<Scalars['date']>>
}

/** columns and relationships of "downloads" */
export type Downloads = {
  __typename?: 'downloads'
  date: Scalars['timestamptz']
  id: Scalars['uuid']
  media_file_id: Scalars['uuid']
  /** An object relationship */
  photo: Media_Files
  /** An object relationship */
  user?: Maybe<Users>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregated selection of "downloads" */
export type Downloads_Aggregate = {
  __typename?: 'downloads_aggregate'
  aggregate?: Maybe<Downloads_Aggregate_Fields>
  nodes: Array<Downloads>
}

/** aggregate fields of "downloads" */
export type Downloads_Aggregate_Fields = {
  __typename?: 'downloads_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Downloads_Max_Fields>
  min?: Maybe<Downloads_Min_Fields>
}

/** aggregate fields of "downloads" */
export type Downloads_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Downloads_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "downloads" */
export type Downloads_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Downloads_Max_Order_By>
  min?: Maybe<Downloads_Min_Order_By>
}

/** input type for inserting array relation for remote table "downloads" */
export type Downloads_Arr_Rel_Insert_Input = {
  data: Array<Downloads_Insert_Input>
  on_conflict?: Maybe<Downloads_On_Conflict>
}

/** Boolean expression to filter rows from the table "downloads". All fields are combined with a logical 'AND'. */
export type Downloads_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Downloads_Bool_Exp>>>
  _not?: Maybe<Downloads_Bool_Exp>
  _or?: Maybe<Array<Maybe<Downloads_Bool_Exp>>>
  date?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  media_file_id?: Maybe<Uuid_Comparison_Exp>
  photo?: Maybe<Media_Files_Bool_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "downloads" */
export enum Downloads_Constraint {
  /** unique or primary key constraint */
  DownloadsPkey = 'downloads_pkey'
}

/** input type for inserting data into table "downloads" */
export type Downloads_Insert_Input = {
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  photo?: Maybe<Media_Files_Obj_Rel_Insert_Input>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Downloads_Max_Fields = {
  __typename?: 'downloads_max_fields'
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "downloads" */
export type Downloads_Max_Order_By = {
  date?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_file_id?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Downloads_Min_Fields = {
  __typename?: 'downloads_min_fields'
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "downloads" */
export type Downloads_Min_Order_By = {
  date?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_file_id?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "downloads" */
export type Downloads_Mutation_Response = {
  __typename?: 'downloads_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Downloads>
}

/** input type for inserting object relation for remote table "downloads" */
export type Downloads_Obj_Rel_Insert_Input = {
  data: Downloads_Insert_Input
  on_conflict?: Maybe<Downloads_On_Conflict>
}

/** on conflict condition type for table "downloads" */
export type Downloads_On_Conflict = {
  constraint: Downloads_Constraint
  update_columns: Array<Downloads_Update_Column>
  where?: Maybe<Downloads_Bool_Exp>
}

/** ordering options when selecting data from "downloads" */
export type Downloads_Order_By = {
  date?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_file_id?: Maybe<Order_By>
  photo?: Maybe<Media_Files_Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "downloads" */
export type Downloads_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "downloads" */
export enum Downloads_Select_Column {
  /** column name */
  Date = 'date',
  /** column name */
  Id = 'id',
  /** column name */
  MediaFileId = 'media_file_id',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "downloads" */
export type Downloads_Set_Input = {
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "downloads" */
export enum Downloads_Update_Column {
  /** column name */
  Date = 'date',
  /** column name */
  Id = 'id',
  /** column name */
  MediaFileId = 'media_file_id',
  /** column name */
  UserId = 'user_id'
}

/** columns and relationships of "events" */
export type Events = {
  __typename?: 'events'
  comment: Scalars['String']
  /** An object relationship */
  cover?: Maybe<Media_Files>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at: Scalars['timestamptz']
  /** An object relationship */
  customer: Users
  customer_id: Scalars['uuid']
  data?: Maybe<Scalars['jsonb']>
  date: Scalars['timestamptz']
  /** An object relationship */
  events_status: Events_Statuses
  id: Scalars['uuid']
  /** An object relationship */
  location?: Maybe<Locations>
  location_id?: Maybe<Scalars['uuid']>
  /** An array relationship */
  locations: Array<Locations>
  /** An aggregated array relationship */
  locations_aggregate: Locations_Aggregate
  /** An array relationship */
  media_files: Array<Media_Files>
  /** An aggregated array relationship */
  media_files_aggregate: Media_Files_Aggregate
  /** An array relationship */
  messages: Array<Messages>
  /** An aggregated array relationship */
  messages_aggregate: Messages_Aggregate
  name?: Maybe<Scalars['String']>
  /** Comment from photographer */
  note: Scalars['String']
  /** An array relationship */
  payment_types: Array<Events_Payment_Types>
  /** An aggregated array relationship */
  payment_types_aggregate: Events_Payment_Types_Aggregate
  private: Scalars['Boolean']
  retouching: Scalars['Boolean']
  /** An array relationship */
  reviews: Array<Reviews>
  /** An aggregated array relationship */
  reviews_aggregate: Reviews_Aggregate
  status: Events_Statuses_Enum
  /** Time for uploading in hours */
  time_for_upload: Scalars['Int']
  total_media_files: Scalars['Int']
  /** An array relationship */
  transactions: Array<Transactions>
  /** An aggregated array relationship */
  transactions_aggregate: Transactions_Aggregate
  updated_at: Scalars['timestamptz']
  /** Timestamp all photos uploaded */
  uploaded_time?: Maybe<Scalars['timestamptz']>
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** columns and relationships of "events" */
export type EventsDataArgs = {
  path?: Maybe<Scalars['String']>
}

/** columns and relationships of "events" */
export type EventsLocationsArgs = {
  distinct_on?: Maybe<Array<Locations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locations_Order_By>>
  where?: Maybe<Locations_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsLocations_AggregateArgs = {
  distinct_on?: Maybe<Array<Locations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locations_Order_By>>
  where?: Maybe<Locations_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsMedia_FilesArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsMedia_Files_AggregateArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsMessagesArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsMessages_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsPayment_TypesArgs = {
  distinct_on?: Maybe<Array<Events_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Payment_Types_Order_By>>
  where?: Maybe<Events_Payment_Types_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsPayment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Payment_Types_Order_By>>
  where?: Maybe<Events_Payment_Types_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsReviewsArgs = {
  distinct_on?: Maybe<Array<Reviews_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Reviews_Order_By>>
  where?: Maybe<Reviews_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsReviews_AggregateArgs = {
  distinct_on?: Maybe<Array<Reviews_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Reviews_Order_By>>
  where?: Maybe<Reviews_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsTransactionsArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** columns and relationships of "events" */
export type EventsTransactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** aggregated selection of "events" */
export type Events_Aggregate = {
  __typename?: 'events_aggregate'
  aggregate?: Maybe<Events_Aggregate_Fields>
  nodes: Array<Events>
}

/** aggregate fields of "events" */
export type Events_Aggregate_Fields = {
  __typename?: 'events_aggregate_fields'
  avg?: Maybe<Events_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Events_Max_Fields>
  min?: Maybe<Events_Min_Fields>
  stddev?: Maybe<Events_Stddev_Fields>
  stddev_pop?: Maybe<Events_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Events_Stddev_Samp_Fields>
  sum?: Maybe<Events_Sum_Fields>
  var_pop?: Maybe<Events_Var_Pop_Fields>
  var_samp?: Maybe<Events_Var_Samp_Fields>
  variance?: Maybe<Events_Variance_Fields>
}

/** aggregate fields of "events" */
export type Events_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Events_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "events" */
export type Events_Aggregate_Order_By = {
  avg?: Maybe<Events_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Events_Max_Order_By>
  min?: Maybe<Events_Min_Order_By>
  stddev?: Maybe<Events_Stddev_Order_By>
  stddev_pop?: Maybe<Events_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Events_Stddev_Samp_Order_By>
  sum?: Maybe<Events_Sum_Order_By>
  var_pop?: Maybe<Events_Var_Pop_Order_By>
  var_samp?: Maybe<Events_Var_Samp_Order_By>
  variance?: Maybe<Events_Variance_Order_By>
}

/** append existing jsonb value of filtered columns with new jsonb value */
export type Events_Append_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** input type for inserting array relation for remote table "events" */
export type Events_Arr_Rel_Insert_Input = {
  data: Array<Events_Insert_Input>
  on_conflict?: Maybe<Events_On_Conflict>
}

/** aggregate avg on columns */
export type Events_Avg_Fields = {
  __typename?: 'events_avg_fields'
  time_for_upload?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "events" */
export type Events_Avg_Order_By = {
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "events". All fields are combined with a logical 'AND'. */
export type Events_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Events_Bool_Exp>>>
  _not?: Maybe<Events_Bool_Exp>
  _or?: Maybe<Array<Maybe<Events_Bool_Exp>>>
  comment?: Maybe<String_Comparison_Exp>
  cover?: Maybe<Media_Files_Bool_Exp>
  cover_media_file_id?: Maybe<Uuid_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  customer?: Maybe<Users_Bool_Exp>
  customer_id?: Maybe<Uuid_Comparison_Exp>
  data?: Maybe<Jsonb_Comparison_Exp>
  date?: Maybe<Timestamptz_Comparison_Exp>
  events_status?: Maybe<Events_Statuses_Bool_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  location?: Maybe<Locations_Bool_Exp>
  location_id?: Maybe<Uuid_Comparison_Exp>
  locations?: Maybe<Locations_Bool_Exp>
  media_files?: Maybe<Media_Files_Bool_Exp>
  messages?: Maybe<Messages_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
  note?: Maybe<String_Comparison_Exp>
  payment_types?: Maybe<Events_Payment_Types_Bool_Exp>
  private?: Maybe<Boolean_Comparison_Exp>
  retouching?: Maybe<Boolean_Comparison_Exp>
  reviews?: Maybe<Reviews_Bool_Exp>
  status?: Maybe<Events_Statuses_Enum_Comparison_Exp>
  time_for_upload?: Maybe<Int_Comparison_Exp>
  total_media_files?: Maybe<Int_Comparison_Exp>
  transactions?: Maybe<Transactions_Bool_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  uploaded_time?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "events" */
export enum Events_Constraint {
  /** unique or primary key constraint */
  EventsPkey = 'events_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Events_Delete_At_Path_Input = {
  data?: Maybe<Array<Maybe<Scalars['String']>>>
}

/**
 * delete the array element with specified index (negative integers count from the
 * end). throws an error if top level container is not an array
 */
export type Events_Delete_Elem_Input = {
  data?: Maybe<Scalars['Int']>
}

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Events_Delete_Key_Input = {
  data?: Maybe<Scalars['String']>
}

/** input type for incrementing integer column in table "events" */
export type Events_Inc_Input = {
  time_for_upload?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "events" */
export type Events_Insert_Input = {
  comment?: Maybe<Scalars['String']>
  cover?: Maybe<Media_Files_Obj_Rel_Insert_Input>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  customer?: Maybe<Users_Obj_Rel_Insert_Input>
  customer_id?: Maybe<Scalars['uuid']>
  data?: Maybe<Scalars['jsonb']>
  date?: Maybe<Scalars['timestamptz']>
  events_status?: Maybe<Events_Statuses_Obj_Rel_Insert_Input>
  id?: Maybe<Scalars['uuid']>
  location?: Maybe<Locations_Obj_Rel_Insert_Input>
  location_id?: Maybe<Scalars['uuid']>
  locations?: Maybe<Locations_Arr_Rel_Insert_Input>
  media_files?: Maybe<Media_Files_Arr_Rel_Insert_Input>
  messages?: Maybe<Messages_Arr_Rel_Insert_Input>
  name?: Maybe<Scalars['String']>
  note?: Maybe<Scalars['String']>
  payment_types?: Maybe<Events_Payment_Types_Arr_Rel_Insert_Input>
  private?: Maybe<Scalars['Boolean']>
  retouching?: Maybe<Scalars['Boolean']>
  reviews?: Maybe<Reviews_Arr_Rel_Insert_Input>
  status?: Maybe<Events_Statuses_Enum>
  time_for_upload?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
  transactions?: Maybe<Transactions_Arr_Rel_Insert_Input>
  updated_at?: Maybe<Scalars['timestamptz']>
  uploaded_time?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Events_Max_Fields = {
  __typename?: 'events_max_fields'
  comment?: Maybe<Scalars['String']>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  customer_id?: Maybe<Scalars['uuid']>
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  location_id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  note?: Maybe<Scalars['String']>
  time_for_upload?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  uploaded_time?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "events" */
export type Events_Max_Order_By = {
  comment?: Maybe<Order_By>
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  customer_id?: Maybe<Order_By>
  date?: Maybe<Order_By>
  id?: Maybe<Order_By>
  location_id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  note?: Maybe<Order_By>
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  uploaded_time?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Events_Min_Fields = {
  __typename?: 'events_min_fields'
  comment?: Maybe<Scalars['String']>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  customer_id?: Maybe<Scalars['uuid']>
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  location_id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  note?: Maybe<Scalars['String']>
  time_for_upload?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  uploaded_time?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "events" */
export type Events_Min_Order_By = {
  comment?: Maybe<Order_By>
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  customer_id?: Maybe<Order_By>
  date?: Maybe<Order_By>
  id?: Maybe<Order_By>
  location_id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  note?: Maybe<Order_By>
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  uploaded_time?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "events" */
export type Events_Mutation_Response = {
  __typename?: 'events_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Events>
}

/** input type for inserting object relation for remote table "events" */
export type Events_Obj_Rel_Insert_Input = {
  data: Events_Insert_Input
  on_conflict?: Maybe<Events_On_Conflict>
}

/** on conflict condition type for table "events" */
export type Events_On_Conflict = {
  constraint: Events_Constraint
  update_columns: Array<Events_Update_Column>
  where?: Maybe<Events_Bool_Exp>
}

/** ordering options when selecting data from "events" */
export type Events_Order_By = {
  comment?: Maybe<Order_By>
  cover?: Maybe<Media_Files_Order_By>
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  customer?: Maybe<Users_Order_By>
  customer_id?: Maybe<Order_By>
  data?: Maybe<Order_By>
  date?: Maybe<Order_By>
  events_status?: Maybe<Events_Statuses_Order_By>
  id?: Maybe<Order_By>
  location?: Maybe<Locations_Order_By>
  location_id?: Maybe<Order_By>
  locations_aggregate?: Maybe<Locations_Aggregate_Order_By>
  media_files_aggregate?: Maybe<Media_Files_Aggregate_Order_By>
  messages_aggregate?: Maybe<Messages_Aggregate_Order_By>
  name?: Maybe<Order_By>
  note?: Maybe<Order_By>
  payment_types_aggregate?: Maybe<Events_Payment_Types_Aggregate_Order_By>
  private?: Maybe<Order_By>
  retouching?: Maybe<Order_By>
  reviews_aggregate?: Maybe<Reviews_Aggregate_Order_By>
  status?: Maybe<Order_By>
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
  transactions_aggregate?: Maybe<Transactions_Aggregate_Order_By>
  updated_at?: Maybe<Order_By>
  uploaded_time?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** columns and relationships of "events_payment_types" */
export type Events_Payment_Types = {
  __typename?: 'events_payment_types'
  created_at?: Maybe<Scalars['timestamptz']>
  data?: Maybe<Scalars['jsonb']>
  /** An object relationship */
  event: Events
  event_id: Scalars['uuid']
  id: Scalars['uuid']
  /** An object relationship */
  paymentTypeByPaymentType: Payment_Types
  payment_type: Payment_Types_Enum
  price: Scalars['numeric']
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** columns and relationships of "events_payment_types" */
export type Events_Payment_TypesDataArgs = {
  path?: Maybe<Scalars['String']>
}

/** aggregated selection of "events_payment_types" */
export type Events_Payment_Types_Aggregate = {
  __typename?: 'events_payment_types_aggregate'
  aggregate?: Maybe<Events_Payment_Types_Aggregate_Fields>
  nodes: Array<Events_Payment_Types>
}

/** aggregate fields of "events_payment_types" */
export type Events_Payment_Types_Aggregate_Fields = {
  __typename?: 'events_payment_types_aggregate_fields'
  avg?: Maybe<Events_Payment_Types_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Events_Payment_Types_Max_Fields>
  min?: Maybe<Events_Payment_Types_Min_Fields>
  stddev?: Maybe<Events_Payment_Types_Stddev_Fields>
  stddev_pop?: Maybe<Events_Payment_Types_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Events_Payment_Types_Stddev_Samp_Fields>
  sum?: Maybe<Events_Payment_Types_Sum_Fields>
  var_pop?: Maybe<Events_Payment_Types_Var_Pop_Fields>
  var_samp?: Maybe<Events_Payment_Types_Var_Samp_Fields>
  variance?: Maybe<Events_Payment_Types_Variance_Fields>
}

/** aggregate fields of "events_payment_types" */
export type Events_Payment_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Events_Payment_Types_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "events_payment_types" */
export type Events_Payment_Types_Aggregate_Order_By = {
  avg?: Maybe<Events_Payment_Types_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Events_Payment_Types_Max_Order_By>
  min?: Maybe<Events_Payment_Types_Min_Order_By>
  stddev?: Maybe<Events_Payment_Types_Stddev_Order_By>
  stddev_pop?: Maybe<Events_Payment_Types_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Events_Payment_Types_Stddev_Samp_Order_By>
  sum?: Maybe<Events_Payment_Types_Sum_Order_By>
  var_pop?: Maybe<Events_Payment_Types_Var_Pop_Order_By>
  var_samp?: Maybe<Events_Payment_Types_Var_Samp_Order_By>
  variance?: Maybe<Events_Payment_Types_Variance_Order_By>
}

/** append existing jsonb value of filtered columns with new jsonb value */
export type Events_Payment_Types_Append_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** input type for inserting array relation for remote table "events_payment_types" */
export type Events_Payment_Types_Arr_Rel_Insert_Input = {
  data: Array<Events_Payment_Types_Insert_Input>
  on_conflict?: Maybe<Events_Payment_Types_On_Conflict>
}

/** aggregate avg on columns */
export type Events_Payment_Types_Avg_Fields = {
  __typename?: 'events_payment_types_avg_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "events_payment_types" */
export type Events_Payment_Types_Avg_Order_By = {
  price?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "events_payment_types". All fields are combined with a logical 'AND'. */
export type Events_Payment_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Events_Payment_Types_Bool_Exp>>>
  _not?: Maybe<Events_Payment_Types_Bool_Exp>
  _or?: Maybe<Array<Maybe<Events_Payment_Types_Bool_Exp>>>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  data?: Maybe<Jsonb_Comparison_Exp>
  event?: Maybe<Events_Bool_Exp>
  event_id?: Maybe<Uuid_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  paymentTypeByPaymentType?: Maybe<Payment_Types_Bool_Exp>
  payment_type?: Maybe<Payment_Types_Enum_Comparison_Exp>
  price?: Maybe<Numeric_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "events_payment_types" */
export enum Events_Payment_Types_Constraint {
  /** unique or primary key constraint */
  EventsPaymentTypesEventIdPaymentTypeKey = 'events_payment_types_event_id_payment_type_key',
  /** unique or primary key constraint */
  EventsPaymentTypesPkey = 'events_payment_types_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Events_Payment_Types_Delete_At_Path_Input = {
  data?: Maybe<Array<Maybe<Scalars['String']>>>
}

/**
 * delete the array element with specified index (negative integers count from the
 * end). throws an error if top level container is not an array
 */
export type Events_Payment_Types_Delete_Elem_Input = {
  data?: Maybe<Scalars['Int']>
}

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Events_Payment_Types_Delete_Key_Input = {
  data?: Maybe<Scalars['String']>
}

/** input type for incrementing integer column in table "events_payment_types" */
export type Events_Payment_Types_Inc_Input = {
  price?: Maybe<Scalars['numeric']>
}

/** input type for inserting data into table "events_payment_types" */
export type Events_Payment_Types_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  data?: Maybe<Scalars['jsonb']>
  event?: Maybe<Events_Obj_Rel_Insert_Input>
  event_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  paymentTypeByPaymentType?: Maybe<Payment_Types_Obj_Rel_Insert_Input>
  payment_type?: Maybe<Payment_Types_Enum>
  price?: Maybe<Scalars['numeric']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Events_Payment_Types_Max_Fields = {
  __typename?: 'events_payment_types_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  price?: Maybe<Scalars['numeric']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "events_payment_types" */
export type Events_Payment_Types_Max_Order_By = {
  created_at?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  price?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Events_Payment_Types_Min_Fields = {
  __typename?: 'events_payment_types_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  price?: Maybe<Scalars['numeric']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "events_payment_types" */
export type Events_Payment_Types_Min_Order_By = {
  created_at?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  price?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "events_payment_types" */
export type Events_Payment_Types_Mutation_Response = {
  __typename?: 'events_payment_types_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Events_Payment_Types>
}

/** input type for inserting object relation for remote table "events_payment_types" */
export type Events_Payment_Types_Obj_Rel_Insert_Input = {
  data: Events_Payment_Types_Insert_Input
  on_conflict?: Maybe<Events_Payment_Types_On_Conflict>
}

/** on conflict condition type for table "events_payment_types" */
export type Events_Payment_Types_On_Conflict = {
  constraint: Events_Payment_Types_Constraint
  update_columns: Array<Events_Payment_Types_Update_Column>
  where?: Maybe<Events_Payment_Types_Bool_Exp>
}

/** ordering options when selecting data from "events_payment_types" */
export type Events_Payment_Types_Order_By = {
  created_at?: Maybe<Order_By>
  data?: Maybe<Order_By>
  event?: Maybe<Events_Order_By>
  event_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  paymentTypeByPaymentType?: Maybe<Payment_Types_Order_By>
  payment_type?: Maybe<Order_By>
  price?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "events_payment_types" */
export type Events_Payment_Types_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Events_Payment_Types_Prepend_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** select columns of table "events_payment_types" */
export enum Events_Payment_Types_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Data = 'data',
  /** column name */
  EventId = 'event_id',
  /** column name */
  Id = 'id',
  /** column name */
  PaymentType = 'payment_type',
  /** column name */
  Price = 'price',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "events_payment_types" */
export type Events_Payment_Types_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  data?: Maybe<Scalars['jsonb']>
  event_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  payment_type?: Maybe<Payment_Types_Enum>
  price?: Maybe<Scalars['numeric']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate stddev on columns */
export type Events_Payment_Types_Stddev_Fields = {
  __typename?: 'events_payment_types_stddev_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "events_payment_types" */
export type Events_Payment_Types_Stddev_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Events_Payment_Types_Stddev_Pop_Fields = {
  __typename?: 'events_payment_types_stddev_pop_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "events_payment_types" */
export type Events_Payment_Types_Stddev_Pop_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Events_Payment_Types_Stddev_Samp_Fields = {
  __typename?: 'events_payment_types_stddev_samp_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "events_payment_types" */
export type Events_Payment_Types_Stddev_Samp_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Events_Payment_Types_Sum_Fields = {
  __typename?: 'events_payment_types_sum_fields'
  price?: Maybe<Scalars['numeric']>
}

/** order by sum() on columns of table "events_payment_types" */
export type Events_Payment_Types_Sum_Order_By = {
  price?: Maybe<Order_By>
}

/** update columns of table "events_payment_types" */
export enum Events_Payment_Types_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Data = 'data',
  /** column name */
  EventId = 'event_id',
  /** column name */
  Id = 'id',
  /** column name */
  PaymentType = 'payment_type',
  /** column name */
  Price = 'price',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate var_pop on columns */
export type Events_Payment_Types_Var_Pop_Fields = {
  __typename?: 'events_payment_types_var_pop_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "events_payment_types" */
export type Events_Payment_Types_Var_Pop_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Events_Payment_Types_Var_Samp_Fields = {
  __typename?: 'events_payment_types_var_samp_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "events_payment_types" */
export type Events_Payment_Types_Var_Samp_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Events_Payment_Types_Variance_Fields = {
  __typename?: 'events_payment_types_variance_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "events_payment_types" */
export type Events_Payment_Types_Variance_Order_By = {
  price?: Maybe<Order_By>
}

/** primary key columns input for table: "events" */
export type Events_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Events_Prepend_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** select columns of table "events" */
export enum Events_Select_Column {
  /** column name */
  Comment = 'comment',
  /** column name */
  CoverMediaFileId = 'cover_media_file_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CustomerId = 'customer_id',
  /** column name */
  Data = 'data',
  /** column name */
  Date = 'date',
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Name = 'name',
  /** column name */
  Note = 'note',
  /** column name */
  Private = 'private',
  /** column name */
  Retouching = 'retouching',
  /** column name */
  Status = 'status',
  /** column name */
  TimeForUpload = 'time_for_upload',
  /** column name */
  TotalMediaFiles = 'total_media_files',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UploadedTime = 'uploaded_time',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "events" */
export type Events_Set_Input = {
  comment?: Maybe<Scalars['String']>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  customer_id?: Maybe<Scalars['uuid']>
  data?: Maybe<Scalars['jsonb']>
  date?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  location_id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  note?: Maybe<Scalars['String']>
  private?: Maybe<Scalars['Boolean']>
  retouching?: Maybe<Scalars['Boolean']>
  status?: Maybe<Events_Statuses_Enum>
  time_for_upload?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  uploaded_time?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** columns and relationships of "events_statuses" */
export type Events_Statuses = {
  __typename?: 'events_statuses'
  /** An array relationship */
  events: Array<Events>
  /** An aggregated array relationship */
  events_aggregate: Events_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "events_statuses" */
export type Events_StatusesEventsArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** columns and relationships of "events_statuses" */
export type Events_StatusesEvents_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** aggregated selection of "events_statuses" */
export type Events_Statuses_Aggregate = {
  __typename?: 'events_statuses_aggregate'
  aggregate?: Maybe<Events_Statuses_Aggregate_Fields>
  nodes: Array<Events_Statuses>
}

/** aggregate fields of "events_statuses" */
export type Events_Statuses_Aggregate_Fields = {
  __typename?: 'events_statuses_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Events_Statuses_Max_Fields>
  min?: Maybe<Events_Statuses_Min_Fields>
}

/** aggregate fields of "events_statuses" */
export type Events_Statuses_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Events_Statuses_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "events_statuses" */
export type Events_Statuses_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Events_Statuses_Max_Order_By>
  min?: Maybe<Events_Statuses_Min_Order_By>
}

/** input type for inserting array relation for remote table "events_statuses" */
export type Events_Statuses_Arr_Rel_Insert_Input = {
  data: Array<Events_Statuses_Insert_Input>
  on_conflict?: Maybe<Events_Statuses_On_Conflict>
}

/** Boolean expression to filter rows from the table "events_statuses". All fields are combined with a logical 'AND'. */
export type Events_Statuses_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Events_Statuses_Bool_Exp>>>
  _not?: Maybe<Events_Statuses_Bool_Exp>
  _or?: Maybe<Array<Maybe<Events_Statuses_Bool_Exp>>>
  events?: Maybe<Events_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "events_statuses" */
export enum Events_Statuses_Constraint {
  /** unique or primary key constraint */
  EventsStatusesPkey = 'events_statuses_pkey'
}

export enum Events_Statuses_Enum {
  Canceled = 'CANCELED',
  Declined = 'DECLINED',
  Finished = 'FINISHED',
  New = 'NEW',
  Offer = 'OFFER',
  Started = 'STARTED'
}

/** expression to compare columns of type events_statuses_enum. All fields are combined with logical 'AND'. */
export type Events_Statuses_Enum_Comparison_Exp = {
  _eq?: Maybe<Events_Statuses_Enum>
  _in?: Maybe<Array<Events_Statuses_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Events_Statuses_Enum>
  _nin?: Maybe<Array<Events_Statuses_Enum>>
}

/** input type for inserting data into table "events_statuses" */
export type Events_Statuses_Insert_Input = {
  events?: Maybe<Events_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Events_Statuses_Max_Fields = {
  __typename?: 'events_statuses_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "events_statuses" */
export type Events_Statuses_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Events_Statuses_Min_Fields = {
  __typename?: 'events_statuses_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "events_statuses" */
export type Events_Statuses_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "events_statuses" */
export type Events_Statuses_Mutation_Response = {
  __typename?: 'events_statuses_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Events_Statuses>
}

/** input type for inserting object relation for remote table "events_statuses" */
export type Events_Statuses_Obj_Rel_Insert_Input = {
  data: Events_Statuses_Insert_Input
  on_conflict?: Maybe<Events_Statuses_On_Conflict>
}

/** on conflict condition type for table "events_statuses" */
export type Events_Statuses_On_Conflict = {
  constraint: Events_Statuses_Constraint
  update_columns: Array<Events_Statuses_Update_Column>
  where?: Maybe<Events_Statuses_Bool_Exp>
}

/** ordering options when selecting data from "events_statuses" */
export type Events_Statuses_Order_By = {
  events_aggregate?: Maybe<Events_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "events_statuses" */
export type Events_Statuses_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "events_statuses" */
export enum Events_Statuses_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "events_statuses" */
export type Events_Statuses_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "events_statuses" */
export enum Events_Statuses_Update_Column {
  /** column name */
  Value = 'value'
}

/** aggregate stddev on columns */
export type Events_Stddev_Fields = {
  __typename?: 'events_stddev_fields'
  time_for_upload?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "events" */
export type Events_Stddev_Order_By = {
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Events_Stddev_Pop_Fields = {
  __typename?: 'events_stddev_pop_fields'
  time_for_upload?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "events" */
export type Events_Stddev_Pop_Order_By = {
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Events_Stddev_Samp_Fields = {
  __typename?: 'events_stddev_samp_fields'
  time_for_upload?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "events" */
export type Events_Stddev_Samp_Order_By = {
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Events_Sum_Fields = {
  __typename?: 'events_sum_fields'
  time_for_upload?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "events" */
export type Events_Sum_Order_By = {
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** update columns of table "events" */
export enum Events_Update_Column {
  /** column name */
  Comment = 'comment',
  /** column name */
  CoverMediaFileId = 'cover_media_file_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CustomerId = 'customer_id',
  /** column name */
  Data = 'data',
  /** column name */
  Date = 'date',
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Name = 'name',
  /** column name */
  Note = 'note',
  /** column name */
  Private = 'private',
  /** column name */
  Retouching = 'retouching',
  /** column name */
  Status = 'status',
  /** column name */
  TimeForUpload = 'time_for_upload',
  /** column name */
  TotalMediaFiles = 'total_media_files',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UploadedTime = 'uploaded_time',
  /** column name */
  UserId = 'user_id'
}

/** aggregate var_pop on columns */
export type Events_Var_Pop_Fields = {
  __typename?: 'events_var_pop_fields'
  time_for_upload?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "events" */
export type Events_Var_Pop_Order_By = {
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Events_Var_Samp_Fields = {
  __typename?: 'events_var_samp_fields'
  time_for_upload?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "events" */
export type Events_Var_Samp_Order_By = {
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Events_Variance_Fields = {
  __typename?: 'events_variance_fields'
  time_for_upload?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "events" */
export type Events_Variance_Order_By = {
  time_for_upload?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** columns and relationships of "favorites" */
export type Favorites = {
  __typename?: 'favorites'
  /** An object relationship */
  album?: Maybe<Albums>
  album_id?: Maybe<Scalars['uuid']>
  id: Scalars['uuid']
  /** An object relationship */
  media_file?: Maybe<Media_Files>
  media_file_id?: Maybe<Scalars['uuid']>
  /** An object relationship */
  owner: Users
  owner_id: Scalars['uuid']
  /** An object relationship */
  user?: Maybe<Users>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregated selection of "favorites" */
export type Favorites_Aggregate = {
  __typename?: 'favorites_aggregate'
  aggregate?: Maybe<Favorites_Aggregate_Fields>
  nodes: Array<Favorites>
}

/** aggregate fields of "favorites" */
export type Favorites_Aggregate_Fields = {
  __typename?: 'favorites_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Favorites_Max_Fields>
  min?: Maybe<Favorites_Min_Fields>
}

/** aggregate fields of "favorites" */
export type Favorites_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Favorites_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "favorites" */
export type Favorites_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Favorites_Max_Order_By>
  min?: Maybe<Favorites_Min_Order_By>
}

/** input type for inserting array relation for remote table "favorites" */
export type Favorites_Arr_Rel_Insert_Input = {
  data: Array<Favorites_Insert_Input>
  on_conflict?: Maybe<Favorites_On_Conflict>
}

/** Boolean expression to filter rows from the table "favorites". All fields are combined with a logical 'AND'. */
export type Favorites_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Favorites_Bool_Exp>>>
  _not?: Maybe<Favorites_Bool_Exp>
  _or?: Maybe<Array<Maybe<Favorites_Bool_Exp>>>
  album?: Maybe<Albums_Bool_Exp>
  album_id?: Maybe<Uuid_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  media_file?: Maybe<Media_Files_Bool_Exp>
  media_file_id?: Maybe<Uuid_Comparison_Exp>
  owner?: Maybe<Users_Bool_Exp>
  owner_id?: Maybe<Uuid_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "favorites" */
export enum Favorites_Constraint {
  /** unique or primary key constraint */
  FavoritesOwnerIdPhotoIdKey = 'favorites_owner_id_photo_id_key',
  /** unique or primary key constraint */
  FavoritesPkey = 'favorites_pkey',
  /** unique or primary key constraint */
  FavoritesUserIdAlbumIdKey = 'favorites_user_id_album_id_key',
  /** unique or primary key constraint */
  FavoritesUserIdPhotographerIdKey = 'favorites_user_id_photographer_id_key'
}

/** input type for inserting data into table "favorites" */
export type Favorites_Insert_Input = {
  album?: Maybe<Albums_Obj_Rel_Insert_Input>
  album_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  media_file?: Maybe<Media_Files_Obj_Rel_Insert_Input>
  media_file_id?: Maybe<Scalars['uuid']>
  owner?: Maybe<Users_Obj_Rel_Insert_Input>
  owner_id?: Maybe<Scalars['uuid']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Favorites_Max_Fields = {
  __typename?: 'favorites_max_fields'
  album_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  owner_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "favorites" */
export type Favorites_Max_Order_By = {
  album_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_file_id?: Maybe<Order_By>
  owner_id?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Favorites_Min_Fields = {
  __typename?: 'favorites_min_fields'
  album_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  owner_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "favorites" */
export type Favorites_Min_Order_By = {
  album_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_file_id?: Maybe<Order_By>
  owner_id?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "favorites" */
export type Favorites_Mutation_Response = {
  __typename?: 'favorites_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Favorites>
}

/** input type for inserting object relation for remote table "favorites" */
export type Favorites_Obj_Rel_Insert_Input = {
  data: Favorites_Insert_Input
  on_conflict?: Maybe<Favorites_On_Conflict>
}

/** on conflict condition type for table "favorites" */
export type Favorites_On_Conflict = {
  constraint: Favorites_Constraint
  update_columns: Array<Favorites_Update_Column>
  where?: Maybe<Favorites_Bool_Exp>
}

/** ordering options when selecting data from "favorites" */
export type Favorites_Order_By = {
  album?: Maybe<Albums_Order_By>
  album_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_file?: Maybe<Media_Files_Order_By>
  media_file_id?: Maybe<Order_By>
  owner?: Maybe<Users_Order_By>
  owner_id?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "favorites" */
export type Favorites_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "favorites" */
export enum Favorites_Select_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Id = 'id',
  /** column name */
  MediaFileId = 'media_file_id',
  /** column name */
  OwnerId = 'owner_id',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "favorites" */
export type Favorites_Set_Input = {
  album_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  owner_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "favorites" */
export enum Favorites_Update_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Id = 'id',
  /** column name */
  MediaFileId = 'media_file_id',
  /** column name */
  OwnerId = 'owner_id',
  /** column name */
  UserId = 'user_id'
}

/** columns and relationships of "folders" */
export type Folders = {
  __typename?: 'folders'
  /** An object relationship */
  cover?: Maybe<Media_Files>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  /** An array relationship */
  media_files: Array<Media_Files>
  /** An aggregated array relationship */
  media_files_aggregate: Media_Files_Aggregate
  name: Scalars['String']
  total_media_files: Scalars['Int']
  total_size: Scalars['bigint']
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** columns and relationships of "folders" */
export type FoldersMedia_FilesArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** columns and relationships of "folders" */
export type FoldersMedia_Files_AggregateArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** aggregated selection of "folders" */
export type Folders_Aggregate = {
  __typename?: 'folders_aggregate'
  aggregate?: Maybe<Folders_Aggregate_Fields>
  nodes: Array<Folders>
}

/** aggregate fields of "folders" */
export type Folders_Aggregate_Fields = {
  __typename?: 'folders_aggregate_fields'
  avg?: Maybe<Folders_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Folders_Max_Fields>
  min?: Maybe<Folders_Min_Fields>
  stddev?: Maybe<Folders_Stddev_Fields>
  stddev_pop?: Maybe<Folders_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Folders_Stddev_Samp_Fields>
  sum?: Maybe<Folders_Sum_Fields>
  var_pop?: Maybe<Folders_Var_Pop_Fields>
  var_samp?: Maybe<Folders_Var_Samp_Fields>
  variance?: Maybe<Folders_Variance_Fields>
}

/** aggregate fields of "folders" */
export type Folders_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Folders_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "folders" */
export type Folders_Aggregate_Order_By = {
  avg?: Maybe<Folders_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Folders_Max_Order_By>
  min?: Maybe<Folders_Min_Order_By>
  stddev?: Maybe<Folders_Stddev_Order_By>
  stddev_pop?: Maybe<Folders_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Folders_Stddev_Samp_Order_By>
  sum?: Maybe<Folders_Sum_Order_By>
  var_pop?: Maybe<Folders_Var_Pop_Order_By>
  var_samp?: Maybe<Folders_Var_Samp_Order_By>
  variance?: Maybe<Folders_Variance_Order_By>
}

/** input type for inserting array relation for remote table "folders" */
export type Folders_Arr_Rel_Insert_Input = {
  data: Array<Folders_Insert_Input>
  on_conflict?: Maybe<Folders_On_Conflict>
}

/** aggregate avg on columns */
export type Folders_Avg_Fields = {
  __typename?: 'folders_avg_fields'
  total_media_files?: Maybe<Scalars['Float']>
  total_size?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "folders" */
export type Folders_Avg_Order_By = {
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "folders". All fields are combined with a logical 'AND'. */
export type Folders_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Folders_Bool_Exp>>>
  _not?: Maybe<Folders_Bool_Exp>
  _or?: Maybe<Array<Maybe<Folders_Bool_Exp>>>
  cover?: Maybe<Media_Files_Bool_Exp>
  cover_media_file_id?: Maybe<Uuid_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  media_files?: Maybe<Media_Files_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
  total_media_files?: Maybe<Int_Comparison_Exp>
  total_size?: Maybe<Bigint_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "folders" */
export enum Folders_Constraint {
  /** unique or primary key constraint */
  FoldersPkey = 'folders_pkey',
  /** unique or primary key constraint */
  FoldersUserIdNameKey = 'folders_user_id_name_key'
}

/** input type for incrementing integer column in table "folders" */
export type Folders_Inc_Input = {
  total_media_files?: Maybe<Scalars['Int']>
  total_size?: Maybe<Scalars['bigint']>
}

/** input type for inserting data into table "folders" */
export type Folders_Insert_Input = {
  cover?: Maybe<Media_Files_Obj_Rel_Insert_Input>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  media_files?: Maybe<Media_Files_Arr_Rel_Insert_Input>
  name?: Maybe<Scalars['String']>
  total_media_files?: Maybe<Scalars['Int']>
  total_size?: Maybe<Scalars['bigint']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Folders_Max_Fields = {
  __typename?: 'folders_max_fields'
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  total_media_files?: Maybe<Scalars['Int']>
  total_size?: Maybe<Scalars['bigint']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "folders" */
export type Folders_Max_Order_By = {
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Folders_Min_Fields = {
  __typename?: 'folders_min_fields'
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  total_media_files?: Maybe<Scalars['Int']>
  total_size?: Maybe<Scalars['bigint']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "folders" */
export type Folders_Min_Order_By = {
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "folders" */
export type Folders_Mutation_Response = {
  __typename?: 'folders_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Folders>
}

/** input type for inserting object relation for remote table "folders" */
export type Folders_Obj_Rel_Insert_Input = {
  data: Folders_Insert_Input
  on_conflict?: Maybe<Folders_On_Conflict>
}

/** on conflict condition type for table "folders" */
export type Folders_On_Conflict = {
  constraint: Folders_Constraint
  update_columns: Array<Folders_Update_Column>
  where?: Maybe<Folders_Bool_Exp>
}

/** ordering options when selecting data from "folders" */
export type Folders_Order_By = {
  cover?: Maybe<Media_Files_Order_By>
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_files_aggregate?: Maybe<Media_Files_Aggregate_Order_By>
  name?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "folders" */
export type Folders_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "folders" */
export enum Folders_Select_Column {
  /** column name */
  CoverMediaFileId = 'cover_media_file_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  TotalMediaFiles = 'total_media_files',
  /** column name */
  TotalSize = 'total_size',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "folders" */
export type Folders_Set_Input = {
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  total_media_files?: Maybe<Scalars['Int']>
  total_size?: Maybe<Scalars['bigint']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate stddev on columns */
export type Folders_Stddev_Fields = {
  __typename?: 'folders_stddev_fields'
  total_media_files?: Maybe<Scalars['Float']>
  total_size?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "folders" */
export type Folders_Stddev_Order_By = {
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Folders_Stddev_Pop_Fields = {
  __typename?: 'folders_stddev_pop_fields'
  total_media_files?: Maybe<Scalars['Float']>
  total_size?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "folders" */
export type Folders_Stddev_Pop_Order_By = {
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Folders_Stddev_Samp_Fields = {
  __typename?: 'folders_stddev_samp_fields'
  total_media_files?: Maybe<Scalars['Float']>
  total_size?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "folders" */
export type Folders_Stddev_Samp_Order_By = {
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Folders_Sum_Fields = {
  __typename?: 'folders_sum_fields'
  total_media_files?: Maybe<Scalars['Int']>
  total_size?: Maybe<Scalars['bigint']>
}

/** order by sum() on columns of table "folders" */
export type Folders_Sum_Order_By = {
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
}

/** update columns of table "folders" */
export enum Folders_Update_Column {
  /** column name */
  CoverMediaFileId = 'cover_media_file_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  TotalMediaFiles = 'total_media_files',
  /** column name */
  TotalSize = 'total_size',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** aggregate var_pop on columns */
export type Folders_Var_Pop_Fields = {
  __typename?: 'folders_var_pop_fields'
  total_media_files?: Maybe<Scalars['Float']>
  total_size?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "folders" */
export type Folders_Var_Pop_Order_By = {
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Folders_Var_Samp_Fields = {
  __typename?: 'folders_var_samp_fields'
  total_media_files?: Maybe<Scalars['Float']>
  total_size?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "folders" */
export type Folders_Var_Samp_Order_By = {
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Folders_Variance_Fields = {
  __typename?: 'folders_variance_fields'
  total_media_files?: Maybe<Scalars['Float']>
  total_size?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "folders" */
export type Folders_Variance_Order_By = {
  total_media_files?: Maybe<Order_By>
  total_size?: Maybe<Order_By>
}

/**
 * Expression to compare the result of casting a column of type geography. Multiple
 * cast targets are combined with logical 'AND'.
 */
export type Geography_Cast_Exp = {
  geometry?: Maybe<Geometry_Comparison_Exp>
}

/** columns and relationships of "geography_columns" */
export type Geography_Columns = {
  __typename?: 'geography_columns'
  coord_dimension?: Maybe<Scalars['Int']>
  f_geography_column?: Maybe<Scalars['name']>
  f_table_catalog?: Maybe<Scalars['name']>
  f_table_name?: Maybe<Scalars['name']>
  f_table_schema?: Maybe<Scalars['name']>
  srid?: Maybe<Scalars['Int']>
  type?: Maybe<Scalars['String']>
}

/** aggregated selection of "geography_columns" */
export type Geography_Columns_Aggregate = {
  __typename?: 'geography_columns_aggregate'
  aggregate?: Maybe<Geography_Columns_Aggregate_Fields>
  nodes: Array<Geography_Columns>
}

/** aggregate fields of "geography_columns" */
export type Geography_Columns_Aggregate_Fields = {
  __typename?: 'geography_columns_aggregate_fields'
  avg?: Maybe<Geography_Columns_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Geography_Columns_Max_Fields>
  min?: Maybe<Geography_Columns_Min_Fields>
  stddev?: Maybe<Geography_Columns_Stddev_Fields>
  stddev_pop?: Maybe<Geography_Columns_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Geography_Columns_Stddev_Samp_Fields>
  sum?: Maybe<Geography_Columns_Sum_Fields>
  var_pop?: Maybe<Geography_Columns_Var_Pop_Fields>
  var_samp?: Maybe<Geography_Columns_Var_Samp_Fields>
  variance?: Maybe<Geography_Columns_Variance_Fields>
}

/** aggregate fields of "geography_columns" */
export type Geography_Columns_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Geography_Columns_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "geography_columns" */
export type Geography_Columns_Aggregate_Order_By = {
  avg?: Maybe<Geography_Columns_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Geography_Columns_Max_Order_By>
  min?: Maybe<Geography_Columns_Min_Order_By>
  stddev?: Maybe<Geography_Columns_Stddev_Order_By>
  stddev_pop?: Maybe<Geography_Columns_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Geography_Columns_Stddev_Samp_Order_By>
  sum?: Maybe<Geography_Columns_Sum_Order_By>
  var_pop?: Maybe<Geography_Columns_Var_Pop_Order_By>
  var_samp?: Maybe<Geography_Columns_Var_Samp_Order_By>
  variance?: Maybe<Geography_Columns_Variance_Order_By>
}

/** aggregate avg on columns */
export type Geography_Columns_Avg_Fields = {
  __typename?: 'geography_columns_avg_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "geography_columns" */
export type Geography_Columns_Avg_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "geography_columns". All fields are combined with a logical 'AND'. */
export type Geography_Columns_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Geography_Columns_Bool_Exp>>>
  _not?: Maybe<Geography_Columns_Bool_Exp>
  _or?: Maybe<Array<Maybe<Geography_Columns_Bool_Exp>>>
  coord_dimension?: Maybe<Int_Comparison_Exp>
  f_geography_column?: Maybe<Name_Comparison_Exp>
  f_table_catalog?: Maybe<Name_Comparison_Exp>
  f_table_name?: Maybe<Name_Comparison_Exp>
  f_table_schema?: Maybe<Name_Comparison_Exp>
  srid?: Maybe<Int_Comparison_Exp>
  type?: Maybe<String_Comparison_Exp>
}

/** aggregate max on columns */
export type Geography_Columns_Max_Fields = {
  __typename?: 'geography_columns_max_fields'
  coord_dimension?: Maybe<Scalars['Int']>
  srid?: Maybe<Scalars['Int']>
  type?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "geography_columns" */
export type Geography_Columns_Max_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
  type?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Geography_Columns_Min_Fields = {
  __typename?: 'geography_columns_min_fields'
  coord_dimension?: Maybe<Scalars['Int']>
  srid?: Maybe<Scalars['Int']>
  type?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "geography_columns" */
export type Geography_Columns_Min_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
  type?: Maybe<Order_By>
}

/** ordering options when selecting data from "geography_columns" */
export type Geography_Columns_Order_By = {
  coord_dimension?: Maybe<Order_By>
  f_geography_column?: Maybe<Order_By>
  f_table_catalog?: Maybe<Order_By>
  f_table_name?: Maybe<Order_By>
  f_table_schema?: Maybe<Order_By>
  srid?: Maybe<Order_By>
  type?: Maybe<Order_By>
}

/** select columns of table "geography_columns" */
export enum Geography_Columns_Select_Column {
  /** column name */
  CoordDimension = 'coord_dimension',
  /** column name */
  FGeographyColumn = 'f_geography_column',
  /** column name */
  FTableCatalog = 'f_table_catalog',
  /** column name */
  FTableName = 'f_table_name',
  /** column name */
  FTableSchema = 'f_table_schema',
  /** column name */
  Srid = 'srid',
  /** column name */
  Type = 'type'
}

/** aggregate stddev on columns */
export type Geography_Columns_Stddev_Fields = {
  __typename?: 'geography_columns_stddev_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "geography_columns" */
export type Geography_Columns_Stddev_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Geography_Columns_Stddev_Pop_Fields = {
  __typename?: 'geography_columns_stddev_pop_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "geography_columns" */
export type Geography_Columns_Stddev_Pop_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Geography_Columns_Stddev_Samp_Fields = {
  __typename?: 'geography_columns_stddev_samp_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "geography_columns" */
export type Geography_Columns_Stddev_Samp_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Geography_Columns_Sum_Fields = {
  __typename?: 'geography_columns_sum_fields'
  coord_dimension?: Maybe<Scalars['Int']>
  srid?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "geography_columns" */
export type Geography_Columns_Sum_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate var_pop on columns */
export type Geography_Columns_Var_Pop_Fields = {
  __typename?: 'geography_columns_var_pop_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "geography_columns" */
export type Geography_Columns_Var_Pop_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Geography_Columns_Var_Samp_Fields = {
  __typename?: 'geography_columns_var_samp_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "geography_columns" */
export type Geography_Columns_Var_Samp_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Geography_Columns_Variance_Fields = {
  __typename?: 'geography_columns_variance_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "geography_columns" */
export type Geography_Columns_Variance_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** expression to compare columns of type geography. All fields are combined with logical 'AND'. */
export type Geography_Comparison_Exp = {
  _cast?: Maybe<Geography_Cast_Exp>
  _eq?: Maybe<Scalars['geography']>
  _gt?: Maybe<Scalars['geography']>
  _gte?: Maybe<Scalars['geography']>
  _in?: Maybe<Array<Scalars['geography']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['geography']>
  _lte?: Maybe<Scalars['geography']>
  _neq?: Maybe<Scalars['geography']>
  _nin?: Maybe<Array<Scalars['geography']>>
  /** is the column within a distance from a geography value */
  _st_d_within?: Maybe<St_D_Within_Geography_Input>
  /** does the column spatially intersect the given geography value */
  _st_intersects?: Maybe<Scalars['geography']>
}

/**
 * Expression to compare the result of casting a column of type geometry. Multiple
 * cast targets are combined with logical 'AND'.
 */
export type Geometry_Cast_Exp = {
  geography?: Maybe<Geography_Comparison_Exp>
}

/** columns and relationships of "geometry_columns" */
export type Geometry_Columns = {
  __typename?: 'geometry_columns'
  coord_dimension?: Maybe<Scalars['Int']>
  f_geometry_column?: Maybe<Scalars['name']>
  f_table_catalog?: Maybe<Scalars['String']>
  f_table_name?: Maybe<Scalars['name']>
  f_table_schema?: Maybe<Scalars['name']>
  srid?: Maybe<Scalars['Int']>
  type?: Maybe<Scalars['String']>
}

/** aggregated selection of "geometry_columns" */
export type Geometry_Columns_Aggregate = {
  __typename?: 'geometry_columns_aggregate'
  aggregate?: Maybe<Geometry_Columns_Aggregate_Fields>
  nodes: Array<Geometry_Columns>
}

/** aggregate fields of "geometry_columns" */
export type Geometry_Columns_Aggregate_Fields = {
  __typename?: 'geometry_columns_aggregate_fields'
  avg?: Maybe<Geometry_Columns_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Geometry_Columns_Max_Fields>
  min?: Maybe<Geometry_Columns_Min_Fields>
  stddev?: Maybe<Geometry_Columns_Stddev_Fields>
  stddev_pop?: Maybe<Geometry_Columns_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Geometry_Columns_Stddev_Samp_Fields>
  sum?: Maybe<Geometry_Columns_Sum_Fields>
  var_pop?: Maybe<Geometry_Columns_Var_Pop_Fields>
  var_samp?: Maybe<Geometry_Columns_Var_Samp_Fields>
  variance?: Maybe<Geometry_Columns_Variance_Fields>
}

/** aggregate fields of "geometry_columns" */
export type Geometry_Columns_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Geometry_Columns_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "geometry_columns" */
export type Geometry_Columns_Aggregate_Order_By = {
  avg?: Maybe<Geometry_Columns_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Geometry_Columns_Max_Order_By>
  min?: Maybe<Geometry_Columns_Min_Order_By>
  stddev?: Maybe<Geometry_Columns_Stddev_Order_By>
  stddev_pop?: Maybe<Geometry_Columns_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Geometry_Columns_Stddev_Samp_Order_By>
  sum?: Maybe<Geometry_Columns_Sum_Order_By>
  var_pop?: Maybe<Geometry_Columns_Var_Pop_Order_By>
  var_samp?: Maybe<Geometry_Columns_Var_Samp_Order_By>
  variance?: Maybe<Geometry_Columns_Variance_Order_By>
}

/** input type for inserting array relation for remote table "geometry_columns" */
export type Geometry_Columns_Arr_Rel_Insert_Input = {
  data: Array<Geometry_Columns_Insert_Input>
}

/** aggregate avg on columns */
export type Geometry_Columns_Avg_Fields = {
  __typename?: 'geometry_columns_avg_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "geometry_columns" */
export type Geometry_Columns_Avg_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "geometry_columns". All fields are combined with a logical 'AND'. */
export type Geometry_Columns_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Geometry_Columns_Bool_Exp>>>
  _not?: Maybe<Geometry_Columns_Bool_Exp>
  _or?: Maybe<Array<Maybe<Geometry_Columns_Bool_Exp>>>
  coord_dimension?: Maybe<Int_Comparison_Exp>
  f_geometry_column?: Maybe<Name_Comparison_Exp>
  f_table_catalog?: Maybe<String_Comparison_Exp>
  f_table_name?: Maybe<Name_Comparison_Exp>
  f_table_schema?: Maybe<Name_Comparison_Exp>
  srid?: Maybe<Int_Comparison_Exp>
  type?: Maybe<String_Comparison_Exp>
}

/** input type for incrementing integer column in table "geometry_columns" */
export type Geometry_Columns_Inc_Input = {
  coord_dimension?: Maybe<Scalars['Int']>
  srid?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "geometry_columns" */
export type Geometry_Columns_Insert_Input = {
  coord_dimension?: Maybe<Scalars['Int']>
  f_geometry_column?: Maybe<Scalars['name']>
  f_table_catalog?: Maybe<Scalars['String']>
  f_table_name?: Maybe<Scalars['name']>
  f_table_schema?: Maybe<Scalars['name']>
  srid?: Maybe<Scalars['Int']>
  type?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Geometry_Columns_Max_Fields = {
  __typename?: 'geometry_columns_max_fields'
  coord_dimension?: Maybe<Scalars['Int']>
  f_table_catalog?: Maybe<Scalars['String']>
  srid?: Maybe<Scalars['Int']>
  type?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "geometry_columns" */
export type Geometry_Columns_Max_Order_By = {
  coord_dimension?: Maybe<Order_By>
  f_table_catalog?: Maybe<Order_By>
  srid?: Maybe<Order_By>
  type?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Geometry_Columns_Min_Fields = {
  __typename?: 'geometry_columns_min_fields'
  coord_dimension?: Maybe<Scalars['Int']>
  f_table_catalog?: Maybe<Scalars['String']>
  srid?: Maybe<Scalars['Int']>
  type?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "geometry_columns" */
export type Geometry_Columns_Min_Order_By = {
  coord_dimension?: Maybe<Order_By>
  f_table_catalog?: Maybe<Order_By>
  srid?: Maybe<Order_By>
  type?: Maybe<Order_By>
}

/** response of any mutation on the table "geometry_columns" */
export type Geometry_Columns_Mutation_Response = {
  __typename?: 'geometry_columns_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Geometry_Columns>
}

/** input type for inserting object relation for remote table "geometry_columns" */
export type Geometry_Columns_Obj_Rel_Insert_Input = {
  data: Geometry_Columns_Insert_Input
}

/** ordering options when selecting data from "geometry_columns" */
export type Geometry_Columns_Order_By = {
  coord_dimension?: Maybe<Order_By>
  f_geometry_column?: Maybe<Order_By>
  f_table_catalog?: Maybe<Order_By>
  f_table_name?: Maybe<Order_By>
  f_table_schema?: Maybe<Order_By>
  srid?: Maybe<Order_By>
  type?: Maybe<Order_By>
}

/** select columns of table "geometry_columns" */
export enum Geometry_Columns_Select_Column {
  /** column name */
  CoordDimension = 'coord_dimension',
  /** column name */
  FGeometryColumn = 'f_geometry_column',
  /** column name */
  FTableCatalog = 'f_table_catalog',
  /** column name */
  FTableName = 'f_table_name',
  /** column name */
  FTableSchema = 'f_table_schema',
  /** column name */
  Srid = 'srid',
  /** column name */
  Type = 'type'
}

/** input type for updating data in table "geometry_columns" */
export type Geometry_Columns_Set_Input = {
  coord_dimension?: Maybe<Scalars['Int']>
  f_geometry_column?: Maybe<Scalars['name']>
  f_table_catalog?: Maybe<Scalars['String']>
  f_table_name?: Maybe<Scalars['name']>
  f_table_schema?: Maybe<Scalars['name']>
  srid?: Maybe<Scalars['Int']>
  type?: Maybe<Scalars['String']>
}

/** aggregate stddev on columns */
export type Geometry_Columns_Stddev_Fields = {
  __typename?: 'geometry_columns_stddev_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "geometry_columns" */
export type Geometry_Columns_Stddev_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Geometry_Columns_Stddev_Pop_Fields = {
  __typename?: 'geometry_columns_stddev_pop_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "geometry_columns" */
export type Geometry_Columns_Stddev_Pop_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Geometry_Columns_Stddev_Samp_Fields = {
  __typename?: 'geometry_columns_stddev_samp_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "geometry_columns" */
export type Geometry_Columns_Stddev_Samp_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Geometry_Columns_Sum_Fields = {
  __typename?: 'geometry_columns_sum_fields'
  coord_dimension?: Maybe<Scalars['Int']>
  srid?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "geometry_columns" */
export type Geometry_Columns_Sum_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate var_pop on columns */
export type Geometry_Columns_Var_Pop_Fields = {
  __typename?: 'geometry_columns_var_pop_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "geometry_columns" */
export type Geometry_Columns_Var_Pop_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Geometry_Columns_Var_Samp_Fields = {
  __typename?: 'geometry_columns_var_samp_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "geometry_columns" */
export type Geometry_Columns_Var_Samp_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Geometry_Columns_Variance_Fields = {
  __typename?: 'geometry_columns_variance_fields'
  coord_dimension?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "geometry_columns" */
export type Geometry_Columns_Variance_Order_By = {
  coord_dimension?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** expression to compare columns of type geometry. All fields are combined with logical 'AND'. */
export type Geometry_Comparison_Exp = {
  _cast?: Maybe<Geometry_Cast_Exp>
  _eq?: Maybe<Scalars['geometry']>
  _gt?: Maybe<Scalars['geometry']>
  _gte?: Maybe<Scalars['geometry']>
  _in?: Maybe<Array<Scalars['geometry']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['geometry']>
  _lte?: Maybe<Scalars['geometry']>
  _neq?: Maybe<Scalars['geometry']>
  _nin?: Maybe<Array<Scalars['geometry']>>
  /** does the column contain the given geometry value */
  _st_contains?: Maybe<Scalars['geometry']>
  /** does the column crosses the given geometry value */
  _st_crosses?: Maybe<Scalars['geometry']>
  /** is the column within a distance from a geometry value */
  _st_d_within?: Maybe<St_D_Within_Input>
  /** is the column equal to given geometry value. Directionality is ignored */
  _st_equals?: Maybe<Scalars['geometry']>
  /** does the column spatially intersect the given geometry value */
  _st_intersects?: Maybe<Scalars['geometry']>
  /** does the column 'spatially overlap' (intersect but not completely contain) the given geometry value */
  _st_overlaps?: Maybe<Scalars['geometry']>
  /** does the column have atleast one point in common with the given geometry value */
  _st_touches?: Maybe<Scalars['geometry']>
  /** is the column contained in the given geometry value */
  _st_within?: Maybe<Scalars['geometry']>
}

export type GeoOutput = {
  __typename?: 'GeoOutput'
  address_line1?: Maybe<Scalars['String']>
  address_line2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  country?: Maybe<Scalars['String']>
  country_code?: Maybe<Scalars['String']>
  county?: Maybe<Scalars['String']>
  display_name?: Maybe<Scalars['String']>
  geo?: Maybe<Scalars['geography']>
  name?: Maybe<Scalars['String']>
  postcode?: Maybe<Scalars['String']>
  region?: Maybe<Scalars['String']>
  result_type?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
}

/** expression to compare columns of type Int. All fields are combined with logical 'AND'. */
export type Int_Comparison_Exp = {
  _eq?: Maybe<Scalars['Int']>
  _gt?: Maybe<Scalars['Int']>
  _gte?: Maybe<Scalars['Int']>
  _in?: Maybe<Array<Scalars['Int']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['Int']>
  _lte?: Maybe<Scalars['Int']>
  _neq?: Maybe<Scalars['Int']>
  _nin?: Maybe<Array<Scalars['Int']>>
}

/** expression to compare columns of type jsonb. All fields are combined with logical 'AND'. */
export type Jsonb_Comparison_Exp = {
  /** is the column contained in the given json value */
  _contained_in?: Maybe<Scalars['jsonb']>
  /** does the column contain the given json value at the top level */
  _contains?: Maybe<Scalars['jsonb']>
  _eq?: Maybe<Scalars['jsonb']>
  _gt?: Maybe<Scalars['jsonb']>
  _gte?: Maybe<Scalars['jsonb']>
  /** does the string exist as a top-level key in the column */
  _has_key?: Maybe<Scalars['String']>
  /** do all of these strings exist as top-level keys in the column */
  _has_keys_all?: Maybe<Array<Scalars['String']>>
  /** do any of these strings exist as top-level keys in the column */
  _has_keys_any?: Maybe<Array<Scalars['String']>>
  _in?: Maybe<Array<Scalars['jsonb']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['jsonb']>
  _lte?: Maybe<Scalars['jsonb']>
  _neq?: Maybe<Scalars['jsonb']>
  _nin?: Maybe<Array<Scalars['jsonb']>>
}

/** columns and relationships of "locales" */
export type Locales = {
  __typename?: 'locales'
  /** An array relationship */
  localizations: Array<Localizations>
  /** An aggregated array relationship */
  localizations_aggregate: Localizations_Aggregate
  name: Scalars['String']
  /** An array relationship */
  pages_contents: Array<Pages_Contents>
  /** An aggregated array relationship */
  pages_contents_aggregate: Pages_Contents_Aggregate
  /** An array relationship */
  system_messages: Array<System_Messages>
  /** An aggregated array relationship */
  system_messages_aggregate: System_Messages_Aggregate
  /** An array relationship */
  users: Array<Users>
  /** An aggregated array relationship */
  users_aggregate: Users_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "locales" */
export type LocalesLocalizationsArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** columns and relationships of "locales" */
export type LocalesLocalizations_AggregateArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** columns and relationships of "locales" */
export type LocalesPages_ContentsArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Order_By>>
  where?: Maybe<Pages_Contents_Bool_Exp>
}

/** columns and relationships of "locales" */
export type LocalesPages_Contents_AggregateArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Order_By>>
  where?: Maybe<Pages_Contents_Bool_Exp>
}

/** columns and relationships of "locales" */
export type LocalesSystem_MessagesArgs = {
  distinct_on?: Maybe<Array<System_Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<System_Messages_Order_By>>
  where?: Maybe<System_Messages_Bool_Exp>
}

/** columns and relationships of "locales" */
export type LocalesSystem_Messages_AggregateArgs = {
  distinct_on?: Maybe<Array<System_Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<System_Messages_Order_By>>
  where?: Maybe<System_Messages_Bool_Exp>
}

/** columns and relationships of "locales" */
export type LocalesUsersArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** columns and relationships of "locales" */
export type LocalesUsers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** aggregated selection of "locales" */
export type Locales_Aggregate = {
  __typename?: 'locales_aggregate'
  aggregate?: Maybe<Locales_Aggregate_Fields>
  nodes: Array<Locales>
}

/** aggregate fields of "locales" */
export type Locales_Aggregate_Fields = {
  __typename?: 'locales_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Locales_Max_Fields>
  min?: Maybe<Locales_Min_Fields>
}

/** aggregate fields of "locales" */
export type Locales_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Locales_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "locales" */
export type Locales_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Locales_Max_Order_By>
  min?: Maybe<Locales_Min_Order_By>
}

/** input type for inserting array relation for remote table "locales" */
export type Locales_Arr_Rel_Insert_Input = {
  data: Array<Locales_Insert_Input>
  on_conflict?: Maybe<Locales_On_Conflict>
}

/** Boolean expression to filter rows from the table "locales". All fields are combined with a logical 'AND'. */
export type Locales_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Locales_Bool_Exp>>>
  _not?: Maybe<Locales_Bool_Exp>
  _or?: Maybe<Array<Maybe<Locales_Bool_Exp>>>
  localizations?: Maybe<Localizations_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
  pages_contents?: Maybe<Pages_Contents_Bool_Exp>
  system_messages?: Maybe<System_Messages_Bool_Exp>
  users?: Maybe<Users_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "locales" */
export enum Locales_Constraint {
  /** unique or primary key constraint */
  LocalesPkey = 'locales_pkey'
}

export enum Locales_Enum {
  /** Deutsche */
  De = 'DE',
  /** English */
  En = 'EN',
  /** Indonesian */
  Id = 'ID',
  /** Русский */
  Ru = 'RU'
}

/** expression to compare columns of type locales_enum. All fields are combined with logical 'AND'. */
export type Locales_Enum_Comparison_Exp = {
  _eq?: Maybe<Locales_Enum>
  _in?: Maybe<Array<Locales_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Locales_Enum>
  _nin?: Maybe<Array<Locales_Enum>>
}

/** input type for inserting data into table "locales" */
export type Locales_Insert_Input = {
  localizations?: Maybe<Localizations_Arr_Rel_Insert_Input>
  name?: Maybe<Scalars['String']>
  pages_contents?: Maybe<Pages_Contents_Arr_Rel_Insert_Input>
  system_messages?: Maybe<System_Messages_Arr_Rel_Insert_Input>
  users?: Maybe<Users_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Locales_Max_Fields = {
  __typename?: 'locales_max_fields'
  name?: Maybe<Scalars['String']>
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "locales" */
export type Locales_Max_Order_By = {
  name?: Maybe<Order_By>
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Locales_Min_Fields = {
  __typename?: 'locales_min_fields'
  name?: Maybe<Scalars['String']>
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "locales" */
export type Locales_Min_Order_By = {
  name?: Maybe<Order_By>
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "locales" */
export type Locales_Mutation_Response = {
  __typename?: 'locales_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Locales>
}

/** input type for inserting object relation for remote table "locales" */
export type Locales_Obj_Rel_Insert_Input = {
  data: Locales_Insert_Input
  on_conflict?: Maybe<Locales_On_Conflict>
}

/** on conflict condition type for table "locales" */
export type Locales_On_Conflict = {
  constraint: Locales_Constraint
  update_columns: Array<Locales_Update_Column>
  where?: Maybe<Locales_Bool_Exp>
}

/** ordering options when selecting data from "locales" */
export type Locales_Order_By = {
  localizations_aggregate?: Maybe<Localizations_Aggregate_Order_By>
  name?: Maybe<Order_By>
  pages_contents_aggregate?: Maybe<Pages_Contents_Aggregate_Order_By>
  system_messages_aggregate?: Maybe<System_Messages_Aggregate_Order_By>
  users_aggregate?: Maybe<Users_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "locales" */
export type Locales_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "locales" */
export enum Locales_Select_Column {
  /** column name */
  Name = 'name',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "locales" */
export type Locales_Set_Input = {
  name?: Maybe<Scalars['String']>
  value?: Maybe<Scalars['String']>
}

/** update columns of table "locales" */
export enum Locales_Update_Column {
  /** column name */
  Name = 'name',
  /** column name */
  Value = 'value'
}

/** columns and relationships of "localizations" */
export type Localizations = {
  __typename?: 'localizations'
  data?: Maybe<Scalars['jsonb']>
  id: Scalars['uuid']
  locale: Locales_Enum
  /** An object relationship */
  localeByLocale: Locales
  /** An object relationship */
  maker?: Maybe<Makers>
  maker_id?: Maybe<Scalars['uuid']>
  menu_id?: Maybe<Scalars['uuid']>
  /** An object relationship */
  menu_item?: Maybe<Menu_Items>
  /** An object relationship */
  style?: Maybe<Styles>
  style_id?: Maybe<Scalars['uuid']>
  value: Scalars['String']
}

/** columns and relationships of "localizations" */
export type LocalizationsDataArgs = {
  path?: Maybe<Scalars['String']>
}

/** aggregated selection of "localizations" */
export type Localizations_Aggregate = {
  __typename?: 'localizations_aggregate'
  aggregate?: Maybe<Localizations_Aggregate_Fields>
  nodes: Array<Localizations>
}

/** aggregate fields of "localizations" */
export type Localizations_Aggregate_Fields = {
  __typename?: 'localizations_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Localizations_Max_Fields>
  min?: Maybe<Localizations_Min_Fields>
}

/** aggregate fields of "localizations" */
export type Localizations_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Localizations_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "localizations" */
export type Localizations_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Localizations_Max_Order_By>
  min?: Maybe<Localizations_Min_Order_By>
}

/** append existing jsonb value of filtered columns with new jsonb value */
export type Localizations_Append_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** input type for inserting array relation for remote table "localizations" */
export type Localizations_Arr_Rel_Insert_Input = {
  data: Array<Localizations_Insert_Input>
  on_conflict?: Maybe<Localizations_On_Conflict>
}

/** Boolean expression to filter rows from the table "localizations". All fields are combined with a logical 'AND'. */
export type Localizations_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Localizations_Bool_Exp>>>
  _not?: Maybe<Localizations_Bool_Exp>
  _or?: Maybe<Array<Maybe<Localizations_Bool_Exp>>>
  data?: Maybe<Jsonb_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  locale?: Maybe<Locales_Enum_Comparison_Exp>
  localeByLocale?: Maybe<Locales_Bool_Exp>
  maker?: Maybe<Makers_Bool_Exp>
  maker_id?: Maybe<Uuid_Comparison_Exp>
  menu_id?: Maybe<Uuid_Comparison_Exp>
  menu_item?: Maybe<Menu_Items_Bool_Exp>
  style?: Maybe<Styles_Bool_Exp>
  style_id?: Maybe<Uuid_Comparison_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "localizations" */
export enum Localizations_Constraint {
  /** unique or primary key constraint */
  LocalizationsItemIdLocaleKey = 'localizations_item_id_locale_key',
  /** unique or primary key constraint */
  LocalizationsMakerIdLocaleKey = 'localizations_maker_id_locale_key',
  /** unique or primary key constraint */
  LocalizationsMenuIdLocaleKey = 'localizations_menu_id_locale_key',
  /** unique or primary key constraint */
  LocalizationsPkey = 'localizations_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Localizations_Delete_At_Path_Input = {
  data?: Maybe<Array<Maybe<Scalars['String']>>>
}

/**
 * delete the array element with specified index (negative integers count from the
 * end). throws an error if top level container is not an array
 */
export type Localizations_Delete_Elem_Input = {
  data?: Maybe<Scalars['Int']>
}

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Localizations_Delete_Key_Input = {
  data?: Maybe<Scalars['String']>
}

/** input type for inserting data into table "localizations" */
export type Localizations_Insert_Input = {
  data?: Maybe<Scalars['jsonb']>
  id?: Maybe<Scalars['uuid']>
  locale?: Maybe<Locales_Enum>
  localeByLocale?: Maybe<Locales_Obj_Rel_Insert_Input>
  maker?: Maybe<Makers_Obj_Rel_Insert_Input>
  maker_id?: Maybe<Scalars['uuid']>
  menu_id?: Maybe<Scalars['uuid']>
  menu_item?: Maybe<Menu_Items_Obj_Rel_Insert_Input>
  style?: Maybe<Styles_Obj_Rel_Insert_Input>
  style_id?: Maybe<Scalars['uuid']>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Localizations_Max_Fields = {
  __typename?: 'localizations_max_fields'
  id?: Maybe<Scalars['uuid']>
  maker_id?: Maybe<Scalars['uuid']>
  menu_id?: Maybe<Scalars['uuid']>
  style_id?: Maybe<Scalars['uuid']>
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "localizations" */
export type Localizations_Max_Order_By = {
  id?: Maybe<Order_By>
  maker_id?: Maybe<Order_By>
  menu_id?: Maybe<Order_By>
  style_id?: Maybe<Order_By>
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Localizations_Min_Fields = {
  __typename?: 'localizations_min_fields'
  id?: Maybe<Scalars['uuid']>
  maker_id?: Maybe<Scalars['uuid']>
  menu_id?: Maybe<Scalars['uuid']>
  style_id?: Maybe<Scalars['uuid']>
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "localizations" */
export type Localizations_Min_Order_By = {
  id?: Maybe<Order_By>
  maker_id?: Maybe<Order_By>
  menu_id?: Maybe<Order_By>
  style_id?: Maybe<Order_By>
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "localizations" */
export type Localizations_Mutation_Response = {
  __typename?: 'localizations_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Localizations>
}

/** input type for inserting object relation for remote table "localizations" */
export type Localizations_Obj_Rel_Insert_Input = {
  data: Localizations_Insert_Input
  on_conflict?: Maybe<Localizations_On_Conflict>
}

/** on conflict condition type for table "localizations" */
export type Localizations_On_Conflict = {
  constraint: Localizations_Constraint
  update_columns: Array<Localizations_Update_Column>
  where?: Maybe<Localizations_Bool_Exp>
}

/** ordering options when selecting data from "localizations" */
export type Localizations_Order_By = {
  data?: Maybe<Order_By>
  id?: Maybe<Order_By>
  locale?: Maybe<Order_By>
  localeByLocale?: Maybe<Locales_Order_By>
  maker?: Maybe<Makers_Order_By>
  maker_id?: Maybe<Order_By>
  menu_id?: Maybe<Order_By>
  menu_item?: Maybe<Menu_Items_Order_By>
  style?: Maybe<Styles_Order_By>
  style_id?: Maybe<Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "localizations" */
export type Localizations_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Localizations_Prepend_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** select columns of table "localizations" */
export enum Localizations_Select_Column {
  /** column name */
  Data = 'data',
  /** column name */
  Id = 'id',
  /** column name */
  Locale = 'locale',
  /** column name */
  MakerId = 'maker_id',
  /** column name */
  MenuId = 'menu_id',
  /** column name */
  StyleId = 'style_id',
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "localizations" */
export type Localizations_Set_Input = {
  data?: Maybe<Scalars['jsonb']>
  id?: Maybe<Scalars['uuid']>
  locale?: Maybe<Locales_Enum>
  maker_id?: Maybe<Scalars['uuid']>
  menu_id?: Maybe<Scalars['uuid']>
  style_id?: Maybe<Scalars['uuid']>
  value?: Maybe<Scalars['String']>
}

/** update columns of table "localizations" */
export enum Localizations_Update_Column {
  /** column name */
  Data = 'data',
  /** column name */
  Id = 'id',
  /** column name */
  Locale = 'locale',
  /** column name */
  MakerId = 'maker_id',
  /** column name */
  MenuId = 'menu_id',
  /** column name */
  StyleId = 'style_id',
  /** column name */
  Value = 'value'
}

/** columns and relationships of "locations" */
export type Locations = {
  __typename?: 'locations'
  address_line1?: Maybe<Scalars['String']>
  address_line2?: Maybe<Scalars['String']>
  /** An array relationship */
  albums: Array<Albums>
  /** An aggregated array relationship */
  albums_aggregate: Albums_Aggregate
  city?: Maybe<Scalars['String']>
  country?: Maybe<Scalars['String']>
  country_code?: Maybe<Scalars['String']>
  county?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  display_name?: Maybe<Scalars['String']>
  /** An object relationship */
  event?: Maybe<Events>
  event_id?: Maybe<Scalars['uuid']>
  /** An array relationship */
  events: Array<Events>
  /** An aggregated array relationship */
  events_aggregate: Events_Aggregate
  geo?: Maybe<Scalars['geography']>
  id: Scalars['uuid']
  /** An array relationship */
  messages_events_updateds_new: Array<Messages_Events_Updated>
  /** An aggregated array relationship */
  messages_events_updateds_new_aggregate: Messages_Events_Updated_Aggregate
  /** An array relationship */
  messages_events_updateds_old: Array<Messages_Events_Updated>
  /** An aggregated array relationship */
  messages_events_updateds_old_aggregate: Messages_Events_Updated_Aggregate
  name: Scalars['String']
  postcode?: Maybe<Scalars['String']>
  region?: Maybe<Scalars['String']>
  result_type?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  /** An array relationship */
  stores: Array<Store>
  /** An aggregated array relationship */
  stores_aggregate: Store_Aggregate
  updated_at?: Maybe<Scalars['timestamptz']>
  /** An object relationship */
  user?: Maybe<Users>
  user_id?: Maybe<Scalars['uuid']>
  /** An array relationship */
  users: Array<Users>
  /** An aggregated array relationship */
  users_aggregate: Users_Aggregate
}

/** columns and relationships of "locations" */
export type LocationsAlbumsArgs = {
  distinct_on?: Maybe<Array<Albums_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Order_By>>
  where?: Maybe<Albums_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsAlbums_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Order_By>>
  where?: Maybe<Albums_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsEventsArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsEvents_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsMessages_Events_Updateds_NewArgs = {
  distinct_on?: Maybe<Array<Messages_Events_Updated_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Events_Updated_Order_By>>
  where?: Maybe<Messages_Events_Updated_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsMessages_Events_Updateds_New_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Events_Updated_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Events_Updated_Order_By>>
  where?: Maybe<Messages_Events_Updated_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsMessages_Events_Updateds_OldArgs = {
  distinct_on?: Maybe<Array<Messages_Events_Updated_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Events_Updated_Order_By>>
  where?: Maybe<Messages_Events_Updated_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsMessages_Events_Updateds_Old_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Events_Updated_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Events_Updated_Order_By>>
  where?: Maybe<Messages_Events_Updated_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsStoresArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsStores_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsUsersArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** columns and relationships of "locations" */
export type LocationsUsers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** aggregated selection of "locations" */
export type Locations_Aggregate = {
  __typename?: 'locations_aggregate'
  aggregate?: Maybe<Locations_Aggregate_Fields>
  nodes: Array<Locations>
}

/** aggregate fields of "locations" */
export type Locations_Aggregate_Fields = {
  __typename?: 'locations_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Locations_Max_Fields>
  min?: Maybe<Locations_Min_Fields>
}

/** aggregate fields of "locations" */
export type Locations_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Locations_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "locations" */
export type Locations_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Locations_Max_Order_By>
  min?: Maybe<Locations_Min_Order_By>
}

/** input type for inserting array relation for remote table "locations" */
export type Locations_Arr_Rel_Insert_Input = {
  data: Array<Locations_Insert_Input>
  on_conflict?: Maybe<Locations_On_Conflict>
}

/** Boolean expression to filter rows from the table "locations". All fields are combined with a logical 'AND'. */
export type Locations_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Locations_Bool_Exp>>>
  _not?: Maybe<Locations_Bool_Exp>
  _or?: Maybe<Array<Maybe<Locations_Bool_Exp>>>
  address_line1?: Maybe<String_Comparison_Exp>
  address_line2?: Maybe<String_Comparison_Exp>
  albums?: Maybe<Albums_Bool_Exp>
  city?: Maybe<String_Comparison_Exp>
  country?: Maybe<String_Comparison_Exp>
  country_code?: Maybe<String_Comparison_Exp>
  county?: Maybe<String_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  display_name?: Maybe<String_Comparison_Exp>
  event?: Maybe<Events_Bool_Exp>
  event_id?: Maybe<Uuid_Comparison_Exp>
  events?: Maybe<Events_Bool_Exp>
  geo?: Maybe<Geography_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  messages_events_updateds_new?: Maybe<Messages_Events_Updated_Bool_Exp>
  messages_events_updateds_old?: Maybe<Messages_Events_Updated_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
  postcode?: Maybe<String_Comparison_Exp>
  region?: Maybe<String_Comparison_Exp>
  result_type?: Maybe<String_Comparison_Exp>
  state?: Maybe<String_Comparison_Exp>
  stores?: Maybe<Store_Bool_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
  users?: Maybe<Users_Bool_Exp>
}

/** unique or primary key constraints on table "locations" */
export enum Locations_Constraint {
  /** unique or primary key constraint */
  LocationPkey = 'location_pkey'
}

/** input type for inserting data into table "locations" */
export type Locations_Insert_Input = {
  address_line1?: Maybe<Scalars['String']>
  address_line2?: Maybe<Scalars['String']>
  albums?: Maybe<Albums_Arr_Rel_Insert_Input>
  city?: Maybe<Scalars['String']>
  country?: Maybe<Scalars['String']>
  country_code?: Maybe<Scalars['String']>
  county?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  display_name?: Maybe<Scalars['String']>
  event?: Maybe<Events_Obj_Rel_Insert_Input>
  event_id?: Maybe<Scalars['uuid']>
  events?: Maybe<Events_Arr_Rel_Insert_Input>
  geo?: Maybe<Scalars['geography']>
  id?: Maybe<Scalars['uuid']>
  messages_events_updateds_new?: Maybe<
    Messages_Events_Updated_Arr_Rel_Insert_Input
  >
  messages_events_updateds_old?: Maybe<
    Messages_Events_Updated_Arr_Rel_Insert_Input
  >
  name?: Maybe<Scalars['String']>
  postcode?: Maybe<Scalars['String']>
  region?: Maybe<Scalars['String']>
  result_type?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  stores?: Maybe<Store_Arr_Rel_Insert_Input>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
  users?: Maybe<Users_Arr_Rel_Insert_Input>
}

/** aggregate max on columns */
export type Locations_Max_Fields = {
  __typename?: 'locations_max_fields'
  address_line1?: Maybe<Scalars['String']>
  address_line2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  country?: Maybe<Scalars['String']>
  country_code?: Maybe<Scalars['String']>
  county?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  display_name?: Maybe<Scalars['String']>
  event_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  postcode?: Maybe<Scalars['String']>
  region?: Maybe<Scalars['String']>
  result_type?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "locations" */
export type Locations_Max_Order_By = {
  address_line1?: Maybe<Order_By>
  address_line2?: Maybe<Order_By>
  city?: Maybe<Order_By>
  country?: Maybe<Order_By>
  country_code?: Maybe<Order_By>
  county?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  display_name?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  postcode?: Maybe<Order_By>
  region?: Maybe<Order_By>
  result_type?: Maybe<Order_By>
  state?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Locations_Min_Fields = {
  __typename?: 'locations_min_fields'
  address_line1?: Maybe<Scalars['String']>
  address_line2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  country?: Maybe<Scalars['String']>
  country_code?: Maybe<Scalars['String']>
  county?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  display_name?: Maybe<Scalars['String']>
  event_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  postcode?: Maybe<Scalars['String']>
  region?: Maybe<Scalars['String']>
  result_type?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "locations" */
export type Locations_Min_Order_By = {
  address_line1?: Maybe<Order_By>
  address_line2?: Maybe<Order_By>
  city?: Maybe<Order_By>
  country?: Maybe<Order_By>
  country_code?: Maybe<Order_By>
  county?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  display_name?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  postcode?: Maybe<Order_By>
  region?: Maybe<Order_By>
  result_type?: Maybe<Order_By>
  state?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "locations" */
export type Locations_Mutation_Response = {
  __typename?: 'locations_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Locations>
}

/** input type for inserting object relation for remote table "locations" */
export type Locations_Obj_Rel_Insert_Input = {
  data: Locations_Insert_Input
  on_conflict?: Maybe<Locations_On_Conflict>
}

/** on conflict condition type for table "locations" */
export type Locations_On_Conflict = {
  constraint: Locations_Constraint
  update_columns: Array<Locations_Update_Column>
  where?: Maybe<Locations_Bool_Exp>
}

/** ordering options when selecting data from "locations" */
export type Locations_Order_By = {
  address_line1?: Maybe<Order_By>
  address_line2?: Maybe<Order_By>
  albums_aggregate?: Maybe<Albums_Aggregate_Order_By>
  city?: Maybe<Order_By>
  country?: Maybe<Order_By>
  country_code?: Maybe<Order_By>
  county?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  display_name?: Maybe<Order_By>
  event?: Maybe<Events_Order_By>
  event_id?: Maybe<Order_By>
  events_aggregate?: Maybe<Events_Aggregate_Order_By>
  geo?: Maybe<Order_By>
  id?: Maybe<Order_By>
  messages_events_updateds_new_aggregate?: Maybe<
    Messages_Events_Updated_Aggregate_Order_By
  >
  messages_events_updateds_old_aggregate?: Maybe<
    Messages_Events_Updated_Aggregate_Order_By
  >
  name?: Maybe<Order_By>
  postcode?: Maybe<Order_By>
  region?: Maybe<Order_By>
  result_type?: Maybe<Order_By>
  state?: Maybe<Order_By>
  stores_aggregate?: Maybe<Store_Aggregate_Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
  users_aggregate?: Maybe<Users_Aggregate_Order_By>
}

/** primary key columns input for table: "locations" */
export type Locations_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "locations" */
export enum Locations_Select_Column {
  /** column name */
  AddressLine1 = 'address_line1',
  /** column name */
  AddressLine2 = 'address_line2',
  /** column name */
  City = 'city',
  /** column name */
  Country = 'country',
  /** column name */
  CountryCode = 'country_code',
  /** column name */
  County = 'county',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DisplayName = 'display_name',
  /** column name */
  EventId = 'event_id',
  /** column name */
  Geo = 'geo',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Postcode = 'postcode',
  /** column name */
  Region = 'region',
  /** column name */
  ResultType = 'result_type',
  /** column name */
  State = 'state',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "locations" */
export type Locations_Set_Input = {
  address_line1?: Maybe<Scalars['String']>
  address_line2?: Maybe<Scalars['String']>
  city?: Maybe<Scalars['String']>
  country?: Maybe<Scalars['String']>
  country_code?: Maybe<Scalars['String']>
  county?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  display_name?: Maybe<Scalars['String']>
  event_id?: Maybe<Scalars['uuid']>
  geo?: Maybe<Scalars['geography']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  postcode?: Maybe<Scalars['String']>
  region?: Maybe<Scalars['String']>
  result_type?: Maybe<Scalars['String']>
  state?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "locations" */
export enum Locations_Update_Column {
  /** column name */
  AddressLine1 = 'address_line1',
  /** column name */
  AddressLine2 = 'address_line2',
  /** column name */
  City = 'city',
  /** column name */
  Country = 'country',
  /** column name */
  CountryCode = 'country_code',
  /** column name */
  County = 'county',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DisplayName = 'display_name',
  /** column name */
  EventId = 'event_id',
  /** column name */
  Geo = 'geo',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Postcode = 'postcode',
  /** column name */
  Region = 'region',
  /** column name */
  ResultType = 'result_type',
  /** column name */
  State = 'state',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** columns and relationships of "maker_types" */
export type Maker_Types = {
  __typename?: 'maker_types'
  /** An array relationship */
  users_maker_types: Array<Users_Maker_Types>
  /** An aggregated array relationship */
  users_maker_types_aggregate: Users_Maker_Types_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "maker_types" */
export type Maker_TypesUsers_Maker_TypesArgs = {
  distinct_on?: Maybe<Array<Users_Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Maker_Types_Order_By>>
  where?: Maybe<Users_Maker_Types_Bool_Exp>
}

/** columns and relationships of "maker_types" */
export type Maker_TypesUsers_Maker_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Maker_Types_Order_By>>
  where?: Maybe<Users_Maker_Types_Bool_Exp>
}

/** aggregated selection of "maker_types" */
export type Maker_Types_Aggregate = {
  __typename?: 'maker_types_aggregate'
  aggregate?: Maybe<Maker_Types_Aggregate_Fields>
  nodes: Array<Maker_Types>
}

/** aggregate fields of "maker_types" */
export type Maker_Types_Aggregate_Fields = {
  __typename?: 'maker_types_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Maker_Types_Max_Fields>
  min?: Maybe<Maker_Types_Min_Fields>
}

/** aggregate fields of "maker_types" */
export type Maker_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Maker_Types_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "maker_types" */
export type Maker_Types_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Maker_Types_Max_Order_By>
  min?: Maybe<Maker_Types_Min_Order_By>
}

/** input type for inserting array relation for remote table "maker_types" */
export type Maker_Types_Arr_Rel_Insert_Input = {
  data: Array<Maker_Types_Insert_Input>
  on_conflict?: Maybe<Maker_Types_On_Conflict>
}

/** Boolean expression to filter rows from the table "maker_types". All fields are combined with a logical 'AND'. */
export type Maker_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Maker_Types_Bool_Exp>>>
  _not?: Maybe<Maker_Types_Bool_Exp>
  _or?: Maybe<Array<Maybe<Maker_Types_Bool_Exp>>>
  users_maker_types?: Maybe<Users_Maker_Types_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "maker_types" */
export enum Maker_Types_Constraint {
  /** unique or primary key constraint */
  ProfessionalTypePkey = 'professional_type_pkey'
}

export enum Maker_Types_Enum {
  Artists = 'ARTISTS',
  Photographers = 'PHOTOGRAPHERS',
  Places = 'PLACES',
  Videographers = 'VIDEOGRAPHERS'
}

/** expression to compare columns of type maker_types_enum. All fields are combined with logical 'AND'. */
export type Maker_Types_Enum_Comparison_Exp = {
  _eq?: Maybe<Maker_Types_Enum>
  _in?: Maybe<Array<Maker_Types_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Maker_Types_Enum>
  _nin?: Maybe<Array<Maker_Types_Enum>>
}

/** input type for inserting data into table "maker_types" */
export type Maker_Types_Insert_Input = {
  users_maker_types?: Maybe<Users_Maker_Types_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Maker_Types_Max_Fields = {
  __typename?: 'maker_types_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "maker_types" */
export type Maker_Types_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Maker_Types_Min_Fields = {
  __typename?: 'maker_types_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "maker_types" */
export type Maker_Types_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "maker_types" */
export type Maker_Types_Mutation_Response = {
  __typename?: 'maker_types_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Maker_Types>
}

/** input type for inserting object relation for remote table "maker_types" */
export type Maker_Types_Obj_Rel_Insert_Input = {
  data: Maker_Types_Insert_Input
  on_conflict?: Maybe<Maker_Types_On_Conflict>
}

/** on conflict condition type for table "maker_types" */
export type Maker_Types_On_Conflict = {
  constraint: Maker_Types_Constraint
  update_columns: Array<Maker_Types_Update_Column>
  where?: Maybe<Maker_Types_Bool_Exp>
}

/** ordering options when selecting data from "maker_types" */
export type Maker_Types_Order_By = {
  users_maker_types_aggregate?: Maybe<Users_Maker_Types_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "maker_types" */
export type Maker_Types_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "maker_types" */
export enum Maker_Types_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "maker_types" */
export type Maker_Types_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "maker_types" */
export enum Maker_Types_Update_Column {
  /** column name */
  Value = 'value'
}

/** columns and relationships of "makers" */
export type Makers = {
  __typename?: 'makers'
  id: Scalars['uuid']
  /** An array relationship */
  localizations: Array<Localizations>
  /** An aggregated array relationship */
  localizations_aggregate: Localizations_Aggregate
  name: Scalars['String']
  /** An array relationship */
  users_makers: Array<Users_Makers>
  /** An aggregated array relationship */
  users_makers_aggregate: Users_Makers_Aggregate
}

/** columns and relationships of "makers" */
export type MakersLocalizationsArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** columns and relationships of "makers" */
export type MakersLocalizations_AggregateArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** columns and relationships of "makers" */
export type MakersUsers_MakersArgs = {
  distinct_on?: Maybe<Array<Users_Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Makers_Order_By>>
  where?: Maybe<Users_Makers_Bool_Exp>
}

/** columns and relationships of "makers" */
export type MakersUsers_Makers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Makers_Order_By>>
  where?: Maybe<Users_Makers_Bool_Exp>
}

/** aggregated selection of "makers" */
export type Makers_Aggregate = {
  __typename?: 'makers_aggregate'
  aggregate?: Maybe<Makers_Aggregate_Fields>
  nodes: Array<Makers>
}

/** aggregate fields of "makers" */
export type Makers_Aggregate_Fields = {
  __typename?: 'makers_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Makers_Max_Fields>
  min?: Maybe<Makers_Min_Fields>
}

/** aggregate fields of "makers" */
export type Makers_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Makers_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "makers" */
export type Makers_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Makers_Max_Order_By>
  min?: Maybe<Makers_Min_Order_By>
}

/** input type for inserting array relation for remote table "makers" */
export type Makers_Arr_Rel_Insert_Input = {
  data: Array<Makers_Insert_Input>
  on_conflict?: Maybe<Makers_On_Conflict>
}

/** Boolean expression to filter rows from the table "makers". All fields are combined with a logical 'AND'. */
export type Makers_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Makers_Bool_Exp>>>
  _not?: Maybe<Makers_Bool_Exp>
  _or?: Maybe<Array<Maybe<Makers_Bool_Exp>>>
  id?: Maybe<Uuid_Comparison_Exp>
  localizations?: Maybe<Localizations_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
  users_makers?: Maybe<Users_Makers_Bool_Exp>
}

/** unique or primary key constraints on table "makers" */
export enum Makers_Constraint {
  /** unique or primary key constraint */
  MakersNameKey = 'makers_name_key',
  /** unique or primary key constraint */
  MakersPkey = 'makers_pkey'
}

/** input type for inserting data into table "makers" */
export type Makers_Insert_Input = {
  id?: Maybe<Scalars['uuid']>
  localizations?: Maybe<Localizations_Arr_Rel_Insert_Input>
  name?: Maybe<Scalars['String']>
  users_makers?: Maybe<Users_Makers_Arr_Rel_Insert_Input>
}

/** aggregate max on columns */
export type Makers_Max_Fields = {
  __typename?: 'makers_max_fields'
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "makers" */
export type Makers_Max_Order_By = {
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Makers_Min_Fields = {
  __typename?: 'makers_min_fields'
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "makers" */
export type Makers_Min_Order_By = {
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
}

/** response of any mutation on the table "makers" */
export type Makers_Mutation_Response = {
  __typename?: 'makers_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Makers>
}

/** input type for inserting object relation for remote table "makers" */
export type Makers_Obj_Rel_Insert_Input = {
  data: Makers_Insert_Input
  on_conflict?: Maybe<Makers_On_Conflict>
}

/** on conflict condition type for table "makers" */
export type Makers_On_Conflict = {
  constraint: Makers_Constraint
  update_columns: Array<Makers_Update_Column>
  where?: Maybe<Makers_Bool_Exp>
}

/** ordering options when selecting data from "makers" */
export type Makers_Order_By = {
  id?: Maybe<Order_By>
  localizations_aggregate?: Maybe<Localizations_Aggregate_Order_By>
  name?: Maybe<Order_By>
  users_makers_aggregate?: Maybe<Users_Makers_Aggregate_Order_By>
}

/** primary key columns input for table: "makers" */
export type Makers_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "makers" */
export enum Makers_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "makers" */
export type Makers_Set_Input = {
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
}

/** update columns of table "makers" */
export enum Makers_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** columns and relationships of "media_file_types" */
export type Media_File_Types = {
  __typename?: 'media_file_types'
  id: Scalars['String']
  /** An array relationship */
  media_files: Array<Media_Files>
  /** An aggregated array relationship */
  media_files_aggregate: Media_Files_Aggregate
  name: Scalars['String']
}

/** columns and relationships of "media_file_types" */
export type Media_File_TypesMedia_FilesArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** columns and relationships of "media_file_types" */
export type Media_File_TypesMedia_Files_AggregateArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** aggregated selection of "media_file_types" */
export type Media_File_Types_Aggregate = {
  __typename?: 'media_file_types_aggregate'
  aggregate?: Maybe<Media_File_Types_Aggregate_Fields>
  nodes: Array<Media_File_Types>
}

/** aggregate fields of "media_file_types" */
export type Media_File_Types_Aggregate_Fields = {
  __typename?: 'media_file_types_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Media_File_Types_Max_Fields>
  min?: Maybe<Media_File_Types_Min_Fields>
}

/** aggregate fields of "media_file_types" */
export type Media_File_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Media_File_Types_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "media_file_types" */
export type Media_File_Types_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Media_File_Types_Max_Order_By>
  min?: Maybe<Media_File_Types_Min_Order_By>
}

/** input type for inserting array relation for remote table "media_file_types" */
export type Media_File_Types_Arr_Rel_Insert_Input = {
  data: Array<Media_File_Types_Insert_Input>
  on_conflict?: Maybe<Media_File_Types_On_Conflict>
}

/** Boolean expression to filter rows from the table "media_file_types". All fields are combined with a logical 'AND'. */
export type Media_File_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Media_File_Types_Bool_Exp>>>
  _not?: Maybe<Media_File_Types_Bool_Exp>
  _or?: Maybe<Array<Maybe<Media_File_Types_Bool_Exp>>>
  id?: Maybe<String_Comparison_Exp>
  media_files?: Maybe<Media_Files_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "media_file_types" */
export enum Media_File_Types_Constraint {
  /** unique or primary key constraint */
  MediaFileTypesPkey = 'media_file_types_pkey'
}

export enum Media_File_Types_Enum {
  /** Image file */
  Image = 'IMAGE',
  /** Video file */
  Video = 'VIDEO'
}

/** expression to compare columns of type media_file_types_enum. All fields are combined with logical 'AND'. */
export type Media_File_Types_Enum_Comparison_Exp = {
  _eq?: Maybe<Media_File_Types_Enum>
  _in?: Maybe<Array<Media_File_Types_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Media_File_Types_Enum>
  _nin?: Maybe<Array<Media_File_Types_Enum>>
}

/** input type for inserting data into table "media_file_types" */
export type Media_File_Types_Insert_Input = {
  id?: Maybe<Scalars['String']>
  media_files?: Maybe<Media_Files_Arr_Rel_Insert_Input>
  name?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Media_File_Types_Max_Fields = {
  __typename?: 'media_file_types_max_fields'
  id?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "media_file_types" */
export type Media_File_Types_Max_Order_By = {
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Media_File_Types_Min_Fields = {
  __typename?: 'media_file_types_min_fields'
  id?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "media_file_types" */
export type Media_File_Types_Min_Order_By = {
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
}

/** response of any mutation on the table "media_file_types" */
export type Media_File_Types_Mutation_Response = {
  __typename?: 'media_file_types_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Media_File_Types>
}

/** input type for inserting object relation for remote table "media_file_types" */
export type Media_File_Types_Obj_Rel_Insert_Input = {
  data: Media_File_Types_Insert_Input
  on_conflict?: Maybe<Media_File_Types_On_Conflict>
}

/** on conflict condition type for table "media_file_types" */
export type Media_File_Types_On_Conflict = {
  constraint: Media_File_Types_Constraint
  update_columns: Array<Media_File_Types_Update_Column>
  where?: Maybe<Media_File_Types_Bool_Exp>
}

/** ordering options when selecting data from "media_file_types" */
export type Media_File_Types_Order_By = {
  id?: Maybe<Order_By>
  media_files_aggregate?: Maybe<Media_Files_Aggregate_Order_By>
  name?: Maybe<Order_By>
}

/** primary key columns input for table: "media_file_types" */
export type Media_File_Types_Pk_Columns_Input = {
  id: Scalars['String']
}

/** select columns of table "media_file_types" */
export enum Media_File_Types_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "media_file_types" */
export type Media_File_Types_Set_Input = {
  id?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
}

/** update columns of table "media_file_types" */
export enum Media_File_Types_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** columns and relationships of "media_files" */
export type Media_Files = {
  __typename?: 'media_files'
  /** An object relationship */
  album?: Maybe<Albums>
  /** An array relationship */
  album_covers: Array<Albums>
  /** An aggregated array relationship */
  album_covers_aggregate: Albums_Aggregate
  album_id?: Maybe<Scalars['uuid']>
  blurhash?: Maybe<Scalars['String']>
  created_at: Scalars['timestamptz']
  /** An array relationship */
  downloads: Array<Downloads>
  /** An aggregated array relationship */
  downloads_aggregate: Downloads_Aggregate
  /** An object relationship */
  event?: Maybe<Events>
  event_id?: Maybe<Scalars['uuid']>
  /** An array relationship */
  events: Array<Events>
  /** An aggregated array relationship */
  events_aggregate: Events_Aggregate
  /** An array relationship */
  favorites: Array<Favorites>
  /** An aggregated array relationship */
  favorites_aggregate: Favorites_Aggregate
  /** An object relationship */
  folder?: Maybe<Folders>
  folder_id?: Maybe<Scalars['uuid']>
  /** An array relationship */
  folders: Array<Folders>
  /** An aggregated array relationship */
  folders_aggregate: Folders_Aggregate
  height: Scalars['Int']
  id: Scalars['uuid']
  /** A computed field, executes function "media_file_is_favorite" */
  is_favorite?: Maybe<Scalars['Boolean']>
  /** An object relationship */
  media_file_type: Media_File_Types
  name?: Maybe<Scalars['String']>
  preview_url?: Maybe<Scalars['String']>
  private: Scalars['Boolean']
  /** An array relationship */
  purchased: Array<Purchased>
  /** An aggregated array relationship */
  purchased_aggregate: Purchased_Aggregate
  size: Scalars['Int']
  /** An object relationship */
  store?: Maybe<Store>
  /** An array relationship */
  store_covers: Array<Store>
  /** An aggregated array relationship */
  store_covers_aggregate: Store_Aggregate
  store_id?: Maybe<Scalars['uuid']>
  thumb_url?: Maybe<Scalars['String']>
  type: Media_File_Types_Enum
  updated_at: Scalars['timestamptz']
  url?: Maybe<Scalars['String']>
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
  /** An array relationship */
  users_search_photos: Array<Users_Search_Media_Files>
  /** An aggregated array relationship */
  users_search_photos_aggregate: Users_Search_Media_Files_Aggregate
  width: Scalars['Int']
}

/** columns and relationships of "media_files" */
export type Media_FilesAlbum_CoversArgs = {
  distinct_on?: Maybe<Array<Albums_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Order_By>>
  where?: Maybe<Albums_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesAlbum_Covers_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Order_By>>
  where?: Maybe<Albums_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesDownloadsArgs = {
  distinct_on?: Maybe<Array<Downloads_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Downloads_Order_By>>
  where?: Maybe<Downloads_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesDownloads_AggregateArgs = {
  distinct_on?: Maybe<Array<Downloads_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Downloads_Order_By>>
  where?: Maybe<Downloads_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesEventsArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesEvents_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesFavoritesArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesFavorites_AggregateArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesFoldersArgs = {
  distinct_on?: Maybe<Array<Folders_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Folders_Order_By>>
  where?: Maybe<Folders_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesFolders_AggregateArgs = {
  distinct_on?: Maybe<Array<Folders_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Folders_Order_By>>
  where?: Maybe<Folders_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesPurchasedArgs = {
  distinct_on?: Maybe<Array<Purchased_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Purchased_Order_By>>
  where?: Maybe<Purchased_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesPurchased_AggregateArgs = {
  distinct_on?: Maybe<Array<Purchased_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Purchased_Order_By>>
  where?: Maybe<Purchased_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesStore_CoversArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesStore_Covers_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesUsers_Search_PhotosArgs = {
  distinct_on?: Maybe<Array<Users_Search_Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Search_Media_Files_Order_By>>
  where?: Maybe<Users_Search_Media_Files_Bool_Exp>
}

/** columns and relationships of "media_files" */
export type Media_FilesUsers_Search_Photos_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Search_Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Search_Media_Files_Order_By>>
  where?: Maybe<Users_Search_Media_Files_Bool_Exp>
}

/** aggregated selection of "media_files" */
export type Media_Files_Aggregate = {
  __typename?: 'media_files_aggregate'
  aggregate?: Maybe<Media_Files_Aggregate_Fields>
  nodes: Array<Media_Files>
}

/** aggregate fields of "media_files" */
export type Media_Files_Aggregate_Fields = {
  __typename?: 'media_files_aggregate_fields'
  avg?: Maybe<Media_Files_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Media_Files_Max_Fields>
  min?: Maybe<Media_Files_Min_Fields>
  stddev?: Maybe<Media_Files_Stddev_Fields>
  stddev_pop?: Maybe<Media_Files_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Media_Files_Stddev_Samp_Fields>
  sum?: Maybe<Media_Files_Sum_Fields>
  var_pop?: Maybe<Media_Files_Var_Pop_Fields>
  var_samp?: Maybe<Media_Files_Var_Samp_Fields>
  variance?: Maybe<Media_Files_Variance_Fields>
}

/** aggregate fields of "media_files" */
export type Media_Files_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Media_Files_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "media_files" */
export type Media_Files_Aggregate_Order_By = {
  avg?: Maybe<Media_Files_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Media_Files_Max_Order_By>
  min?: Maybe<Media_Files_Min_Order_By>
  stddev?: Maybe<Media_Files_Stddev_Order_By>
  stddev_pop?: Maybe<Media_Files_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Media_Files_Stddev_Samp_Order_By>
  sum?: Maybe<Media_Files_Sum_Order_By>
  var_pop?: Maybe<Media_Files_Var_Pop_Order_By>
  var_samp?: Maybe<Media_Files_Var_Samp_Order_By>
  variance?: Maybe<Media_Files_Variance_Order_By>
}

/** input type for inserting array relation for remote table "media_files" */
export type Media_Files_Arr_Rel_Insert_Input = {
  data: Array<Media_Files_Insert_Input>
  on_conflict?: Maybe<Media_Files_On_Conflict>
}

/** aggregate avg on columns */
export type Media_Files_Avg_Fields = {
  __typename?: 'media_files_avg_fields'
  height?: Maybe<Scalars['Float']>
  size?: Maybe<Scalars['Float']>
  width?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "media_files" */
export type Media_Files_Avg_Order_By = {
  height?: Maybe<Order_By>
  size?: Maybe<Order_By>
  width?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "media_files". All fields are combined with a logical 'AND'. */
export type Media_Files_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Media_Files_Bool_Exp>>>
  _not?: Maybe<Media_Files_Bool_Exp>
  _or?: Maybe<Array<Maybe<Media_Files_Bool_Exp>>>
  album?: Maybe<Albums_Bool_Exp>
  album_covers?: Maybe<Albums_Bool_Exp>
  album_id?: Maybe<Uuid_Comparison_Exp>
  blurhash?: Maybe<String_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  downloads?: Maybe<Downloads_Bool_Exp>
  event?: Maybe<Events_Bool_Exp>
  event_id?: Maybe<Uuid_Comparison_Exp>
  events?: Maybe<Events_Bool_Exp>
  favorites?: Maybe<Favorites_Bool_Exp>
  folder?: Maybe<Folders_Bool_Exp>
  folder_id?: Maybe<Uuid_Comparison_Exp>
  folders?: Maybe<Folders_Bool_Exp>
  height?: Maybe<Int_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  media_file_type?: Maybe<Media_File_Types_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
  preview_url?: Maybe<String_Comparison_Exp>
  private?: Maybe<Boolean_Comparison_Exp>
  purchased?: Maybe<Purchased_Bool_Exp>
  size?: Maybe<Int_Comparison_Exp>
  store?: Maybe<Store_Bool_Exp>
  store_covers?: Maybe<Store_Bool_Exp>
  store_id?: Maybe<Uuid_Comparison_Exp>
  thumb_url?: Maybe<String_Comparison_Exp>
  type?: Maybe<Media_File_Types_Enum_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  url?: Maybe<String_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
  users_search_photos?: Maybe<Users_Search_Media_Files_Bool_Exp>
  width?: Maybe<Int_Comparison_Exp>
}

/** unique or primary key constraints on table "media_files" */
export enum Media_Files_Constraint {
  /** unique or primary key constraint */
  MediaFilesPkey = 'media_files_pkey'
}

/** input type for incrementing integer column in table "media_files" */
export type Media_Files_Inc_Input = {
  height?: Maybe<Scalars['Int']>
  size?: Maybe<Scalars['Int']>
  width?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "media_files" */
export type Media_Files_Insert_Input = {
  album?: Maybe<Albums_Obj_Rel_Insert_Input>
  album_covers?: Maybe<Albums_Arr_Rel_Insert_Input>
  album_id?: Maybe<Scalars['uuid']>
  blurhash?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  downloads?: Maybe<Downloads_Arr_Rel_Insert_Input>
  event?: Maybe<Events_Obj_Rel_Insert_Input>
  event_id?: Maybe<Scalars['uuid']>
  events?: Maybe<Events_Arr_Rel_Insert_Input>
  favorites?: Maybe<Favorites_Arr_Rel_Insert_Input>
  folder?: Maybe<Folders_Obj_Rel_Insert_Input>
  folder_id?: Maybe<Scalars['uuid']>
  folders?: Maybe<Folders_Arr_Rel_Insert_Input>
  height?: Maybe<Scalars['Int']>
  id?: Maybe<Scalars['uuid']>
  media_file_type?: Maybe<Media_File_Types_Obj_Rel_Insert_Input>
  name?: Maybe<Scalars['String']>
  preview_url?: Maybe<Scalars['String']>
  private?: Maybe<Scalars['Boolean']>
  purchased?: Maybe<Purchased_Arr_Rel_Insert_Input>
  size?: Maybe<Scalars['Int']>
  store?: Maybe<Store_Obj_Rel_Insert_Input>
  store_covers?: Maybe<Store_Arr_Rel_Insert_Input>
  store_id?: Maybe<Scalars['uuid']>
  thumb_url?: Maybe<Scalars['String']>
  type?: Maybe<Media_File_Types_Enum>
  updated_at?: Maybe<Scalars['timestamptz']>
  url?: Maybe<Scalars['String']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
  users_search_photos?: Maybe<Users_Search_Media_Files_Arr_Rel_Insert_Input>
  width?: Maybe<Scalars['Int']>
}

/** aggregate max on columns */
export type Media_Files_Max_Fields = {
  __typename?: 'media_files_max_fields'
  album_id?: Maybe<Scalars['uuid']>
  blurhash?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  folder_id?: Maybe<Scalars['uuid']>
  height?: Maybe<Scalars['Int']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  preview_url?: Maybe<Scalars['String']>
  size?: Maybe<Scalars['Int']>
  store_id?: Maybe<Scalars['uuid']>
  thumb_url?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  url?: Maybe<Scalars['String']>
  user_id?: Maybe<Scalars['uuid']>
  width?: Maybe<Scalars['Int']>
}

/** order by max() on columns of table "media_files" */
export type Media_Files_Max_Order_By = {
  album_id?: Maybe<Order_By>
  blurhash?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  folder_id?: Maybe<Order_By>
  height?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  preview_url?: Maybe<Order_By>
  size?: Maybe<Order_By>
  store_id?: Maybe<Order_By>
  thumb_url?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  url?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
  width?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Media_Files_Min_Fields = {
  __typename?: 'media_files_min_fields'
  album_id?: Maybe<Scalars['uuid']>
  blurhash?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  folder_id?: Maybe<Scalars['uuid']>
  height?: Maybe<Scalars['Int']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  preview_url?: Maybe<Scalars['String']>
  size?: Maybe<Scalars['Int']>
  store_id?: Maybe<Scalars['uuid']>
  thumb_url?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  url?: Maybe<Scalars['String']>
  user_id?: Maybe<Scalars['uuid']>
  width?: Maybe<Scalars['Int']>
}

/** order by min() on columns of table "media_files" */
export type Media_Files_Min_Order_By = {
  album_id?: Maybe<Order_By>
  blurhash?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  folder_id?: Maybe<Order_By>
  height?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  preview_url?: Maybe<Order_By>
  size?: Maybe<Order_By>
  store_id?: Maybe<Order_By>
  thumb_url?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  url?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
  width?: Maybe<Order_By>
}

/** response of any mutation on the table "media_files" */
export type Media_Files_Mutation_Response = {
  __typename?: 'media_files_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Media_Files>
}

/** input type for inserting object relation for remote table "media_files" */
export type Media_Files_Obj_Rel_Insert_Input = {
  data: Media_Files_Insert_Input
  on_conflict?: Maybe<Media_Files_On_Conflict>
}

/** on conflict condition type for table "media_files" */
export type Media_Files_On_Conflict = {
  constraint: Media_Files_Constraint
  update_columns: Array<Media_Files_Update_Column>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** ordering options when selecting data from "media_files" */
export type Media_Files_Order_By = {
  album?: Maybe<Albums_Order_By>
  album_covers_aggregate?: Maybe<Albums_Aggregate_Order_By>
  album_id?: Maybe<Order_By>
  blurhash?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  downloads_aggregate?: Maybe<Downloads_Aggregate_Order_By>
  event?: Maybe<Events_Order_By>
  event_id?: Maybe<Order_By>
  events_aggregate?: Maybe<Events_Aggregate_Order_By>
  favorites_aggregate?: Maybe<Favorites_Aggregate_Order_By>
  folder?: Maybe<Folders_Order_By>
  folder_id?: Maybe<Order_By>
  folders_aggregate?: Maybe<Folders_Aggregate_Order_By>
  height?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_file_type?: Maybe<Media_File_Types_Order_By>
  name?: Maybe<Order_By>
  preview_url?: Maybe<Order_By>
  private?: Maybe<Order_By>
  purchased_aggregate?: Maybe<Purchased_Aggregate_Order_By>
  size?: Maybe<Order_By>
  store?: Maybe<Store_Order_By>
  store_covers_aggregate?: Maybe<Store_Aggregate_Order_By>
  store_id?: Maybe<Order_By>
  thumb_url?: Maybe<Order_By>
  type?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  url?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
  users_search_photos_aggregate?: Maybe<
    Users_Search_Media_Files_Aggregate_Order_By
  >
  width?: Maybe<Order_By>
}

/** primary key columns input for table: "media_files" */
export type Media_Files_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "media_files" */
export enum Media_Files_Select_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Blurhash = 'blurhash',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  EventId = 'event_id',
  /** column name */
  FolderId = 'folder_id',
  /** column name */
  Height = 'height',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  PreviewUrl = 'preview_url',
  /** column name */
  Private = 'private',
  /** column name */
  Size = 'size',
  /** column name */
  StoreId = 'store_id',
  /** column name */
  ThumbUrl = 'thumb_url',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Url = 'url',
  /** column name */
  UserId = 'user_id',
  /** column name */
  Width = 'width'
}

/** input type for updating data in table "media_files" */
export type Media_Files_Set_Input = {
  album_id?: Maybe<Scalars['uuid']>
  blurhash?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  folder_id?: Maybe<Scalars['uuid']>
  height?: Maybe<Scalars['Int']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  preview_url?: Maybe<Scalars['String']>
  private?: Maybe<Scalars['Boolean']>
  size?: Maybe<Scalars['Int']>
  store_id?: Maybe<Scalars['uuid']>
  thumb_url?: Maybe<Scalars['String']>
  type?: Maybe<Media_File_Types_Enum>
  updated_at?: Maybe<Scalars['timestamptz']>
  url?: Maybe<Scalars['String']>
  user_id?: Maybe<Scalars['uuid']>
  width?: Maybe<Scalars['Int']>
}

/** aggregate stddev on columns */
export type Media_Files_Stddev_Fields = {
  __typename?: 'media_files_stddev_fields'
  height?: Maybe<Scalars['Float']>
  size?: Maybe<Scalars['Float']>
  width?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "media_files" */
export type Media_Files_Stddev_Order_By = {
  height?: Maybe<Order_By>
  size?: Maybe<Order_By>
  width?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Media_Files_Stddev_Pop_Fields = {
  __typename?: 'media_files_stddev_pop_fields'
  height?: Maybe<Scalars['Float']>
  size?: Maybe<Scalars['Float']>
  width?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "media_files" */
export type Media_Files_Stddev_Pop_Order_By = {
  height?: Maybe<Order_By>
  size?: Maybe<Order_By>
  width?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Media_Files_Stddev_Samp_Fields = {
  __typename?: 'media_files_stddev_samp_fields'
  height?: Maybe<Scalars['Float']>
  size?: Maybe<Scalars['Float']>
  width?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "media_files" */
export type Media_Files_Stddev_Samp_Order_By = {
  height?: Maybe<Order_By>
  size?: Maybe<Order_By>
  width?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Media_Files_Sum_Fields = {
  __typename?: 'media_files_sum_fields'
  height?: Maybe<Scalars['Int']>
  size?: Maybe<Scalars['Int']>
  width?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "media_files" */
export type Media_Files_Sum_Order_By = {
  height?: Maybe<Order_By>
  size?: Maybe<Order_By>
  width?: Maybe<Order_By>
}

/** update columns of table "media_files" */
export enum Media_Files_Update_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Blurhash = 'blurhash',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  EventId = 'event_id',
  /** column name */
  FolderId = 'folder_id',
  /** column name */
  Height = 'height',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  PreviewUrl = 'preview_url',
  /** column name */
  Private = 'private',
  /** column name */
  Size = 'size',
  /** column name */
  StoreId = 'store_id',
  /** column name */
  ThumbUrl = 'thumb_url',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Url = 'url',
  /** column name */
  UserId = 'user_id',
  /** column name */
  Width = 'width'
}

/** aggregate var_pop on columns */
export type Media_Files_Var_Pop_Fields = {
  __typename?: 'media_files_var_pop_fields'
  height?: Maybe<Scalars['Float']>
  size?: Maybe<Scalars['Float']>
  width?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "media_files" */
export type Media_Files_Var_Pop_Order_By = {
  height?: Maybe<Order_By>
  size?: Maybe<Order_By>
  width?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Media_Files_Var_Samp_Fields = {
  __typename?: 'media_files_var_samp_fields'
  height?: Maybe<Scalars['Float']>
  size?: Maybe<Scalars['Float']>
  width?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "media_files" */
export type Media_Files_Var_Samp_Order_By = {
  height?: Maybe<Order_By>
  size?: Maybe<Order_By>
  width?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Media_Files_Variance_Fields = {
  __typename?: 'media_files_variance_fields'
  height?: Maybe<Scalars['Float']>
  size?: Maybe<Scalars['Float']>
  width?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "media_files" */
export type Media_Files_Variance_Order_By = {
  height?: Maybe<Order_By>
  size?: Maybe<Order_By>
  width?: Maybe<Order_By>
}

/** columns and relationships of "menu_items" */
export type Menu_Items = {
  __typename?: 'menu_items'
  external: Scalars['Boolean']
  id: Scalars['uuid']
  /** An array relationship */
  localizations: Array<Localizations>
  /** An aggregated array relationship */
  localizations_aggregate: Localizations_Aggregate
  /** An object relationship */
  menu_type: Menu_Type
  order: Scalars['Int']
  type: Menu_Type_Enum
  url: Scalars['String']
}

/** columns and relationships of "menu_items" */
export type Menu_ItemsLocalizationsArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** columns and relationships of "menu_items" */
export type Menu_ItemsLocalizations_AggregateArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** aggregated selection of "menu_items" */
export type Menu_Items_Aggregate = {
  __typename?: 'menu_items_aggregate'
  aggregate?: Maybe<Menu_Items_Aggregate_Fields>
  nodes: Array<Menu_Items>
}

/** aggregate fields of "menu_items" */
export type Menu_Items_Aggregate_Fields = {
  __typename?: 'menu_items_aggregate_fields'
  avg?: Maybe<Menu_Items_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Menu_Items_Max_Fields>
  min?: Maybe<Menu_Items_Min_Fields>
  stddev?: Maybe<Menu_Items_Stddev_Fields>
  stddev_pop?: Maybe<Menu_Items_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Menu_Items_Stddev_Samp_Fields>
  sum?: Maybe<Menu_Items_Sum_Fields>
  var_pop?: Maybe<Menu_Items_Var_Pop_Fields>
  var_samp?: Maybe<Menu_Items_Var_Samp_Fields>
  variance?: Maybe<Menu_Items_Variance_Fields>
}

/** aggregate fields of "menu_items" */
export type Menu_Items_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Menu_Items_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "menu_items" */
export type Menu_Items_Aggregate_Order_By = {
  avg?: Maybe<Menu_Items_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Menu_Items_Max_Order_By>
  min?: Maybe<Menu_Items_Min_Order_By>
  stddev?: Maybe<Menu_Items_Stddev_Order_By>
  stddev_pop?: Maybe<Menu_Items_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Menu_Items_Stddev_Samp_Order_By>
  sum?: Maybe<Menu_Items_Sum_Order_By>
  var_pop?: Maybe<Menu_Items_Var_Pop_Order_By>
  var_samp?: Maybe<Menu_Items_Var_Samp_Order_By>
  variance?: Maybe<Menu_Items_Variance_Order_By>
}

/** input type for inserting array relation for remote table "menu_items" */
export type Menu_Items_Arr_Rel_Insert_Input = {
  data: Array<Menu_Items_Insert_Input>
  on_conflict?: Maybe<Menu_Items_On_Conflict>
}

/** aggregate avg on columns */
export type Menu_Items_Avg_Fields = {
  __typename?: 'menu_items_avg_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "menu_items" */
export type Menu_Items_Avg_Order_By = {
  order?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "menu_items". All fields are combined with a logical 'AND'. */
export type Menu_Items_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Menu_Items_Bool_Exp>>>
  _not?: Maybe<Menu_Items_Bool_Exp>
  _or?: Maybe<Array<Maybe<Menu_Items_Bool_Exp>>>
  external?: Maybe<Boolean_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  localizations?: Maybe<Localizations_Bool_Exp>
  menu_type?: Maybe<Menu_Type_Bool_Exp>
  order?: Maybe<Int_Comparison_Exp>
  type?: Maybe<Menu_Type_Enum_Comparison_Exp>
  url?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "menu_items" */
export enum Menu_Items_Constraint {
  /** unique or primary key constraint */
  MenuItemsTypeUrlKey = 'menu_items_type_url_key',
  /** unique or primary key constraint */
  MenuPkey = 'menu_pkey'
}

/** input type for incrementing integer column in table "menu_items" */
export type Menu_Items_Inc_Input = {
  order?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "menu_items" */
export type Menu_Items_Insert_Input = {
  external?: Maybe<Scalars['Boolean']>
  id?: Maybe<Scalars['uuid']>
  localizations?: Maybe<Localizations_Arr_Rel_Insert_Input>
  menu_type?: Maybe<Menu_Type_Obj_Rel_Insert_Input>
  order?: Maybe<Scalars['Int']>
  type?: Maybe<Menu_Type_Enum>
  url?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Menu_Items_Max_Fields = {
  __typename?: 'menu_items_max_fields'
  id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  url?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "menu_items" */
export type Menu_Items_Max_Order_By = {
  id?: Maybe<Order_By>
  order?: Maybe<Order_By>
  url?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Menu_Items_Min_Fields = {
  __typename?: 'menu_items_min_fields'
  id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  url?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "menu_items" */
export type Menu_Items_Min_Order_By = {
  id?: Maybe<Order_By>
  order?: Maybe<Order_By>
  url?: Maybe<Order_By>
}

/** response of any mutation on the table "menu_items" */
export type Menu_Items_Mutation_Response = {
  __typename?: 'menu_items_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Menu_Items>
}

/** input type for inserting object relation for remote table "menu_items" */
export type Menu_Items_Obj_Rel_Insert_Input = {
  data: Menu_Items_Insert_Input
  on_conflict?: Maybe<Menu_Items_On_Conflict>
}

/** on conflict condition type for table "menu_items" */
export type Menu_Items_On_Conflict = {
  constraint: Menu_Items_Constraint
  update_columns: Array<Menu_Items_Update_Column>
  where?: Maybe<Menu_Items_Bool_Exp>
}

/** ordering options when selecting data from "menu_items" */
export type Menu_Items_Order_By = {
  external?: Maybe<Order_By>
  id?: Maybe<Order_By>
  localizations_aggregate?: Maybe<Localizations_Aggregate_Order_By>
  menu_type?: Maybe<Menu_Type_Order_By>
  order?: Maybe<Order_By>
  type?: Maybe<Order_By>
  url?: Maybe<Order_By>
}

/** primary key columns input for table: "menu_items" */
export type Menu_Items_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "menu_items" */
export enum Menu_Items_Select_Column {
  /** column name */
  External = 'external',
  /** column name */
  Id = 'id',
  /** column name */
  Order = 'order',
  /** column name */
  Type = 'type',
  /** column name */
  Url = 'url'
}

/** input type for updating data in table "menu_items" */
export type Menu_Items_Set_Input = {
  external?: Maybe<Scalars['Boolean']>
  id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  type?: Maybe<Menu_Type_Enum>
  url?: Maybe<Scalars['String']>
}

/** aggregate stddev on columns */
export type Menu_Items_Stddev_Fields = {
  __typename?: 'menu_items_stddev_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "menu_items" */
export type Menu_Items_Stddev_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Menu_Items_Stddev_Pop_Fields = {
  __typename?: 'menu_items_stddev_pop_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "menu_items" */
export type Menu_Items_Stddev_Pop_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Menu_Items_Stddev_Samp_Fields = {
  __typename?: 'menu_items_stddev_samp_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "menu_items" */
export type Menu_Items_Stddev_Samp_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Menu_Items_Sum_Fields = {
  __typename?: 'menu_items_sum_fields'
  order?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "menu_items" */
export type Menu_Items_Sum_Order_By = {
  order?: Maybe<Order_By>
}

/** update columns of table "menu_items" */
export enum Menu_Items_Update_Column {
  /** column name */
  External = 'external',
  /** column name */
  Id = 'id',
  /** column name */
  Order = 'order',
  /** column name */
  Type = 'type',
  /** column name */
  Url = 'url'
}

/** aggregate var_pop on columns */
export type Menu_Items_Var_Pop_Fields = {
  __typename?: 'menu_items_var_pop_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "menu_items" */
export type Menu_Items_Var_Pop_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Menu_Items_Var_Samp_Fields = {
  __typename?: 'menu_items_var_samp_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "menu_items" */
export type Menu_Items_Var_Samp_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Menu_Items_Variance_Fields = {
  __typename?: 'menu_items_variance_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "menu_items" */
export type Menu_Items_Variance_Order_By = {
  order?: Maybe<Order_By>
}

/** columns and relationships of "menu_pages" */
export type Menu_Pages = {
  __typename?: 'menu_pages'
  id: Scalars['uuid']
  order: Scalars['Int']
  /** An object relationship */
  page: Pages
  page_id: Scalars['uuid']
}

/** aggregated selection of "menu_pages" */
export type Menu_Pages_Aggregate = {
  __typename?: 'menu_pages_aggregate'
  aggregate?: Maybe<Menu_Pages_Aggregate_Fields>
  nodes: Array<Menu_Pages>
}

/** aggregate fields of "menu_pages" */
export type Menu_Pages_Aggregate_Fields = {
  __typename?: 'menu_pages_aggregate_fields'
  avg?: Maybe<Menu_Pages_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Menu_Pages_Max_Fields>
  min?: Maybe<Menu_Pages_Min_Fields>
  stddev?: Maybe<Menu_Pages_Stddev_Fields>
  stddev_pop?: Maybe<Menu_Pages_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Menu_Pages_Stddev_Samp_Fields>
  sum?: Maybe<Menu_Pages_Sum_Fields>
  var_pop?: Maybe<Menu_Pages_Var_Pop_Fields>
  var_samp?: Maybe<Menu_Pages_Var_Samp_Fields>
  variance?: Maybe<Menu_Pages_Variance_Fields>
}

/** aggregate fields of "menu_pages" */
export type Menu_Pages_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Menu_Pages_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "menu_pages" */
export type Menu_Pages_Aggregate_Order_By = {
  avg?: Maybe<Menu_Pages_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Menu_Pages_Max_Order_By>
  min?: Maybe<Menu_Pages_Min_Order_By>
  stddev?: Maybe<Menu_Pages_Stddev_Order_By>
  stddev_pop?: Maybe<Menu_Pages_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Menu_Pages_Stddev_Samp_Order_By>
  sum?: Maybe<Menu_Pages_Sum_Order_By>
  var_pop?: Maybe<Menu_Pages_Var_Pop_Order_By>
  var_samp?: Maybe<Menu_Pages_Var_Samp_Order_By>
  variance?: Maybe<Menu_Pages_Variance_Order_By>
}

/** input type for inserting array relation for remote table "menu_pages" */
export type Menu_Pages_Arr_Rel_Insert_Input = {
  data: Array<Menu_Pages_Insert_Input>
  on_conflict?: Maybe<Menu_Pages_On_Conflict>
}

/** aggregate avg on columns */
export type Menu_Pages_Avg_Fields = {
  __typename?: 'menu_pages_avg_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "menu_pages" */
export type Menu_Pages_Avg_Order_By = {
  order?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "menu_pages". All fields are combined with a logical 'AND'. */
export type Menu_Pages_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Menu_Pages_Bool_Exp>>>
  _not?: Maybe<Menu_Pages_Bool_Exp>
  _or?: Maybe<Array<Maybe<Menu_Pages_Bool_Exp>>>
  id?: Maybe<Uuid_Comparison_Exp>
  order?: Maybe<Int_Comparison_Exp>
  page?: Maybe<Pages_Bool_Exp>
  page_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "menu_pages" */
export enum Menu_Pages_Constraint {
  /** unique or primary key constraint */
  MenuPagesPageIdKey = 'menu_pages_page_id_key',
  /** unique or primary key constraint */
  MenuPagesPkey = 'menu_pages_pkey'
}

/** input type for incrementing integer column in table "menu_pages" */
export type Menu_Pages_Inc_Input = {
  order?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "menu_pages" */
export type Menu_Pages_Insert_Input = {
  id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  page?: Maybe<Pages_Obj_Rel_Insert_Input>
  page_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Menu_Pages_Max_Fields = {
  __typename?: 'menu_pages_max_fields'
  id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  page_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "menu_pages" */
export type Menu_Pages_Max_Order_By = {
  id?: Maybe<Order_By>
  order?: Maybe<Order_By>
  page_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Menu_Pages_Min_Fields = {
  __typename?: 'menu_pages_min_fields'
  id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  page_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "menu_pages" */
export type Menu_Pages_Min_Order_By = {
  id?: Maybe<Order_By>
  order?: Maybe<Order_By>
  page_id?: Maybe<Order_By>
}

/** response of any mutation on the table "menu_pages" */
export type Menu_Pages_Mutation_Response = {
  __typename?: 'menu_pages_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Menu_Pages>
}

/** input type for inserting object relation for remote table "menu_pages" */
export type Menu_Pages_Obj_Rel_Insert_Input = {
  data: Menu_Pages_Insert_Input
  on_conflict?: Maybe<Menu_Pages_On_Conflict>
}

/** on conflict condition type for table "menu_pages" */
export type Menu_Pages_On_Conflict = {
  constraint: Menu_Pages_Constraint
  update_columns: Array<Menu_Pages_Update_Column>
  where?: Maybe<Menu_Pages_Bool_Exp>
}

/** ordering options when selecting data from "menu_pages" */
export type Menu_Pages_Order_By = {
  id?: Maybe<Order_By>
  order?: Maybe<Order_By>
  page?: Maybe<Pages_Order_By>
  page_id?: Maybe<Order_By>
}

/** primary key columns input for table: "menu_pages" */
export type Menu_Pages_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "menu_pages" */
export enum Menu_Pages_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Order = 'order',
  /** column name */
  PageId = 'page_id'
}

/** input type for updating data in table "menu_pages" */
export type Menu_Pages_Set_Input = {
  id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  page_id?: Maybe<Scalars['uuid']>
}

/** aggregate stddev on columns */
export type Menu_Pages_Stddev_Fields = {
  __typename?: 'menu_pages_stddev_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "menu_pages" */
export type Menu_Pages_Stddev_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Menu_Pages_Stddev_Pop_Fields = {
  __typename?: 'menu_pages_stddev_pop_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "menu_pages" */
export type Menu_Pages_Stddev_Pop_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Menu_Pages_Stddev_Samp_Fields = {
  __typename?: 'menu_pages_stddev_samp_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "menu_pages" */
export type Menu_Pages_Stddev_Samp_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Menu_Pages_Sum_Fields = {
  __typename?: 'menu_pages_sum_fields'
  order?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "menu_pages" */
export type Menu_Pages_Sum_Order_By = {
  order?: Maybe<Order_By>
}

/** update columns of table "menu_pages" */
export enum Menu_Pages_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Order = 'order',
  /** column name */
  PageId = 'page_id'
}

/** aggregate var_pop on columns */
export type Menu_Pages_Var_Pop_Fields = {
  __typename?: 'menu_pages_var_pop_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "menu_pages" */
export type Menu_Pages_Var_Pop_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Menu_Pages_Var_Samp_Fields = {
  __typename?: 'menu_pages_var_samp_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "menu_pages" */
export type Menu_Pages_Var_Samp_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Menu_Pages_Variance_Fields = {
  __typename?: 'menu_pages_variance_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "menu_pages" */
export type Menu_Pages_Variance_Order_By = {
  order?: Maybe<Order_By>
}

/** columns and relationships of "menu_type" */
export type Menu_Type = {
  __typename?: 'menu_type'
  /** An array relationship */
  menus: Array<Menu_Items>
  /** An aggregated array relationship */
  menus_aggregate: Menu_Items_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "menu_type" */
export type Menu_TypeMenusArgs = {
  distinct_on?: Maybe<Array<Menu_Items_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Items_Order_By>>
  where?: Maybe<Menu_Items_Bool_Exp>
}

/** columns and relationships of "menu_type" */
export type Menu_TypeMenus_AggregateArgs = {
  distinct_on?: Maybe<Array<Menu_Items_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Items_Order_By>>
  where?: Maybe<Menu_Items_Bool_Exp>
}

/** aggregated selection of "menu_type" */
export type Menu_Type_Aggregate = {
  __typename?: 'menu_type_aggregate'
  aggregate?: Maybe<Menu_Type_Aggregate_Fields>
  nodes: Array<Menu_Type>
}

/** aggregate fields of "menu_type" */
export type Menu_Type_Aggregate_Fields = {
  __typename?: 'menu_type_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Menu_Type_Max_Fields>
  min?: Maybe<Menu_Type_Min_Fields>
}

/** aggregate fields of "menu_type" */
export type Menu_Type_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Menu_Type_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "menu_type" */
export type Menu_Type_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Menu_Type_Max_Order_By>
  min?: Maybe<Menu_Type_Min_Order_By>
}

/** input type for inserting array relation for remote table "menu_type" */
export type Menu_Type_Arr_Rel_Insert_Input = {
  data: Array<Menu_Type_Insert_Input>
  on_conflict?: Maybe<Menu_Type_On_Conflict>
}

/** Boolean expression to filter rows from the table "menu_type". All fields are combined with a logical 'AND'. */
export type Menu_Type_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Menu_Type_Bool_Exp>>>
  _not?: Maybe<Menu_Type_Bool_Exp>
  _or?: Maybe<Array<Maybe<Menu_Type_Bool_Exp>>>
  menus?: Maybe<Menu_Items_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "menu_type" */
export enum Menu_Type_Constraint {
  /** unique or primary key constraint */
  MenuTypePkey = 'menu_type_pkey'
}

export enum Menu_Type_Enum {
  Footer = 'FOOTER',
  Header = 'HEADER',
  Sidebar = 'SIDEBAR',
  SidebarSocial = 'SIDEBAR_SOCIAL'
}

/** expression to compare columns of type menu_type_enum. All fields are combined with logical 'AND'. */
export type Menu_Type_Enum_Comparison_Exp = {
  _eq?: Maybe<Menu_Type_Enum>
  _in?: Maybe<Array<Menu_Type_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Menu_Type_Enum>
  _nin?: Maybe<Array<Menu_Type_Enum>>
}

/** input type for inserting data into table "menu_type" */
export type Menu_Type_Insert_Input = {
  menus?: Maybe<Menu_Items_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Menu_Type_Max_Fields = {
  __typename?: 'menu_type_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "menu_type" */
export type Menu_Type_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Menu_Type_Min_Fields = {
  __typename?: 'menu_type_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "menu_type" */
export type Menu_Type_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "menu_type" */
export type Menu_Type_Mutation_Response = {
  __typename?: 'menu_type_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Menu_Type>
}

/** input type for inserting object relation for remote table "menu_type" */
export type Menu_Type_Obj_Rel_Insert_Input = {
  data: Menu_Type_Insert_Input
  on_conflict?: Maybe<Menu_Type_On_Conflict>
}

/** on conflict condition type for table "menu_type" */
export type Menu_Type_On_Conflict = {
  constraint: Menu_Type_Constraint
  update_columns: Array<Menu_Type_Update_Column>
  where?: Maybe<Menu_Type_Bool_Exp>
}

/** ordering options when selecting data from "menu_type" */
export type Menu_Type_Order_By = {
  menus_aggregate?: Maybe<Menu_Items_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "menu_type" */
export type Menu_Type_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "menu_type" */
export enum Menu_Type_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "menu_type" */
export type Menu_Type_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "menu_type" */
export enum Menu_Type_Update_Column {
  /** column name */
  Value = 'value'
}

/** columns and relationships of "messages" */
export type Messages = {
  __typename?: 'messages'
  /** A computed field, executes function "linza_get_message_contact" */
  contacts?: Maybe<Array<Contacts>>
  created_at: Scalars['timestamptz']
  data?: Maybe<Scalars['jsonb']>
  /** An object relationship */
  event?: Maybe<Events>
  event_id?: Maybe<Scalars['uuid']>
  /** An object relationship */
  event_updated?: Maybe<Messages_Events_Updated>
  /** An object relationship */
  from_user?: Maybe<Users>
  from_user_id?: Maybe<Scalars['uuid']>
  id: Scalars['uuid']
  message: Scalars['String']
  /** An object relationship */
  messages_status: Messages_Statuses
  /** An object relationship */
  messages_type: Messages_Types
  notification_id?: Maybe<Scalars['uuid']>
  status: Messages_Statuses_Enum
  system: Scalars['Boolean']
  /** An object relationship */
  to_user: Users
  to_user_id: Scalars['uuid']
  type: Messages_Types_Enum
  updated_at: Scalars['timestamptz']
}

/** columns and relationships of "messages" */
export type MessagesContactsArgs = {
  distinct_on?: Maybe<Array<Contacts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Contacts_Order_By>>
  where?: Maybe<Contacts_Bool_Exp>
}

/** columns and relationships of "messages" */
export type MessagesDataArgs = {
  path?: Maybe<Scalars['String']>
}

/** aggregated selection of "messages" */
export type Messages_Aggregate = {
  __typename?: 'messages_aggregate'
  aggregate?: Maybe<Messages_Aggregate_Fields>
  nodes: Array<Messages>
}

/** aggregate fields of "messages" */
export type Messages_Aggregate_Fields = {
  __typename?: 'messages_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Messages_Max_Fields>
  min?: Maybe<Messages_Min_Fields>
}

/** aggregate fields of "messages" */
export type Messages_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Messages_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "messages" */
export type Messages_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Messages_Max_Order_By>
  min?: Maybe<Messages_Min_Order_By>
}

/** append existing jsonb value of filtered columns with new jsonb value */
export type Messages_Append_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** input type for inserting array relation for remote table "messages" */
export type Messages_Arr_Rel_Insert_Input = {
  data: Array<Messages_Insert_Input>
  on_conflict?: Maybe<Messages_On_Conflict>
}

/** Boolean expression to filter rows from the table "messages". All fields are combined with a logical 'AND'. */
export type Messages_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Messages_Bool_Exp>>>
  _not?: Maybe<Messages_Bool_Exp>
  _or?: Maybe<Array<Maybe<Messages_Bool_Exp>>>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  data?: Maybe<Jsonb_Comparison_Exp>
  event?: Maybe<Events_Bool_Exp>
  event_id?: Maybe<Uuid_Comparison_Exp>
  event_updated?: Maybe<Messages_Events_Updated_Bool_Exp>
  from_user?: Maybe<Users_Bool_Exp>
  from_user_id?: Maybe<Uuid_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  message?: Maybe<String_Comparison_Exp>
  messages_status?: Maybe<Messages_Statuses_Bool_Exp>
  messages_type?: Maybe<Messages_Types_Bool_Exp>
  notification_id?: Maybe<Uuid_Comparison_Exp>
  status?: Maybe<Messages_Statuses_Enum_Comparison_Exp>
  system?: Maybe<Boolean_Comparison_Exp>
  to_user?: Maybe<Users_Bool_Exp>
  to_user_id?: Maybe<Uuid_Comparison_Exp>
  type?: Maybe<Messages_Types_Enum_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "messages" */
export enum Messages_Constraint {
  /** unique or primary key constraint */
  MessagesPkey = 'messages_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Messages_Delete_At_Path_Input = {
  data?: Maybe<Array<Maybe<Scalars['String']>>>
}

/**
 * delete the array element with specified index (negative integers count from the
 * end). throws an error if top level container is not an array
 */
export type Messages_Delete_Elem_Input = {
  data?: Maybe<Scalars['Int']>
}

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Messages_Delete_Key_Input = {
  data?: Maybe<Scalars['String']>
}

/** columns and relationships of "messages_events_updated" */
export type Messages_Events_Updated = {
  __typename?: 'messages_events_updated'
  id: Scalars['uuid']
  /** An object relationship */
  message: Messages
  message_id: Scalars['uuid']
  new_comment?: Maybe<Scalars['String']>
  new_date?: Maybe<Scalars['timestamptz']>
  /** An object relationship */
  new_location?: Maybe<Locations>
  new_location_id?: Maybe<Scalars['uuid']>
  new_name?: Maybe<Scalars['String']>
  new_note?: Maybe<Scalars['String']>
  new_retouching?: Maybe<Scalars['Boolean']>
  new_time_for_upload?: Maybe<Scalars['Int']>
  old_comment?: Maybe<Scalars['String']>
  old_date?: Maybe<Scalars['timestamptz']>
  /** An object relationship */
  old_location?: Maybe<Locations>
  old_location_id?: Maybe<Scalars['uuid']>
  old_name?: Maybe<Scalars['String']>
  old_note?: Maybe<Scalars['String']>
  old_retouching?: Maybe<Scalars['Boolean']>
  old_time_for_upload?: Maybe<Scalars['Int']>
}

/** aggregated selection of "messages_events_updated" */
export type Messages_Events_Updated_Aggregate = {
  __typename?: 'messages_events_updated_aggregate'
  aggregate?: Maybe<Messages_Events_Updated_Aggregate_Fields>
  nodes: Array<Messages_Events_Updated>
}

/** aggregate fields of "messages_events_updated" */
export type Messages_Events_Updated_Aggregate_Fields = {
  __typename?: 'messages_events_updated_aggregate_fields'
  avg?: Maybe<Messages_Events_Updated_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Messages_Events_Updated_Max_Fields>
  min?: Maybe<Messages_Events_Updated_Min_Fields>
  stddev?: Maybe<Messages_Events_Updated_Stddev_Fields>
  stddev_pop?: Maybe<Messages_Events_Updated_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Messages_Events_Updated_Stddev_Samp_Fields>
  sum?: Maybe<Messages_Events_Updated_Sum_Fields>
  var_pop?: Maybe<Messages_Events_Updated_Var_Pop_Fields>
  var_samp?: Maybe<Messages_Events_Updated_Var_Samp_Fields>
  variance?: Maybe<Messages_Events_Updated_Variance_Fields>
}

/** aggregate fields of "messages_events_updated" */
export type Messages_Events_Updated_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Messages_Events_Updated_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "messages_events_updated" */
export type Messages_Events_Updated_Aggregate_Order_By = {
  avg?: Maybe<Messages_Events_Updated_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Messages_Events_Updated_Max_Order_By>
  min?: Maybe<Messages_Events_Updated_Min_Order_By>
  stddev?: Maybe<Messages_Events_Updated_Stddev_Order_By>
  stddev_pop?: Maybe<Messages_Events_Updated_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Messages_Events_Updated_Stddev_Samp_Order_By>
  sum?: Maybe<Messages_Events_Updated_Sum_Order_By>
  var_pop?: Maybe<Messages_Events_Updated_Var_Pop_Order_By>
  var_samp?: Maybe<Messages_Events_Updated_Var_Samp_Order_By>
  variance?: Maybe<Messages_Events_Updated_Variance_Order_By>
}

/** input type for inserting array relation for remote table "messages_events_updated" */
export type Messages_Events_Updated_Arr_Rel_Insert_Input = {
  data: Array<Messages_Events_Updated_Insert_Input>
  on_conflict?: Maybe<Messages_Events_Updated_On_Conflict>
}

/** aggregate avg on columns */
export type Messages_Events_Updated_Avg_Fields = {
  __typename?: 'messages_events_updated_avg_fields'
  new_time_for_upload?: Maybe<Scalars['Float']>
  old_time_for_upload?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "messages_events_updated" */
export type Messages_Events_Updated_Avg_Order_By = {
  new_time_for_upload?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "messages_events_updated". All fields are combined with a logical 'AND'. */
export type Messages_Events_Updated_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Messages_Events_Updated_Bool_Exp>>>
  _not?: Maybe<Messages_Events_Updated_Bool_Exp>
  _or?: Maybe<Array<Maybe<Messages_Events_Updated_Bool_Exp>>>
  id?: Maybe<Uuid_Comparison_Exp>
  message?: Maybe<Messages_Bool_Exp>
  message_id?: Maybe<Uuid_Comparison_Exp>
  new_comment?: Maybe<String_Comparison_Exp>
  new_date?: Maybe<Timestamptz_Comparison_Exp>
  new_location?: Maybe<Locations_Bool_Exp>
  new_location_id?: Maybe<Uuid_Comparison_Exp>
  new_name?: Maybe<String_Comparison_Exp>
  new_note?: Maybe<String_Comparison_Exp>
  new_retouching?: Maybe<Boolean_Comparison_Exp>
  new_time_for_upload?: Maybe<Int_Comparison_Exp>
  old_comment?: Maybe<String_Comparison_Exp>
  old_date?: Maybe<Timestamptz_Comparison_Exp>
  old_location?: Maybe<Locations_Bool_Exp>
  old_location_id?: Maybe<Uuid_Comparison_Exp>
  old_name?: Maybe<String_Comparison_Exp>
  old_note?: Maybe<String_Comparison_Exp>
  old_retouching?: Maybe<Boolean_Comparison_Exp>
  old_time_for_upload?: Maybe<Int_Comparison_Exp>
}

/** unique or primary key constraints on table "messages_events_updated" */
export enum Messages_Events_Updated_Constraint {
  /** unique or primary key constraint */
  MessagesEventsUpdatedMessageIdKey = 'messages_events_updated_message_id_key',
  /** unique or primary key constraint */
  MessagesEventsUpdatedPkey = 'messages_events_updated_pkey'
}

/** input type for incrementing integer column in table "messages_events_updated" */
export type Messages_Events_Updated_Inc_Input = {
  new_time_for_upload?: Maybe<Scalars['Int']>
  old_time_for_upload?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "messages_events_updated" */
export type Messages_Events_Updated_Insert_Input = {
  id?: Maybe<Scalars['uuid']>
  message?: Maybe<Messages_Obj_Rel_Insert_Input>
  message_id?: Maybe<Scalars['uuid']>
  new_comment?: Maybe<Scalars['String']>
  new_date?: Maybe<Scalars['timestamptz']>
  new_location?: Maybe<Locations_Obj_Rel_Insert_Input>
  new_location_id?: Maybe<Scalars['uuid']>
  new_name?: Maybe<Scalars['String']>
  new_note?: Maybe<Scalars['String']>
  new_retouching?: Maybe<Scalars['Boolean']>
  new_time_for_upload?: Maybe<Scalars['Int']>
  old_comment?: Maybe<Scalars['String']>
  old_date?: Maybe<Scalars['timestamptz']>
  old_location?: Maybe<Locations_Obj_Rel_Insert_Input>
  old_location_id?: Maybe<Scalars['uuid']>
  old_name?: Maybe<Scalars['String']>
  old_note?: Maybe<Scalars['String']>
  old_retouching?: Maybe<Scalars['Boolean']>
  old_time_for_upload?: Maybe<Scalars['Int']>
}

/** aggregate max on columns */
export type Messages_Events_Updated_Max_Fields = {
  __typename?: 'messages_events_updated_max_fields'
  id?: Maybe<Scalars['uuid']>
  message_id?: Maybe<Scalars['uuid']>
  new_comment?: Maybe<Scalars['String']>
  new_date?: Maybe<Scalars['timestamptz']>
  new_location_id?: Maybe<Scalars['uuid']>
  new_name?: Maybe<Scalars['String']>
  new_note?: Maybe<Scalars['String']>
  new_time_for_upload?: Maybe<Scalars['Int']>
  old_comment?: Maybe<Scalars['String']>
  old_date?: Maybe<Scalars['timestamptz']>
  old_location_id?: Maybe<Scalars['uuid']>
  old_name?: Maybe<Scalars['String']>
  old_note?: Maybe<Scalars['String']>
  old_time_for_upload?: Maybe<Scalars['Int']>
}

/** order by max() on columns of table "messages_events_updated" */
export type Messages_Events_Updated_Max_Order_By = {
  id?: Maybe<Order_By>
  message_id?: Maybe<Order_By>
  new_comment?: Maybe<Order_By>
  new_date?: Maybe<Order_By>
  new_location_id?: Maybe<Order_By>
  new_name?: Maybe<Order_By>
  new_note?: Maybe<Order_By>
  new_time_for_upload?: Maybe<Order_By>
  old_comment?: Maybe<Order_By>
  old_date?: Maybe<Order_By>
  old_location_id?: Maybe<Order_By>
  old_name?: Maybe<Order_By>
  old_note?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Messages_Events_Updated_Min_Fields = {
  __typename?: 'messages_events_updated_min_fields'
  id?: Maybe<Scalars['uuid']>
  message_id?: Maybe<Scalars['uuid']>
  new_comment?: Maybe<Scalars['String']>
  new_date?: Maybe<Scalars['timestamptz']>
  new_location_id?: Maybe<Scalars['uuid']>
  new_name?: Maybe<Scalars['String']>
  new_note?: Maybe<Scalars['String']>
  new_time_for_upload?: Maybe<Scalars['Int']>
  old_comment?: Maybe<Scalars['String']>
  old_date?: Maybe<Scalars['timestamptz']>
  old_location_id?: Maybe<Scalars['uuid']>
  old_name?: Maybe<Scalars['String']>
  old_note?: Maybe<Scalars['String']>
  old_time_for_upload?: Maybe<Scalars['Int']>
}

/** order by min() on columns of table "messages_events_updated" */
export type Messages_Events_Updated_Min_Order_By = {
  id?: Maybe<Order_By>
  message_id?: Maybe<Order_By>
  new_comment?: Maybe<Order_By>
  new_date?: Maybe<Order_By>
  new_location_id?: Maybe<Order_By>
  new_name?: Maybe<Order_By>
  new_note?: Maybe<Order_By>
  new_time_for_upload?: Maybe<Order_By>
  old_comment?: Maybe<Order_By>
  old_date?: Maybe<Order_By>
  old_location_id?: Maybe<Order_By>
  old_name?: Maybe<Order_By>
  old_note?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** response of any mutation on the table "messages_events_updated" */
export type Messages_Events_Updated_Mutation_Response = {
  __typename?: 'messages_events_updated_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Messages_Events_Updated>
}

/** input type for inserting object relation for remote table "messages_events_updated" */
export type Messages_Events_Updated_Obj_Rel_Insert_Input = {
  data: Messages_Events_Updated_Insert_Input
  on_conflict?: Maybe<Messages_Events_Updated_On_Conflict>
}

/** on conflict condition type for table "messages_events_updated" */
export type Messages_Events_Updated_On_Conflict = {
  constraint: Messages_Events_Updated_Constraint
  update_columns: Array<Messages_Events_Updated_Update_Column>
  where?: Maybe<Messages_Events_Updated_Bool_Exp>
}

/** ordering options when selecting data from "messages_events_updated" */
export type Messages_Events_Updated_Order_By = {
  id?: Maybe<Order_By>
  message?: Maybe<Messages_Order_By>
  message_id?: Maybe<Order_By>
  new_comment?: Maybe<Order_By>
  new_date?: Maybe<Order_By>
  new_location?: Maybe<Locations_Order_By>
  new_location_id?: Maybe<Order_By>
  new_name?: Maybe<Order_By>
  new_note?: Maybe<Order_By>
  new_retouching?: Maybe<Order_By>
  new_time_for_upload?: Maybe<Order_By>
  old_comment?: Maybe<Order_By>
  old_date?: Maybe<Order_By>
  old_location?: Maybe<Locations_Order_By>
  old_location_id?: Maybe<Order_By>
  old_name?: Maybe<Order_By>
  old_note?: Maybe<Order_By>
  old_retouching?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** primary key columns input for table: "messages_events_updated" */
export type Messages_Events_Updated_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "messages_events_updated" */
export enum Messages_Events_Updated_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  MessageId = 'message_id',
  /** column name */
  NewComment = 'new_comment',
  /** column name */
  NewDate = 'new_date',
  /** column name */
  NewLocationId = 'new_location_id',
  /** column name */
  NewName = 'new_name',
  /** column name */
  NewNote = 'new_note',
  /** column name */
  NewRetouching = 'new_retouching',
  /** column name */
  NewTimeForUpload = 'new_time_for_upload',
  /** column name */
  OldComment = 'old_comment',
  /** column name */
  OldDate = 'old_date',
  /** column name */
  OldLocationId = 'old_location_id',
  /** column name */
  OldName = 'old_name',
  /** column name */
  OldNote = 'old_note',
  /** column name */
  OldRetouching = 'old_retouching',
  /** column name */
  OldTimeForUpload = 'old_time_for_upload'
}

/** input type for updating data in table "messages_events_updated" */
export type Messages_Events_Updated_Set_Input = {
  id?: Maybe<Scalars['uuid']>
  message_id?: Maybe<Scalars['uuid']>
  new_comment?: Maybe<Scalars['String']>
  new_date?: Maybe<Scalars['timestamptz']>
  new_location_id?: Maybe<Scalars['uuid']>
  new_name?: Maybe<Scalars['String']>
  new_note?: Maybe<Scalars['String']>
  new_retouching?: Maybe<Scalars['Boolean']>
  new_time_for_upload?: Maybe<Scalars['Int']>
  old_comment?: Maybe<Scalars['String']>
  old_date?: Maybe<Scalars['timestamptz']>
  old_location_id?: Maybe<Scalars['uuid']>
  old_name?: Maybe<Scalars['String']>
  old_note?: Maybe<Scalars['String']>
  old_retouching?: Maybe<Scalars['Boolean']>
  old_time_for_upload?: Maybe<Scalars['Int']>
}

/** aggregate stddev on columns */
export type Messages_Events_Updated_Stddev_Fields = {
  __typename?: 'messages_events_updated_stddev_fields'
  new_time_for_upload?: Maybe<Scalars['Float']>
  old_time_for_upload?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "messages_events_updated" */
export type Messages_Events_Updated_Stddev_Order_By = {
  new_time_for_upload?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Messages_Events_Updated_Stddev_Pop_Fields = {
  __typename?: 'messages_events_updated_stddev_pop_fields'
  new_time_for_upload?: Maybe<Scalars['Float']>
  old_time_for_upload?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "messages_events_updated" */
export type Messages_Events_Updated_Stddev_Pop_Order_By = {
  new_time_for_upload?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Messages_Events_Updated_Stddev_Samp_Fields = {
  __typename?: 'messages_events_updated_stddev_samp_fields'
  new_time_for_upload?: Maybe<Scalars['Float']>
  old_time_for_upload?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "messages_events_updated" */
export type Messages_Events_Updated_Stddev_Samp_Order_By = {
  new_time_for_upload?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Messages_Events_Updated_Sum_Fields = {
  __typename?: 'messages_events_updated_sum_fields'
  new_time_for_upload?: Maybe<Scalars['Int']>
  old_time_for_upload?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "messages_events_updated" */
export type Messages_Events_Updated_Sum_Order_By = {
  new_time_for_upload?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** update columns of table "messages_events_updated" */
export enum Messages_Events_Updated_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  MessageId = 'message_id',
  /** column name */
  NewComment = 'new_comment',
  /** column name */
  NewDate = 'new_date',
  /** column name */
  NewLocationId = 'new_location_id',
  /** column name */
  NewName = 'new_name',
  /** column name */
  NewNote = 'new_note',
  /** column name */
  NewRetouching = 'new_retouching',
  /** column name */
  NewTimeForUpload = 'new_time_for_upload',
  /** column name */
  OldComment = 'old_comment',
  /** column name */
  OldDate = 'old_date',
  /** column name */
  OldLocationId = 'old_location_id',
  /** column name */
  OldName = 'old_name',
  /** column name */
  OldNote = 'old_note',
  /** column name */
  OldRetouching = 'old_retouching',
  /** column name */
  OldTimeForUpload = 'old_time_for_upload'
}

/** aggregate var_pop on columns */
export type Messages_Events_Updated_Var_Pop_Fields = {
  __typename?: 'messages_events_updated_var_pop_fields'
  new_time_for_upload?: Maybe<Scalars['Float']>
  old_time_for_upload?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "messages_events_updated" */
export type Messages_Events_Updated_Var_Pop_Order_By = {
  new_time_for_upload?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Messages_Events_Updated_Var_Samp_Fields = {
  __typename?: 'messages_events_updated_var_samp_fields'
  new_time_for_upload?: Maybe<Scalars['Float']>
  old_time_for_upload?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "messages_events_updated" */
export type Messages_Events_Updated_Var_Samp_Order_By = {
  new_time_for_upload?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Messages_Events_Updated_Variance_Fields = {
  __typename?: 'messages_events_updated_variance_fields'
  new_time_for_upload?: Maybe<Scalars['Float']>
  old_time_for_upload?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "messages_events_updated" */
export type Messages_Events_Updated_Variance_Order_By = {
  new_time_for_upload?: Maybe<Order_By>
  old_time_for_upload?: Maybe<Order_By>
}

/** input type for inserting data into table "messages" */
export type Messages_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  data?: Maybe<Scalars['jsonb']>
  event?: Maybe<Events_Obj_Rel_Insert_Input>
  event_id?: Maybe<Scalars['uuid']>
  event_updated?: Maybe<Messages_Events_Updated_Obj_Rel_Insert_Input>
  from_user?: Maybe<Users_Obj_Rel_Insert_Input>
  from_user_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  message?: Maybe<Scalars['String']>
  messages_status?: Maybe<Messages_Statuses_Obj_Rel_Insert_Input>
  messages_type?: Maybe<Messages_Types_Obj_Rel_Insert_Input>
  notification_id?: Maybe<Scalars['uuid']>
  status?: Maybe<Messages_Statuses_Enum>
  system?: Maybe<Scalars['Boolean']>
  to_user?: Maybe<Users_Obj_Rel_Insert_Input>
  to_user_id?: Maybe<Scalars['uuid']>
  type?: Maybe<Messages_Types_Enum>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Messages_Max_Fields = {
  __typename?: 'messages_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  from_user_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  message?: Maybe<Scalars['String']>
  notification_id?: Maybe<Scalars['uuid']>
  to_user_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "messages" */
export type Messages_Max_Order_By = {
  created_at?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  from_user_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  message?: Maybe<Order_By>
  notification_id?: Maybe<Order_By>
  to_user_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Messages_Min_Fields = {
  __typename?: 'messages_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  from_user_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  message?: Maybe<Scalars['String']>
  notification_id?: Maybe<Scalars['uuid']>
  to_user_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "messages" */
export type Messages_Min_Order_By = {
  created_at?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  from_user_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  message?: Maybe<Order_By>
  notification_id?: Maybe<Order_By>
  to_user_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "messages" */
export type Messages_Mutation_Response = {
  __typename?: 'messages_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Messages>
}

/** input type for inserting object relation for remote table "messages" */
export type Messages_Obj_Rel_Insert_Input = {
  data: Messages_Insert_Input
  on_conflict?: Maybe<Messages_On_Conflict>
}

/** on conflict condition type for table "messages" */
export type Messages_On_Conflict = {
  constraint: Messages_Constraint
  update_columns: Array<Messages_Update_Column>
  where?: Maybe<Messages_Bool_Exp>
}

/** ordering options when selecting data from "messages" */
export type Messages_Order_By = {
  created_at?: Maybe<Order_By>
  data?: Maybe<Order_By>
  event?: Maybe<Events_Order_By>
  event_id?: Maybe<Order_By>
  event_updated?: Maybe<Messages_Events_Updated_Order_By>
  from_user?: Maybe<Users_Order_By>
  from_user_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  message?: Maybe<Order_By>
  messages_status?: Maybe<Messages_Statuses_Order_By>
  messages_type?: Maybe<Messages_Types_Order_By>
  notification_id?: Maybe<Order_By>
  status?: Maybe<Order_By>
  system?: Maybe<Order_By>
  to_user?: Maybe<Users_Order_By>
  to_user_id?: Maybe<Order_By>
  type?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "messages" */
export type Messages_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Messages_Prepend_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** select columns of table "messages" */
export enum Messages_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Data = 'data',
  /** column name */
  EventId = 'event_id',
  /** column name */
  FromUserId = 'from_user_id',
  /** column name */
  Id = 'id',
  /** column name */
  Message = 'message',
  /** column name */
  NotificationId = 'notification_id',
  /** column name */
  Status = 'status',
  /** column name */
  System = 'system',
  /** column name */
  ToUserId = 'to_user_id',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "messages" */
export type Messages_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  data?: Maybe<Scalars['jsonb']>
  event_id?: Maybe<Scalars['uuid']>
  from_user_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  message?: Maybe<Scalars['String']>
  notification_id?: Maybe<Scalars['uuid']>
  status?: Maybe<Messages_Statuses_Enum>
  system?: Maybe<Scalars['Boolean']>
  to_user_id?: Maybe<Scalars['uuid']>
  type?: Maybe<Messages_Types_Enum>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** columns and relationships of "messages_statuses" */
export type Messages_Statuses = {
  __typename?: 'messages_statuses'
  /** An array relationship */
  messages: Array<Messages>
  /** An aggregated array relationship */
  messages_aggregate: Messages_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "messages_statuses" */
export type Messages_StatusesMessagesArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** columns and relationships of "messages_statuses" */
export type Messages_StatusesMessages_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** aggregated selection of "messages_statuses" */
export type Messages_Statuses_Aggregate = {
  __typename?: 'messages_statuses_aggregate'
  aggregate?: Maybe<Messages_Statuses_Aggregate_Fields>
  nodes: Array<Messages_Statuses>
}

/** aggregate fields of "messages_statuses" */
export type Messages_Statuses_Aggregate_Fields = {
  __typename?: 'messages_statuses_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Messages_Statuses_Max_Fields>
  min?: Maybe<Messages_Statuses_Min_Fields>
}

/** aggregate fields of "messages_statuses" */
export type Messages_Statuses_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Messages_Statuses_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "messages_statuses" */
export type Messages_Statuses_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Messages_Statuses_Max_Order_By>
  min?: Maybe<Messages_Statuses_Min_Order_By>
}

/** input type for inserting array relation for remote table "messages_statuses" */
export type Messages_Statuses_Arr_Rel_Insert_Input = {
  data: Array<Messages_Statuses_Insert_Input>
  on_conflict?: Maybe<Messages_Statuses_On_Conflict>
}

/** Boolean expression to filter rows from the table "messages_statuses". All fields are combined with a logical 'AND'. */
export type Messages_Statuses_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Messages_Statuses_Bool_Exp>>>
  _not?: Maybe<Messages_Statuses_Bool_Exp>
  _or?: Maybe<Array<Maybe<Messages_Statuses_Bool_Exp>>>
  messages?: Maybe<Messages_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "messages_statuses" */
export enum Messages_Statuses_Constraint {
  /** unique or primary key constraint */
  MessagesStatusesPkey = 'messages_statuses_pkey'
}

export enum Messages_Statuses_Enum {
  Delivered = 'DELIVERED',
  New = 'NEW',
  Read = 'READ'
}

/** expression to compare columns of type messages_statuses_enum. All fields are combined with logical 'AND'. */
export type Messages_Statuses_Enum_Comparison_Exp = {
  _eq?: Maybe<Messages_Statuses_Enum>
  _in?: Maybe<Array<Messages_Statuses_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Messages_Statuses_Enum>
  _nin?: Maybe<Array<Messages_Statuses_Enum>>
}

/** input type for inserting data into table "messages_statuses" */
export type Messages_Statuses_Insert_Input = {
  messages?: Maybe<Messages_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Messages_Statuses_Max_Fields = {
  __typename?: 'messages_statuses_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "messages_statuses" */
export type Messages_Statuses_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Messages_Statuses_Min_Fields = {
  __typename?: 'messages_statuses_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "messages_statuses" */
export type Messages_Statuses_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "messages_statuses" */
export type Messages_Statuses_Mutation_Response = {
  __typename?: 'messages_statuses_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Messages_Statuses>
}

/** input type for inserting object relation for remote table "messages_statuses" */
export type Messages_Statuses_Obj_Rel_Insert_Input = {
  data: Messages_Statuses_Insert_Input
  on_conflict?: Maybe<Messages_Statuses_On_Conflict>
}

/** on conflict condition type for table "messages_statuses" */
export type Messages_Statuses_On_Conflict = {
  constraint: Messages_Statuses_Constraint
  update_columns: Array<Messages_Statuses_Update_Column>
  where?: Maybe<Messages_Statuses_Bool_Exp>
}

/** ordering options when selecting data from "messages_statuses" */
export type Messages_Statuses_Order_By = {
  messages_aggregate?: Maybe<Messages_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "messages_statuses" */
export type Messages_Statuses_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "messages_statuses" */
export enum Messages_Statuses_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "messages_statuses" */
export type Messages_Statuses_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "messages_statuses" */
export enum Messages_Statuses_Update_Column {
  /** column name */
  Value = 'value'
}

/** columns and relationships of "messages_types" */
export type Messages_Types = {
  __typename?: 'messages_types'
  /** An array relationship */
  messages: Array<Messages>
  /** An aggregated array relationship */
  messages_aggregate: Messages_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "messages_types" */
export type Messages_TypesMessagesArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** columns and relationships of "messages_types" */
export type Messages_TypesMessages_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** aggregated selection of "messages_types" */
export type Messages_Types_Aggregate = {
  __typename?: 'messages_types_aggregate'
  aggregate?: Maybe<Messages_Types_Aggregate_Fields>
  nodes: Array<Messages_Types>
}

/** aggregate fields of "messages_types" */
export type Messages_Types_Aggregate_Fields = {
  __typename?: 'messages_types_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Messages_Types_Max_Fields>
  min?: Maybe<Messages_Types_Min_Fields>
}

/** aggregate fields of "messages_types" */
export type Messages_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Messages_Types_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "messages_types" */
export type Messages_Types_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Messages_Types_Max_Order_By>
  min?: Maybe<Messages_Types_Min_Order_By>
}

/** input type for inserting array relation for remote table "messages_types" */
export type Messages_Types_Arr_Rel_Insert_Input = {
  data: Array<Messages_Types_Insert_Input>
  on_conflict?: Maybe<Messages_Types_On_Conflict>
}

/** Boolean expression to filter rows from the table "messages_types". All fields are combined with a logical 'AND'. */
export type Messages_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Messages_Types_Bool_Exp>>>
  _not?: Maybe<Messages_Types_Bool_Exp>
  _or?: Maybe<Array<Maybe<Messages_Types_Bool_Exp>>>
  messages?: Maybe<Messages_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "messages_types" */
export enum Messages_Types_Constraint {
  /** unique or primary key constraint */
  MessagesTypesPkey = 'messages_types_pkey'
}

export enum Messages_Types_Enum {
  Default = 'DEFAULT',
  SessionCanceled = 'SESSION_CANCELED',
  SessionChanged = 'SESSION_CHANGED',
  SessionDeclined = 'SESSION_DECLINED',
  SessionFinished = 'SESSION_FINISHED',
  SessionOffer = 'SESSION_OFFER',
  SessionRequest = 'SESSION_REQUEST',
  SessionStarted = 'SESSION_STARTED'
}

/** expression to compare columns of type messages_types_enum. All fields are combined with logical 'AND'. */
export type Messages_Types_Enum_Comparison_Exp = {
  _eq?: Maybe<Messages_Types_Enum>
  _in?: Maybe<Array<Messages_Types_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Messages_Types_Enum>
  _nin?: Maybe<Array<Messages_Types_Enum>>
}

/** input type for inserting data into table "messages_types" */
export type Messages_Types_Insert_Input = {
  messages?: Maybe<Messages_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Messages_Types_Max_Fields = {
  __typename?: 'messages_types_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "messages_types" */
export type Messages_Types_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Messages_Types_Min_Fields = {
  __typename?: 'messages_types_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "messages_types" */
export type Messages_Types_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "messages_types" */
export type Messages_Types_Mutation_Response = {
  __typename?: 'messages_types_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Messages_Types>
}

/** input type for inserting object relation for remote table "messages_types" */
export type Messages_Types_Obj_Rel_Insert_Input = {
  data: Messages_Types_Insert_Input
  on_conflict?: Maybe<Messages_Types_On_Conflict>
}

/** on conflict condition type for table "messages_types" */
export type Messages_Types_On_Conflict = {
  constraint: Messages_Types_Constraint
  update_columns: Array<Messages_Types_Update_Column>
  where?: Maybe<Messages_Types_Bool_Exp>
}

/** ordering options when selecting data from "messages_types" */
export type Messages_Types_Order_By = {
  messages_aggregate?: Maybe<Messages_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "messages_types" */
export type Messages_Types_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "messages_types" */
export enum Messages_Types_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "messages_types" */
export type Messages_Types_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "messages_types" */
export enum Messages_Types_Update_Column {
  /** column name */
  Value = 'value'
}

/** update columns of table "messages" */
export enum Messages_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Data = 'data',
  /** column name */
  EventId = 'event_id',
  /** column name */
  FromUserId = 'from_user_id',
  /** column name */
  Id = 'id',
  /** column name */
  Message = 'message',
  /** column name */
  NotificationId = 'notification_id',
  /** column name */
  Status = 'status',
  /** column name */
  System = 'system',
  /** column name */
  ToUserId = 'to_user_id',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at'
}

export type Mutation = {
  __typename?: 'Mutation'
  change_event_status: StatusOutput
  update_last_session: StatusOutput
}

export type MutationChange_Event_StatusArgs = {
  id: Scalars['uuid']
  status: Scalars['String']
}

/** mutation root */
export type Mutation_Root = {
  __typename?: 'mutation_root'
  change_event_status: StatusOutput
  /** delete data from the table: "albums" */
  delete_albums?: Maybe<Albums_Mutation_Response>
  /** delete single row from the table: "albums" */
  delete_albums_by_pk?: Maybe<Albums>
  /** delete data from the table: "albums_payment_types" */
  delete_albums_payment_types?: Maybe<Albums_Payment_Types_Mutation_Response>
  /** delete single row from the table: "albums_payment_types" */
  delete_albums_payment_types_by_pk?: Maybe<Albums_Payment_Types>
  /** delete data from the table: "albums_statistics_view" */
  delete_albums_statistics_view?: Maybe<
    Albums_Statistics_View_Mutation_Response
  >
  /** delete data from the table: "albums_tags" */
  delete_albums_tags?: Maybe<Albums_Tags_Mutation_Response>
  /** delete single row from the table: "albums_tags" */
  delete_albums_tags_by_pk?: Maybe<Albums_Tags>
  /** delete data from the table: "albums_visits" */
  delete_albums_visits?: Maybe<Albums_Visits_Mutation_Response>
  /** delete single row from the table: "albums_visits" */
  delete_albums_visits_by_pk?: Maybe<Albums_Visits>
  /** delete data from the table: "auth.account_providers" */
  delete_auth_account_providers?: Maybe<
    Auth_Account_Providers_Mutation_Response
  >
  /** delete single row from the table: "auth.account_providers" */
  delete_auth_account_providers_by_pk?: Maybe<Auth_Account_Providers>
  /** delete data from the table: "auth.account_roles" */
  delete_auth_account_roles?: Maybe<Auth_Account_Roles_Mutation_Response>
  /** delete single row from the table: "auth.account_roles" */
  delete_auth_account_roles_by_pk?: Maybe<Auth_Account_Roles>
  /** delete data from the table: "auth.accounts" */
  delete_auth_accounts?: Maybe<Auth_Accounts_Mutation_Response>
  /** delete single row from the table: "auth.accounts" */
  delete_auth_accounts_by_pk?: Maybe<Auth_Accounts>
  /** delete data from the table: "auth.providers" */
  delete_auth_providers?: Maybe<Auth_Providers_Mutation_Response>
  /** delete single row from the table: "auth.providers" */
  delete_auth_providers_by_pk?: Maybe<Auth_Providers>
  /** delete data from the table: "auth.refresh_tokens" */
  delete_auth_refresh_tokens?: Maybe<Auth_Refresh_Tokens_Mutation_Response>
  /** delete single row from the table: "auth.refresh_tokens" */
  delete_auth_refresh_tokens_by_pk?: Maybe<Auth_Refresh_Tokens>
  /** delete data from the table: "auth.roles" */
  delete_auth_roles?: Maybe<Auth_Roles_Mutation_Response>
  /** delete single row from the table: "auth.roles" */
  delete_auth_roles_by_pk?: Maybe<Auth_Roles>
  /** delete data from the table: "config" */
  delete_config?: Maybe<Config_Mutation_Response>
  /** delete single row from the table: "config" */
  delete_config_by_pk?: Maybe<Config>
  /** delete data from the table: "config_keys" */
  delete_config_keys?: Maybe<Config_Keys_Mutation_Response>
  /** delete single row from the table: "config_keys" */
  delete_config_keys_by_pk?: Maybe<Config_Keys>
  /** delete data from the table: "contacts" */
  delete_contacts?: Maybe<Contacts_Mutation_Response>
  /** delete single row from the table: "contacts" */
  delete_contacts_by_pk?: Maybe<Contacts>
  /** delete data from the table: "cover_images" */
  delete_cover_images?: Maybe<Cover_Images_Mutation_Response>
  /** delete single row from the table: "cover_images" */
  delete_cover_images_by_pk?: Maybe<Cover_Images>
  /** delete data from the table: "credit_cards" */
  delete_credit_cards?: Maybe<Credit_Cards_Mutation_Response>
  /** delete single row from the table: "credit_cards" */
  delete_credit_cards_by_pk?: Maybe<Credit_Cards>
  /** delete data from the table: "downloads" */
  delete_downloads?: Maybe<Downloads_Mutation_Response>
  /** delete single row from the table: "downloads" */
  delete_downloads_by_pk?: Maybe<Downloads>
  /** delete data from the table: "events" */
  delete_events?: Maybe<Events_Mutation_Response>
  /** delete single row from the table: "events" */
  delete_events_by_pk?: Maybe<Events>
  /** delete data from the table: "events_payment_types" */
  delete_events_payment_types?: Maybe<Events_Payment_Types_Mutation_Response>
  /** delete single row from the table: "events_payment_types" */
  delete_events_payment_types_by_pk?: Maybe<Events_Payment_Types>
  /** delete data from the table: "events_statuses" */
  delete_events_statuses?: Maybe<Events_Statuses_Mutation_Response>
  /** delete single row from the table: "events_statuses" */
  delete_events_statuses_by_pk?: Maybe<Events_Statuses>
  /** delete data from the table: "favorites" */
  delete_favorites?: Maybe<Favorites_Mutation_Response>
  /** delete single row from the table: "favorites" */
  delete_favorites_by_pk?: Maybe<Favorites>
  /** delete data from the table: "folders" */
  delete_folders?: Maybe<Folders_Mutation_Response>
  /** delete single row from the table: "folders" */
  delete_folders_by_pk?: Maybe<Folders>
  /** delete data from the table: "geometry_columns" */
  delete_geometry_columns?: Maybe<Geometry_Columns_Mutation_Response>
  /** delete data from the table: "locales" */
  delete_locales?: Maybe<Locales_Mutation_Response>
  /** delete single row from the table: "locales" */
  delete_locales_by_pk?: Maybe<Locales>
  /** delete data from the table: "localizations" */
  delete_localizations?: Maybe<Localizations_Mutation_Response>
  /** delete single row from the table: "localizations" */
  delete_localizations_by_pk?: Maybe<Localizations>
  /** delete data from the table: "locations" */
  delete_locations?: Maybe<Locations_Mutation_Response>
  /** delete single row from the table: "locations" */
  delete_locations_by_pk?: Maybe<Locations>
  /** delete data from the table: "maker_types" */
  delete_maker_types?: Maybe<Maker_Types_Mutation_Response>
  /** delete single row from the table: "maker_types" */
  delete_maker_types_by_pk?: Maybe<Maker_Types>
  /** delete data from the table: "makers" */
  delete_makers?: Maybe<Makers_Mutation_Response>
  /** delete single row from the table: "makers" */
  delete_makers_by_pk?: Maybe<Makers>
  /** delete data from the table: "media_file_types" */
  delete_media_file_types?: Maybe<Media_File_Types_Mutation_Response>
  /** delete single row from the table: "media_file_types" */
  delete_media_file_types_by_pk?: Maybe<Media_File_Types>
  /** delete data from the table: "media_files" */
  delete_media_files?: Maybe<Media_Files_Mutation_Response>
  /** delete single row from the table: "media_files" */
  delete_media_files_by_pk?: Maybe<Media_Files>
  /** delete data from the table: "menu_items" */
  delete_menu_items?: Maybe<Menu_Items_Mutation_Response>
  /** delete single row from the table: "menu_items" */
  delete_menu_items_by_pk?: Maybe<Menu_Items>
  /** delete data from the table: "menu_pages" */
  delete_menu_pages?: Maybe<Menu_Pages_Mutation_Response>
  /** delete single row from the table: "menu_pages" */
  delete_menu_pages_by_pk?: Maybe<Menu_Pages>
  /** delete data from the table: "menu_type" */
  delete_menu_type?: Maybe<Menu_Type_Mutation_Response>
  /** delete single row from the table: "menu_type" */
  delete_menu_type_by_pk?: Maybe<Menu_Type>
  /** delete data from the table: "messages" */
  delete_messages?: Maybe<Messages_Mutation_Response>
  /** delete single row from the table: "messages" */
  delete_messages_by_pk?: Maybe<Messages>
  /** delete data from the table: "messages_events_updated" */
  delete_messages_events_updated?: Maybe<
    Messages_Events_Updated_Mutation_Response
  >
  /** delete single row from the table: "messages_events_updated" */
  delete_messages_events_updated_by_pk?: Maybe<Messages_Events_Updated>
  /** delete data from the table: "messages_statuses" */
  delete_messages_statuses?: Maybe<Messages_Statuses_Mutation_Response>
  /** delete single row from the table: "messages_statuses" */
  delete_messages_statuses_by_pk?: Maybe<Messages_Statuses>
  /** delete data from the table: "messages_types" */
  delete_messages_types?: Maybe<Messages_Types_Mutation_Response>
  /** delete single row from the table: "messages_types" */
  delete_messages_types_by_pk?: Maybe<Messages_Types>
  /** delete data from the table: "network_transactions" */
  delete_network_transactions?: Maybe<Network_Transactions_Mutation_Response>
  /** delete single row from the table: "network_transactions" */
  delete_network_transactions_by_pk?: Maybe<Network_Transactions>
  /** delete data from the table: "pages" */
  delete_pages?: Maybe<Pages_Mutation_Response>
  /** delete single row from the table: "pages" */
  delete_pages_by_pk?: Maybe<Pages>
  /** delete data from the table: "pages_contents" */
  delete_pages_contents?: Maybe<Pages_Contents_Mutation_Response>
  /** delete data from the table: "pages_contents_blocks" */
  delete_pages_contents_blocks?: Maybe<Pages_Contents_Blocks_Mutation_Response>
  /** delete single row from the table: "pages_contents_blocks" */
  delete_pages_contents_blocks_by_pk?: Maybe<Pages_Contents_Blocks>
  /** delete single row from the table: "pages_contents" */
  delete_pages_contents_by_pk?: Maybe<Pages_Contents>
  /** delete data from the table: "payment_types" */
  delete_payment_types?: Maybe<Payment_Types_Mutation_Response>
  /** delete single row from the table: "payment_types" */
  delete_payment_types_by_pk?: Maybe<Payment_Types>
  /** delete data from the table: "permissions" */
  delete_permissions?: Maybe<Permissions_Mutation_Response>
  /** delete single row from the table: "permissions" */
  delete_permissions_by_pk?: Maybe<Permissions>
  /** delete data from the table: "purchased" */
  delete_purchased?: Maybe<Purchased_Mutation_Response>
  /** delete single row from the table: "purchased" */
  delete_purchased_by_pk?: Maybe<Purchased>
  /** delete data from the table: "reviews" */
  delete_reviews?: Maybe<Reviews_Mutation_Response>
  /** delete single row from the table: "reviews" */
  delete_reviews_by_pk?: Maybe<Reviews>
  /** delete data from the table: "seo" */
  delete_seo?: Maybe<Seo_Mutation_Response>
  /** delete single row from the table: "seo" */
  delete_seo_by_pk?: Maybe<Seo>
  /** delete data from the table: "spatial_ref_sys" */
  delete_spatial_ref_sys?: Maybe<Spatial_Ref_Sys_Mutation_Response>
  /** delete single row from the table: "spatial_ref_sys" */
  delete_spatial_ref_sys_by_pk?: Maybe<Spatial_Ref_Sys>
  /** delete data from the table: "storages" */
  delete_storages?: Maybe<Storages_Mutation_Response>
  /** delete single row from the table: "storages" */
  delete_storages_by_pk?: Maybe<Storages>
  /** delete data from the table: "storages_view" */
  delete_storages_view?: Maybe<Storages_View_Mutation_Response>
  /** delete data from the table: "store" */
  delete_store?: Maybe<Store_Mutation_Response>
  /** delete single row from the table: "store" */
  delete_store_by_pk?: Maybe<Store>
  /** delete data from the table: "store_categories" */
  delete_store_categories?: Maybe<Store_Categories_Mutation_Response>
  /** delete single row from the table: "store_categories" */
  delete_store_categories_by_pk?: Maybe<Store_Categories>
  /** delete data from the table: "store_types" */
  delete_store_types?: Maybe<Store_Types_Mutation_Response>
  /** delete single row from the table: "store_types" */
  delete_store_types_by_pk?: Maybe<Store_Types>
  /** delete data from the table: "styles" */
  delete_styles?: Maybe<Styles_Mutation_Response>
  /** delete single row from the table: "styles" */
  delete_styles_by_pk?: Maybe<Styles>
  /** delete data from the table: "system_messages" */
  delete_system_messages?: Maybe<System_Messages_Mutation_Response>
  /** delete single row from the table: "system_messages" */
  delete_system_messages_by_pk?: Maybe<System_Messages>
  /** delete data from the table: "tags" */
  delete_tags?: Maybe<Tags_Mutation_Response>
  /** delete single row from the table: "tags" */
  delete_tags_by_pk?: Maybe<Tags>
  /** delete data from the table: "tariffs" */
  delete_tariffs?: Maybe<Tariffs_Mutation_Response>
  /** delete single row from the table: "tariffs" */
  delete_tariffs_by_pk?: Maybe<Tariffs>
  /** delete data from the table: "transaction_status" */
  delete_transaction_status?: Maybe<Transaction_Status_Mutation_Response>
  /** delete single row from the table: "transaction_status" */
  delete_transaction_status_by_pk?: Maybe<Transaction_Status>
  /** delete data from the table: "transaction_type" */
  delete_transaction_type?: Maybe<Transaction_Type_Mutation_Response>
  /** delete single row from the table: "transaction_type" */
  delete_transaction_type_by_pk?: Maybe<Transaction_Type>
  /** delete data from the table: "transactions" */
  delete_transactions?: Maybe<Transactions_Mutation_Response>
  /** delete single row from the table: "transactions" */
  delete_transactions_by_pk?: Maybe<Transactions>
  /** delete data from the table: "users" */
  delete_users?: Maybe<Users_Mutation_Response>
  /** delete single row from the table: "users" */
  delete_users_by_pk?: Maybe<Users>
  /** delete data from the table: "users_maker_types" */
  delete_users_maker_types?: Maybe<Users_Maker_Types_Mutation_Response>
  /** delete single row from the table: "users_maker_types" */
  delete_users_maker_types_by_pk?: Maybe<Users_Maker_Types>
  /** delete data from the table: "users_makers" */
  delete_users_makers?: Maybe<Users_Makers_Mutation_Response>
  /** delete single row from the table: "users_makers" */
  delete_users_makers_by_pk?: Maybe<Users_Makers>
  /** delete data from the table: "users_payment_types" */
  delete_users_payment_types?: Maybe<Users_Payment_Types_Mutation_Response>
  /** delete single row from the table: "users_payment_types" */
  delete_users_payment_types_by_pk?: Maybe<Users_Payment_Types>
  /** delete data from the table: "users_permissions" */
  delete_users_permissions?: Maybe<Users_Permissions_Mutation_Response>
  /** delete single row from the table: "users_permissions" */
  delete_users_permissions_by_pk?: Maybe<Users_Permissions>
  /** delete data from the table: "users_search_media_files" */
  delete_users_search_media_files?: Maybe<
    Users_Search_Media_Files_Mutation_Response
  >
  /** delete single row from the table: "users_search_media_files" */
  delete_users_search_media_files_by_pk?: Maybe<Users_Search_Media_Files>
  /** delete data from the table: "users_stats" */
  delete_users_stats?: Maybe<Users_Stats_Mutation_Response>
  /** delete single row from the table: "users_stats" */
  delete_users_stats_by_pk?: Maybe<Users_Stats>
  /** delete data from the table: "users_styles" */
  delete_users_styles?: Maybe<Users_Styles_Mutation_Response>
  /** delete single row from the table: "users_styles" */
  delete_users_styles_by_pk?: Maybe<Users_Styles>
  /** delete data from the table: "users_tariffs" */
  delete_users_tariffs?: Maybe<Users_Tariffs_Mutation_Response>
  /** delete single row from the table: "users_tariffs" */
  delete_users_tariffs_by_pk?: Maybe<Users_Tariffs>
  /** delete data from the table: "wallets" */
  delete_wallets?: Maybe<Wallets_Mutation_Response>
  /** delete single row from the table: "wallets" */
  delete_wallets_by_pk?: Maybe<Wallets>
  /** insert data into the table: "albums" */
  insert_albums?: Maybe<Albums_Mutation_Response>
  /** insert a single row into the table: "albums" */
  insert_albums_one?: Maybe<Albums>
  /** insert data into the table: "albums_payment_types" */
  insert_albums_payment_types?: Maybe<Albums_Payment_Types_Mutation_Response>
  /** insert a single row into the table: "albums_payment_types" */
  insert_albums_payment_types_one?: Maybe<Albums_Payment_Types>
  /** insert data into the table: "albums_statistics_view" */
  insert_albums_statistics_view?: Maybe<
    Albums_Statistics_View_Mutation_Response
  >
  /** insert a single row into the table: "albums_statistics_view" */
  insert_albums_statistics_view_one?: Maybe<Albums_Statistics_View>
  /** insert data into the table: "albums_tags" */
  insert_albums_tags?: Maybe<Albums_Tags_Mutation_Response>
  /** insert a single row into the table: "albums_tags" */
  insert_albums_tags_one?: Maybe<Albums_Tags>
  /** insert data into the table: "albums_visits" */
  insert_albums_visits?: Maybe<Albums_Visits_Mutation_Response>
  /** insert a single row into the table: "albums_visits" */
  insert_albums_visits_one?: Maybe<Albums_Visits>
  /** insert data into the table: "auth.account_providers" */
  insert_auth_account_providers?: Maybe<
    Auth_Account_Providers_Mutation_Response
  >
  /** insert a single row into the table: "auth.account_providers" */
  insert_auth_account_providers_one?: Maybe<Auth_Account_Providers>
  /** insert data into the table: "auth.account_roles" */
  insert_auth_account_roles?: Maybe<Auth_Account_Roles_Mutation_Response>
  /** insert a single row into the table: "auth.account_roles" */
  insert_auth_account_roles_one?: Maybe<Auth_Account_Roles>
  /** insert data into the table: "auth.accounts" */
  insert_auth_accounts?: Maybe<Auth_Accounts_Mutation_Response>
  /** insert a single row into the table: "auth.accounts" */
  insert_auth_accounts_one?: Maybe<Auth_Accounts>
  /** insert data into the table: "auth.providers" */
  insert_auth_providers?: Maybe<Auth_Providers_Mutation_Response>
  /** insert a single row into the table: "auth.providers" */
  insert_auth_providers_one?: Maybe<Auth_Providers>
  /** insert data into the table: "auth.refresh_tokens" */
  insert_auth_refresh_tokens?: Maybe<Auth_Refresh_Tokens_Mutation_Response>
  /** insert a single row into the table: "auth.refresh_tokens" */
  insert_auth_refresh_tokens_one?: Maybe<Auth_Refresh_Tokens>
  /** insert data into the table: "auth.roles" */
  insert_auth_roles?: Maybe<Auth_Roles_Mutation_Response>
  /** insert a single row into the table: "auth.roles" */
  insert_auth_roles_one?: Maybe<Auth_Roles>
  /** insert data into the table: "config" */
  insert_config?: Maybe<Config_Mutation_Response>
  /** insert data into the table: "config_keys" */
  insert_config_keys?: Maybe<Config_Keys_Mutation_Response>
  /** insert a single row into the table: "config_keys" */
  insert_config_keys_one?: Maybe<Config_Keys>
  /** insert a single row into the table: "config" */
  insert_config_one?: Maybe<Config>
  /** insert data into the table: "contacts" */
  insert_contacts?: Maybe<Contacts_Mutation_Response>
  /** insert a single row into the table: "contacts" */
  insert_contacts_one?: Maybe<Contacts>
  /** insert data into the table: "cover_images" */
  insert_cover_images?: Maybe<Cover_Images_Mutation_Response>
  /** insert a single row into the table: "cover_images" */
  insert_cover_images_one?: Maybe<Cover_Images>
  /** insert data into the table: "credit_cards" */
  insert_credit_cards?: Maybe<Credit_Cards_Mutation_Response>
  /** insert a single row into the table: "credit_cards" */
  insert_credit_cards_one?: Maybe<Credit_Cards>
  /** insert data into the table: "downloads" */
  insert_downloads?: Maybe<Downloads_Mutation_Response>
  /** insert a single row into the table: "downloads" */
  insert_downloads_one?: Maybe<Downloads>
  /** insert data into the table: "events" */
  insert_events?: Maybe<Events_Mutation_Response>
  /** insert a single row into the table: "events" */
  insert_events_one?: Maybe<Events>
  /** insert data into the table: "events_payment_types" */
  insert_events_payment_types?: Maybe<Events_Payment_Types_Mutation_Response>
  /** insert a single row into the table: "events_payment_types" */
  insert_events_payment_types_one?: Maybe<Events_Payment_Types>
  /** insert data into the table: "events_statuses" */
  insert_events_statuses?: Maybe<Events_Statuses_Mutation_Response>
  /** insert a single row into the table: "events_statuses" */
  insert_events_statuses_one?: Maybe<Events_Statuses>
  /** insert data into the table: "favorites" */
  insert_favorites?: Maybe<Favorites_Mutation_Response>
  /** insert a single row into the table: "favorites" */
  insert_favorites_one?: Maybe<Favorites>
  /** insert data into the table: "folders" */
  insert_folders?: Maybe<Folders_Mutation_Response>
  /** insert a single row into the table: "folders" */
  insert_folders_one?: Maybe<Folders>
  /** insert data into the table: "geometry_columns" */
  insert_geometry_columns?: Maybe<Geometry_Columns_Mutation_Response>
  /** insert a single row into the table: "geometry_columns" */
  insert_geometry_columns_one?: Maybe<Geometry_Columns>
  /** insert data into the table: "locales" */
  insert_locales?: Maybe<Locales_Mutation_Response>
  /** insert a single row into the table: "locales" */
  insert_locales_one?: Maybe<Locales>
  /** insert data into the table: "localizations" */
  insert_localizations?: Maybe<Localizations_Mutation_Response>
  /** insert a single row into the table: "localizations" */
  insert_localizations_one?: Maybe<Localizations>
  /** insert data into the table: "locations" */
  insert_locations?: Maybe<Locations_Mutation_Response>
  /** insert a single row into the table: "locations" */
  insert_locations_one?: Maybe<Locations>
  /** insert data into the table: "maker_types" */
  insert_maker_types?: Maybe<Maker_Types_Mutation_Response>
  /** insert a single row into the table: "maker_types" */
  insert_maker_types_one?: Maybe<Maker_Types>
  /** insert data into the table: "makers" */
  insert_makers?: Maybe<Makers_Mutation_Response>
  /** insert a single row into the table: "makers" */
  insert_makers_one?: Maybe<Makers>
  /** insert data into the table: "media_file_types" */
  insert_media_file_types?: Maybe<Media_File_Types_Mutation_Response>
  /** insert a single row into the table: "media_file_types" */
  insert_media_file_types_one?: Maybe<Media_File_Types>
  /** insert data into the table: "media_files" */
  insert_media_files?: Maybe<Media_Files_Mutation_Response>
  /** insert a single row into the table: "media_files" */
  insert_media_files_one?: Maybe<Media_Files>
  /** insert data into the table: "menu_items" */
  insert_menu_items?: Maybe<Menu_Items_Mutation_Response>
  /** insert a single row into the table: "menu_items" */
  insert_menu_items_one?: Maybe<Menu_Items>
  /** insert data into the table: "menu_pages" */
  insert_menu_pages?: Maybe<Menu_Pages_Mutation_Response>
  /** insert a single row into the table: "menu_pages" */
  insert_menu_pages_one?: Maybe<Menu_Pages>
  /** insert data into the table: "menu_type" */
  insert_menu_type?: Maybe<Menu_Type_Mutation_Response>
  /** insert a single row into the table: "menu_type" */
  insert_menu_type_one?: Maybe<Menu_Type>
  /** insert data into the table: "messages" */
  insert_messages?: Maybe<Messages_Mutation_Response>
  /** insert data into the table: "messages_events_updated" */
  insert_messages_events_updated?: Maybe<
    Messages_Events_Updated_Mutation_Response
  >
  /** insert a single row into the table: "messages_events_updated" */
  insert_messages_events_updated_one?: Maybe<Messages_Events_Updated>
  /** insert a single row into the table: "messages" */
  insert_messages_one?: Maybe<Messages>
  /** insert data into the table: "messages_statuses" */
  insert_messages_statuses?: Maybe<Messages_Statuses_Mutation_Response>
  /** insert a single row into the table: "messages_statuses" */
  insert_messages_statuses_one?: Maybe<Messages_Statuses>
  /** insert data into the table: "messages_types" */
  insert_messages_types?: Maybe<Messages_Types_Mutation_Response>
  /** insert a single row into the table: "messages_types" */
  insert_messages_types_one?: Maybe<Messages_Types>
  /** insert data into the table: "network_transactions" */
  insert_network_transactions?: Maybe<Network_Transactions_Mutation_Response>
  /** insert a single row into the table: "network_transactions" */
  insert_network_transactions_one?: Maybe<Network_Transactions>
  /** insert data into the table: "pages" */
  insert_pages?: Maybe<Pages_Mutation_Response>
  /** insert data into the table: "pages_contents" */
  insert_pages_contents?: Maybe<Pages_Contents_Mutation_Response>
  /** insert data into the table: "pages_contents_blocks" */
  insert_pages_contents_blocks?: Maybe<Pages_Contents_Blocks_Mutation_Response>
  /** insert a single row into the table: "pages_contents_blocks" */
  insert_pages_contents_blocks_one?: Maybe<Pages_Contents_Blocks>
  /** insert a single row into the table: "pages_contents" */
  insert_pages_contents_one?: Maybe<Pages_Contents>
  /** insert a single row into the table: "pages" */
  insert_pages_one?: Maybe<Pages>
  /** insert data into the table: "payment_types" */
  insert_payment_types?: Maybe<Payment_Types_Mutation_Response>
  /** insert a single row into the table: "payment_types" */
  insert_payment_types_one?: Maybe<Payment_Types>
  /** insert data into the table: "permissions" */
  insert_permissions?: Maybe<Permissions_Mutation_Response>
  /** insert a single row into the table: "permissions" */
  insert_permissions_one?: Maybe<Permissions>
  /** insert data into the table: "purchased" */
  insert_purchased?: Maybe<Purchased_Mutation_Response>
  /** insert a single row into the table: "purchased" */
  insert_purchased_one?: Maybe<Purchased>
  /** insert data into the table: "reviews" */
  insert_reviews?: Maybe<Reviews_Mutation_Response>
  /** insert a single row into the table: "reviews" */
  insert_reviews_one?: Maybe<Reviews>
  /** insert data into the table: "seo" */
  insert_seo?: Maybe<Seo_Mutation_Response>
  /** insert a single row into the table: "seo" */
  insert_seo_one?: Maybe<Seo>
  /** insert data into the table: "spatial_ref_sys" */
  insert_spatial_ref_sys?: Maybe<Spatial_Ref_Sys_Mutation_Response>
  /** insert a single row into the table: "spatial_ref_sys" */
  insert_spatial_ref_sys_one?: Maybe<Spatial_Ref_Sys>
  /** insert data into the table: "storages" */
  insert_storages?: Maybe<Storages_Mutation_Response>
  /** insert a single row into the table: "storages" */
  insert_storages_one?: Maybe<Storages>
  /** insert data into the table: "storages_view" */
  insert_storages_view?: Maybe<Storages_View_Mutation_Response>
  /** insert a single row into the table: "storages_view" */
  insert_storages_view_one?: Maybe<Storages_View>
  /** insert data into the table: "store" */
  insert_store?: Maybe<Store_Mutation_Response>
  /** insert data into the table: "store_categories" */
  insert_store_categories?: Maybe<Store_Categories_Mutation_Response>
  /** insert a single row into the table: "store_categories" */
  insert_store_categories_one?: Maybe<Store_Categories>
  /** insert a single row into the table: "store" */
  insert_store_one?: Maybe<Store>
  /** insert data into the table: "store_types" */
  insert_store_types?: Maybe<Store_Types_Mutation_Response>
  /** insert a single row into the table: "store_types" */
  insert_store_types_one?: Maybe<Store_Types>
  /** insert data into the table: "styles" */
  insert_styles?: Maybe<Styles_Mutation_Response>
  /** insert a single row into the table: "styles" */
  insert_styles_one?: Maybe<Styles>
  /** insert data into the table: "system_messages" */
  insert_system_messages?: Maybe<System_Messages_Mutation_Response>
  /** insert a single row into the table: "system_messages" */
  insert_system_messages_one?: Maybe<System_Messages>
  /** insert data into the table: "tags" */
  insert_tags?: Maybe<Tags_Mutation_Response>
  /** insert a single row into the table: "tags" */
  insert_tags_one?: Maybe<Tags>
  /** insert data into the table: "tariffs" */
  insert_tariffs?: Maybe<Tariffs_Mutation_Response>
  /** insert a single row into the table: "tariffs" */
  insert_tariffs_one?: Maybe<Tariffs>
  /** insert data into the table: "transaction_status" */
  insert_transaction_status?: Maybe<Transaction_Status_Mutation_Response>
  /** insert a single row into the table: "transaction_status" */
  insert_transaction_status_one?: Maybe<Transaction_Status>
  /** insert data into the table: "transaction_type" */
  insert_transaction_type?: Maybe<Transaction_Type_Mutation_Response>
  /** insert a single row into the table: "transaction_type" */
  insert_transaction_type_one?: Maybe<Transaction_Type>
  /** insert data into the table: "transactions" */
  insert_transactions?: Maybe<Transactions_Mutation_Response>
  /** insert a single row into the table: "transactions" */
  insert_transactions_one?: Maybe<Transactions>
  /** insert data into the table: "users" */
  insert_users?: Maybe<Users_Mutation_Response>
  /** insert data into the table: "users_maker_types" */
  insert_users_maker_types?: Maybe<Users_Maker_Types_Mutation_Response>
  /** insert a single row into the table: "users_maker_types" */
  insert_users_maker_types_one?: Maybe<Users_Maker_Types>
  /** insert data into the table: "users_makers" */
  insert_users_makers?: Maybe<Users_Makers_Mutation_Response>
  /** insert a single row into the table: "users_makers" */
  insert_users_makers_one?: Maybe<Users_Makers>
  /** insert a single row into the table: "users" */
  insert_users_one?: Maybe<Users>
  /** insert data into the table: "users_payment_types" */
  insert_users_payment_types?: Maybe<Users_Payment_Types_Mutation_Response>
  /** insert a single row into the table: "users_payment_types" */
  insert_users_payment_types_one?: Maybe<Users_Payment_Types>
  /** insert data into the table: "users_permissions" */
  insert_users_permissions?: Maybe<Users_Permissions_Mutation_Response>
  /** insert a single row into the table: "users_permissions" */
  insert_users_permissions_one?: Maybe<Users_Permissions>
  /** insert data into the table: "users_search_media_files" */
  insert_users_search_media_files?: Maybe<
    Users_Search_Media_Files_Mutation_Response
  >
  /** insert a single row into the table: "users_search_media_files" */
  insert_users_search_media_files_one?: Maybe<Users_Search_Media_Files>
  /** insert data into the table: "users_stats" */
  insert_users_stats?: Maybe<Users_Stats_Mutation_Response>
  /** insert a single row into the table: "users_stats" */
  insert_users_stats_one?: Maybe<Users_Stats>
  /** insert data into the table: "users_styles" */
  insert_users_styles?: Maybe<Users_Styles_Mutation_Response>
  /** insert a single row into the table: "users_styles" */
  insert_users_styles_one?: Maybe<Users_Styles>
  /** insert data into the table: "users_tariffs" */
  insert_users_tariffs?: Maybe<Users_Tariffs_Mutation_Response>
  /** insert a single row into the table: "users_tariffs" */
  insert_users_tariffs_one?: Maybe<Users_Tariffs>
  /** insert data into the table: "wallets" */
  insert_wallets?: Maybe<Wallets_Mutation_Response>
  /** insert a single row into the table: "wallets" */
  insert_wallets_one?: Maybe<Wallets>
  /** update data of the table: "albums" */
  update_albums?: Maybe<Albums_Mutation_Response>
  /** update single row of the table: "albums" */
  update_albums_by_pk?: Maybe<Albums>
  /** update data of the table: "albums_payment_types" */
  update_albums_payment_types?: Maybe<Albums_Payment_Types_Mutation_Response>
  /** update single row of the table: "albums_payment_types" */
  update_albums_payment_types_by_pk?: Maybe<Albums_Payment_Types>
  /** update data of the table: "albums_statistics_view" */
  update_albums_statistics_view?: Maybe<
    Albums_Statistics_View_Mutation_Response
  >
  /** update data of the table: "albums_tags" */
  update_albums_tags?: Maybe<Albums_Tags_Mutation_Response>
  /** update single row of the table: "albums_tags" */
  update_albums_tags_by_pk?: Maybe<Albums_Tags>
  /** update data of the table: "albums_visits" */
  update_albums_visits?: Maybe<Albums_Visits_Mutation_Response>
  /** update single row of the table: "albums_visits" */
  update_albums_visits_by_pk?: Maybe<Albums_Visits>
  /** update data of the table: "auth.account_providers" */
  update_auth_account_providers?: Maybe<
    Auth_Account_Providers_Mutation_Response
  >
  /** update single row of the table: "auth.account_providers" */
  update_auth_account_providers_by_pk?: Maybe<Auth_Account_Providers>
  /** update data of the table: "auth.account_roles" */
  update_auth_account_roles?: Maybe<Auth_Account_Roles_Mutation_Response>
  /** update single row of the table: "auth.account_roles" */
  update_auth_account_roles_by_pk?: Maybe<Auth_Account_Roles>
  /** update data of the table: "auth.accounts" */
  update_auth_accounts?: Maybe<Auth_Accounts_Mutation_Response>
  /** update single row of the table: "auth.accounts" */
  update_auth_accounts_by_pk?: Maybe<Auth_Accounts>
  /** update data of the table: "auth.providers" */
  update_auth_providers?: Maybe<Auth_Providers_Mutation_Response>
  /** update single row of the table: "auth.providers" */
  update_auth_providers_by_pk?: Maybe<Auth_Providers>
  /** update data of the table: "auth.refresh_tokens" */
  update_auth_refresh_tokens?: Maybe<Auth_Refresh_Tokens_Mutation_Response>
  /** update single row of the table: "auth.refresh_tokens" */
  update_auth_refresh_tokens_by_pk?: Maybe<Auth_Refresh_Tokens>
  /** update data of the table: "auth.roles" */
  update_auth_roles?: Maybe<Auth_Roles_Mutation_Response>
  /** update single row of the table: "auth.roles" */
  update_auth_roles_by_pk?: Maybe<Auth_Roles>
  /** update data of the table: "config" */
  update_config?: Maybe<Config_Mutation_Response>
  /** update single row of the table: "config" */
  update_config_by_pk?: Maybe<Config>
  /** update data of the table: "config_keys" */
  update_config_keys?: Maybe<Config_Keys_Mutation_Response>
  /** update single row of the table: "config_keys" */
  update_config_keys_by_pk?: Maybe<Config_Keys>
  /** update data of the table: "contacts" */
  update_contacts?: Maybe<Contacts_Mutation_Response>
  /** update single row of the table: "contacts" */
  update_contacts_by_pk?: Maybe<Contacts>
  /** update data of the table: "cover_images" */
  update_cover_images?: Maybe<Cover_Images_Mutation_Response>
  /** update single row of the table: "cover_images" */
  update_cover_images_by_pk?: Maybe<Cover_Images>
  /** update data of the table: "credit_cards" */
  update_credit_cards?: Maybe<Credit_Cards_Mutation_Response>
  /** update single row of the table: "credit_cards" */
  update_credit_cards_by_pk?: Maybe<Credit_Cards>
  /** update data of the table: "downloads" */
  update_downloads?: Maybe<Downloads_Mutation_Response>
  /** update single row of the table: "downloads" */
  update_downloads_by_pk?: Maybe<Downloads>
  /** update data of the table: "events" */
  update_events?: Maybe<Events_Mutation_Response>
  /** update single row of the table: "events" */
  update_events_by_pk?: Maybe<Events>
  /** update data of the table: "events_payment_types" */
  update_events_payment_types?: Maybe<Events_Payment_Types_Mutation_Response>
  /** update single row of the table: "events_payment_types" */
  update_events_payment_types_by_pk?: Maybe<Events_Payment_Types>
  /** update data of the table: "events_statuses" */
  update_events_statuses?: Maybe<Events_Statuses_Mutation_Response>
  /** update single row of the table: "events_statuses" */
  update_events_statuses_by_pk?: Maybe<Events_Statuses>
  /** update data of the table: "favorites" */
  update_favorites?: Maybe<Favorites_Mutation_Response>
  /** update single row of the table: "favorites" */
  update_favorites_by_pk?: Maybe<Favorites>
  /** update data of the table: "folders" */
  update_folders?: Maybe<Folders_Mutation_Response>
  /** update single row of the table: "folders" */
  update_folders_by_pk?: Maybe<Folders>
  /** update data of the table: "geometry_columns" */
  update_geometry_columns?: Maybe<Geometry_Columns_Mutation_Response>
  update_last_session: StatusOutput
  /** update data of the table: "locales" */
  update_locales?: Maybe<Locales_Mutation_Response>
  /** update single row of the table: "locales" */
  update_locales_by_pk?: Maybe<Locales>
  /** update data of the table: "localizations" */
  update_localizations?: Maybe<Localizations_Mutation_Response>
  /** update single row of the table: "localizations" */
  update_localizations_by_pk?: Maybe<Localizations>
  /** update data of the table: "locations" */
  update_locations?: Maybe<Locations_Mutation_Response>
  /** update single row of the table: "locations" */
  update_locations_by_pk?: Maybe<Locations>
  /** update data of the table: "maker_types" */
  update_maker_types?: Maybe<Maker_Types_Mutation_Response>
  /** update single row of the table: "maker_types" */
  update_maker_types_by_pk?: Maybe<Maker_Types>
  /** update data of the table: "makers" */
  update_makers?: Maybe<Makers_Mutation_Response>
  /** update single row of the table: "makers" */
  update_makers_by_pk?: Maybe<Makers>
  /** update data of the table: "media_file_types" */
  update_media_file_types?: Maybe<Media_File_Types_Mutation_Response>
  /** update single row of the table: "media_file_types" */
  update_media_file_types_by_pk?: Maybe<Media_File_Types>
  /** update data of the table: "media_files" */
  update_media_files?: Maybe<Media_Files_Mutation_Response>
  /** update single row of the table: "media_files" */
  update_media_files_by_pk?: Maybe<Media_Files>
  /** update data of the table: "menu_items" */
  update_menu_items?: Maybe<Menu_Items_Mutation_Response>
  /** update single row of the table: "menu_items" */
  update_menu_items_by_pk?: Maybe<Menu_Items>
  /** update data of the table: "menu_pages" */
  update_menu_pages?: Maybe<Menu_Pages_Mutation_Response>
  /** update single row of the table: "menu_pages" */
  update_menu_pages_by_pk?: Maybe<Menu_Pages>
  /** update data of the table: "menu_type" */
  update_menu_type?: Maybe<Menu_Type_Mutation_Response>
  /** update single row of the table: "menu_type" */
  update_menu_type_by_pk?: Maybe<Menu_Type>
  /** update data of the table: "messages" */
  update_messages?: Maybe<Messages_Mutation_Response>
  /** update single row of the table: "messages" */
  update_messages_by_pk?: Maybe<Messages>
  /** update data of the table: "messages_events_updated" */
  update_messages_events_updated?: Maybe<
    Messages_Events_Updated_Mutation_Response
  >
  /** update single row of the table: "messages_events_updated" */
  update_messages_events_updated_by_pk?: Maybe<Messages_Events_Updated>
  /** update data of the table: "messages_statuses" */
  update_messages_statuses?: Maybe<Messages_Statuses_Mutation_Response>
  /** update single row of the table: "messages_statuses" */
  update_messages_statuses_by_pk?: Maybe<Messages_Statuses>
  /** update data of the table: "messages_types" */
  update_messages_types?: Maybe<Messages_Types_Mutation_Response>
  /** update single row of the table: "messages_types" */
  update_messages_types_by_pk?: Maybe<Messages_Types>
  /** update data of the table: "network_transactions" */
  update_network_transactions?: Maybe<Network_Transactions_Mutation_Response>
  /** update single row of the table: "network_transactions" */
  update_network_transactions_by_pk?: Maybe<Network_Transactions>
  /** update data of the table: "pages" */
  update_pages?: Maybe<Pages_Mutation_Response>
  /** update single row of the table: "pages" */
  update_pages_by_pk?: Maybe<Pages>
  /** update data of the table: "pages_contents" */
  update_pages_contents?: Maybe<Pages_Contents_Mutation_Response>
  /** update data of the table: "pages_contents_blocks" */
  update_pages_contents_blocks?: Maybe<Pages_Contents_Blocks_Mutation_Response>
  /** update single row of the table: "pages_contents_blocks" */
  update_pages_contents_blocks_by_pk?: Maybe<Pages_Contents_Blocks>
  /** update single row of the table: "pages_contents" */
  update_pages_contents_by_pk?: Maybe<Pages_Contents>
  /** update data of the table: "payment_types" */
  update_payment_types?: Maybe<Payment_Types_Mutation_Response>
  /** update single row of the table: "payment_types" */
  update_payment_types_by_pk?: Maybe<Payment_Types>
  /** update data of the table: "permissions" */
  update_permissions?: Maybe<Permissions_Mutation_Response>
  /** update single row of the table: "permissions" */
  update_permissions_by_pk?: Maybe<Permissions>
  /** update data of the table: "purchased" */
  update_purchased?: Maybe<Purchased_Mutation_Response>
  /** update single row of the table: "purchased" */
  update_purchased_by_pk?: Maybe<Purchased>
  /** update data of the table: "reviews" */
  update_reviews?: Maybe<Reviews_Mutation_Response>
  /** update single row of the table: "reviews" */
  update_reviews_by_pk?: Maybe<Reviews>
  /** update data of the table: "seo" */
  update_seo?: Maybe<Seo_Mutation_Response>
  /** update single row of the table: "seo" */
  update_seo_by_pk?: Maybe<Seo>
  /** update data of the table: "spatial_ref_sys" */
  update_spatial_ref_sys?: Maybe<Spatial_Ref_Sys_Mutation_Response>
  /** update single row of the table: "spatial_ref_sys" */
  update_spatial_ref_sys_by_pk?: Maybe<Spatial_Ref_Sys>
  /** update data of the table: "storages" */
  update_storages?: Maybe<Storages_Mutation_Response>
  /** update single row of the table: "storages" */
  update_storages_by_pk?: Maybe<Storages>
  /** update data of the table: "storages_view" */
  update_storages_view?: Maybe<Storages_View_Mutation_Response>
  /** update data of the table: "store" */
  update_store?: Maybe<Store_Mutation_Response>
  /** update single row of the table: "store" */
  update_store_by_pk?: Maybe<Store>
  /** update data of the table: "store_categories" */
  update_store_categories?: Maybe<Store_Categories_Mutation_Response>
  /** update single row of the table: "store_categories" */
  update_store_categories_by_pk?: Maybe<Store_Categories>
  /** update data of the table: "store_types" */
  update_store_types?: Maybe<Store_Types_Mutation_Response>
  /** update single row of the table: "store_types" */
  update_store_types_by_pk?: Maybe<Store_Types>
  /** update data of the table: "styles" */
  update_styles?: Maybe<Styles_Mutation_Response>
  /** update single row of the table: "styles" */
  update_styles_by_pk?: Maybe<Styles>
  /** update data of the table: "system_messages" */
  update_system_messages?: Maybe<System_Messages_Mutation_Response>
  /** update single row of the table: "system_messages" */
  update_system_messages_by_pk?: Maybe<System_Messages>
  /** update data of the table: "tags" */
  update_tags?: Maybe<Tags_Mutation_Response>
  /** update single row of the table: "tags" */
  update_tags_by_pk?: Maybe<Tags>
  /** update data of the table: "tariffs" */
  update_tariffs?: Maybe<Tariffs_Mutation_Response>
  /** update single row of the table: "tariffs" */
  update_tariffs_by_pk?: Maybe<Tariffs>
  /** update data of the table: "transaction_status" */
  update_transaction_status?: Maybe<Transaction_Status_Mutation_Response>
  /** update single row of the table: "transaction_status" */
  update_transaction_status_by_pk?: Maybe<Transaction_Status>
  /** update data of the table: "transaction_type" */
  update_transaction_type?: Maybe<Transaction_Type_Mutation_Response>
  /** update single row of the table: "transaction_type" */
  update_transaction_type_by_pk?: Maybe<Transaction_Type>
  /** update data of the table: "transactions" */
  update_transactions?: Maybe<Transactions_Mutation_Response>
  /** update single row of the table: "transactions" */
  update_transactions_by_pk?: Maybe<Transactions>
  /** update data of the table: "users" */
  update_users?: Maybe<Users_Mutation_Response>
  /** update single row of the table: "users" */
  update_users_by_pk?: Maybe<Users>
  /** update data of the table: "users_maker_types" */
  update_users_maker_types?: Maybe<Users_Maker_Types_Mutation_Response>
  /** update single row of the table: "users_maker_types" */
  update_users_maker_types_by_pk?: Maybe<Users_Maker_Types>
  /** update data of the table: "users_makers" */
  update_users_makers?: Maybe<Users_Makers_Mutation_Response>
  /** update single row of the table: "users_makers" */
  update_users_makers_by_pk?: Maybe<Users_Makers>
  /** update data of the table: "users_payment_types" */
  update_users_payment_types?: Maybe<Users_Payment_Types_Mutation_Response>
  /** update single row of the table: "users_payment_types" */
  update_users_payment_types_by_pk?: Maybe<Users_Payment_Types>
  /** update data of the table: "users_permissions" */
  update_users_permissions?: Maybe<Users_Permissions_Mutation_Response>
  /** update single row of the table: "users_permissions" */
  update_users_permissions_by_pk?: Maybe<Users_Permissions>
  /** update data of the table: "users_search_media_files" */
  update_users_search_media_files?: Maybe<
    Users_Search_Media_Files_Mutation_Response
  >
  /** update single row of the table: "users_search_media_files" */
  update_users_search_media_files_by_pk?: Maybe<Users_Search_Media_Files>
  /** update data of the table: "users_stats" */
  update_users_stats?: Maybe<Users_Stats_Mutation_Response>
  /** update single row of the table: "users_stats" */
  update_users_stats_by_pk?: Maybe<Users_Stats>
  /** update data of the table: "users_styles" */
  update_users_styles?: Maybe<Users_Styles_Mutation_Response>
  /** update single row of the table: "users_styles" */
  update_users_styles_by_pk?: Maybe<Users_Styles>
  /** update data of the table: "users_tariffs" */
  update_users_tariffs?: Maybe<Users_Tariffs_Mutation_Response>
  /** update single row of the table: "users_tariffs" */
  update_users_tariffs_by_pk?: Maybe<Users_Tariffs>
  /** update data of the table: "wallets" */
  update_wallets?: Maybe<Wallets_Mutation_Response>
  /** update single row of the table: "wallets" */
  update_wallets_by_pk?: Maybe<Wallets>
}

/** mutation root */
export type Mutation_RootChange_Event_StatusArgs = {
  id: Scalars['uuid']
  status: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_AlbumsArgs = {
  where: Albums_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Albums_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Albums_Payment_TypesArgs = {
  where: Albums_Payment_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Albums_Payment_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Albums_Statistics_ViewArgs = {
  where: Albums_Statistics_View_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Albums_TagsArgs = {
  where: Albums_Tags_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Albums_Tags_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Albums_VisitsArgs = {
  where: Albums_Visits_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Albums_Visits_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Auth_Account_ProvidersArgs = {
  where: Auth_Account_Providers_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Auth_Account_Providers_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Auth_Account_RolesArgs = {
  where: Auth_Account_Roles_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Auth_Account_Roles_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Auth_AccountsArgs = {
  where: Auth_Accounts_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Auth_Accounts_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Auth_ProvidersArgs = {
  where: Auth_Providers_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Auth_Providers_By_PkArgs = {
  provider: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_Auth_Refresh_TokensArgs = {
  where: Auth_Refresh_Tokens_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Auth_Refresh_Tokens_By_PkArgs = {
  refresh_token: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Auth_RolesArgs = {
  where: Auth_Roles_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Auth_Roles_By_PkArgs = {
  role: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_ConfigArgs = {
  where: Config_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Config_By_PkArgs = {
  key: Config_Keys_Enum
}

/** mutation root */
export type Mutation_RootDelete_Config_KeysArgs = {
  where: Config_Keys_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Config_Keys_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_ContactsArgs = {
  where: Contacts_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Contacts_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Cover_ImagesArgs = {
  where: Cover_Images_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Cover_Images_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Credit_CardsArgs = {
  where: Credit_Cards_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Credit_Cards_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_DownloadsArgs = {
  where: Downloads_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Downloads_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_EventsArgs = {
  where: Events_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Events_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Events_Payment_TypesArgs = {
  where: Events_Payment_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Events_Payment_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Events_StatusesArgs = {
  where: Events_Statuses_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Events_Statuses_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_FavoritesArgs = {
  where: Favorites_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Favorites_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_FoldersArgs = {
  where: Folders_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Folders_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Geometry_ColumnsArgs = {
  where: Geometry_Columns_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_LocalesArgs = {
  where: Locales_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Locales_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_LocalizationsArgs = {
  where: Localizations_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Localizations_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_LocationsArgs = {
  where: Locations_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Locations_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Maker_TypesArgs = {
  where: Maker_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Maker_Types_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_MakersArgs = {
  where: Makers_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Makers_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Media_File_TypesArgs = {
  where: Media_File_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Media_File_Types_By_PkArgs = {
  id: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_Media_FilesArgs = {
  where: Media_Files_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Media_Files_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Menu_ItemsArgs = {
  where: Menu_Items_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Menu_Items_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Menu_PagesArgs = {
  where: Menu_Pages_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Menu_Pages_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Menu_TypeArgs = {
  where: Menu_Type_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Menu_Type_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_MessagesArgs = {
  where: Messages_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Messages_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Messages_Events_UpdatedArgs = {
  where: Messages_Events_Updated_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Messages_Events_Updated_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Messages_StatusesArgs = {
  where: Messages_Statuses_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Messages_Statuses_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_Messages_TypesArgs = {
  where: Messages_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Messages_Types_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_Network_TransactionsArgs = {
  where: Network_Transactions_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Network_Transactions_By_PkArgs = {
  network: Scalars['String']
  tx_hash: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_PagesArgs = {
  where: Pages_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Pages_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Pages_ContentsArgs = {
  where: Pages_Contents_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Pages_Contents_BlocksArgs = {
  where: Pages_Contents_Blocks_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Pages_Contents_Blocks_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Pages_Contents_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Payment_TypesArgs = {
  where: Payment_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Payment_Types_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_PermissionsArgs = {
  where: Permissions_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Permissions_By_PkArgs = {
  id: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_PurchasedArgs = {
  where: Purchased_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Purchased_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_ReviewsArgs = {
  where: Reviews_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Reviews_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_SeoArgs = {
  where: Seo_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Seo_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Spatial_Ref_SysArgs = {
  where: Spatial_Ref_Sys_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Spatial_Ref_Sys_By_PkArgs = {
  srid: Scalars['Int']
}

/** mutation root */
export type Mutation_RootDelete_StoragesArgs = {
  where: Storages_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Storages_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Storages_ViewArgs = {
  where: Storages_View_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_StoreArgs = {
  where: Store_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Store_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Store_CategoriesArgs = {
  where: Store_Categories_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Store_Categories_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_Store_TypesArgs = {
  where: Store_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Store_Types_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_StylesArgs = {
  where: Styles_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Styles_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_System_MessagesArgs = {
  where: System_Messages_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_System_Messages_By_PkArgs = {
  id: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_TagsArgs = {
  where: Tags_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Tags_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_TariffsArgs = {
  where: Tariffs_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Tariffs_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Transaction_StatusArgs = {
  where: Transaction_Status_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Transaction_Status_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_Transaction_TypeArgs = {
  where: Transaction_Type_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Transaction_Type_By_PkArgs = {
  value: Scalars['String']
}

/** mutation root */
export type Mutation_RootDelete_TransactionsArgs = {
  where: Transactions_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Transactions_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_UsersArgs = {
  where: Users_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Users_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Users_Maker_TypesArgs = {
  where: Users_Maker_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Users_Maker_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Users_MakersArgs = {
  where: Users_Makers_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Users_Makers_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Users_Payment_TypesArgs = {
  where: Users_Payment_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Users_Payment_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Users_PermissionsArgs = {
  where: Users_Permissions_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Users_Permissions_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Users_Search_Media_FilesArgs = {
  where: Users_Search_Media_Files_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Users_Search_Media_Files_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Users_StatsArgs = {
  where: Users_Stats_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Users_Stats_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Users_StylesArgs = {
  where: Users_Styles_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Users_Styles_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_Users_TariffsArgs = {
  where: Users_Tariffs_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Users_Tariffs_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootDelete_WalletsArgs = {
  where: Wallets_Bool_Exp
}

/** mutation root */
export type Mutation_RootDelete_Wallets_By_PkArgs = {
  id: Scalars['uuid']
}

/** mutation root */
export type Mutation_RootInsert_AlbumsArgs = {
  objects: Array<Albums_Insert_Input>
  on_conflict?: Maybe<Albums_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Albums_OneArgs = {
  object: Albums_Insert_Input
  on_conflict?: Maybe<Albums_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Albums_Payment_TypesArgs = {
  objects: Array<Albums_Payment_Types_Insert_Input>
  on_conflict?: Maybe<Albums_Payment_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Albums_Payment_Types_OneArgs = {
  object: Albums_Payment_Types_Insert_Input
  on_conflict?: Maybe<Albums_Payment_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Albums_Statistics_ViewArgs = {
  objects: Array<Albums_Statistics_View_Insert_Input>
}

/** mutation root */
export type Mutation_RootInsert_Albums_Statistics_View_OneArgs = {
  object: Albums_Statistics_View_Insert_Input
}

/** mutation root */
export type Mutation_RootInsert_Albums_TagsArgs = {
  objects: Array<Albums_Tags_Insert_Input>
  on_conflict?: Maybe<Albums_Tags_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Albums_Tags_OneArgs = {
  object: Albums_Tags_Insert_Input
  on_conflict?: Maybe<Albums_Tags_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Albums_VisitsArgs = {
  objects: Array<Albums_Visits_Insert_Input>
  on_conflict?: Maybe<Albums_Visits_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Albums_Visits_OneArgs = {
  object: Albums_Visits_Insert_Input
  on_conflict?: Maybe<Albums_Visits_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_Account_ProvidersArgs = {
  objects: Array<Auth_Account_Providers_Insert_Input>
  on_conflict?: Maybe<Auth_Account_Providers_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_Account_Providers_OneArgs = {
  object: Auth_Account_Providers_Insert_Input
  on_conflict?: Maybe<Auth_Account_Providers_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_Account_RolesArgs = {
  objects: Array<Auth_Account_Roles_Insert_Input>
  on_conflict?: Maybe<Auth_Account_Roles_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_Account_Roles_OneArgs = {
  object: Auth_Account_Roles_Insert_Input
  on_conflict?: Maybe<Auth_Account_Roles_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_AccountsArgs = {
  objects: Array<Auth_Accounts_Insert_Input>
  on_conflict?: Maybe<Auth_Accounts_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_Accounts_OneArgs = {
  object: Auth_Accounts_Insert_Input
  on_conflict?: Maybe<Auth_Accounts_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_ProvidersArgs = {
  objects: Array<Auth_Providers_Insert_Input>
  on_conflict?: Maybe<Auth_Providers_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_Providers_OneArgs = {
  object: Auth_Providers_Insert_Input
  on_conflict?: Maybe<Auth_Providers_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_Refresh_TokensArgs = {
  objects: Array<Auth_Refresh_Tokens_Insert_Input>
  on_conflict?: Maybe<Auth_Refresh_Tokens_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_Refresh_Tokens_OneArgs = {
  object: Auth_Refresh_Tokens_Insert_Input
  on_conflict?: Maybe<Auth_Refresh_Tokens_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_RolesArgs = {
  objects: Array<Auth_Roles_Insert_Input>
  on_conflict?: Maybe<Auth_Roles_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Auth_Roles_OneArgs = {
  object: Auth_Roles_Insert_Input
  on_conflict?: Maybe<Auth_Roles_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_ConfigArgs = {
  objects: Array<Config_Insert_Input>
  on_conflict?: Maybe<Config_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Config_KeysArgs = {
  objects: Array<Config_Keys_Insert_Input>
  on_conflict?: Maybe<Config_Keys_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Config_Keys_OneArgs = {
  object: Config_Keys_Insert_Input
  on_conflict?: Maybe<Config_Keys_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Config_OneArgs = {
  object: Config_Insert_Input
  on_conflict?: Maybe<Config_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_ContactsArgs = {
  objects: Array<Contacts_Insert_Input>
  on_conflict?: Maybe<Contacts_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Contacts_OneArgs = {
  object: Contacts_Insert_Input
  on_conflict?: Maybe<Contacts_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Cover_ImagesArgs = {
  objects: Array<Cover_Images_Insert_Input>
  on_conflict?: Maybe<Cover_Images_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Cover_Images_OneArgs = {
  object: Cover_Images_Insert_Input
  on_conflict?: Maybe<Cover_Images_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Credit_CardsArgs = {
  objects: Array<Credit_Cards_Insert_Input>
  on_conflict?: Maybe<Credit_Cards_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Credit_Cards_OneArgs = {
  object: Credit_Cards_Insert_Input
  on_conflict?: Maybe<Credit_Cards_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_DownloadsArgs = {
  objects: Array<Downloads_Insert_Input>
  on_conflict?: Maybe<Downloads_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Downloads_OneArgs = {
  object: Downloads_Insert_Input
  on_conflict?: Maybe<Downloads_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_EventsArgs = {
  objects: Array<Events_Insert_Input>
  on_conflict?: Maybe<Events_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Events_OneArgs = {
  object: Events_Insert_Input
  on_conflict?: Maybe<Events_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Events_Payment_TypesArgs = {
  objects: Array<Events_Payment_Types_Insert_Input>
  on_conflict?: Maybe<Events_Payment_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Events_Payment_Types_OneArgs = {
  object: Events_Payment_Types_Insert_Input
  on_conflict?: Maybe<Events_Payment_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Events_StatusesArgs = {
  objects: Array<Events_Statuses_Insert_Input>
  on_conflict?: Maybe<Events_Statuses_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Events_Statuses_OneArgs = {
  object: Events_Statuses_Insert_Input
  on_conflict?: Maybe<Events_Statuses_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_FavoritesArgs = {
  objects: Array<Favorites_Insert_Input>
  on_conflict?: Maybe<Favorites_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Favorites_OneArgs = {
  object: Favorites_Insert_Input
  on_conflict?: Maybe<Favorites_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_FoldersArgs = {
  objects: Array<Folders_Insert_Input>
  on_conflict?: Maybe<Folders_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Folders_OneArgs = {
  object: Folders_Insert_Input
  on_conflict?: Maybe<Folders_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Geometry_ColumnsArgs = {
  objects: Array<Geometry_Columns_Insert_Input>
}

/** mutation root */
export type Mutation_RootInsert_Geometry_Columns_OneArgs = {
  object: Geometry_Columns_Insert_Input
}

/** mutation root */
export type Mutation_RootInsert_LocalesArgs = {
  objects: Array<Locales_Insert_Input>
  on_conflict?: Maybe<Locales_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Locales_OneArgs = {
  object: Locales_Insert_Input
  on_conflict?: Maybe<Locales_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_LocalizationsArgs = {
  objects: Array<Localizations_Insert_Input>
  on_conflict?: Maybe<Localizations_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Localizations_OneArgs = {
  object: Localizations_Insert_Input
  on_conflict?: Maybe<Localizations_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_LocationsArgs = {
  objects: Array<Locations_Insert_Input>
  on_conflict?: Maybe<Locations_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Locations_OneArgs = {
  object: Locations_Insert_Input
  on_conflict?: Maybe<Locations_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Maker_TypesArgs = {
  objects: Array<Maker_Types_Insert_Input>
  on_conflict?: Maybe<Maker_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Maker_Types_OneArgs = {
  object: Maker_Types_Insert_Input
  on_conflict?: Maybe<Maker_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_MakersArgs = {
  objects: Array<Makers_Insert_Input>
  on_conflict?: Maybe<Makers_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Makers_OneArgs = {
  object: Makers_Insert_Input
  on_conflict?: Maybe<Makers_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Media_File_TypesArgs = {
  objects: Array<Media_File_Types_Insert_Input>
  on_conflict?: Maybe<Media_File_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Media_File_Types_OneArgs = {
  object: Media_File_Types_Insert_Input
  on_conflict?: Maybe<Media_File_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Media_FilesArgs = {
  objects: Array<Media_Files_Insert_Input>
  on_conflict?: Maybe<Media_Files_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Media_Files_OneArgs = {
  object: Media_Files_Insert_Input
  on_conflict?: Maybe<Media_Files_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Menu_ItemsArgs = {
  objects: Array<Menu_Items_Insert_Input>
  on_conflict?: Maybe<Menu_Items_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Menu_Items_OneArgs = {
  object: Menu_Items_Insert_Input
  on_conflict?: Maybe<Menu_Items_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Menu_PagesArgs = {
  objects: Array<Menu_Pages_Insert_Input>
  on_conflict?: Maybe<Menu_Pages_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Menu_Pages_OneArgs = {
  object: Menu_Pages_Insert_Input
  on_conflict?: Maybe<Menu_Pages_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Menu_TypeArgs = {
  objects: Array<Menu_Type_Insert_Input>
  on_conflict?: Maybe<Menu_Type_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Menu_Type_OneArgs = {
  object: Menu_Type_Insert_Input
  on_conflict?: Maybe<Menu_Type_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_MessagesArgs = {
  objects: Array<Messages_Insert_Input>
  on_conflict?: Maybe<Messages_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Messages_Events_UpdatedArgs = {
  objects: Array<Messages_Events_Updated_Insert_Input>
  on_conflict?: Maybe<Messages_Events_Updated_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Messages_Events_Updated_OneArgs = {
  object: Messages_Events_Updated_Insert_Input
  on_conflict?: Maybe<Messages_Events_Updated_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Messages_OneArgs = {
  object: Messages_Insert_Input
  on_conflict?: Maybe<Messages_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Messages_StatusesArgs = {
  objects: Array<Messages_Statuses_Insert_Input>
  on_conflict?: Maybe<Messages_Statuses_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Messages_Statuses_OneArgs = {
  object: Messages_Statuses_Insert_Input
  on_conflict?: Maybe<Messages_Statuses_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Messages_TypesArgs = {
  objects: Array<Messages_Types_Insert_Input>
  on_conflict?: Maybe<Messages_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Messages_Types_OneArgs = {
  object: Messages_Types_Insert_Input
  on_conflict?: Maybe<Messages_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Network_TransactionsArgs = {
  objects: Array<Network_Transactions_Insert_Input>
  on_conflict?: Maybe<Network_Transactions_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Network_Transactions_OneArgs = {
  object: Network_Transactions_Insert_Input
  on_conflict?: Maybe<Network_Transactions_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_PagesArgs = {
  objects: Array<Pages_Insert_Input>
  on_conflict?: Maybe<Pages_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Pages_ContentsArgs = {
  objects: Array<Pages_Contents_Insert_Input>
  on_conflict?: Maybe<Pages_Contents_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Pages_Contents_BlocksArgs = {
  objects: Array<Pages_Contents_Blocks_Insert_Input>
  on_conflict?: Maybe<Pages_Contents_Blocks_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Pages_Contents_Blocks_OneArgs = {
  object: Pages_Contents_Blocks_Insert_Input
  on_conflict?: Maybe<Pages_Contents_Blocks_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Pages_Contents_OneArgs = {
  object: Pages_Contents_Insert_Input
  on_conflict?: Maybe<Pages_Contents_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Pages_OneArgs = {
  object: Pages_Insert_Input
  on_conflict?: Maybe<Pages_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Payment_TypesArgs = {
  objects: Array<Payment_Types_Insert_Input>
  on_conflict?: Maybe<Payment_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Payment_Types_OneArgs = {
  object: Payment_Types_Insert_Input
  on_conflict?: Maybe<Payment_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_PermissionsArgs = {
  objects: Array<Permissions_Insert_Input>
  on_conflict?: Maybe<Permissions_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Permissions_OneArgs = {
  object: Permissions_Insert_Input
  on_conflict?: Maybe<Permissions_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_PurchasedArgs = {
  objects: Array<Purchased_Insert_Input>
  on_conflict?: Maybe<Purchased_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Purchased_OneArgs = {
  object: Purchased_Insert_Input
  on_conflict?: Maybe<Purchased_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_ReviewsArgs = {
  objects: Array<Reviews_Insert_Input>
  on_conflict?: Maybe<Reviews_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Reviews_OneArgs = {
  object: Reviews_Insert_Input
  on_conflict?: Maybe<Reviews_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_SeoArgs = {
  objects: Array<Seo_Insert_Input>
  on_conflict?: Maybe<Seo_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Seo_OneArgs = {
  object: Seo_Insert_Input
  on_conflict?: Maybe<Seo_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Spatial_Ref_SysArgs = {
  objects: Array<Spatial_Ref_Sys_Insert_Input>
  on_conflict?: Maybe<Spatial_Ref_Sys_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Spatial_Ref_Sys_OneArgs = {
  object: Spatial_Ref_Sys_Insert_Input
  on_conflict?: Maybe<Spatial_Ref_Sys_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_StoragesArgs = {
  objects: Array<Storages_Insert_Input>
  on_conflict?: Maybe<Storages_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Storages_OneArgs = {
  object: Storages_Insert_Input
  on_conflict?: Maybe<Storages_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Storages_ViewArgs = {
  objects: Array<Storages_View_Insert_Input>
}

/** mutation root */
export type Mutation_RootInsert_Storages_View_OneArgs = {
  object: Storages_View_Insert_Input
}

/** mutation root */
export type Mutation_RootInsert_StoreArgs = {
  objects: Array<Store_Insert_Input>
  on_conflict?: Maybe<Store_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Store_CategoriesArgs = {
  objects: Array<Store_Categories_Insert_Input>
  on_conflict?: Maybe<Store_Categories_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Store_Categories_OneArgs = {
  object: Store_Categories_Insert_Input
  on_conflict?: Maybe<Store_Categories_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Store_OneArgs = {
  object: Store_Insert_Input
  on_conflict?: Maybe<Store_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Store_TypesArgs = {
  objects: Array<Store_Types_Insert_Input>
  on_conflict?: Maybe<Store_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Store_Types_OneArgs = {
  object: Store_Types_Insert_Input
  on_conflict?: Maybe<Store_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_StylesArgs = {
  objects: Array<Styles_Insert_Input>
  on_conflict?: Maybe<Styles_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Styles_OneArgs = {
  object: Styles_Insert_Input
  on_conflict?: Maybe<Styles_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_System_MessagesArgs = {
  objects: Array<System_Messages_Insert_Input>
  on_conflict?: Maybe<System_Messages_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_System_Messages_OneArgs = {
  object: System_Messages_Insert_Input
  on_conflict?: Maybe<System_Messages_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_TagsArgs = {
  objects: Array<Tags_Insert_Input>
  on_conflict?: Maybe<Tags_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Tags_OneArgs = {
  object: Tags_Insert_Input
  on_conflict?: Maybe<Tags_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_TariffsArgs = {
  objects: Array<Tariffs_Insert_Input>
  on_conflict?: Maybe<Tariffs_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Tariffs_OneArgs = {
  object: Tariffs_Insert_Input
  on_conflict?: Maybe<Tariffs_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Transaction_StatusArgs = {
  objects: Array<Transaction_Status_Insert_Input>
  on_conflict?: Maybe<Transaction_Status_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Transaction_Status_OneArgs = {
  object: Transaction_Status_Insert_Input
  on_conflict?: Maybe<Transaction_Status_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Transaction_TypeArgs = {
  objects: Array<Transaction_Type_Insert_Input>
  on_conflict?: Maybe<Transaction_Type_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Transaction_Type_OneArgs = {
  object: Transaction_Type_Insert_Input
  on_conflict?: Maybe<Transaction_Type_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_TransactionsArgs = {
  objects: Array<Transactions_Insert_Input>
  on_conflict?: Maybe<Transactions_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Transactions_OneArgs = {
  object: Transactions_Insert_Input
  on_conflict?: Maybe<Transactions_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_UsersArgs = {
  objects: Array<Users_Insert_Input>
  on_conflict?: Maybe<Users_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Maker_TypesArgs = {
  objects: Array<Users_Maker_Types_Insert_Input>
  on_conflict?: Maybe<Users_Maker_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Maker_Types_OneArgs = {
  object: Users_Maker_Types_Insert_Input
  on_conflict?: Maybe<Users_Maker_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_MakersArgs = {
  objects: Array<Users_Makers_Insert_Input>
  on_conflict?: Maybe<Users_Makers_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Makers_OneArgs = {
  object: Users_Makers_Insert_Input
  on_conflict?: Maybe<Users_Makers_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_OneArgs = {
  object: Users_Insert_Input
  on_conflict?: Maybe<Users_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Payment_TypesArgs = {
  objects: Array<Users_Payment_Types_Insert_Input>
  on_conflict?: Maybe<Users_Payment_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Payment_Types_OneArgs = {
  object: Users_Payment_Types_Insert_Input
  on_conflict?: Maybe<Users_Payment_Types_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_PermissionsArgs = {
  objects: Array<Users_Permissions_Insert_Input>
  on_conflict?: Maybe<Users_Permissions_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Permissions_OneArgs = {
  object: Users_Permissions_Insert_Input
  on_conflict?: Maybe<Users_Permissions_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Search_Media_FilesArgs = {
  objects: Array<Users_Search_Media_Files_Insert_Input>
  on_conflict?: Maybe<Users_Search_Media_Files_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Search_Media_Files_OneArgs = {
  object: Users_Search_Media_Files_Insert_Input
  on_conflict?: Maybe<Users_Search_Media_Files_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_StatsArgs = {
  objects: Array<Users_Stats_Insert_Input>
  on_conflict?: Maybe<Users_Stats_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Stats_OneArgs = {
  object: Users_Stats_Insert_Input
  on_conflict?: Maybe<Users_Stats_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_StylesArgs = {
  objects: Array<Users_Styles_Insert_Input>
  on_conflict?: Maybe<Users_Styles_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Styles_OneArgs = {
  object: Users_Styles_Insert_Input
  on_conflict?: Maybe<Users_Styles_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_TariffsArgs = {
  objects: Array<Users_Tariffs_Insert_Input>
  on_conflict?: Maybe<Users_Tariffs_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Users_Tariffs_OneArgs = {
  object: Users_Tariffs_Insert_Input
  on_conflict?: Maybe<Users_Tariffs_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_WalletsArgs = {
  objects: Array<Wallets_Insert_Input>
  on_conflict?: Maybe<Wallets_On_Conflict>
}

/** mutation root */
export type Mutation_RootInsert_Wallets_OneArgs = {
  object: Wallets_Insert_Input
  on_conflict?: Maybe<Wallets_On_Conflict>
}

/** mutation root */
export type Mutation_RootUpdate_AlbumsArgs = {
  _inc?: Maybe<Albums_Inc_Input>
  _set?: Maybe<Albums_Set_Input>
  where: Albums_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Albums_By_PkArgs = {
  _inc?: Maybe<Albums_Inc_Input>
  _set?: Maybe<Albums_Set_Input>
  pk_columns: Albums_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Albums_Payment_TypesArgs = {
  _append?: Maybe<Albums_Payment_Types_Append_Input>
  _delete_at_path?: Maybe<Albums_Payment_Types_Delete_At_Path_Input>
  _delete_elem?: Maybe<Albums_Payment_Types_Delete_Elem_Input>
  _delete_key?: Maybe<Albums_Payment_Types_Delete_Key_Input>
  _inc?: Maybe<Albums_Payment_Types_Inc_Input>
  _prepend?: Maybe<Albums_Payment_Types_Prepend_Input>
  _set?: Maybe<Albums_Payment_Types_Set_Input>
  where: Albums_Payment_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Albums_Payment_Types_By_PkArgs = {
  _append?: Maybe<Albums_Payment_Types_Append_Input>
  _delete_at_path?: Maybe<Albums_Payment_Types_Delete_At_Path_Input>
  _delete_elem?: Maybe<Albums_Payment_Types_Delete_Elem_Input>
  _delete_key?: Maybe<Albums_Payment_Types_Delete_Key_Input>
  _inc?: Maybe<Albums_Payment_Types_Inc_Input>
  _prepend?: Maybe<Albums_Payment_Types_Prepend_Input>
  _set?: Maybe<Albums_Payment_Types_Set_Input>
  pk_columns: Albums_Payment_Types_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Albums_Statistics_ViewArgs = {
  _inc?: Maybe<Albums_Statistics_View_Inc_Input>
  _set?: Maybe<Albums_Statistics_View_Set_Input>
  where: Albums_Statistics_View_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Albums_TagsArgs = {
  _set?: Maybe<Albums_Tags_Set_Input>
  where: Albums_Tags_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Albums_Tags_By_PkArgs = {
  _set?: Maybe<Albums_Tags_Set_Input>
  pk_columns: Albums_Tags_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Albums_VisitsArgs = {
  _set?: Maybe<Albums_Visits_Set_Input>
  where: Albums_Visits_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Albums_Visits_By_PkArgs = {
  _set?: Maybe<Albums_Visits_Set_Input>
  pk_columns: Albums_Visits_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Auth_Account_ProvidersArgs = {
  _set?: Maybe<Auth_Account_Providers_Set_Input>
  where: Auth_Account_Providers_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Auth_Account_Providers_By_PkArgs = {
  _set?: Maybe<Auth_Account_Providers_Set_Input>
  pk_columns: Auth_Account_Providers_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Auth_Account_RolesArgs = {
  _set?: Maybe<Auth_Account_Roles_Set_Input>
  where: Auth_Account_Roles_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Auth_Account_Roles_By_PkArgs = {
  _set?: Maybe<Auth_Account_Roles_Set_Input>
  pk_columns: Auth_Account_Roles_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Auth_AccountsArgs = {
  _append?: Maybe<Auth_Accounts_Append_Input>
  _delete_at_path?: Maybe<Auth_Accounts_Delete_At_Path_Input>
  _delete_elem?: Maybe<Auth_Accounts_Delete_Elem_Input>
  _delete_key?: Maybe<Auth_Accounts_Delete_Key_Input>
  _prepend?: Maybe<Auth_Accounts_Prepend_Input>
  _set?: Maybe<Auth_Accounts_Set_Input>
  where: Auth_Accounts_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Auth_Accounts_By_PkArgs = {
  _append?: Maybe<Auth_Accounts_Append_Input>
  _delete_at_path?: Maybe<Auth_Accounts_Delete_At_Path_Input>
  _delete_elem?: Maybe<Auth_Accounts_Delete_Elem_Input>
  _delete_key?: Maybe<Auth_Accounts_Delete_Key_Input>
  _prepend?: Maybe<Auth_Accounts_Prepend_Input>
  _set?: Maybe<Auth_Accounts_Set_Input>
  pk_columns: Auth_Accounts_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Auth_ProvidersArgs = {
  _set?: Maybe<Auth_Providers_Set_Input>
  where: Auth_Providers_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Auth_Providers_By_PkArgs = {
  _set?: Maybe<Auth_Providers_Set_Input>
  pk_columns: Auth_Providers_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Auth_Refresh_TokensArgs = {
  _set?: Maybe<Auth_Refresh_Tokens_Set_Input>
  where: Auth_Refresh_Tokens_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Auth_Refresh_Tokens_By_PkArgs = {
  _set?: Maybe<Auth_Refresh_Tokens_Set_Input>
  pk_columns: Auth_Refresh_Tokens_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Auth_RolesArgs = {
  _set?: Maybe<Auth_Roles_Set_Input>
  where: Auth_Roles_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Auth_Roles_By_PkArgs = {
  _set?: Maybe<Auth_Roles_Set_Input>
  pk_columns: Auth_Roles_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_ConfigArgs = {
  _append?: Maybe<Config_Append_Input>
  _delete_at_path?: Maybe<Config_Delete_At_Path_Input>
  _delete_elem?: Maybe<Config_Delete_Elem_Input>
  _delete_key?: Maybe<Config_Delete_Key_Input>
  _prepend?: Maybe<Config_Prepend_Input>
  _set?: Maybe<Config_Set_Input>
  where: Config_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Config_By_PkArgs = {
  _append?: Maybe<Config_Append_Input>
  _delete_at_path?: Maybe<Config_Delete_At_Path_Input>
  _delete_elem?: Maybe<Config_Delete_Elem_Input>
  _delete_key?: Maybe<Config_Delete_Key_Input>
  _prepend?: Maybe<Config_Prepend_Input>
  _set?: Maybe<Config_Set_Input>
  pk_columns: Config_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Config_KeysArgs = {
  _set?: Maybe<Config_Keys_Set_Input>
  where: Config_Keys_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Config_Keys_By_PkArgs = {
  _set?: Maybe<Config_Keys_Set_Input>
  pk_columns: Config_Keys_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_ContactsArgs = {
  _set?: Maybe<Contacts_Set_Input>
  where: Contacts_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Contacts_By_PkArgs = {
  _set?: Maybe<Contacts_Set_Input>
  pk_columns: Contacts_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Cover_ImagesArgs = {
  _set?: Maybe<Cover_Images_Set_Input>
  where: Cover_Images_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Cover_Images_By_PkArgs = {
  _set?: Maybe<Cover_Images_Set_Input>
  pk_columns: Cover_Images_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Credit_CardsArgs = {
  _set?: Maybe<Credit_Cards_Set_Input>
  where: Credit_Cards_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Credit_Cards_By_PkArgs = {
  _set?: Maybe<Credit_Cards_Set_Input>
  pk_columns: Credit_Cards_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_DownloadsArgs = {
  _set?: Maybe<Downloads_Set_Input>
  where: Downloads_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Downloads_By_PkArgs = {
  _set?: Maybe<Downloads_Set_Input>
  pk_columns: Downloads_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_EventsArgs = {
  _append?: Maybe<Events_Append_Input>
  _delete_at_path?: Maybe<Events_Delete_At_Path_Input>
  _delete_elem?: Maybe<Events_Delete_Elem_Input>
  _delete_key?: Maybe<Events_Delete_Key_Input>
  _inc?: Maybe<Events_Inc_Input>
  _prepend?: Maybe<Events_Prepend_Input>
  _set?: Maybe<Events_Set_Input>
  where: Events_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Events_By_PkArgs = {
  _append?: Maybe<Events_Append_Input>
  _delete_at_path?: Maybe<Events_Delete_At_Path_Input>
  _delete_elem?: Maybe<Events_Delete_Elem_Input>
  _delete_key?: Maybe<Events_Delete_Key_Input>
  _inc?: Maybe<Events_Inc_Input>
  _prepend?: Maybe<Events_Prepend_Input>
  _set?: Maybe<Events_Set_Input>
  pk_columns: Events_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Events_Payment_TypesArgs = {
  _append?: Maybe<Events_Payment_Types_Append_Input>
  _delete_at_path?: Maybe<Events_Payment_Types_Delete_At_Path_Input>
  _delete_elem?: Maybe<Events_Payment_Types_Delete_Elem_Input>
  _delete_key?: Maybe<Events_Payment_Types_Delete_Key_Input>
  _inc?: Maybe<Events_Payment_Types_Inc_Input>
  _prepend?: Maybe<Events_Payment_Types_Prepend_Input>
  _set?: Maybe<Events_Payment_Types_Set_Input>
  where: Events_Payment_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Events_Payment_Types_By_PkArgs = {
  _append?: Maybe<Events_Payment_Types_Append_Input>
  _delete_at_path?: Maybe<Events_Payment_Types_Delete_At_Path_Input>
  _delete_elem?: Maybe<Events_Payment_Types_Delete_Elem_Input>
  _delete_key?: Maybe<Events_Payment_Types_Delete_Key_Input>
  _inc?: Maybe<Events_Payment_Types_Inc_Input>
  _prepend?: Maybe<Events_Payment_Types_Prepend_Input>
  _set?: Maybe<Events_Payment_Types_Set_Input>
  pk_columns: Events_Payment_Types_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Events_StatusesArgs = {
  _set?: Maybe<Events_Statuses_Set_Input>
  where: Events_Statuses_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Events_Statuses_By_PkArgs = {
  _set?: Maybe<Events_Statuses_Set_Input>
  pk_columns: Events_Statuses_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_FavoritesArgs = {
  _set?: Maybe<Favorites_Set_Input>
  where: Favorites_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Favorites_By_PkArgs = {
  _set?: Maybe<Favorites_Set_Input>
  pk_columns: Favorites_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_FoldersArgs = {
  _inc?: Maybe<Folders_Inc_Input>
  _set?: Maybe<Folders_Set_Input>
  where: Folders_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Folders_By_PkArgs = {
  _inc?: Maybe<Folders_Inc_Input>
  _set?: Maybe<Folders_Set_Input>
  pk_columns: Folders_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Geometry_ColumnsArgs = {
  _inc?: Maybe<Geometry_Columns_Inc_Input>
  _set?: Maybe<Geometry_Columns_Set_Input>
  where: Geometry_Columns_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_LocalesArgs = {
  _set?: Maybe<Locales_Set_Input>
  where: Locales_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Locales_By_PkArgs = {
  _set?: Maybe<Locales_Set_Input>
  pk_columns: Locales_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_LocalizationsArgs = {
  _append?: Maybe<Localizations_Append_Input>
  _delete_at_path?: Maybe<Localizations_Delete_At_Path_Input>
  _delete_elem?: Maybe<Localizations_Delete_Elem_Input>
  _delete_key?: Maybe<Localizations_Delete_Key_Input>
  _prepend?: Maybe<Localizations_Prepend_Input>
  _set?: Maybe<Localizations_Set_Input>
  where: Localizations_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Localizations_By_PkArgs = {
  _append?: Maybe<Localizations_Append_Input>
  _delete_at_path?: Maybe<Localizations_Delete_At_Path_Input>
  _delete_elem?: Maybe<Localizations_Delete_Elem_Input>
  _delete_key?: Maybe<Localizations_Delete_Key_Input>
  _prepend?: Maybe<Localizations_Prepend_Input>
  _set?: Maybe<Localizations_Set_Input>
  pk_columns: Localizations_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_LocationsArgs = {
  _set?: Maybe<Locations_Set_Input>
  where: Locations_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Locations_By_PkArgs = {
  _set?: Maybe<Locations_Set_Input>
  pk_columns: Locations_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Maker_TypesArgs = {
  _set?: Maybe<Maker_Types_Set_Input>
  where: Maker_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Maker_Types_By_PkArgs = {
  _set?: Maybe<Maker_Types_Set_Input>
  pk_columns: Maker_Types_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_MakersArgs = {
  _set?: Maybe<Makers_Set_Input>
  where: Makers_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Makers_By_PkArgs = {
  _set?: Maybe<Makers_Set_Input>
  pk_columns: Makers_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Media_File_TypesArgs = {
  _set?: Maybe<Media_File_Types_Set_Input>
  where: Media_File_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Media_File_Types_By_PkArgs = {
  _set?: Maybe<Media_File_Types_Set_Input>
  pk_columns: Media_File_Types_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Media_FilesArgs = {
  _inc?: Maybe<Media_Files_Inc_Input>
  _set?: Maybe<Media_Files_Set_Input>
  where: Media_Files_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Media_Files_By_PkArgs = {
  _inc?: Maybe<Media_Files_Inc_Input>
  _set?: Maybe<Media_Files_Set_Input>
  pk_columns: Media_Files_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Menu_ItemsArgs = {
  _inc?: Maybe<Menu_Items_Inc_Input>
  _set?: Maybe<Menu_Items_Set_Input>
  where: Menu_Items_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Menu_Items_By_PkArgs = {
  _inc?: Maybe<Menu_Items_Inc_Input>
  _set?: Maybe<Menu_Items_Set_Input>
  pk_columns: Menu_Items_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Menu_PagesArgs = {
  _inc?: Maybe<Menu_Pages_Inc_Input>
  _set?: Maybe<Menu_Pages_Set_Input>
  where: Menu_Pages_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Menu_Pages_By_PkArgs = {
  _inc?: Maybe<Menu_Pages_Inc_Input>
  _set?: Maybe<Menu_Pages_Set_Input>
  pk_columns: Menu_Pages_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Menu_TypeArgs = {
  _set?: Maybe<Menu_Type_Set_Input>
  where: Menu_Type_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Menu_Type_By_PkArgs = {
  _set?: Maybe<Menu_Type_Set_Input>
  pk_columns: Menu_Type_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_MessagesArgs = {
  _append?: Maybe<Messages_Append_Input>
  _delete_at_path?: Maybe<Messages_Delete_At_Path_Input>
  _delete_elem?: Maybe<Messages_Delete_Elem_Input>
  _delete_key?: Maybe<Messages_Delete_Key_Input>
  _prepend?: Maybe<Messages_Prepend_Input>
  _set?: Maybe<Messages_Set_Input>
  where: Messages_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Messages_By_PkArgs = {
  _append?: Maybe<Messages_Append_Input>
  _delete_at_path?: Maybe<Messages_Delete_At_Path_Input>
  _delete_elem?: Maybe<Messages_Delete_Elem_Input>
  _delete_key?: Maybe<Messages_Delete_Key_Input>
  _prepend?: Maybe<Messages_Prepend_Input>
  _set?: Maybe<Messages_Set_Input>
  pk_columns: Messages_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Messages_Events_UpdatedArgs = {
  _inc?: Maybe<Messages_Events_Updated_Inc_Input>
  _set?: Maybe<Messages_Events_Updated_Set_Input>
  where: Messages_Events_Updated_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Messages_Events_Updated_By_PkArgs = {
  _inc?: Maybe<Messages_Events_Updated_Inc_Input>
  _set?: Maybe<Messages_Events_Updated_Set_Input>
  pk_columns: Messages_Events_Updated_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Messages_StatusesArgs = {
  _set?: Maybe<Messages_Statuses_Set_Input>
  where: Messages_Statuses_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Messages_Statuses_By_PkArgs = {
  _set?: Maybe<Messages_Statuses_Set_Input>
  pk_columns: Messages_Statuses_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Messages_TypesArgs = {
  _set?: Maybe<Messages_Types_Set_Input>
  where: Messages_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Messages_Types_By_PkArgs = {
  _set?: Maybe<Messages_Types_Set_Input>
  pk_columns: Messages_Types_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Network_TransactionsArgs = {
  _append?: Maybe<Network_Transactions_Append_Input>
  _delete_at_path?: Maybe<Network_Transactions_Delete_At_Path_Input>
  _delete_elem?: Maybe<Network_Transactions_Delete_Elem_Input>
  _delete_key?: Maybe<Network_Transactions_Delete_Key_Input>
  _prepend?: Maybe<Network_Transactions_Prepend_Input>
  _set?: Maybe<Network_Transactions_Set_Input>
  where: Network_Transactions_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Network_Transactions_By_PkArgs = {
  _append?: Maybe<Network_Transactions_Append_Input>
  _delete_at_path?: Maybe<Network_Transactions_Delete_At_Path_Input>
  _delete_elem?: Maybe<Network_Transactions_Delete_Elem_Input>
  _delete_key?: Maybe<Network_Transactions_Delete_Key_Input>
  _prepend?: Maybe<Network_Transactions_Prepend_Input>
  _set?: Maybe<Network_Transactions_Set_Input>
  pk_columns: Network_Transactions_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_PagesArgs = {
  _append?: Maybe<Pages_Append_Input>
  _delete_at_path?: Maybe<Pages_Delete_At_Path_Input>
  _delete_elem?: Maybe<Pages_Delete_Elem_Input>
  _delete_key?: Maybe<Pages_Delete_Key_Input>
  _prepend?: Maybe<Pages_Prepend_Input>
  _set?: Maybe<Pages_Set_Input>
  where: Pages_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Pages_By_PkArgs = {
  _append?: Maybe<Pages_Append_Input>
  _delete_at_path?: Maybe<Pages_Delete_At_Path_Input>
  _delete_elem?: Maybe<Pages_Delete_Elem_Input>
  _delete_key?: Maybe<Pages_Delete_Key_Input>
  _prepend?: Maybe<Pages_Prepend_Input>
  _set?: Maybe<Pages_Set_Input>
  pk_columns: Pages_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Pages_ContentsArgs = {
  _set?: Maybe<Pages_Contents_Set_Input>
  where: Pages_Contents_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Pages_Contents_BlocksArgs = {
  _inc?: Maybe<Pages_Contents_Blocks_Inc_Input>
  _set?: Maybe<Pages_Contents_Blocks_Set_Input>
  where: Pages_Contents_Blocks_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Pages_Contents_Blocks_By_PkArgs = {
  _inc?: Maybe<Pages_Contents_Blocks_Inc_Input>
  _set?: Maybe<Pages_Contents_Blocks_Set_Input>
  pk_columns: Pages_Contents_Blocks_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Pages_Contents_By_PkArgs = {
  _set?: Maybe<Pages_Contents_Set_Input>
  pk_columns: Pages_Contents_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Payment_TypesArgs = {
  _set?: Maybe<Payment_Types_Set_Input>
  where: Payment_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Payment_Types_By_PkArgs = {
  _set?: Maybe<Payment_Types_Set_Input>
  pk_columns: Payment_Types_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_PermissionsArgs = {
  _set?: Maybe<Permissions_Set_Input>
  where: Permissions_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Permissions_By_PkArgs = {
  _set?: Maybe<Permissions_Set_Input>
  pk_columns: Permissions_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_PurchasedArgs = {
  _set?: Maybe<Purchased_Set_Input>
  where: Purchased_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Purchased_By_PkArgs = {
  _set?: Maybe<Purchased_Set_Input>
  pk_columns: Purchased_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_ReviewsArgs = {
  _inc?: Maybe<Reviews_Inc_Input>
  _set?: Maybe<Reviews_Set_Input>
  where: Reviews_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Reviews_By_PkArgs = {
  _inc?: Maybe<Reviews_Inc_Input>
  _set?: Maybe<Reviews_Set_Input>
  pk_columns: Reviews_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_SeoArgs = {
  _set?: Maybe<Seo_Set_Input>
  where: Seo_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Seo_By_PkArgs = {
  _set?: Maybe<Seo_Set_Input>
  pk_columns: Seo_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Spatial_Ref_SysArgs = {
  _inc?: Maybe<Spatial_Ref_Sys_Inc_Input>
  _set?: Maybe<Spatial_Ref_Sys_Set_Input>
  where: Spatial_Ref_Sys_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Spatial_Ref_Sys_By_PkArgs = {
  _inc?: Maybe<Spatial_Ref_Sys_Inc_Input>
  _set?: Maybe<Spatial_Ref_Sys_Set_Input>
  pk_columns: Spatial_Ref_Sys_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_StoragesArgs = {
  _inc?: Maybe<Storages_Inc_Input>
  _set?: Maybe<Storages_Set_Input>
  where: Storages_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Storages_By_PkArgs = {
  _inc?: Maybe<Storages_Inc_Input>
  _set?: Maybe<Storages_Set_Input>
  pk_columns: Storages_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Storages_ViewArgs = {
  _inc?: Maybe<Storages_View_Inc_Input>
  _set?: Maybe<Storages_View_Set_Input>
  where: Storages_View_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_StoreArgs = {
  _inc?: Maybe<Store_Inc_Input>
  _set?: Maybe<Store_Set_Input>
  where: Store_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Store_By_PkArgs = {
  _inc?: Maybe<Store_Inc_Input>
  _set?: Maybe<Store_Set_Input>
  pk_columns: Store_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Store_CategoriesArgs = {
  _set?: Maybe<Store_Categories_Set_Input>
  where: Store_Categories_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Store_Categories_By_PkArgs = {
  _set?: Maybe<Store_Categories_Set_Input>
  pk_columns: Store_Categories_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Store_TypesArgs = {
  _set?: Maybe<Store_Types_Set_Input>
  where: Store_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Store_Types_By_PkArgs = {
  _set?: Maybe<Store_Types_Set_Input>
  pk_columns: Store_Types_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_StylesArgs = {
  _set?: Maybe<Styles_Set_Input>
  where: Styles_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Styles_By_PkArgs = {
  _set?: Maybe<Styles_Set_Input>
  pk_columns: Styles_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_System_MessagesArgs = {
  _set?: Maybe<System_Messages_Set_Input>
  where: System_Messages_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_System_Messages_By_PkArgs = {
  _set?: Maybe<System_Messages_Set_Input>
  pk_columns: System_Messages_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_TagsArgs = {
  _set?: Maybe<Tags_Set_Input>
  where: Tags_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Tags_By_PkArgs = {
  _set?: Maybe<Tags_Set_Input>
  pk_columns: Tags_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_TariffsArgs = {
  _inc?: Maybe<Tariffs_Inc_Input>
  _set?: Maybe<Tariffs_Set_Input>
  where: Tariffs_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Tariffs_By_PkArgs = {
  _inc?: Maybe<Tariffs_Inc_Input>
  _set?: Maybe<Tariffs_Set_Input>
  pk_columns: Tariffs_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Transaction_StatusArgs = {
  _set?: Maybe<Transaction_Status_Set_Input>
  where: Transaction_Status_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Transaction_Status_By_PkArgs = {
  _set?: Maybe<Transaction_Status_Set_Input>
  pk_columns: Transaction_Status_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Transaction_TypeArgs = {
  _set?: Maybe<Transaction_Type_Set_Input>
  where: Transaction_Type_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Transaction_Type_By_PkArgs = {
  _set?: Maybe<Transaction_Type_Set_Input>
  pk_columns: Transaction_Type_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_TransactionsArgs = {
  _append?: Maybe<Transactions_Append_Input>
  _delete_at_path?: Maybe<Transactions_Delete_At_Path_Input>
  _delete_elem?: Maybe<Transactions_Delete_Elem_Input>
  _delete_key?: Maybe<Transactions_Delete_Key_Input>
  _inc?: Maybe<Transactions_Inc_Input>
  _prepend?: Maybe<Transactions_Prepend_Input>
  _set?: Maybe<Transactions_Set_Input>
  where: Transactions_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Transactions_By_PkArgs = {
  _append?: Maybe<Transactions_Append_Input>
  _delete_at_path?: Maybe<Transactions_Delete_At_Path_Input>
  _delete_elem?: Maybe<Transactions_Delete_Elem_Input>
  _delete_key?: Maybe<Transactions_Delete_Key_Input>
  _inc?: Maybe<Transactions_Inc_Input>
  _prepend?: Maybe<Transactions_Prepend_Input>
  _set?: Maybe<Transactions_Set_Input>
  pk_columns: Transactions_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_UsersArgs = {
  _inc?: Maybe<Users_Inc_Input>
  _set?: Maybe<Users_Set_Input>
  where: Users_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Users_By_PkArgs = {
  _inc?: Maybe<Users_Inc_Input>
  _set?: Maybe<Users_Set_Input>
  pk_columns: Users_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Users_Maker_TypesArgs = {
  _set?: Maybe<Users_Maker_Types_Set_Input>
  where: Users_Maker_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Users_Maker_Types_By_PkArgs = {
  _set?: Maybe<Users_Maker_Types_Set_Input>
  pk_columns: Users_Maker_Types_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Users_MakersArgs = {
  _set?: Maybe<Users_Makers_Set_Input>
  where: Users_Makers_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Users_Makers_By_PkArgs = {
  _set?: Maybe<Users_Makers_Set_Input>
  pk_columns: Users_Makers_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Users_Payment_TypesArgs = {
  _inc?: Maybe<Users_Payment_Types_Inc_Input>
  _set?: Maybe<Users_Payment_Types_Set_Input>
  where: Users_Payment_Types_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Users_Payment_Types_By_PkArgs = {
  _inc?: Maybe<Users_Payment_Types_Inc_Input>
  _set?: Maybe<Users_Payment_Types_Set_Input>
  pk_columns: Users_Payment_Types_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Users_PermissionsArgs = {
  _set?: Maybe<Users_Permissions_Set_Input>
  where: Users_Permissions_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Users_Permissions_By_PkArgs = {
  _set?: Maybe<Users_Permissions_Set_Input>
  pk_columns: Users_Permissions_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Users_Search_Media_FilesArgs = {
  _inc?: Maybe<Users_Search_Media_Files_Inc_Input>
  _set?: Maybe<Users_Search_Media_Files_Set_Input>
  where: Users_Search_Media_Files_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Users_Search_Media_Files_By_PkArgs = {
  _inc?: Maybe<Users_Search_Media_Files_Inc_Input>
  _set?: Maybe<Users_Search_Media_Files_Set_Input>
  pk_columns: Users_Search_Media_Files_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Users_StatsArgs = {
  _inc?: Maybe<Users_Stats_Inc_Input>
  _set?: Maybe<Users_Stats_Set_Input>
  where: Users_Stats_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Users_Stats_By_PkArgs = {
  _inc?: Maybe<Users_Stats_Inc_Input>
  _set?: Maybe<Users_Stats_Set_Input>
  pk_columns: Users_Stats_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Users_StylesArgs = {
  _set?: Maybe<Users_Styles_Set_Input>
  where: Users_Styles_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Users_Styles_By_PkArgs = {
  _set?: Maybe<Users_Styles_Set_Input>
  pk_columns: Users_Styles_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_Users_TariffsArgs = {
  _inc?: Maybe<Users_Tariffs_Inc_Input>
  _set?: Maybe<Users_Tariffs_Set_Input>
  where: Users_Tariffs_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Users_Tariffs_By_PkArgs = {
  _inc?: Maybe<Users_Tariffs_Inc_Input>
  _set?: Maybe<Users_Tariffs_Set_Input>
  pk_columns: Users_Tariffs_Pk_Columns_Input
}

/** mutation root */
export type Mutation_RootUpdate_WalletsArgs = {
  _inc?: Maybe<Wallets_Inc_Input>
  _set?: Maybe<Wallets_Set_Input>
  where: Wallets_Bool_Exp
}

/** mutation root */
export type Mutation_RootUpdate_Wallets_By_PkArgs = {
  _inc?: Maybe<Wallets_Inc_Input>
  _set?: Maybe<Wallets_Set_Input>
  pk_columns: Wallets_Pk_Columns_Input
}

/** expression to compare columns of type name. All fields are combined with logical 'AND'. */
export type Name_Comparison_Exp = {
  _eq?: Maybe<Scalars['name']>
  _gt?: Maybe<Scalars['name']>
  _gte?: Maybe<Scalars['name']>
  _in?: Maybe<Array<Scalars['name']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['name']>
  _lte?: Maybe<Scalars['name']>
  _neq?: Maybe<Scalars['name']>
  _nin?: Maybe<Array<Scalars['name']>>
}

/** columns and relationships of "network_transactions" */
export type Network_Transactions = {
  __typename?: 'network_transactions'
  created_at: Scalars['timestamptz']
  crypto_amount: Scalars['String']
  data?: Maybe<Scalars['jsonb']>
  network: Scalars['String']
  status: Scalars['String']
  /** An object relationship */
  transaction?: Maybe<Transactions>
  transaction_id?: Maybe<Scalars['uuid']>
  tx_hash: Scalars['String']
  updated_at: Scalars['timestamptz']
  usd_amount: Scalars['String']
}

/** columns and relationships of "network_transactions" */
export type Network_TransactionsDataArgs = {
  path?: Maybe<Scalars['String']>
}

/** aggregated selection of "network_transactions" */
export type Network_Transactions_Aggregate = {
  __typename?: 'network_transactions_aggregate'
  aggregate?: Maybe<Network_Transactions_Aggregate_Fields>
  nodes: Array<Network_Transactions>
}

/** aggregate fields of "network_transactions" */
export type Network_Transactions_Aggregate_Fields = {
  __typename?: 'network_transactions_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Network_Transactions_Max_Fields>
  min?: Maybe<Network_Transactions_Min_Fields>
}

/** aggregate fields of "network_transactions" */
export type Network_Transactions_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Network_Transactions_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "network_transactions" */
export type Network_Transactions_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Network_Transactions_Max_Order_By>
  min?: Maybe<Network_Transactions_Min_Order_By>
}

/** append existing jsonb value of filtered columns with new jsonb value */
export type Network_Transactions_Append_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** input type for inserting array relation for remote table "network_transactions" */
export type Network_Transactions_Arr_Rel_Insert_Input = {
  data: Array<Network_Transactions_Insert_Input>
  on_conflict?: Maybe<Network_Transactions_On_Conflict>
}

/** Boolean expression to filter rows from the table "network_transactions". All fields are combined with a logical 'AND'. */
export type Network_Transactions_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Network_Transactions_Bool_Exp>>>
  _not?: Maybe<Network_Transactions_Bool_Exp>
  _or?: Maybe<Array<Maybe<Network_Transactions_Bool_Exp>>>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  crypto_amount?: Maybe<String_Comparison_Exp>
  data?: Maybe<Jsonb_Comparison_Exp>
  network?: Maybe<String_Comparison_Exp>
  status?: Maybe<String_Comparison_Exp>
  transaction?: Maybe<Transactions_Bool_Exp>
  transaction_id?: Maybe<Uuid_Comparison_Exp>
  tx_hash?: Maybe<String_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  usd_amount?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "network_transactions" */
export enum Network_Transactions_Constraint {
  /** unique or primary key constraint */
  NetworkTransactionsPkey = 'network_transactions_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Network_Transactions_Delete_At_Path_Input = {
  data?: Maybe<Array<Maybe<Scalars['String']>>>
}

/**
 * delete the array element with specified index (negative integers count from the
 * end). throws an error if top level container is not an array
 */
export type Network_Transactions_Delete_Elem_Input = {
  data?: Maybe<Scalars['Int']>
}

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Network_Transactions_Delete_Key_Input = {
  data?: Maybe<Scalars['String']>
}

/** input type for inserting data into table "network_transactions" */
export type Network_Transactions_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  crypto_amount?: Maybe<Scalars['String']>
  data?: Maybe<Scalars['jsonb']>
  network?: Maybe<Scalars['String']>
  status?: Maybe<Scalars['String']>
  transaction?: Maybe<Transactions_Obj_Rel_Insert_Input>
  transaction_id?: Maybe<Scalars['uuid']>
  tx_hash?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  usd_amount?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Network_Transactions_Max_Fields = {
  __typename?: 'network_transactions_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  crypto_amount?: Maybe<Scalars['String']>
  network?: Maybe<Scalars['String']>
  status?: Maybe<Scalars['String']>
  transaction_id?: Maybe<Scalars['uuid']>
  tx_hash?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  usd_amount?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "network_transactions" */
export type Network_Transactions_Max_Order_By = {
  created_at?: Maybe<Order_By>
  crypto_amount?: Maybe<Order_By>
  network?: Maybe<Order_By>
  status?: Maybe<Order_By>
  transaction_id?: Maybe<Order_By>
  tx_hash?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  usd_amount?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Network_Transactions_Min_Fields = {
  __typename?: 'network_transactions_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  crypto_amount?: Maybe<Scalars['String']>
  network?: Maybe<Scalars['String']>
  status?: Maybe<Scalars['String']>
  transaction_id?: Maybe<Scalars['uuid']>
  tx_hash?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  usd_amount?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "network_transactions" */
export type Network_Transactions_Min_Order_By = {
  created_at?: Maybe<Order_By>
  crypto_amount?: Maybe<Order_By>
  network?: Maybe<Order_By>
  status?: Maybe<Order_By>
  transaction_id?: Maybe<Order_By>
  tx_hash?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  usd_amount?: Maybe<Order_By>
}

/** response of any mutation on the table "network_transactions" */
export type Network_Transactions_Mutation_Response = {
  __typename?: 'network_transactions_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Network_Transactions>
}

/** input type for inserting object relation for remote table "network_transactions" */
export type Network_Transactions_Obj_Rel_Insert_Input = {
  data: Network_Transactions_Insert_Input
  on_conflict?: Maybe<Network_Transactions_On_Conflict>
}

/** on conflict condition type for table "network_transactions" */
export type Network_Transactions_On_Conflict = {
  constraint: Network_Transactions_Constraint
  update_columns: Array<Network_Transactions_Update_Column>
  where?: Maybe<Network_Transactions_Bool_Exp>
}

/** ordering options when selecting data from "network_transactions" */
export type Network_Transactions_Order_By = {
  created_at?: Maybe<Order_By>
  crypto_amount?: Maybe<Order_By>
  data?: Maybe<Order_By>
  network?: Maybe<Order_By>
  status?: Maybe<Order_By>
  transaction?: Maybe<Transactions_Order_By>
  transaction_id?: Maybe<Order_By>
  tx_hash?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  usd_amount?: Maybe<Order_By>
}

/** primary key columns input for table: "network_transactions" */
export type Network_Transactions_Pk_Columns_Input = {
  network: Scalars['String']
  tx_hash: Scalars['String']
}

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Network_Transactions_Prepend_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** select columns of table "network_transactions" */
export enum Network_Transactions_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CryptoAmount = 'crypto_amount',
  /** column name */
  Data = 'data',
  /** column name */
  Network = 'network',
  /** column name */
  Status = 'status',
  /** column name */
  TransactionId = 'transaction_id',
  /** column name */
  TxHash = 'tx_hash',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UsdAmount = 'usd_amount'
}

/** input type for updating data in table "network_transactions" */
export type Network_Transactions_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  crypto_amount?: Maybe<Scalars['String']>
  data?: Maybe<Scalars['jsonb']>
  network?: Maybe<Scalars['String']>
  status?: Maybe<Scalars['String']>
  transaction_id?: Maybe<Scalars['uuid']>
  tx_hash?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  usd_amount?: Maybe<Scalars['String']>
}

/** update columns of table "network_transactions" */
export enum Network_Transactions_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  CryptoAmount = 'crypto_amount',
  /** column name */
  Data = 'data',
  /** column name */
  Network = 'network',
  /** column name */
  Status = 'status',
  /** column name */
  TransactionId = 'transaction_id',
  /** column name */
  TxHash = 'tx_hash',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UsdAmount = 'usd_amount'
}

/** columns and relationships of "new_messages_view" */
export type New_Messages_View = {
  __typename?: 'new_messages_view'
  id?: Maybe<Scalars['uuid']>
  new_messages?: Maybe<Scalars['Int']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregated selection of "new_messages_view" */
export type New_Messages_View_Aggregate = {
  __typename?: 'new_messages_view_aggregate'
  aggregate?: Maybe<New_Messages_View_Aggregate_Fields>
  nodes: Array<New_Messages_View>
}

/** aggregate fields of "new_messages_view" */
export type New_Messages_View_Aggregate_Fields = {
  __typename?: 'new_messages_view_aggregate_fields'
  avg?: Maybe<New_Messages_View_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<New_Messages_View_Max_Fields>
  min?: Maybe<New_Messages_View_Min_Fields>
  stddev?: Maybe<New_Messages_View_Stddev_Fields>
  stddev_pop?: Maybe<New_Messages_View_Stddev_Pop_Fields>
  stddev_samp?: Maybe<New_Messages_View_Stddev_Samp_Fields>
  sum?: Maybe<New_Messages_View_Sum_Fields>
  var_pop?: Maybe<New_Messages_View_Var_Pop_Fields>
  var_samp?: Maybe<New_Messages_View_Var_Samp_Fields>
  variance?: Maybe<New_Messages_View_Variance_Fields>
}

/** aggregate fields of "new_messages_view" */
export type New_Messages_View_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<New_Messages_View_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "new_messages_view" */
export type New_Messages_View_Aggregate_Order_By = {
  avg?: Maybe<New_Messages_View_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<New_Messages_View_Max_Order_By>
  min?: Maybe<New_Messages_View_Min_Order_By>
  stddev?: Maybe<New_Messages_View_Stddev_Order_By>
  stddev_pop?: Maybe<New_Messages_View_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<New_Messages_View_Stddev_Samp_Order_By>
  sum?: Maybe<New_Messages_View_Sum_Order_By>
  var_pop?: Maybe<New_Messages_View_Var_Pop_Order_By>
  var_samp?: Maybe<New_Messages_View_Var_Samp_Order_By>
  variance?: Maybe<New_Messages_View_Variance_Order_By>
}

/** aggregate avg on columns */
export type New_Messages_View_Avg_Fields = {
  __typename?: 'new_messages_view_avg_fields'
  new_messages?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "new_messages_view" */
export type New_Messages_View_Avg_Order_By = {
  new_messages?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "new_messages_view". All fields are combined with a logical 'AND'. */
export type New_Messages_View_Bool_Exp = {
  _and?: Maybe<Array<Maybe<New_Messages_View_Bool_Exp>>>
  _not?: Maybe<New_Messages_View_Bool_Exp>
  _or?: Maybe<Array<Maybe<New_Messages_View_Bool_Exp>>>
  id?: Maybe<Uuid_Comparison_Exp>
  new_messages?: Maybe<Int_Comparison_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** aggregate max on columns */
export type New_Messages_View_Max_Fields = {
  __typename?: 'new_messages_view_max_fields'
  id?: Maybe<Scalars['uuid']>
  new_messages?: Maybe<Scalars['Int']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "new_messages_view" */
export type New_Messages_View_Max_Order_By = {
  id?: Maybe<Order_By>
  new_messages?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type New_Messages_View_Min_Fields = {
  __typename?: 'new_messages_view_min_fields'
  id?: Maybe<Scalars['uuid']>
  new_messages?: Maybe<Scalars['Int']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "new_messages_view" */
export type New_Messages_View_Min_Order_By = {
  id?: Maybe<Order_By>
  new_messages?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** ordering options when selecting data from "new_messages_view" */
export type New_Messages_View_Order_By = {
  id?: Maybe<Order_By>
  new_messages?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** select columns of table "new_messages_view" */
export enum New_Messages_View_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  NewMessages = 'new_messages',
  /** column name */
  UserId = 'user_id'
}

/** aggregate stddev on columns */
export type New_Messages_View_Stddev_Fields = {
  __typename?: 'new_messages_view_stddev_fields'
  new_messages?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "new_messages_view" */
export type New_Messages_View_Stddev_Order_By = {
  new_messages?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type New_Messages_View_Stddev_Pop_Fields = {
  __typename?: 'new_messages_view_stddev_pop_fields'
  new_messages?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "new_messages_view" */
export type New_Messages_View_Stddev_Pop_Order_By = {
  new_messages?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type New_Messages_View_Stddev_Samp_Fields = {
  __typename?: 'new_messages_view_stddev_samp_fields'
  new_messages?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "new_messages_view" */
export type New_Messages_View_Stddev_Samp_Order_By = {
  new_messages?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type New_Messages_View_Sum_Fields = {
  __typename?: 'new_messages_view_sum_fields'
  new_messages?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "new_messages_view" */
export type New_Messages_View_Sum_Order_By = {
  new_messages?: Maybe<Order_By>
}

/** aggregate var_pop on columns */
export type New_Messages_View_Var_Pop_Fields = {
  __typename?: 'new_messages_view_var_pop_fields'
  new_messages?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "new_messages_view" */
export type New_Messages_View_Var_Pop_Order_By = {
  new_messages?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type New_Messages_View_Var_Samp_Fields = {
  __typename?: 'new_messages_view_var_samp_fields'
  new_messages?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "new_messages_view" */
export type New_Messages_View_Var_Samp_Order_By = {
  new_messages?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type New_Messages_View_Variance_Fields = {
  __typename?: 'new_messages_view_variance_fields'
  new_messages?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "new_messages_view" */
export type New_Messages_View_Variance_Order_By = {
  new_messages?: Maybe<Order_By>
}

/** expression to compare columns of type numeric. All fields are combined with logical 'AND'. */
export type Numeric_Comparison_Exp = {
  _eq?: Maybe<Scalars['numeric']>
  _gt?: Maybe<Scalars['numeric']>
  _gte?: Maybe<Scalars['numeric']>
  _in?: Maybe<Array<Scalars['numeric']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['numeric']>
  _lte?: Maybe<Scalars['numeric']>
  _neq?: Maybe<Scalars['numeric']>
  _nin?: Maybe<Array<Scalars['numeric']>>
}

/** column ordering options */
export enum Order_By {
  /** in the ascending order, nulls last */
  Asc = 'asc',
  /** in the ascending order, nulls first */
  AscNullsFirst = 'asc_nulls_first',
  /** in the ascending order, nulls last */
  AscNullsLast = 'asc_nulls_last',
  /** in the descending order, nulls first */
  Desc = 'desc',
  /** in the descending order, nulls first */
  DescNullsFirst = 'desc_nulls_first',
  /** in the descending order, nulls last */
  DescNullsLast = 'desc_nulls_last'
}

/** columns and relationships of "pages" */
export type Pages = {
  __typename?: 'pages'
  active: Scalars['Boolean']
  created_at: Scalars['timestamp']
  data?: Maybe<Scalars['jsonb']>
  id: Scalars['uuid']
  /** An object relationship */
  menu_page?: Maybe<Menu_Pages>
  /** An array relationship */
  menu_pages: Array<Menu_Pages>
  /** An aggregated array relationship */
  menu_pages_aggregate: Menu_Pages_Aggregate
  name: Scalars['String']
  /** An array relationship */
  pages_contents: Array<Pages_Contents>
  /** An aggregated array relationship */
  pages_contents_aggregate: Pages_Contents_Aggregate
  slug: Scalars['String']
  updated_at: Scalars['timestamptz']
}

/** columns and relationships of "pages" */
export type PagesDataArgs = {
  path?: Maybe<Scalars['String']>
}

/** columns and relationships of "pages" */
export type PagesMenu_PagesArgs = {
  distinct_on?: Maybe<Array<Menu_Pages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Pages_Order_By>>
  where?: Maybe<Menu_Pages_Bool_Exp>
}

/** columns and relationships of "pages" */
export type PagesMenu_Pages_AggregateArgs = {
  distinct_on?: Maybe<Array<Menu_Pages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Pages_Order_By>>
  where?: Maybe<Menu_Pages_Bool_Exp>
}

/** columns and relationships of "pages" */
export type PagesPages_ContentsArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Order_By>>
  where?: Maybe<Pages_Contents_Bool_Exp>
}

/** columns and relationships of "pages" */
export type PagesPages_Contents_AggregateArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Order_By>>
  where?: Maybe<Pages_Contents_Bool_Exp>
}

/** aggregated selection of "pages" */
export type Pages_Aggregate = {
  __typename?: 'pages_aggregate'
  aggregate?: Maybe<Pages_Aggregate_Fields>
  nodes: Array<Pages>
}

/** aggregate fields of "pages" */
export type Pages_Aggregate_Fields = {
  __typename?: 'pages_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Pages_Max_Fields>
  min?: Maybe<Pages_Min_Fields>
}

/** aggregate fields of "pages" */
export type Pages_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Pages_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "pages" */
export type Pages_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Pages_Max_Order_By>
  min?: Maybe<Pages_Min_Order_By>
}

/** append existing jsonb value of filtered columns with new jsonb value */
export type Pages_Append_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** input type for inserting array relation for remote table "pages" */
export type Pages_Arr_Rel_Insert_Input = {
  data: Array<Pages_Insert_Input>
  on_conflict?: Maybe<Pages_On_Conflict>
}

/** Boolean expression to filter rows from the table "pages". All fields are combined with a logical 'AND'. */
export type Pages_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Pages_Bool_Exp>>>
  _not?: Maybe<Pages_Bool_Exp>
  _or?: Maybe<Array<Maybe<Pages_Bool_Exp>>>
  active?: Maybe<Boolean_Comparison_Exp>
  created_at?: Maybe<Timestamp_Comparison_Exp>
  data?: Maybe<Jsonb_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  menu_page?: Maybe<Menu_Pages_Bool_Exp>
  menu_pages?: Maybe<Menu_Pages_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
  pages_contents?: Maybe<Pages_Contents_Bool_Exp>
  slug?: Maybe<String_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "pages" */
export enum Pages_Constraint {
  /** unique or primary key constraint */
  PagesNameKey = 'pages_name_key',
  /** unique or primary key constraint */
  PagesPkey = 'pages_pkey',
  /** unique or primary key constraint */
  PagesSlugKey = 'pages_slug_key'
}

/** columns and relationships of "pages_contents" */
export type Pages_Contents = {
  __typename?: 'pages_contents'
  content?: Maybe<Scalars['String']>
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  locale: Locales_Enum
  /** An object relationship */
  locale_value: Locales
  name_in_menu: Scalars['String']
  /** An object relationship */
  page: Pages
  page_id: Scalars['uuid']
  /** An array relationship */
  pages_contents_blocks: Array<Pages_Contents_Blocks>
  /** An aggregated array relationship */
  pages_contents_blocks_aggregate: Pages_Contents_Blocks_Aggregate
  /** An object relationship */
  seo: Seo
  seo_id: Scalars['uuid']
  title?: Maybe<Scalars['String']>
  updated_at: Scalars['timestamptz']
}

/** columns and relationships of "pages_contents" */
export type Pages_ContentsPages_Contents_BlocksArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Blocks_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Blocks_Order_By>>
  where?: Maybe<Pages_Contents_Blocks_Bool_Exp>
}

/** columns and relationships of "pages_contents" */
export type Pages_ContentsPages_Contents_Blocks_AggregateArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Blocks_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Blocks_Order_By>>
  where?: Maybe<Pages_Contents_Blocks_Bool_Exp>
}

/** aggregated selection of "pages_contents" */
export type Pages_Contents_Aggregate = {
  __typename?: 'pages_contents_aggregate'
  aggregate?: Maybe<Pages_Contents_Aggregate_Fields>
  nodes: Array<Pages_Contents>
}

/** aggregate fields of "pages_contents" */
export type Pages_Contents_Aggregate_Fields = {
  __typename?: 'pages_contents_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Pages_Contents_Max_Fields>
  min?: Maybe<Pages_Contents_Min_Fields>
}

/** aggregate fields of "pages_contents" */
export type Pages_Contents_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Pages_Contents_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "pages_contents" */
export type Pages_Contents_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Pages_Contents_Max_Order_By>
  min?: Maybe<Pages_Contents_Min_Order_By>
}

/** input type for inserting array relation for remote table "pages_contents" */
export type Pages_Contents_Arr_Rel_Insert_Input = {
  data: Array<Pages_Contents_Insert_Input>
  on_conflict?: Maybe<Pages_Contents_On_Conflict>
}

/** columns and relationships of "pages_contents_blocks" */
export type Pages_Contents_Blocks = {
  __typename?: 'pages_contents_blocks'
  active: Scalars['Boolean']
  content: Scalars['String']
  created_at: Scalars['timestamptz']
  hash: Scalars['String']
  id: Scalars['uuid']
  menu_label?: Maybe<Scalars['String']>
  menu_link?: Maybe<Scalars['String']>
  order: Scalars['Int']
  page_content_id: Scalars['uuid']
  /** An object relationship */
  pages_content: Pages_Contents
  title?: Maybe<Scalars['String']>
  updated_at: Scalars['timestamptz']
}

/** aggregated selection of "pages_contents_blocks" */
export type Pages_Contents_Blocks_Aggregate = {
  __typename?: 'pages_contents_blocks_aggregate'
  aggregate?: Maybe<Pages_Contents_Blocks_Aggregate_Fields>
  nodes: Array<Pages_Contents_Blocks>
}

/** aggregate fields of "pages_contents_blocks" */
export type Pages_Contents_Blocks_Aggregate_Fields = {
  __typename?: 'pages_contents_blocks_aggregate_fields'
  avg?: Maybe<Pages_Contents_Blocks_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Pages_Contents_Blocks_Max_Fields>
  min?: Maybe<Pages_Contents_Blocks_Min_Fields>
  stddev?: Maybe<Pages_Contents_Blocks_Stddev_Fields>
  stddev_pop?: Maybe<Pages_Contents_Blocks_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Pages_Contents_Blocks_Stddev_Samp_Fields>
  sum?: Maybe<Pages_Contents_Blocks_Sum_Fields>
  var_pop?: Maybe<Pages_Contents_Blocks_Var_Pop_Fields>
  var_samp?: Maybe<Pages_Contents_Blocks_Var_Samp_Fields>
  variance?: Maybe<Pages_Contents_Blocks_Variance_Fields>
}

/** aggregate fields of "pages_contents_blocks" */
export type Pages_Contents_Blocks_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Pages_Contents_Blocks_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Aggregate_Order_By = {
  avg?: Maybe<Pages_Contents_Blocks_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Pages_Contents_Blocks_Max_Order_By>
  min?: Maybe<Pages_Contents_Blocks_Min_Order_By>
  stddev?: Maybe<Pages_Contents_Blocks_Stddev_Order_By>
  stddev_pop?: Maybe<Pages_Contents_Blocks_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Pages_Contents_Blocks_Stddev_Samp_Order_By>
  sum?: Maybe<Pages_Contents_Blocks_Sum_Order_By>
  var_pop?: Maybe<Pages_Contents_Blocks_Var_Pop_Order_By>
  var_samp?: Maybe<Pages_Contents_Blocks_Var_Samp_Order_By>
  variance?: Maybe<Pages_Contents_Blocks_Variance_Order_By>
}

/** input type for inserting array relation for remote table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Arr_Rel_Insert_Input = {
  data: Array<Pages_Contents_Blocks_Insert_Input>
  on_conflict?: Maybe<Pages_Contents_Blocks_On_Conflict>
}

/** aggregate avg on columns */
export type Pages_Contents_Blocks_Avg_Fields = {
  __typename?: 'pages_contents_blocks_avg_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Avg_Order_By = {
  order?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "pages_contents_blocks". All fields are combined with a logical 'AND'. */
export type Pages_Contents_Blocks_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Pages_Contents_Blocks_Bool_Exp>>>
  _not?: Maybe<Pages_Contents_Blocks_Bool_Exp>
  _or?: Maybe<Array<Maybe<Pages_Contents_Blocks_Bool_Exp>>>
  active?: Maybe<Boolean_Comparison_Exp>
  content?: Maybe<String_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  hash?: Maybe<String_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  menu_label?: Maybe<String_Comparison_Exp>
  menu_link?: Maybe<String_Comparison_Exp>
  order?: Maybe<Int_Comparison_Exp>
  page_content_id?: Maybe<Uuid_Comparison_Exp>
  pages_content?: Maybe<Pages_Contents_Bool_Exp>
  title?: Maybe<String_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "pages_contents_blocks" */
export enum Pages_Contents_Blocks_Constraint {
  /** unique or primary key constraint */
  PagesBlocksPkey = 'pages_blocks_pkey',
  /** unique or primary key constraint */
  PagesContentsBlocksHashPageContentIdKey = 'pages_contents_blocks_hash_page_content_id_key'
}

/** input type for incrementing integer column in table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Inc_Input = {
  order?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Insert_Input = {
  active?: Maybe<Scalars['Boolean']>
  content?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  hash?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  menu_label?: Maybe<Scalars['String']>
  menu_link?: Maybe<Scalars['String']>
  order?: Maybe<Scalars['Int']>
  page_content_id?: Maybe<Scalars['uuid']>
  pages_content?: Maybe<Pages_Contents_Obj_Rel_Insert_Input>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Pages_Contents_Blocks_Max_Fields = {
  __typename?: 'pages_contents_blocks_max_fields'
  content?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  hash?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  menu_label?: Maybe<Scalars['String']>
  menu_link?: Maybe<Scalars['String']>
  order?: Maybe<Scalars['Int']>
  page_content_id?: Maybe<Scalars['uuid']>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Max_Order_By = {
  content?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  hash?: Maybe<Order_By>
  id?: Maybe<Order_By>
  menu_label?: Maybe<Order_By>
  menu_link?: Maybe<Order_By>
  order?: Maybe<Order_By>
  page_content_id?: Maybe<Order_By>
  title?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Pages_Contents_Blocks_Min_Fields = {
  __typename?: 'pages_contents_blocks_min_fields'
  content?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  hash?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  menu_label?: Maybe<Scalars['String']>
  menu_link?: Maybe<Scalars['String']>
  order?: Maybe<Scalars['Int']>
  page_content_id?: Maybe<Scalars['uuid']>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Min_Order_By = {
  content?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  hash?: Maybe<Order_By>
  id?: Maybe<Order_By>
  menu_label?: Maybe<Order_By>
  menu_link?: Maybe<Order_By>
  order?: Maybe<Order_By>
  page_content_id?: Maybe<Order_By>
  title?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Mutation_Response = {
  __typename?: 'pages_contents_blocks_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Pages_Contents_Blocks>
}

/** input type for inserting object relation for remote table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Obj_Rel_Insert_Input = {
  data: Pages_Contents_Blocks_Insert_Input
  on_conflict?: Maybe<Pages_Contents_Blocks_On_Conflict>
}

/** on conflict condition type for table "pages_contents_blocks" */
export type Pages_Contents_Blocks_On_Conflict = {
  constraint: Pages_Contents_Blocks_Constraint
  update_columns: Array<Pages_Contents_Blocks_Update_Column>
  where?: Maybe<Pages_Contents_Blocks_Bool_Exp>
}

/** ordering options when selecting data from "pages_contents_blocks" */
export type Pages_Contents_Blocks_Order_By = {
  active?: Maybe<Order_By>
  content?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  hash?: Maybe<Order_By>
  id?: Maybe<Order_By>
  menu_label?: Maybe<Order_By>
  menu_link?: Maybe<Order_By>
  order?: Maybe<Order_By>
  page_content_id?: Maybe<Order_By>
  pages_content?: Maybe<Pages_Contents_Order_By>
  title?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "pages_contents_blocks" */
export type Pages_Contents_Blocks_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "pages_contents_blocks" */
export enum Pages_Contents_Blocks_Select_Column {
  /** column name */
  Active = 'active',
  /** column name */
  Content = 'content',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Hash = 'hash',
  /** column name */
  Id = 'id',
  /** column name */
  MenuLabel = 'menu_label',
  /** column name */
  MenuLink = 'menu_link',
  /** column name */
  Order = 'order',
  /** column name */
  PageContentId = 'page_content_id',
  /** column name */
  Title = 'title',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Set_Input = {
  active?: Maybe<Scalars['Boolean']>
  content?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  hash?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  menu_label?: Maybe<Scalars['String']>
  menu_link?: Maybe<Scalars['String']>
  order?: Maybe<Scalars['Int']>
  page_content_id?: Maybe<Scalars['uuid']>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate stddev on columns */
export type Pages_Contents_Blocks_Stddev_Fields = {
  __typename?: 'pages_contents_blocks_stddev_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Stddev_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Pages_Contents_Blocks_Stddev_Pop_Fields = {
  __typename?: 'pages_contents_blocks_stddev_pop_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Stddev_Pop_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Pages_Contents_Blocks_Stddev_Samp_Fields = {
  __typename?: 'pages_contents_blocks_stddev_samp_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Stddev_Samp_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Pages_Contents_Blocks_Sum_Fields = {
  __typename?: 'pages_contents_blocks_sum_fields'
  order?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Sum_Order_By = {
  order?: Maybe<Order_By>
}

/** update columns of table "pages_contents_blocks" */
export enum Pages_Contents_Blocks_Update_Column {
  /** column name */
  Active = 'active',
  /** column name */
  Content = 'content',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Hash = 'hash',
  /** column name */
  Id = 'id',
  /** column name */
  MenuLabel = 'menu_label',
  /** column name */
  MenuLink = 'menu_link',
  /** column name */
  Order = 'order',
  /** column name */
  PageContentId = 'page_content_id',
  /** column name */
  Title = 'title',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate var_pop on columns */
export type Pages_Contents_Blocks_Var_Pop_Fields = {
  __typename?: 'pages_contents_blocks_var_pop_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Var_Pop_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Pages_Contents_Blocks_Var_Samp_Fields = {
  __typename?: 'pages_contents_blocks_var_samp_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Var_Samp_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Pages_Contents_Blocks_Variance_Fields = {
  __typename?: 'pages_contents_blocks_variance_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "pages_contents_blocks" */
export type Pages_Contents_Blocks_Variance_Order_By = {
  order?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "pages_contents". All fields are combined with a logical 'AND'. */
export type Pages_Contents_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Pages_Contents_Bool_Exp>>>
  _not?: Maybe<Pages_Contents_Bool_Exp>
  _or?: Maybe<Array<Maybe<Pages_Contents_Bool_Exp>>>
  content?: Maybe<String_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  locale?: Maybe<Locales_Enum_Comparison_Exp>
  locale_value?: Maybe<Locales_Bool_Exp>
  name_in_menu?: Maybe<String_Comparison_Exp>
  page?: Maybe<Pages_Bool_Exp>
  page_id?: Maybe<Uuid_Comparison_Exp>
  pages_contents_blocks?: Maybe<Pages_Contents_Blocks_Bool_Exp>
  seo?: Maybe<Seo_Bool_Exp>
  seo_id?: Maybe<Uuid_Comparison_Exp>
  title?: Maybe<String_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "pages_contents" */
export enum Pages_Contents_Constraint {
  /** unique or primary key constraint */
  PagesContentsPageIdLocaleKey = 'pages_contents_page_id_locale_key',
  /** unique or primary key constraint */
  PagesContentsPkey = 'pages_contents_pkey',
  /** unique or primary key constraint */
  PagesContentsSeoIdKey = 'pages_contents_seo_id_key'
}

/** input type for inserting data into table "pages_contents" */
export type Pages_Contents_Insert_Input = {
  content?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  locale?: Maybe<Locales_Enum>
  locale_value?: Maybe<Locales_Obj_Rel_Insert_Input>
  name_in_menu?: Maybe<Scalars['String']>
  page?: Maybe<Pages_Obj_Rel_Insert_Input>
  page_id?: Maybe<Scalars['uuid']>
  pages_contents_blocks?: Maybe<Pages_Contents_Blocks_Arr_Rel_Insert_Input>
  seo?: Maybe<Seo_Obj_Rel_Insert_Input>
  seo_id?: Maybe<Scalars['uuid']>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Pages_Contents_Max_Fields = {
  __typename?: 'pages_contents_max_fields'
  content?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name_in_menu?: Maybe<Scalars['String']>
  page_id?: Maybe<Scalars['uuid']>
  seo_id?: Maybe<Scalars['uuid']>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "pages_contents" */
export type Pages_Contents_Max_Order_By = {
  content?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name_in_menu?: Maybe<Order_By>
  page_id?: Maybe<Order_By>
  seo_id?: Maybe<Order_By>
  title?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Pages_Contents_Min_Fields = {
  __typename?: 'pages_contents_min_fields'
  content?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name_in_menu?: Maybe<Scalars['String']>
  page_id?: Maybe<Scalars['uuid']>
  seo_id?: Maybe<Scalars['uuid']>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "pages_contents" */
export type Pages_Contents_Min_Order_By = {
  content?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name_in_menu?: Maybe<Order_By>
  page_id?: Maybe<Order_By>
  seo_id?: Maybe<Order_By>
  title?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "pages_contents" */
export type Pages_Contents_Mutation_Response = {
  __typename?: 'pages_contents_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Pages_Contents>
}

/** input type for inserting object relation for remote table "pages_contents" */
export type Pages_Contents_Obj_Rel_Insert_Input = {
  data: Pages_Contents_Insert_Input
  on_conflict?: Maybe<Pages_Contents_On_Conflict>
}

/** on conflict condition type for table "pages_contents" */
export type Pages_Contents_On_Conflict = {
  constraint: Pages_Contents_Constraint
  update_columns: Array<Pages_Contents_Update_Column>
  where?: Maybe<Pages_Contents_Bool_Exp>
}

/** ordering options when selecting data from "pages_contents" */
export type Pages_Contents_Order_By = {
  content?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  locale?: Maybe<Order_By>
  locale_value?: Maybe<Locales_Order_By>
  name_in_menu?: Maybe<Order_By>
  page?: Maybe<Pages_Order_By>
  page_id?: Maybe<Order_By>
  pages_contents_blocks_aggregate?: Maybe<
    Pages_Contents_Blocks_Aggregate_Order_By
  >
  seo?: Maybe<Seo_Order_By>
  seo_id?: Maybe<Order_By>
  title?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "pages_contents" */
export type Pages_Contents_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "pages_contents" */
export enum Pages_Contents_Select_Column {
  /** column name */
  Content = 'content',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Locale = 'locale',
  /** column name */
  NameInMenu = 'name_in_menu',
  /** column name */
  PageId = 'page_id',
  /** column name */
  SeoId = 'seo_id',
  /** column name */
  Title = 'title',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "pages_contents" */
export type Pages_Contents_Set_Input = {
  content?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  locale?: Maybe<Locales_Enum>
  name_in_menu?: Maybe<Scalars['String']>
  page_id?: Maybe<Scalars['uuid']>
  seo_id?: Maybe<Scalars['uuid']>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** update columns of table "pages_contents" */
export enum Pages_Contents_Update_Column {
  /** column name */
  Content = 'content',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Locale = 'locale',
  /** column name */
  NameInMenu = 'name_in_menu',
  /** column name */
  PageId = 'page_id',
  /** column name */
  SeoId = 'seo_id',
  /** column name */
  Title = 'title',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Pages_Delete_At_Path_Input = {
  data?: Maybe<Array<Maybe<Scalars['String']>>>
}

/**
 * delete the array element with specified index (negative integers count from the
 * end). throws an error if top level container is not an array
 */
export type Pages_Delete_Elem_Input = {
  data?: Maybe<Scalars['Int']>
}

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Pages_Delete_Key_Input = {
  data?: Maybe<Scalars['String']>
}

/** input type for inserting data into table "pages" */
export type Pages_Insert_Input = {
  active?: Maybe<Scalars['Boolean']>
  created_at?: Maybe<Scalars['timestamp']>
  data?: Maybe<Scalars['jsonb']>
  id?: Maybe<Scalars['uuid']>
  menu_page?: Maybe<Menu_Pages_Obj_Rel_Insert_Input>
  menu_pages?: Maybe<Menu_Pages_Arr_Rel_Insert_Input>
  name?: Maybe<Scalars['String']>
  pages_contents?: Maybe<Pages_Contents_Arr_Rel_Insert_Input>
  slug?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Pages_Max_Fields = {
  __typename?: 'pages_max_fields'
  created_at?: Maybe<Scalars['timestamp']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  slug?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "pages" */
export type Pages_Max_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  slug?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Pages_Min_Fields = {
  __typename?: 'pages_min_fields'
  created_at?: Maybe<Scalars['timestamp']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  slug?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "pages" */
export type Pages_Min_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  slug?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "pages" */
export type Pages_Mutation_Response = {
  __typename?: 'pages_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Pages>
}

/** input type for inserting object relation for remote table "pages" */
export type Pages_Obj_Rel_Insert_Input = {
  data: Pages_Insert_Input
  on_conflict?: Maybe<Pages_On_Conflict>
}

/** on conflict condition type for table "pages" */
export type Pages_On_Conflict = {
  constraint: Pages_Constraint
  update_columns: Array<Pages_Update_Column>
  where?: Maybe<Pages_Bool_Exp>
}

/** ordering options when selecting data from "pages" */
export type Pages_Order_By = {
  active?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  data?: Maybe<Order_By>
  id?: Maybe<Order_By>
  menu_page?: Maybe<Menu_Pages_Order_By>
  menu_pages_aggregate?: Maybe<Menu_Pages_Aggregate_Order_By>
  name?: Maybe<Order_By>
  pages_contents_aggregate?: Maybe<Pages_Contents_Aggregate_Order_By>
  slug?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "pages" */
export type Pages_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Pages_Prepend_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** select columns of table "pages" */
export enum Pages_Select_Column {
  /** column name */
  Active = 'active',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Data = 'data',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Slug = 'slug',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "pages" */
export type Pages_Set_Input = {
  active?: Maybe<Scalars['Boolean']>
  created_at?: Maybe<Scalars['timestamp']>
  data?: Maybe<Scalars['jsonb']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  slug?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** update columns of table "pages" */
export enum Pages_Update_Column {
  /** column name */
  Active = 'active',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Data = 'data',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Slug = 'slug',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** columns and relationships of "payment_types" */
export type Payment_Types = {
  __typename?: 'payment_types'
  /** An array relationship */
  albums_payment_types: Array<Albums_Payment_Types>
  /** An aggregated array relationship */
  albums_payment_types_aggregate: Albums_Payment_Types_Aggregate
  /** An array relationship */
  events_payment_types: Array<Events_Payment_Types>
  /** An aggregated array relationship */
  events_payment_types_aggregate: Events_Payment_Types_Aggregate
  /** An array relationship */
  users_payment_types: Array<Users_Payment_Types>
  /** An aggregated array relationship */
  users_payment_types_aggregate: Users_Payment_Types_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "payment_types" */
export type Payment_TypesAlbums_Payment_TypesArgs = {
  distinct_on?: Maybe<Array<Albums_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Payment_Types_Order_By>>
  where?: Maybe<Albums_Payment_Types_Bool_Exp>
}

/** columns and relationships of "payment_types" */
export type Payment_TypesAlbums_Payment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Payment_Types_Order_By>>
  where?: Maybe<Albums_Payment_Types_Bool_Exp>
}

/** columns and relationships of "payment_types" */
export type Payment_TypesEvents_Payment_TypesArgs = {
  distinct_on?: Maybe<Array<Events_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Payment_Types_Order_By>>
  where?: Maybe<Events_Payment_Types_Bool_Exp>
}

/** columns and relationships of "payment_types" */
export type Payment_TypesEvents_Payment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Payment_Types_Order_By>>
  where?: Maybe<Events_Payment_Types_Bool_Exp>
}

/** columns and relationships of "payment_types" */
export type Payment_TypesUsers_Payment_TypesArgs = {
  distinct_on?: Maybe<Array<Users_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Payment_Types_Order_By>>
  where?: Maybe<Users_Payment_Types_Bool_Exp>
}

/** columns and relationships of "payment_types" */
export type Payment_TypesUsers_Payment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Payment_Types_Order_By>>
  where?: Maybe<Users_Payment_Types_Bool_Exp>
}

/** aggregated selection of "payment_types" */
export type Payment_Types_Aggregate = {
  __typename?: 'payment_types_aggregate'
  aggregate?: Maybe<Payment_Types_Aggregate_Fields>
  nodes: Array<Payment_Types>
}

/** aggregate fields of "payment_types" */
export type Payment_Types_Aggregate_Fields = {
  __typename?: 'payment_types_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Payment_Types_Max_Fields>
  min?: Maybe<Payment_Types_Min_Fields>
}

/** aggregate fields of "payment_types" */
export type Payment_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Payment_Types_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "payment_types" */
export type Payment_Types_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Payment_Types_Max_Order_By>
  min?: Maybe<Payment_Types_Min_Order_By>
}

/** input type for inserting array relation for remote table "payment_types" */
export type Payment_Types_Arr_Rel_Insert_Input = {
  data: Array<Payment_Types_Insert_Input>
  on_conflict?: Maybe<Payment_Types_On_Conflict>
}

/** Boolean expression to filter rows from the table "payment_types". All fields are combined with a logical 'AND'. */
export type Payment_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Payment_Types_Bool_Exp>>>
  _not?: Maybe<Payment_Types_Bool_Exp>
  _or?: Maybe<Array<Maybe<Payment_Types_Bool_Exp>>>
  albums_payment_types?: Maybe<Albums_Payment_Types_Bool_Exp>
  events_payment_types?: Maybe<Events_Payment_Types_Bool_Exp>
  users_payment_types?: Maybe<Users_Payment_Types_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "payment_types" */
export enum Payment_Types_Constraint {
  /** unique or primary key constraint */
  PaymentTypesPkey = 'payment_types_pkey'
}

export enum Payment_Types_Enum {
  Donation = 'DONATION',
  Fixed = 'FIXED',
  Free = 'FREE',
  PerFile = 'PER_FILE'
}

/** expression to compare columns of type payment_types_enum. All fields are combined with logical 'AND'. */
export type Payment_Types_Enum_Comparison_Exp = {
  _eq?: Maybe<Payment_Types_Enum>
  _in?: Maybe<Array<Payment_Types_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Payment_Types_Enum>
  _nin?: Maybe<Array<Payment_Types_Enum>>
}

/** input type for inserting data into table "payment_types" */
export type Payment_Types_Insert_Input = {
  albums_payment_types?: Maybe<Albums_Payment_Types_Arr_Rel_Insert_Input>
  events_payment_types?: Maybe<Events_Payment_Types_Arr_Rel_Insert_Input>
  users_payment_types?: Maybe<Users_Payment_Types_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Payment_Types_Max_Fields = {
  __typename?: 'payment_types_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "payment_types" */
export type Payment_Types_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Payment_Types_Min_Fields = {
  __typename?: 'payment_types_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "payment_types" */
export type Payment_Types_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "payment_types" */
export type Payment_Types_Mutation_Response = {
  __typename?: 'payment_types_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Payment_Types>
}

/** input type for inserting object relation for remote table "payment_types" */
export type Payment_Types_Obj_Rel_Insert_Input = {
  data: Payment_Types_Insert_Input
  on_conflict?: Maybe<Payment_Types_On_Conflict>
}

/** on conflict condition type for table "payment_types" */
export type Payment_Types_On_Conflict = {
  constraint: Payment_Types_Constraint
  update_columns: Array<Payment_Types_Update_Column>
  where?: Maybe<Payment_Types_Bool_Exp>
}

/** ordering options when selecting data from "payment_types" */
export type Payment_Types_Order_By = {
  albums_payment_types_aggregate?: Maybe<
    Albums_Payment_Types_Aggregate_Order_By
  >
  events_payment_types_aggregate?: Maybe<
    Events_Payment_Types_Aggregate_Order_By
  >
  users_payment_types_aggregate?: Maybe<Users_Payment_Types_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "payment_types" */
export type Payment_Types_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "payment_types" */
export enum Payment_Types_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "payment_types" */
export type Payment_Types_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "payment_types" */
export enum Payment_Types_Update_Column {
  /** column name */
  Value = 'value'
}

/** columns and relationships of "permissions" */
export type Permissions = {
  __typename?: 'permissions'
  id: Scalars['String']
  name?: Maybe<Scalars['String']>
  /** An object relationship */
  users_permission?: Maybe<Users_Permissions>
}

/** aggregated selection of "permissions" */
export type Permissions_Aggregate = {
  __typename?: 'permissions_aggregate'
  aggregate?: Maybe<Permissions_Aggregate_Fields>
  nodes: Array<Permissions>
}

/** aggregate fields of "permissions" */
export type Permissions_Aggregate_Fields = {
  __typename?: 'permissions_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Permissions_Max_Fields>
  min?: Maybe<Permissions_Min_Fields>
}

/** aggregate fields of "permissions" */
export type Permissions_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Permissions_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "permissions" */
export type Permissions_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Permissions_Max_Order_By>
  min?: Maybe<Permissions_Min_Order_By>
}

/** input type for inserting array relation for remote table "permissions" */
export type Permissions_Arr_Rel_Insert_Input = {
  data: Array<Permissions_Insert_Input>
  on_conflict?: Maybe<Permissions_On_Conflict>
}

/** Boolean expression to filter rows from the table "permissions". All fields are combined with a logical 'AND'. */
export type Permissions_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Permissions_Bool_Exp>>>
  _not?: Maybe<Permissions_Bool_Exp>
  _or?: Maybe<Array<Maybe<Permissions_Bool_Exp>>>
  id?: Maybe<String_Comparison_Exp>
  name?: Maybe<String_Comparison_Exp>
  users_permission?: Maybe<Users_Permissions_Bool_Exp>
}

/** unique or primary key constraints on table "permissions" */
export enum Permissions_Constraint {
  /** unique or primary key constraint */
  PermissionsPkey = 'permissions_pkey'
}

export enum Permissions_Enum {
  /** Support */
  Support = 'SUPPORT'
}

/** expression to compare columns of type permissions_enum. All fields are combined with logical 'AND'. */
export type Permissions_Enum_Comparison_Exp = {
  _eq?: Maybe<Permissions_Enum>
  _in?: Maybe<Array<Permissions_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Permissions_Enum>
  _nin?: Maybe<Array<Permissions_Enum>>
}

/** input type for inserting data into table "permissions" */
export type Permissions_Insert_Input = {
  id?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
  users_permission?: Maybe<Users_Permissions_Obj_Rel_Insert_Input>
}

/** aggregate max on columns */
export type Permissions_Max_Fields = {
  __typename?: 'permissions_max_fields'
  id?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "permissions" */
export type Permissions_Max_Order_By = {
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Permissions_Min_Fields = {
  __typename?: 'permissions_min_fields'
  id?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "permissions" */
export type Permissions_Min_Order_By = {
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
}

/** response of any mutation on the table "permissions" */
export type Permissions_Mutation_Response = {
  __typename?: 'permissions_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Permissions>
}

/** input type for inserting object relation for remote table "permissions" */
export type Permissions_Obj_Rel_Insert_Input = {
  data: Permissions_Insert_Input
  on_conflict?: Maybe<Permissions_On_Conflict>
}

/** on conflict condition type for table "permissions" */
export type Permissions_On_Conflict = {
  constraint: Permissions_Constraint
  update_columns: Array<Permissions_Update_Column>
  where?: Maybe<Permissions_Bool_Exp>
}

/** ordering options when selecting data from "permissions" */
export type Permissions_Order_By = {
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  users_permission?: Maybe<Users_Permissions_Order_By>
}

/** primary key columns input for table: "permissions" */
export type Permissions_Pk_Columns_Input = {
  id: Scalars['String']
}

/** select columns of table "permissions" */
export enum Permissions_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "permissions" */
export type Permissions_Set_Input = {
  id?: Maybe<Scalars['String']>
  name?: Maybe<Scalars['String']>
}

/** update columns of table "permissions" */
export enum Permissions_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** columns and relationships of "purchased" */
export type Purchased = {
  __typename?: 'purchased'
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  /** An object relationship */
  media_file: Media_Files
  media_file_id: Scalars['uuid']
  /** An object relationship */
  transaction?: Maybe<Transactions>
  transaction_id?: Maybe<Scalars['uuid']>
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** aggregated selection of "purchased" */
export type Purchased_Aggregate = {
  __typename?: 'purchased_aggregate'
  aggregate?: Maybe<Purchased_Aggregate_Fields>
  nodes: Array<Purchased>
}

/** aggregate fields of "purchased" */
export type Purchased_Aggregate_Fields = {
  __typename?: 'purchased_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Purchased_Max_Fields>
  min?: Maybe<Purchased_Min_Fields>
}

/** aggregate fields of "purchased" */
export type Purchased_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Purchased_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "purchased" */
export type Purchased_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Purchased_Max_Order_By>
  min?: Maybe<Purchased_Min_Order_By>
}

/** input type for inserting array relation for remote table "purchased" */
export type Purchased_Arr_Rel_Insert_Input = {
  data: Array<Purchased_Insert_Input>
  on_conflict?: Maybe<Purchased_On_Conflict>
}

/** Boolean expression to filter rows from the table "purchased". All fields are combined with a logical 'AND'. */
export type Purchased_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Purchased_Bool_Exp>>>
  _not?: Maybe<Purchased_Bool_Exp>
  _or?: Maybe<Array<Maybe<Purchased_Bool_Exp>>>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  media_file?: Maybe<Media_Files_Bool_Exp>
  media_file_id?: Maybe<Uuid_Comparison_Exp>
  transaction?: Maybe<Transactions_Bool_Exp>
  transaction_id?: Maybe<Uuid_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "purchased" */
export enum Purchased_Constraint {
  /** unique or primary key constraint */
  PurchasedPkey = 'purchased_pkey'
}

/** input type for inserting data into table "purchased" */
export type Purchased_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  media_file?: Maybe<Media_Files_Obj_Rel_Insert_Input>
  media_file_id?: Maybe<Scalars['uuid']>
  transaction?: Maybe<Transactions_Obj_Rel_Insert_Input>
  transaction_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Purchased_Max_Fields = {
  __typename?: 'purchased_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  transaction_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "purchased" */
export type Purchased_Max_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_file_id?: Maybe<Order_By>
  transaction_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Purchased_Min_Fields = {
  __typename?: 'purchased_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  transaction_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "purchased" */
export type Purchased_Min_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_file_id?: Maybe<Order_By>
  transaction_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "purchased" */
export type Purchased_Mutation_Response = {
  __typename?: 'purchased_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Purchased>
}

/** input type for inserting object relation for remote table "purchased" */
export type Purchased_Obj_Rel_Insert_Input = {
  data: Purchased_Insert_Input
  on_conflict?: Maybe<Purchased_On_Conflict>
}

/** on conflict condition type for table "purchased" */
export type Purchased_On_Conflict = {
  constraint: Purchased_Constraint
  update_columns: Array<Purchased_Update_Column>
  where?: Maybe<Purchased_Bool_Exp>
}

/** ordering options when selecting data from "purchased" */
export type Purchased_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  media_file?: Maybe<Media_Files_Order_By>
  media_file_id?: Maybe<Order_By>
  transaction?: Maybe<Transactions_Order_By>
  transaction_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "purchased" */
export type Purchased_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "purchased" */
export enum Purchased_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  MediaFileId = 'media_file_id',
  /** column name */
  TransactionId = 'transaction_id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "purchased" */
export type Purchased_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  transaction_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "purchased" */
export enum Purchased_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  MediaFileId = 'media_file_id',
  /** column name */
  TransactionId = 'transaction_id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

export type Query = {
  __typename?: 'Query'
  geo_autocomplete?: Maybe<Array<GeoOutput>>
  geo_reverse?: Maybe<Array<GeoOutput>>
  geo_search?: Maybe<Array<GeoOutput>>
}

export type QueryGeo_AutocompleteArgs = {
  lang?: Maybe<Scalars['String']>
  text: Scalars['String']
  type?: Maybe<Scalars['String']>
}

export type QueryGeo_ReverseArgs = {
  lang?: Maybe<Scalars['String']>
  lat: Scalars['Float']
  lng: Scalars['Float']
  type?: Maybe<Scalars['String']>
}

export type QueryGeo_SearchArgs = {
  lang?: Maybe<Scalars['String']>
  text: Scalars['String']
  type?: Maybe<Scalars['String']>
}

/** query root */
export type Query_Root = {
  __typename?: 'query_root'
  /** fetch data from the table: "albums" */
  albums: Array<Albums>
  /** fetch aggregated fields from the table: "albums" */
  albums_aggregate: Albums_Aggregate
  /** fetch data from the table: "albums" using primary key columns */
  albums_by_pk?: Maybe<Albums>
  /** fetch data from the table: "albums_payment_types" */
  albums_payment_types: Array<Albums_Payment_Types>
  /** fetch aggregated fields from the table: "albums_payment_types" */
  albums_payment_types_aggregate: Albums_Payment_Types_Aggregate
  /** fetch data from the table: "albums_payment_types" using primary key columns */
  albums_payment_types_by_pk?: Maybe<Albums_Payment_Types>
  /** fetch data from the table: "albums_statistics_view" */
  albums_statistics_view: Array<Albums_Statistics_View>
  /** fetch aggregated fields from the table: "albums_statistics_view" */
  albums_statistics_view_aggregate: Albums_Statistics_View_Aggregate
  /** fetch data from the table: "albums_tags" */
  albums_tags: Array<Albums_Tags>
  /** fetch aggregated fields from the table: "albums_tags" */
  albums_tags_aggregate: Albums_Tags_Aggregate
  /** fetch data from the table: "albums_tags" using primary key columns */
  albums_tags_by_pk?: Maybe<Albums_Tags>
  /** fetch data from the table: "albums_visits" */
  albums_visits: Array<Albums_Visits>
  /** fetch aggregated fields from the table: "albums_visits" */
  albums_visits_aggregate: Albums_Visits_Aggregate
  /** fetch data from the table: "albums_visits" using primary key columns */
  albums_visits_by_pk?: Maybe<Albums_Visits>
  /** fetch data from the table: "auth.account_providers" */
  auth_account_providers: Array<Auth_Account_Providers>
  /** fetch aggregated fields from the table: "auth.account_providers" */
  auth_account_providers_aggregate: Auth_Account_Providers_Aggregate
  /** fetch data from the table: "auth.account_providers" using primary key columns */
  auth_account_providers_by_pk?: Maybe<Auth_Account_Providers>
  /** fetch data from the table: "auth.account_roles" */
  auth_account_roles: Array<Auth_Account_Roles>
  /** fetch aggregated fields from the table: "auth.account_roles" */
  auth_account_roles_aggregate: Auth_Account_Roles_Aggregate
  /** fetch data from the table: "auth.account_roles" using primary key columns */
  auth_account_roles_by_pk?: Maybe<Auth_Account_Roles>
  /** fetch data from the table: "auth.accounts" */
  auth_accounts: Array<Auth_Accounts>
  /** fetch aggregated fields from the table: "auth.accounts" */
  auth_accounts_aggregate: Auth_Accounts_Aggregate
  /** fetch data from the table: "auth.accounts" using primary key columns */
  auth_accounts_by_pk?: Maybe<Auth_Accounts>
  /** fetch data from the table: "auth.providers" */
  auth_providers: Array<Auth_Providers>
  /** fetch aggregated fields from the table: "auth.providers" */
  auth_providers_aggregate: Auth_Providers_Aggregate
  /** fetch data from the table: "auth.providers" using primary key columns */
  auth_providers_by_pk?: Maybe<Auth_Providers>
  /** fetch data from the table: "auth.refresh_tokens" */
  auth_refresh_tokens: Array<Auth_Refresh_Tokens>
  /** fetch aggregated fields from the table: "auth.refresh_tokens" */
  auth_refresh_tokens_aggregate: Auth_Refresh_Tokens_Aggregate
  /** fetch data from the table: "auth.refresh_tokens" using primary key columns */
  auth_refresh_tokens_by_pk?: Maybe<Auth_Refresh_Tokens>
  /** fetch data from the table: "auth.roles" */
  auth_roles: Array<Auth_Roles>
  /** fetch aggregated fields from the table: "auth.roles" */
  auth_roles_aggregate: Auth_Roles_Aggregate
  /** fetch data from the table: "auth.roles" using primary key columns */
  auth_roles_by_pk?: Maybe<Auth_Roles>
  /** fetch data from the table: "config" */
  config: Array<Config>
  /** fetch aggregated fields from the table: "config" */
  config_aggregate: Config_Aggregate
  /** fetch data from the table: "config" using primary key columns */
  config_by_pk?: Maybe<Config>
  /** fetch data from the table: "config_keys" */
  config_keys: Array<Config_Keys>
  /** fetch aggregated fields from the table: "config_keys" */
  config_keys_aggregate: Config_Keys_Aggregate
  /** fetch data from the table: "config_keys" using primary key columns */
  config_keys_by_pk?: Maybe<Config_Keys>
  /** fetch data from the table: "contacts" */
  contacts: Array<Contacts>
  /** fetch aggregated fields from the table: "contacts" */
  contacts_aggregate: Contacts_Aggregate
  /** fetch data from the table: "contacts" using primary key columns */
  contacts_by_pk?: Maybe<Contacts>
  /** fetch data from the table: "cover_images" */
  cover_images: Array<Cover_Images>
  /** fetch aggregated fields from the table: "cover_images" */
  cover_images_aggregate: Cover_Images_Aggregate
  /** fetch data from the table: "cover_images" using primary key columns */
  cover_images_by_pk?: Maybe<Cover_Images>
  /** fetch data from the table: "credit_cards" */
  credit_cards: Array<Credit_Cards>
  /** fetch aggregated fields from the table: "credit_cards" */
  credit_cards_aggregate: Credit_Cards_Aggregate
  /** fetch data from the table: "credit_cards" using primary key columns */
  credit_cards_by_pk?: Maybe<Credit_Cards>
  /** fetch data from the table: "downloads" */
  downloads: Array<Downloads>
  /** fetch aggregated fields from the table: "downloads" */
  downloads_aggregate: Downloads_Aggregate
  /** fetch data from the table: "downloads" using primary key columns */
  downloads_by_pk?: Maybe<Downloads>
  /** fetch data from the table: "events" */
  events: Array<Events>
  /** fetch aggregated fields from the table: "events" */
  events_aggregate: Events_Aggregate
  /** fetch data from the table: "events" using primary key columns */
  events_by_pk?: Maybe<Events>
  /** fetch data from the table: "events_payment_types" */
  events_payment_types: Array<Events_Payment_Types>
  /** fetch aggregated fields from the table: "events_payment_types" */
  events_payment_types_aggregate: Events_Payment_Types_Aggregate
  /** fetch data from the table: "events_payment_types" using primary key columns */
  events_payment_types_by_pk?: Maybe<Events_Payment_Types>
  /** fetch data from the table: "events_statuses" */
  events_statuses: Array<Events_Statuses>
  /** fetch aggregated fields from the table: "events_statuses" */
  events_statuses_aggregate: Events_Statuses_Aggregate
  /** fetch data from the table: "events_statuses" using primary key columns */
  events_statuses_by_pk?: Maybe<Events_Statuses>
  /** fetch data from the table: "favorites" */
  favorites: Array<Favorites>
  /** fetch aggregated fields from the table: "favorites" */
  favorites_aggregate: Favorites_Aggregate
  /** fetch data from the table: "favorites" using primary key columns */
  favorites_by_pk?: Maybe<Favorites>
  /** fetch data from the table: "folders" */
  folders: Array<Folders>
  /** fetch aggregated fields from the table: "folders" */
  folders_aggregate: Folders_Aggregate
  /** fetch data from the table: "folders" using primary key columns */
  folders_by_pk?: Maybe<Folders>
  geo_autocomplete?: Maybe<Array<GeoOutput>>
  geo_reverse?: Maybe<Array<GeoOutput>>
  geo_search?: Maybe<Array<GeoOutput>>
  /** fetch data from the table: "geography_columns" */
  geography_columns: Array<Geography_Columns>
  /** fetch aggregated fields from the table: "geography_columns" */
  geography_columns_aggregate: Geography_Columns_Aggregate
  /** fetch data from the table: "geometry_columns" */
  geometry_columns: Array<Geometry_Columns>
  /** fetch aggregated fields from the table: "geometry_columns" */
  geometry_columns_aggregate: Geometry_Columns_Aggregate
  /** fetch data from the table: "locales" */
  locales: Array<Locales>
  /** fetch aggregated fields from the table: "locales" */
  locales_aggregate: Locales_Aggregate
  /** fetch data from the table: "locales" using primary key columns */
  locales_by_pk?: Maybe<Locales>
  /** fetch data from the table: "localizations" */
  localizations: Array<Localizations>
  /** fetch aggregated fields from the table: "localizations" */
  localizations_aggregate: Localizations_Aggregate
  /** fetch data from the table: "localizations" using primary key columns */
  localizations_by_pk?: Maybe<Localizations>
  /** fetch data from the table: "locations" */
  locations: Array<Locations>
  /** fetch aggregated fields from the table: "locations" */
  locations_aggregate: Locations_Aggregate
  /** fetch data from the table: "locations" using primary key columns */
  locations_by_pk?: Maybe<Locations>
  /** fetch data from the table: "maker_types" */
  maker_types: Array<Maker_Types>
  /** fetch aggregated fields from the table: "maker_types" */
  maker_types_aggregate: Maker_Types_Aggregate
  /** fetch data from the table: "maker_types" using primary key columns */
  maker_types_by_pk?: Maybe<Maker_Types>
  /** fetch data from the table: "makers" */
  makers: Array<Makers>
  /** fetch aggregated fields from the table: "makers" */
  makers_aggregate: Makers_Aggregate
  /** fetch data from the table: "makers" using primary key columns */
  makers_by_pk?: Maybe<Makers>
  /** fetch data from the table: "media_file_types" */
  media_file_types: Array<Media_File_Types>
  /** fetch aggregated fields from the table: "media_file_types" */
  media_file_types_aggregate: Media_File_Types_Aggregate
  /** fetch data from the table: "media_file_types" using primary key columns */
  media_file_types_by_pk?: Maybe<Media_File_Types>
  /** fetch data from the table: "media_files" */
  media_files: Array<Media_Files>
  /** fetch aggregated fields from the table: "media_files" */
  media_files_aggregate: Media_Files_Aggregate
  /** fetch data from the table: "media_files" using primary key columns */
  media_files_by_pk?: Maybe<Media_Files>
  /** fetch data from the table: "menu_items" */
  menu_items: Array<Menu_Items>
  /** fetch aggregated fields from the table: "menu_items" */
  menu_items_aggregate: Menu_Items_Aggregate
  /** fetch data from the table: "menu_items" using primary key columns */
  menu_items_by_pk?: Maybe<Menu_Items>
  /** fetch data from the table: "menu_pages" */
  menu_pages: Array<Menu_Pages>
  /** fetch aggregated fields from the table: "menu_pages" */
  menu_pages_aggregate: Menu_Pages_Aggregate
  /** fetch data from the table: "menu_pages" using primary key columns */
  menu_pages_by_pk?: Maybe<Menu_Pages>
  /** fetch data from the table: "menu_type" */
  menu_type: Array<Menu_Type>
  /** fetch aggregated fields from the table: "menu_type" */
  menu_type_aggregate: Menu_Type_Aggregate
  /** fetch data from the table: "menu_type" using primary key columns */
  menu_type_by_pk?: Maybe<Menu_Type>
  /** fetch data from the table: "messages" */
  messages: Array<Messages>
  /** fetch aggregated fields from the table: "messages" */
  messages_aggregate: Messages_Aggregate
  /** fetch data from the table: "messages" using primary key columns */
  messages_by_pk?: Maybe<Messages>
  /** fetch data from the table: "messages_events_updated" */
  messages_events_updated: Array<Messages_Events_Updated>
  /** fetch aggregated fields from the table: "messages_events_updated" */
  messages_events_updated_aggregate: Messages_Events_Updated_Aggregate
  /** fetch data from the table: "messages_events_updated" using primary key columns */
  messages_events_updated_by_pk?: Maybe<Messages_Events_Updated>
  /** fetch data from the table: "messages_statuses" */
  messages_statuses: Array<Messages_Statuses>
  /** fetch aggregated fields from the table: "messages_statuses" */
  messages_statuses_aggregate: Messages_Statuses_Aggregate
  /** fetch data from the table: "messages_statuses" using primary key columns */
  messages_statuses_by_pk?: Maybe<Messages_Statuses>
  /** fetch data from the table: "messages_types" */
  messages_types: Array<Messages_Types>
  /** fetch aggregated fields from the table: "messages_types" */
  messages_types_aggregate: Messages_Types_Aggregate
  /** fetch data from the table: "messages_types" using primary key columns */
  messages_types_by_pk?: Maybe<Messages_Types>
  /** fetch data from the table: "network_transactions" */
  network_transactions: Array<Network_Transactions>
  /** fetch aggregated fields from the table: "network_transactions" */
  network_transactions_aggregate: Network_Transactions_Aggregate
  /** fetch data from the table: "network_transactions" using primary key columns */
  network_transactions_by_pk?: Maybe<Network_Transactions>
  /** fetch data from the table: "new_messages_view" */
  new_messages_view: Array<New_Messages_View>
  /** fetch aggregated fields from the table: "new_messages_view" */
  new_messages_view_aggregate: New_Messages_View_Aggregate
  /** fetch data from the table: "pages" */
  pages: Array<Pages>
  /** fetch aggregated fields from the table: "pages" */
  pages_aggregate: Pages_Aggregate
  /** fetch data from the table: "pages" using primary key columns */
  pages_by_pk?: Maybe<Pages>
  /** fetch data from the table: "pages_contents" */
  pages_contents: Array<Pages_Contents>
  /** fetch aggregated fields from the table: "pages_contents" */
  pages_contents_aggregate: Pages_Contents_Aggregate
  /** fetch data from the table: "pages_contents_blocks" */
  pages_contents_blocks: Array<Pages_Contents_Blocks>
  /** fetch aggregated fields from the table: "pages_contents_blocks" */
  pages_contents_blocks_aggregate: Pages_Contents_Blocks_Aggregate
  /** fetch data from the table: "pages_contents_blocks" using primary key columns */
  pages_contents_blocks_by_pk?: Maybe<Pages_Contents_Blocks>
  /** fetch data from the table: "pages_contents" using primary key columns */
  pages_contents_by_pk?: Maybe<Pages_Contents>
  /** fetch data from the table: "payment_types" */
  payment_types: Array<Payment_Types>
  /** fetch aggregated fields from the table: "payment_types" */
  payment_types_aggregate: Payment_Types_Aggregate
  /** fetch data from the table: "payment_types" using primary key columns */
  payment_types_by_pk?: Maybe<Payment_Types>
  /** fetch data from the table: "permissions" */
  permissions: Array<Permissions>
  /** fetch aggregated fields from the table: "permissions" */
  permissions_aggregate: Permissions_Aggregate
  /** fetch data from the table: "permissions" using primary key columns */
  permissions_by_pk?: Maybe<Permissions>
  /** fetch data from the table: "purchased" */
  purchased: Array<Purchased>
  /** fetch aggregated fields from the table: "purchased" */
  purchased_aggregate: Purchased_Aggregate
  /** fetch data from the table: "purchased" using primary key columns */
  purchased_by_pk?: Maybe<Purchased>
  /** fetch data from the table: "reviews" */
  reviews: Array<Reviews>
  /** fetch aggregated fields from the table: "reviews" */
  reviews_aggregate: Reviews_Aggregate
  /** fetch data from the table: "reviews" using primary key columns */
  reviews_by_pk?: Maybe<Reviews>
  /** execute function "search_makers" which returns "makers" */
  search_makers: Array<Makers>
  /** execute function "search_makers" and query aggregates on result of table type "makers" */
  search_makers_aggregate: Makers_Aggregate
  /** execute function "search_photographers" which returns "users" */
  search_photographers: Array<Users>
  /** execute function "search_photographers" and query aggregates on result of table type "users" */
  search_photographers_aggregate: Users_Aggregate
  /** execute function "search_photographers_by_location" which returns "users" */
  search_photographers_by_location: Array<Users>
  /** execute function "search_photographers_by_location" and query aggregates on result of table type "users" */
  search_photographers_by_location_aggregate: Users_Aggregate
  /** execute function "search_photographers_by_location_box" which returns "users" */
  search_photographers_by_location_box: Array<Users>
  /** execute function "search_photographers_by_location_box" and query aggregates on result of table type "users" */
  search_photographers_by_location_box_aggregate: Users_Aggregate
  /** execute function "search_styles" which returns "styles" */
  search_styles: Array<Styles>
  /** execute function "search_styles" and query aggregates on result of table type "styles" */
  search_styles_aggregate: Styles_Aggregate
  /** fetch data from the table: "seo" */
  seo: Array<Seo>
  /** fetch aggregated fields from the table: "seo" */
  seo_aggregate: Seo_Aggregate
  /** fetch data from the table: "seo" using primary key columns */
  seo_by_pk?: Maybe<Seo>
  /** fetch data from the table: "spatial_ref_sys" */
  spatial_ref_sys: Array<Spatial_Ref_Sys>
  /** fetch aggregated fields from the table: "spatial_ref_sys" */
  spatial_ref_sys_aggregate: Spatial_Ref_Sys_Aggregate
  /** fetch data from the table: "spatial_ref_sys" using primary key columns */
  spatial_ref_sys_by_pk?: Maybe<Spatial_Ref_Sys>
  /** fetch data from the table: "storages" */
  storages: Array<Storages>
  /** fetch aggregated fields from the table: "storages" */
  storages_aggregate: Storages_Aggregate
  /** fetch data from the table: "storages" using primary key columns */
  storages_by_pk?: Maybe<Storages>
  /** fetch data from the table: "storages_view" */
  storages_view: Array<Storages_View>
  /** fetch aggregated fields from the table: "storages_view" */
  storages_view_aggregate: Storages_View_Aggregate
  /** fetch data from the table: "store" */
  store: Array<Store>
  /** fetch aggregated fields from the table: "store" */
  store_aggregate: Store_Aggregate
  /** fetch data from the table: "store" using primary key columns */
  store_by_pk?: Maybe<Store>
  /** fetch data from the table: "store_categories" */
  store_categories: Array<Store_Categories>
  /** fetch aggregated fields from the table: "store_categories" */
  store_categories_aggregate: Store_Categories_Aggregate
  /** fetch data from the table: "store_categories" using primary key columns */
  store_categories_by_pk?: Maybe<Store_Categories>
  /** fetch data from the table: "store_types" */
  store_types: Array<Store_Types>
  /** fetch aggregated fields from the table: "store_types" */
  store_types_aggregate: Store_Types_Aggregate
  /** fetch data from the table: "store_types" using primary key columns */
  store_types_by_pk?: Maybe<Store_Types>
  /** fetch data from the table: "styles" */
  styles: Array<Styles>
  /** fetch aggregated fields from the table: "styles" */
  styles_aggregate: Styles_Aggregate
  /** fetch data from the table: "styles" using primary key columns */
  styles_by_pk?: Maybe<Styles>
  /** fetch data from the table: "system_messages" */
  system_messages: Array<System_Messages>
  /** fetch aggregated fields from the table: "system_messages" */
  system_messages_aggregate: System_Messages_Aggregate
  /** fetch data from the table: "system_messages" using primary key columns */
  system_messages_by_pk?: Maybe<System_Messages>
  /** fetch data from the table: "tags" */
  tags: Array<Tags>
  /** fetch aggregated fields from the table: "tags" */
  tags_aggregate: Tags_Aggregate
  /** fetch data from the table: "tags" using primary key columns */
  tags_by_pk?: Maybe<Tags>
  /** fetch data from the table: "tariffs" */
  tariffs: Array<Tariffs>
  /** fetch aggregated fields from the table: "tariffs" */
  tariffs_aggregate: Tariffs_Aggregate
  /** fetch data from the table: "tariffs" using primary key columns */
  tariffs_by_pk?: Maybe<Tariffs>
  /** fetch data from the table: "transaction_status" */
  transaction_status: Array<Transaction_Status>
  /** fetch aggregated fields from the table: "transaction_status" */
  transaction_status_aggregate: Transaction_Status_Aggregate
  /** fetch data from the table: "transaction_status" using primary key columns */
  transaction_status_by_pk?: Maybe<Transaction_Status>
  /** fetch data from the table: "transaction_type" */
  transaction_type: Array<Transaction_Type>
  /** fetch aggregated fields from the table: "transaction_type" */
  transaction_type_aggregate: Transaction_Type_Aggregate
  /** fetch data from the table: "transaction_type" using primary key columns */
  transaction_type_by_pk?: Maybe<Transaction_Type>
  /** fetch data from the table: "transactions" */
  transactions: Array<Transactions>
  /** fetch aggregated fields from the table: "transactions" */
  transactions_aggregate: Transactions_Aggregate
  /** fetch data from the table: "transactions" using primary key columns */
  transactions_by_pk?: Maybe<Transactions>
  /** fetch data from the table: "users" */
  users: Array<Users>
  /** fetch aggregated fields from the table: "users" */
  users_aggregate: Users_Aggregate
  /** fetch data from the table: "users" using primary key columns */
  users_by_pk?: Maybe<Users>
  /** fetch data from the table: "users_maker_types" */
  users_maker_types: Array<Users_Maker_Types>
  /** fetch aggregated fields from the table: "users_maker_types" */
  users_maker_types_aggregate: Users_Maker_Types_Aggregate
  /** fetch data from the table: "users_maker_types" using primary key columns */
  users_maker_types_by_pk?: Maybe<Users_Maker_Types>
  /** fetch data from the table: "users_makers" */
  users_makers: Array<Users_Makers>
  /** fetch aggregated fields from the table: "users_makers" */
  users_makers_aggregate: Users_Makers_Aggregate
  /** fetch data from the table: "users_makers" using primary key columns */
  users_makers_by_pk?: Maybe<Users_Makers>
  /** fetch data from the table: "users_payment_types" */
  users_payment_types: Array<Users_Payment_Types>
  /** fetch aggregated fields from the table: "users_payment_types" */
  users_payment_types_aggregate: Users_Payment_Types_Aggregate
  /** fetch data from the table: "users_payment_types" using primary key columns */
  users_payment_types_by_pk?: Maybe<Users_Payment_Types>
  /** fetch data from the table: "users_permissions" */
  users_permissions: Array<Users_Permissions>
  /** fetch aggregated fields from the table: "users_permissions" */
  users_permissions_aggregate: Users_Permissions_Aggregate
  /** fetch data from the table: "users_permissions" using primary key columns */
  users_permissions_by_pk?: Maybe<Users_Permissions>
  /** fetch data from the table: "users_search_media_files" */
  users_search_media_files: Array<Users_Search_Media_Files>
  /** fetch aggregated fields from the table: "users_search_media_files" */
  users_search_media_files_aggregate: Users_Search_Media_Files_Aggregate
  /** fetch data from the table: "users_search_media_files" using primary key columns */
  users_search_media_files_by_pk?: Maybe<Users_Search_Media_Files>
  /** fetch data from the table: "users_stats" */
  users_stats: Array<Users_Stats>
  /** fetch aggregated fields from the table: "users_stats" */
  users_stats_aggregate: Users_Stats_Aggregate
  /** fetch data from the table: "users_stats" using primary key columns */
  users_stats_by_pk?: Maybe<Users_Stats>
  /** fetch data from the table: "users_styles" */
  users_styles: Array<Users_Styles>
  /** fetch aggregated fields from the table: "users_styles" */
  users_styles_aggregate: Users_Styles_Aggregate
  /** fetch data from the table: "users_styles" using primary key columns */
  users_styles_by_pk?: Maybe<Users_Styles>
  /** fetch data from the table: "users_tariffs" */
  users_tariffs: Array<Users_Tariffs>
  /** fetch aggregated fields from the table: "users_tariffs" */
  users_tariffs_aggregate: Users_Tariffs_Aggregate
  /** fetch data from the table: "users_tariffs" using primary key columns */
  users_tariffs_by_pk?: Maybe<Users_Tariffs>
  /** fetch data from the table: "wallets" */
  wallets: Array<Wallets>
  /** fetch aggregated fields from the table: "wallets" */
  wallets_aggregate: Wallets_Aggregate
  /** fetch data from the table: "wallets" using primary key columns */
  wallets_by_pk?: Maybe<Wallets>
}

/** query root */
export type Query_RootAlbumsArgs = {
  distinct_on?: Maybe<Array<Albums_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Order_By>>
  where?: Maybe<Albums_Bool_Exp>
}

/** query root */
export type Query_RootAlbums_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Order_By>>
  where?: Maybe<Albums_Bool_Exp>
}

/** query root */
export type Query_RootAlbums_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootAlbums_Payment_TypesArgs = {
  distinct_on?: Maybe<Array<Albums_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Payment_Types_Order_By>>
  where?: Maybe<Albums_Payment_Types_Bool_Exp>
}

/** query root */
export type Query_RootAlbums_Payment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Payment_Types_Order_By>>
  where?: Maybe<Albums_Payment_Types_Bool_Exp>
}

/** query root */
export type Query_RootAlbums_Payment_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootAlbums_Statistics_ViewArgs = {
  distinct_on?: Maybe<Array<Albums_Statistics_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Statistics_View_Order_By>>
  where?: Maybe<Albums_Statistics_View_Bool_Exp>
}

/** query root */
export type Query_RootAlbums_Statistics_View_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Statistics_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Statistics_View_Order_By>>
  where?: Maybe<Albums_Statistics_View_Bool_Exp>
}

/** query root */
export type Query_RootAlbums_TagsArgs = {
  distinct_on?: Maybe<Array<Albums_Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Tags_Order_By>>
  where?: Maybe<Albums_Tags_Bool_Exp>
}

/** query root */
export type Query_RootAlbums_Tags_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Tags_Order_By>>
  where?: Maybe<Albums_Tags_Bool_Exp>
}

/** query root */
export type Query_RootAlbums_Tags_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootAlbums_VisitsArgs = {
  distinct_on?: Maybe<Array<Albums_Visits_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Visits_Order_By>>
  where?: Maybe<Albums_Visits_Bool_Exp>
}

/** query root */
export type Query_RootAlbums_Visits_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Visits_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Visits_Order_By>>
  where?: Maybe<Albums_Visits_Bool_Exp>
}

/** query root */
export type Query_RootAlbums_Visits_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootAuth_Account_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>
  where?: Maybe<Auth_Account_Providers_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Account_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>
  where?: Maybe<Auth_Account_Providers_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Account_Providers_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootAuth_Account_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>
  where?: Maybe<Auth_Account_Roles_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Account_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>
  where?: Maybe<Auth_Account_Roles_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Account_Roles_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootAuth_AccountsArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>
  where?: Maybe<Auth_Accounts_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Accounts_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>
  where?: Maybe<Auth_Accounts_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Accounts_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootAuth_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Providers_Order_By>>
  where?: Maybe<Auth_Providers_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Providers_Order_By>>
  where?: Maybe<Auth_Providers_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Providers_By_PkArgs = {
  provider: Scalars['String']
}

/** query root */
export type Query_RootAuth_Refresh_TokensArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Refresh_Tokens_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Refresh_Tokens_By_PkArgs = {
  refresh_token: Scalars['uuid']
}

/** query root */
export type Query_RootAuth_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Roles_Order_By>>
  where?: Maybe<Auth_Roles_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Roles_Order_By>>
  where?: Maybe<Auth_Roles_Bool_Exp>
}

/** query root */
export type Query_RootAuth_Roles_By_PkArgs = {
  role: Scalars['String']
}

/** query root */
export type Query_RootConfigArgs = {
  distinct_on?: Maybe<Array<Config_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Config_Order_By>>
  where?: Maybe<Config_Bool_Exp>
}

/** query root */
export type Query_RootConfig_AggregateArgs = {
  distinct_on?: Maybe<Array<Config_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Config_Order_By>>
  where?: Maybe<Config_Bool_Exp>
}

/** query root */
export type Query_RootConfig_By_PkArgs = {
  key: Config_Keys_Enum
}

/** query root */
export type Query_RootConfig_KeysArgs = {
  distinct_on?: Maybe<Array<Config_Keys_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Config_Keys_Order_By>>
  where?: Maybe<Config_Keys_Bool_Exp>
}

/** query root */
export type Query_RootConfig_Keys_AggregateArgs = {
  distinct_on?: Maybe<Array<Config_Keys_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Config_Keys_Order_By>>
  where?: Maybe<Config_Keys_Bool_Exp>
}

/** query root */
export type Query_RootConfig_Keys_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootContactsArgs = {
  distinct_on?: Maybe<Array<Contacts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Contacts_Order_By>>
  where?: Maybe<Contacts_Bool_Exp>
}

/** query root */
export type Query_RootContacts_AggregateArgs = {
  distinct_on?: Maybe<Array<Contacts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Contacts_Order_By>>
  where?: Maybe<Contacts_Bool_Exp>
}

/** query root */
export type Query_RootContacts_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootCover_ImagesArgs = {
  distinct_on?: Maybe<Array<Cover_Images_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Cover_Images_Order_By>>
  where?: Maybe<Cover_Images_Bool_Exp>
}

/** query root */
export type Query_RootCover_Images_AggregateArgs = {
  distinct_on?: Maybe<Array<Cover_Images_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Cover_Images_Order_By>>
  where?: Maybe<Cover_Images_Bool_Exp>
}

/** query root */
export type Query_RootCover_Images_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootCredit_CardsArgs = {
  distinct_on?: Maybe<Array<Credit_Cards_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Credit_Cards_Order_By>>
  where?: Maybe<Credit_Cards_Bool_Exp>
}

/** query root */
export type Query_RootCredit_Cards_AggregateArgs = {
  distinct_on?: Maybe<Array<Credit_Cards_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Credit_Cards_Order_By>>
  where?: Maybe<Credit_Cards_Bool_Exp>
}

/** query root */
export type Query_RootCredit_Cards_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootDownloadsArgs = {
  distinct_on?: Maybe<Array<Downloads_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Downloads_Order_By>>
  where?: Maybe<Downloads_Bool_Exp>
}

/** query root */
export type Query_RootDownloads_AggregateArgs = {
  distinct_on?: Maybe<Array<Downloads_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Downloads_Order_By>>
  where?: Maybe<Downloads_Bool_Exp>
}

/** query root */
export type Query_RootDownloads_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootEventsArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** query root */
export type Query_RootEvents_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** query root */
export type Query_RootEvents_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootEvents_Payment_TypesArgs = {
  distinct_on?: Maybe<Array<Events_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Payment_Types_Order_By>>
  where?: Maybe<Events_Payment_Types_Bool_Exp>
}

/** query root */
export type Query_RootEvents_Payment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Payment_Types_Order_By>>
  where?: Maybe<Events_Payment_Types_Bool_Exp>
}

/** query root */
export type Query_RootEvents_Payment_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootEvents_StatusesArgs = {
  distinct_on?: Maybe<Array<Events_Statuses_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Statuses_Order_By>>
  where?: Maybe<Events_Statuses_Bool_Exp>
}

/** query root */
export type Query_RootEvents_Statuses_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Statuses_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Statuses_Order_By>>
  where?: Maybe<Events_Statuses_Bool_Exp>
}

/** query root */
export type Query_RootEvents_Statuses_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootFavoritesArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** query root */
export type Query_RootFavorites_AggregateArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** query root */
export type Query_RootFavorites_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootFoldersArgs = {
  distinct_on?: Maybe<Array<Folders_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Folders_Order_By>>
  where?: Maybe<Folders_Bool_Exp>
}

/** query root */
export type Query_RootFolders_AggregateArgs = {
  distinct_on?: Maybe<Array<Folders_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Folders_Order_By>>
  where?: Maybe<Folders_Bool_Exp>
}

/** query root */
export type Query_RootFolders_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootGeo_AutocompleteArgs = {
  lang?: Maybe<Scalars['String']>
  text: Scalars['String']
  type?: Maybe<Scalars['String']>
}

/** query root */
export type Query_RootGeo_ReverseArgs = {
  lang?: Maybe<Scalars['String']>
  lat: Scalars['Float']
  lng: Scalars['Float']
  type?: Maybe<Scalars['String']>
}

/** query root */
export type Query_RootGeo_SearchArgs = {
  lang?: Maybe<Scalars['String']>
  text: Scalars['String']
  type?: Maybe<Scalars['String']>
}

/** query root */
export type Query_RootGeography_ColumnsArgs = {
  distinct_on?: Maybe<Array<Geography_Columns_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Geography_Columns_Order_By>>
  where?: Maybe<Geography_Columns_Bool_Exp>
}

/** query root */
export type Query_RootGeography_Columns_AggregateArgs = {
  distinct_on?: Maybe<Array<Geography_Columns_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Geography_Columns_Order_By>>
  where?: Maybe<Geography_Columns_Bool_Exp>
}

/** query root */
export type Query_RootGeometry_ColumnsArgs = {
  distinct_on?: Maybe<Array<Geometry_Columns_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Geometry_Columns_Order_By>>
  where?: Maybe<Geometry_Columns_Bool_Exp>
}

/** query root */
export type Query_RootGeometry_Columns_AggregateArgs = {
  distinct_on?: Maybe<Array<Geometry_Columns_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Geometry_Columns_Order_By>>
  where?: Maybe<Geometry_Columns_Bool_Exp>
}

/** query root */
export type Query_RootLocalesArgs = {
  distinct_on?: Maybe<Array<Locales_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locales_Order_By>>
  where?: Maybe<Locales_Bool_Exp>
}

/** query root */
export type Query_RootLocales_AggregateArgs = {
  distinct_on?: Maybe<Array<Locales_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locales_Order_By>>
  where?: Maybe<Locales_Bool_Exp>
}

/** query root */
export type Query_RootLocales_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootLocalizationsArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** query root */
export type Query_RootLocalizations_AggregateArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** query root */
export type Query_RootLocalizations_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootLocationsArgs = {
  distinct_on?: Maybe<Array<Locations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locations_Order_By>>
  where?: Maybe<Locations_Bool_Exp>
}

/** query root */
export type Query_RootLocations_AggregateArgs = {
  distinct_on?: Maybe<Array<Locations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locations_Order_By>>
  where?: Maybe<Locations_Bool_Exp>
}

/** query root */
export type Query_RootLocations_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootMaker_TypesArgs = {
  distinct_on?: Maybe<Array<Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Maker_Types_Order_By>>
  where?: Maybe<Maker_Types_Bool_Exp>
}

/** query root */
export type Query_RootMaker_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Maker_Types_Order_By>>
  where?: Maybe<Maker_Types_Bool_Exp>
}

/** query root */
export type Query_RootMaker_Types_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootMakersArgs = {
  distinct_on?: Maybe<Array<Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Makers_Order_By>>
  where?: Maybe<Makers_Bool_Exp>
}

/** query root */
export type Query_RootMakers_AggregateArgs = {
  distinct_on?: Maybe<Array<Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Makers_Order_By>>
  where?: Maybe<Makers_Bool_Exp>
}

/** query root */
export type Query_RootMakers_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootMedia_File_TypesArgs = {
  distinct_on?: Maybe<Array<Media_File_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_File_Types_Order_By>>
  where?: Maybe<Media_File_Types_Bool_Exp>
}

/** query root */
export type Query_RootMedia_File_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Media_File_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_File_Types_Order_By>>
  where?: Maybe<Media_File_Types_Bool_Exp>
}

/** query root */
export type Query_RootMedia_File_Types_By_PkArgs = {
  id: Scalars['String']
}

/** query root */
export type Query_RootMedia_FilesArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** query root */
export type Query_RootMedia_Files_AggregateArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** query root */
export type Query_RootMedia_Files_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootMenu_ItemsArgs = {
  distinct_on?: Maybe<Array<Menu_Items_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Items_Order_By>>
  where?: Maybe<Menu_Items_Bool_Exp>
}

/** query root */
export type Query_RootMenu_Items_AggregateArgs = {
  distinct_on?: Maybe<Array<Menu_Items_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Items_Order_By>>
  where?: Maybe<Menu_Items_Bool_Exp>
}

/** query root */
export type Query_RootMenu_Items_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootMenu_PagesArgs = {
  distinct_on?: Maybe<Array<Menu_Pages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Pages_Order_By>>
  where?: Maybe<Menu_Pages_Bool_Exp>
}

/** query root */
export type Query_RootMenu_Pages_AggregateArgs = {
  distinct_on?: Maybe<Array<Menu_Pages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Pages_Order_By>>
  where?: Maybe<Menu_Pages_Bool_Exp>
}

/** query root */
export type Query_RootMenu_Pages_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootMenu_TypeArgs = {
  distinct_on?: Maybe<Array<Menu_Type_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Type_Order_By>>
  where?: Maybe<Menu_Type_Bool_Exp>
}

/** query root */
export type Query_RootMenu_Type_AggregateArgs = {
  distinct_on?: Maybe<Array<Menu_Type_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Type_Order_By>>
  where?: Maybe<Menu_Type_Bool_Exp>
}

/** query root */
export type Query_RootMenu_Type_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootMessagesArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** query root */
export type Query_RootMessages_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** query root */
export type Query_RootMessages_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootMessages_Events_UpdatedArgs = {
  distinct_on?: Maybe<Array<Messages_Events_Updated_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Events_Updated_Order_By>>
  where?: Maybe<Messages_Events_Updated_Bool_Exp>
}

/** query root */
export type Query_RootMessages_Events_Updated_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Events_Updated_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Events_Updated_Order_By>>
  where?: Maybe<Messages_Events_Updated_Bool_Exp>
}

/** query root */
export type Query_RootMessages_Events_Updated_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootMessages_StatusesArgs = {
  distinct_on?: Maybe<Array<Messages_Statuses_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Statuses_Order_By>>
  where?: Maybe<Messages_Statuses_Bool_Exp>
}

/** query root */
export type Query_RootMessages_Statuses_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Statuses_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Statuses_Order_By>>
  where?: Maybe<Messages_Statuses_Bool_Exp>
}

/** query root */
export type Query_RootMessages_Statuses_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootMessages_TypesArgs = {
  distinct_on?: Maybe<Array<Messages_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Types_Order_By>>
  where?: Maybe<Messages_Types_Bool_Exp>
}

/** query root */
export type Query_RootMessages_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Types_Order_By>>
  where?: Maybe<Messages_Types_Bool_Exp>
}

/** query root */
export type Query_RootMessages_Types_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootNetwork_TransactionsArgs = {
  distinct_on?: Maybe<Array<Network_Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Network_Transactions_Order_By>>
  where?: Maybe<Network_Transactions_Bool_Exp>
}

/** query root */
export type Query_RootNetwork_Transactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Network_Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Network_Transactions_Order_By>>
  where?: Maybe<Network_Transactions_Bool_Exp>
}

/** query root */
export type Query_RootNetwork_Transactions_By_PkArgs = {
  network: Scalars['String']
  tx_hash: Scalars['String']
}

/** query root */
export type Query_RootNew_Messages_ViewArgs = {
  distinct_on?: Maybe<Array<New_Messages_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<New_Messages_View_Order_By>>
  where?: Maybe<New_Messages_View_Bool_Exp>
}

/** query root */
export type Query_RootNew_Messages_View_AggregateArgs = {
  distinct_on?: Maybe<Array<New_Messages_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<New_Messages_View_Order_By>>
  where?: Maybe<New_Messages_View_Bool_Exp>
}

/** query root */
export type Query_RootPagesArgs = {
  distinct_on?: Maybe<Array<Pages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Order_By>>
  where?: Maybe<Pages_Bool_Exp>
}

/** query root */
export type Query_RootPages_AggregateArgs = {
  distinct_on?: Maybe<Array<Pages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Order_By>>
  where?: Maybe<Pages_Bool_Exp>
}

/** query root */
export type Query_RootPages_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootPages_ContentsArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Order_By>>
  where?: Maybe<Pages_Contents_Bool_Exp>
}

/** query root */
export type Query_RootPages_Contents_AggregateArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Order_By>>
  where?: Maybe<Pages_Contents_Bool_Exp>
}

/** query root */
export type Query_RootPages_Contents_BlocksArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Blocks_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Blocks_Order_By>>
  where?: Maybe<Pages_Contents_Blocks_Bool_Exp>
}

/** query root */
export type Query_RootPages_Contents_Blocks_AggregateArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Blocks_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Blocks_Order_By>>
  where?: Maybe<Pages_Contents_Blocks_Bool_Exp>
}

/** query root */
export type Query_RootPages_Contents_Blocks_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootPages_Contents_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootPayment_TypesArgs = {
  distinct_on?: Maybe<Array<Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Payment_Types_Order_By>>
  where?: Maybe<Payment_Types_Bool_Exp>
}

/** query root */
export type Query_RootPayment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Payment_Types_Order_By>>
  where?: Maybe<Payment_Types_Bool_Exp>
}

/** query root */
export type Query_RootPayment_Types_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootPermissionsArgs = {
  distinct_on?: Maybe<Array<Permissions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Permissions_Order_By>>
  where?: Maybe<Permissions_Bool_Exp>
}

/** query root */
export type Query_RootPermissions_AggregateArgs = {
  distinct_on?: Maybe<Array<Permissions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Permissions_Order_By>>
  where?: Maybe<Permissions_Bool_Exp>
}

/** query root */
export type Query_RootPermissions_By_PkArgs = {
  id: Scalars['String']
}

/** query root */
export type Query_RootPurchasedArgs = {
  distinct_on?: Maybe<Array<Purchased_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Purchased_Order_By>>
  where?: Maybe<Purchased_Bool_Exp>
}

/** query root */
export type Query_RootPurchased_AggregateArgs = {
  distinct_on?: Maybe<Array<Purchased_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Purchased_Order_By>>
  where?: Maybe<Purchased_Bool_Exp>
}

/** query root */
export type Query_RootPurchased_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootReviewsArgs = {
  distinct_on?: Maybe<Array<Reviews_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Reviews_Order_By>>
  where?: Maybe<Reviews_Bool_Exp>
}

/** query root */
export type Query_RootReviews_AggregateArgs = {
  distinct_on?: Maybe<Array<Reviews_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Reviews_Order_By>>
  where?: Maybe<Reviews_Bool_Exp>
}

/** query root */
export type Query_RootReviews_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootSearch_MakersArgs = {
  args: Search_Makers_Args
  distinct_on?: Maybe<Array<Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Makers_Order_By>>
  where?: Maybe<Makers_Bool_Exp>
}

/** query root */
export type Query_RootSearch_Makers_AggregateArgs = {
  args: Search_Makers_Args
  distinct_on?: Maybe<Array<Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Makers_Order_By>>
  where?: Maybe<Makers_Bool_Exp>
}

/** query root */
export type Query_RootSearch_PhotographersArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** query root */
export type Query_RootSearch_Photographers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** query root */
export type Query_RootSearch_Photographers_By_LocationArgs = {
  args: Search_Photographers_By_Location_Args
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** query root */
export type Query_RootSearch_Photographers_By_Location_AggregateArgs = {
  args: Search_Photographers_By_Location_Args
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** query root */
export type Query_RootSearch_Photographers_By_Location_BoxArgs = {
  args: Search_Photographers_By_Location_Box_Args
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** query root */
export type Query_RootSearch_Photographers_By_Location_Box_AggregateArgs = {
  args: Search_Photographers_By_Location_Box_Args
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** query root */
export type Query_RootSearch_StylesArgs = {
  args: Search_Styles_Args
  distinct_on?: Maybe<Array<Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Styles_Order_By>>
  where?: Maybe<Styles_Bool_Exp>
}

/** query root */
export type Query_RootSearch_Styles_AggregateArgs = {
  args: Search_Styles_Args
  distinct_on?: Maybe<Array<Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Styles_Order_By>>
  where?: Maybe<Styles_Bool_Exp>
}

/** query root */
export type Query_RootSeoArgs = {
  distinct_on?: Maybe<Array<Seo_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Seo_Order_By>>
  where?: Maybe<Seo_Bool_Exp>
}

/** query root */
export type Query_RootSeo_AggregateArgs = {
  distinct_on?: Maybe<Array<Seo_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Seo_Order_By>>
  where?: Maybe<Seo_Bool_Exp>
}

/** query root */
export type Query_RootSeo_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootSpatial_Ref_SysArgs = {
  distinct_on?: Maybe<Array<Spatial_Ref_Sys_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Spatial_Ref_Sys_Order_By>>
  where?: Maybe<Spatial_Ref_Sys_Bool_Exp>
}

/** query root */
export type Query_RootSpatial_Ref_Sys_AggregateArgs = {
  distinct_on?: Maybe<Array<Spatial_Ref_Sys_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Spatial_Ref_Sys_Order_By>>
  where?: Maybe<Spatial_Ref_Sys_Bool_Exp>
}

/** query root */
export type Query_RootSpatial_Ref_Sys_By_PkArgs = {
  srid: Scalars['Int']
}

/** query root */
export type Query_RootStoragesArgs = {
  distinct_on?: Maybe<Array<Storages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Storages_Order_By>>
  where?: Maybe<Storages_Bool_Exp>
}

/** query root */
export type Query_RootStorages_AggregateArgs = {
  distinct_on?: Maybe<Array<Storages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Storages_Order_By>>
  where?: Maybe<Storages_Bool_Exp>
}

/** query root */
export type Query_RootStorages_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootStorages_ViewArgs = {
  distinct_on?: Maybe<Array<Storages_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Storages_View_Order_By>>
  where?: Maybe<Storages_View_Bool_Exp>
}

/** query root */
export type Query_RootStorages_View_AggregateArgs = {
  distinct_on?: Maybe<Array<Storages_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Storages_View_Order_By>>
  where?: Maybe<Storages_View_Bool_Exp>
}

/** query root */
export type Query_RootStoreArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** query root */
export type Query_RootStore_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** query root */
export type Query_RootStore_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootStore_CategoriesArgs = {
  distinct_on?: Maybe<Array<Store_Categories_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Categories_Order_By>>
  where?: Maybe<Store_Categories_Bool_Exp>
}

/** query root */
export type Query_RootStore_Categories_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Categories_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Categories_Order_By>>
  where?: Maybe<Store_Categories_Bool_Exp>
}

/** query root */
export type Query_RootStore_Categories_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootStore_TypesArgs = {
  distinct_on?: Maybe<Array<Store_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Types_Order_By>>
  where?: Maybe<Store_Types_Bool_Exp>
}

/** query root */
export type Query_RootStore_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Types_Order_By>>
  where?: Maybe<Store_Types_Bool_Exp>
}

/** query root */
export type Query_RootStore_Types_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootStylesArgs = {
  distinct_on?: Maybe<Array<Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Styles_Order_By>>
  where?: Maybe<Styles_Bool_Exp>
}

/** query root */
export type Query_RootStyles_AggregateArgs = {
  distinct_on?: Maybe<Array<Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Styles_Order_By>>
  where?: Maybe<Styles_Bool_Exp>
}

/** query root */
export type Query_RootStyles_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootSystem_MessagesArgs = {
  distinct_on?: Maybe<Array<System_Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<System_Messages_Order_By>>
  where?: Maybe<System_Messages_Bool_Exp>
}

/** query root */
export type Query_RootSystem_Messages_AggregateArgs = {
  distinct_on?: Maybe<Array<System_Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<System_Messages_Order_By>>
  where?: Maybe<System_Messages_Bool_Exp>
}

/** query root */
export type Query_RootSystem_Messages_By_PkArgs = {
  id: Scalars['String']
}

/** query root */
export type Query_RootTagsArgs = {
  distinct_on?: Maybe<Array<Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Tags_Order_By>>
  where?: Maybe<Tags_Bool_Exp>
}

/** query root */
export type Query_RootTags_AggregateArgs = {
  distinct_on?: Maybe<Array<Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Tags_Order_By>>
  where?: Maybe<Tags_Bool_Exp>
}

/** query root */
export type Query_RootTags_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootTariffsArgs = {
  distinct_on?: Maybe<Array<Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Tariffs_Order_By>>
  where?: Maybe<Tariffs_Bool_Exp>
}

/** query root */
export type Query_RootTariffs_AggregateArgs = {
  distinct_on?: Maybe<Array<Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Tariffs_Order_By>>
  where?: Maybe<Tariffs_Bool_Exp>
}

/** query root */
export type Query_RootTariffs_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootTransaction_StatusArgs = {
  distinct_on?: Maybe<Array<Transaction_Status_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transaction_Status_Order_By>>
  where?: Maybe<Transaction_Status_Bool_Exp>
}

/** query root */
export type Query_RootTransaction_Status_AggregateArgs = {
  distinct_on?: Maybe<Array<Transaction_Status_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transaction_Status_Order_By>>
  where?: Maybe<Transaction_Status_Bool_Exp>
}

/** query root */
export type Query_RootTransaction_Status_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootTransaction_TypeArgs = {
  distinct_on?: Maybe<Array<Transaction_Type_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transaction_Type_Order_By>>
  where?: Maybe<Transaction_Type_Bool_Exp>
}

/** query root */
export type Query_RootTransaction_Type_AggregateArgs = {
  distinct_on?: Maybe<Array<Transaction_Type_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transaction_Type_Order_By>>
  where?: Maybe<Transaction_Type_Bool_Exp>
}

/** query root */
export type Query_RootTransaction_Type_By_PkArgs = {
  value: Scalars['String']
}

/** query root */
export type Query_RootTransactionsArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** query root */
export type Query_RootTransactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** query root */
export type Query_RootTransactions_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootUsersArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** query root */
export type Query_RootUsers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** query root */
export type Query_RootUsers_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootUsers_Maker_TypesArgs = {
  distinct_on?: Maybe<Array<Users_Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Maker_Types_Order_By>>
  where?: Maybe<Users_Maker_Types_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Maker_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Maker_Types_Order_By>>
  where?: Maybe<Users_Maker_Types_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Maker_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootUsers_MakersArgs = {
  distinct_on?: Maybe<Array<Users_Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Makers_Order_By>>
  where?: Maybe<Users_Makers_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Makers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Makers_Order_By>>
  where?: Maybe<Users_Makers_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Makers_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootUsers_Payment_TypesArgs = {
  distinct_on?: Maybe<Array<Users_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Payment_Types_Order_By>>
  where?: Maybe<Users_Payment_Types_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Payment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Payment_Types_Order_By>>
  where?: Maybe<Users_Payment_Types_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Payment_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootUsers_PermissionsArgs = {
  distinct_on?: Maybe<Array<Users_Permissions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Permissions_Order_By>>
  where?: Maybe<Users_Permissions_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Permissions_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Permissions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Permissions_Order_By>>
  where?: Maybe<Users_Permissions_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Permissions_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootUsers_Search_Media_FilesArgs = {
  distinct_on?: Maybe<Array<Users_Search_Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Search_Media_Files_Order_By>>
  where?: Maybe<Users_Search_Media_Files_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Search_Media_Files_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Search_Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Search_Media_Files_Order_By>>
  where?: Maybe<Users_Search_Media_Files_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Search_Media_Files_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootUsers_StatsArgs = {
  distinct_on?: Maybe<Array<Users_Stats_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Stats_Order_By>>
  where?: Maybe<Users_Stats_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Stats_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Stats_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Stats_Order_By>>
  where?: Maybe<Users_Stats_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Stats_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootUsers_StylesArgs = {
  distinct_on?: Maybe<Array<Users_Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Styles_Order_By>>
  where?: Maybe<Users_Styles_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Styles_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Styles_Order_By>>
  where?: Maybe<Users_Styles_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Styles_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootUsers_TariffsArgs = {
  distinct_on?: Maybe<Array<Users_Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Tariffs_Order_By>>
  where?: Maybe<Users_Tariffs_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Tariffs_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Tariffs_Order_By>>
  where?: Maybe<Users_Tariffs_Bool_Exp>
}

/** query root */
export type Query_RootUsers_Tariffs_By_PkArgs = {
  id: Scalars['uuid']
}

/** query root */
export type Query_RootWalletsArgs = {
  distinct_on?: Maybe<Array<Wallets_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Wallets_Order_By>>
  where?: Maybe<Wallets_Bool_Exp>
}

/** query root */
export type Query_RootWallets_AggregateArgs = {
  distinct_on?: Maybe<Array<Wallets_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Wallets_Order_By>>
  where?: Maybe<Wallets_Bool_Exp>
}

/** query root */
export type Query_RootWallets_By_PkArgs = {
  id: Scalars['uuid']
}

/** columns and relationships of "reviews" */
export type Reviews = {
  __typename?: 'reviews'
  comment?: Maybe<Scalars['String']>
  created_at: Scalars['timestamptz']
  /** An object relationship */
  event: Events
  event_id: Scalars['uuid']
  /** An object relationship */
  from_user: Users
  from_user_id: Scalars['uuid']
  id: Scalars['uuid']
  rating: Scalars['Int']
  /** A computed field, executes function "reviews_required_reply" */
  required_reply?: Maybe<Scalars['Boolean']>
  /** An object relationship */
  to_user: Users
  to_user_id: Scalars['uuid']
  updated_at: Scalars['timestamptz']
}

/** aggregated selection of "reviews" */
export type Reviews_Aggregate = {
  __typename?: 'reviews_aggregate'
  aggregate?: Maybe<Reviews_Aggregate_Fields>
  nodes: Array<Reviews>
}

/** aggregate fields of "reviews" */
export type Reviews_Aggregate_Fields = {
  __typename?: 'reviews_aggregate_fields'
  avg?: Maybe<Reviews_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Reviews_Max_Fields>
  min?: Maybe<Reviews_Min_Fields>
  stddev?: Maybe<Reviews_Stddev_Fields>
  stddev_pop?: Maybe<Reviews_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Reviews_Stddev_Samp_Fields>
  sum?: Maybe<Reviews_Sum_Fields>
  var_pop?: Maybe<Reviews_Var_Pop_Fields>
  var_samp?: Maybe<Reviews_Var_Samp_Fields>
  variance?: Maybe<Reviews_Variance_Fields>
}

/** aggregate fields of "reviews" */
export type Reviews_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Reviews_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "reviews" */
export type Reviews_Aggregate_Order_By = {
  avg?: Maybe<Reviews_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Reviews_Max_Order_By>
  min?: Maybe<Reviews_Min_Order_By>
  stddev?: Maybe<Reviews_Stddev_Order_By>
  stddev_pop?: Maybe<Reviews_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Reviews_Stddev_Samp_Order_By>
  sum?: Maybe<Reviews_Sum_Order_By>
  var_pop?: Maybe<Reviews_Var_Pop_Order_By>
  var_samp?: Maybe<Reviews_Var_Samp_Order_By>
  variance?: Maybe<Reviews_Variance_Order_By>
}

/** input type for inserting array relation for remote table "reviews" */
export type Reviews_Arr_Rel_Insert_Input = {
  data: Array<Reviews_Insert_Input>
  on_conflict?: Maybe<Reviews_On_Conflict>
}

/** aggregate avg on columns */
export type Reviews_Avg_Fields = {
  __typename?: 'reviews_avg_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "reviews" */
export type Reviews_Avg_Order_By = {
  rating?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "reviews". All fields are combined with a logical 'AND'. */
export type Reviews_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Reviews_Bool_Exp>>>
  _not?: Maybe<Reviews_Bool_Exp>
  _or?: Maybe<Array<Maybe<Reviews_Bool_Exp>>>
  comment?: Maybe<String_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  event?: Maybe<Events_Bool_Exp>
  event_id?: Maybe<Uuid_Comparison_Exp>
  from_user?: Maybe<Users_Bool_Exp>
  from_user_id?: Maybe<Uuid_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  rating?: Maybe<Int_Comparison_Exp>
  to_user?: Maybe<Users_Bool_Exp>
  to_user_id?: Maybe<Uuid_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "reviews" */
export enum Reviews_Constraint {
  /** unique or primary key constraint */
  ReviewsFromUserIdEventIdKey = 'reviews_from_user_id_event_id_key',
  /** unique or primary key constraint */
  ReviewsPkey = 'reviews_pkey'
}

/** input type for incrementing integer column in table "reviews" */
export type Reviews_Inc_Input = {
  rating?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "reviews" */
export type Reviews_Insert_Input = {
  comment?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  event?: Maybe<Events_Obj_Rel_Insert_Input>
  event_id?: Maybe<Scalars['uuid']>
  from_user?: Maybe<Users_Obj_Rel_Insert_Input>
  from_user_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  rating?: Maybe<Scalars['Int']>
  to_user?: Maybe<Users_Obj_Rel_Insert_Input>
  to_user_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Reviews_Max_Fields = {
  __typename?: 'reviews_max_fields'
  comment?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  from_user_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  rating?: Maybe<Scalars['Int']>
  to_user_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "reviews" */
export type Reviews_Max_Order_By = {
  comment?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  from_user_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  to_user_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Reviews_Min_Fields = {
  __typename?: 'reviews_min_fields'
  comment?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  from_user_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  rating?: Maybe<Scalars['Int']>
  to_user_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "reviews" */
export type Reviews_Min_Order_By = {
  comment?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  from_user_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  to_user_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "reviews" */
export type Reviews_Mutation_Response = {
  __typename?: 'reviews_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Reviews>
}

/** input type for inserting object relation for remote table "reviews" */
export type Reviews_Obj_Rel_Insert_Input = {
  data: Reviews_Insert_Input
  on_conflict?: Maybe<Reviews_On_Conflict>
}

/** on conflict condition type for table "reviews" */
export type Reviews_On_Conflict = {
  constraint: Reviews_Constraint
  update_columns: Array<Reviews_Update_Column>
  where?: Maybe<Reviews_Bool_Exp>
}

/** ordering options when selecting data from "reviews" */
export type Reviews_Order_By = {
  comment?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  event?: Maybe<Events_Order_By>
  event_id?: Maybe<Order_By>
  from_user?: Maybe<Users_Order_By>
  from_user_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  to_user?: Maybe<Users_Order_By>
  to_user_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "reviews" */
export type Reviews_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "reviews" */
export enum Reviews_Select_Column {
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  EventId = 'event_id',
  /** column name */
  FromUserId = 'from_user_id',
  /** column name */
  Id = 'id',
  /** column name */
  Rating = 'rating',
  /** column name */
  ToUserId = 'to_user_id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "reviews" */
export type Reviews_Set_Input = {
  comment?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  from_user_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  rating?: Maybe<Scalars['Int']>
  to_user_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate stddev on columns */
export type Reviews_Stddev_Fields = {
  __typename?: 'reviews_stddev_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "reviews" */
export type Reviews_Stddev_Order_By = {
  rating?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Reviews_Stddev_Pop_Fields = {
  __typename?: 'reviews_stddev_pop_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "reviews" */
export type Reviews_Stddev_Pop_Order_By = {
  rating?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Reviews_Stddev_Samp_Fields = {
  __typename?: 'reviews_stddev_samp_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "reviews" */
export type Reviews_Stddev_Samp_Order_By = {
  rating?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Reviews_Sum_Fields = {
  __typename?: 'reviews_sum_fields'
  rating?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "reviews" */
export type Reviews_Sum_Order_By = {
  rating?: Maybe<Order_By>
}

/** update columns of table "reviews" */
export enum Reviews_Update_Column {
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  EventId = 'event_id',
  /** column name */
  FromUserId = 'from_user_id',
  /** column name */
  Id = 'id',
  /** column name */
  Rating = 'rating',
  /** column name */
  ToUserId = 'to_user_id',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate var_pop on columns */
export type Reviews_Var_Pop_Fields = {
  __typename?: 'reviews_var_pop_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "reviews" */
export type Reviews_Var_Pop_Order_By = {
  rating?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Reviews_Var_Samp_Fields = {
  __typename?: 'reviews_var_samp_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "reviews" */
export type Reviews_Var_Samp_Order_By = {
  rating?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Reviews_Variance_Fields = {
  __typename?: 'reviews_variance_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "reviews" */
export type Reviews_Variance_Order_By = {
  rating?: Maybe<Order_By>
}

export type Search_Makers_Args = {
  lang?: Maybe<Scalars['String']>
  search?: Maybe<Scalars['String']>
}

export type Search_Photographers_By_Location_Args = {
  distance_kms?: Maybe<Scalars['Int']>
  location?: Maybe<Scalars['geography']>
}

export type Search_Photographers_By_Location_Box_Args = {
  xmax?: Maybe<Scalars['float8']>
  xmin?: Maybe<Scalars['float8']>
  ymax?: Maybe<Scalars['float8']>
  ymin?: Maybe<Scalars['float8']>
}

export type Search_Styles_Args = {
  lang?: Maybe<Scalars['String']>
  search?: Maybe<Scalars['String']>
}

/** columns and relationships of "seo" */
export type Seo = {
  __typename?: 'seo'
  created_at: Scalars['timestamptz']
  description?: Maybe<Scalars['String']>
  id: Scalars['uuid']
  image?: Maybe<Scalars['String']>
  keywords?: Maybe<Scalars['String']>
  /** An object relationship */
  pages_content?: Maybe<Pages_Contents>
  title?: Maybe<Scalars['String']>
  updated_at: Scalars['timestamptz']
}

/** aggregated selection of "seo" */
export type Seo_Aggregate = {
  __typename?: 'seo_aggregate'
  aggregate?: Maybe<Seo_Aggregate_Fields>
  nodes: Array<Seo>
}

/** aggregate fields of "seo" */
export type Seo_Aggregate_Fields = {
  __typename?: 'seo_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Seo_Max_Fields>
  min?: Maybe<Seo_Min_Fields>
}

/** aggregate fields of "seo" */
export type Seo_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Seo_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "seo" */
export type Seo_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Seo_Max_Order_By>
  min?: Maybe<Seo_Min_Order_By>
}

/** input type for inserting array relation for remote table "seo" */
export type Seo_Arr_Rel_Insert_Input = {
  data: Array<Seo_Insert_Input>
  on_conflict?: Maybe<Seo_On_Conflict>
}

/** Boolean expression to filter rows from the table "seo". All fields are combined with a logical 'AND'. */
export type Seo_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Seo_Bool_Exp>>>
  _not?: Maybe<Seo_Bool_Exp>
  _or?: Maybe<Array<Maybe<Seo_Bool_Exp>>>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  description?: Maybe<String_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  image?: Maybe<String_Comparison_Exp>
  keywords?: Maybe<String_Comparison_Exp>
  pages_content?: Maybe<Pages_Contents_Bool_Exp>
  title?: Maybe<String_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "seo" */
export enum Seo_Constraint {
  /** unique or primary key constraint */
  SeoPkey = 'seo_pkey'
}

/** input type for inserting data into table "seo" */
export type Seo_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  description?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  image?: Maybe<Scalars['String']>
  keywords?: Maybe<Scalars['String']>
  pages_content?: Maybe<Pages_Contents_Obj_Rel_Insert_Input>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Seo_Max_Fields = {
  __typename?: 'seo_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  description?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  image?: Maybe<Scalars['String']>
  keywords?: Maybe<Scalars['String']>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "seo" */
export type Seo_Max_Order_By = {
  created_at?: Maybe<Order_By>
  description?: Maybe<Order_By>
  id?: Maybe<Order_By>
  image?: Maybe<Order_By>
  keywords?: Maybe<Order_By>
  title?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Seo_Min_Fields = {
  __typename?: 'seo_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  description?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  image?: Maybe<Scalars['String']>
  keywords?: Maybe<Scalars['String']>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "seo" */
export type Seo_Min_Order_By = {
  created_at?: Maybe<Order_By>
  description?: Maybe<Order_By>
  id?: Maybe<Order_By>
  image?: Maybe<Order_By>
  keywords?: Maybe<Order_By>
  title?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "seo" */
export type Seo_Mutation_Response = {
  __typename?: 'seo_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Seo>
}

/** input type for inserting object relation for remote table "seo" */
export type Seo_Obj_Rel_Insert_Input = {
  data: Seo_Insert_Input
  on_conflict?: Maybe<Seo_On_Conflict>
}

/** on conflict condition type for table "seo" */
export type Seo_On_Conflict = {
  constraint: Seo_Constraint
  update_columns: Array<Seo_Update_Column>
  where?: Maybe<Seo_Bool_Exp>
}

/** ordering options when selecting data from "seo" */
export type Seo_Order_By = {
  created_at?: Maybe<Order_By>
  description?: Maybe<Order_By>
  id?: Maybe<Order_By>
  image?: Maybe<Order_By>
  keywords?: Maybe<Order_By>
  pages_content?: Maybe<Pages_Contents_Order_By>
  title?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "seo" */
export type Seo_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "seo" */
export enum Seo_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  Image = 'image',
  /** column name */
  Keywords = 'keywords',
  /** column name */
  Title = 'title',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "seo" */
export type Seo_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  description?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  image?: Maybe<Scalars['String']>
  keywords?: Maybe<Scalars['String']>
  title?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** update columns of table "seo" */
export enum Seo_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Description = 'description',
  /** column name */
  Id = 'id',
  /** column name */
  Image = 'image',
  /** column name */
  Keywords = 'keywords',
  /** column name */
  Title = 'title',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** columns and relationships of "spatial_ref_sys" */
export type Spatial_Ref_Sys = {
  __typename?: 'spatial_ref_sys'
  auth_name?: Maybe<Scalars['String']>
  auth_srid?: Maybe<Scalars['Int']>
  proj4text?: Maybe<Scalars['String']>
  srid: Scalars['Int']
  srtext?: Maybe<Scalars['String']>
}

/** aggregated selection of "spatial_ref_sys" */
export type Spatial_Ref_Sys_Aggregate = {
  __typename?: 'spatial_ref_sys_aggregate'
  aggregate?: Maybe<Spatial_Ref_Sys_Aggregate_Fields>
  nodes: Array<Spatial_Ref_Sys>
}

/** aggregate fields of "spatial_ref_sys" */
export type Spatial_Ref_Sys_Aggregate_Fields = {
  __typename?: 'spatial_ref_sys_aggregate_fields'
  avg?: Maybe<Spatial_Ref_Sys_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Spatial_Ref_Sys_Max_Fields>
  min?: Maybe<Spatial_Ref_Sys_Min_Fields>
  stddev?: Maybe<Spatial_Ref_Sys_Stddev_Fields>
  stddev_pop?: Maybe<Spatial_Ref_Sys_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Spatial_Ref_Sys_Stddev_Samp_Fields>
  sum?: Maybe<Spatial_Ref_Sys_Sum_Fields>
  var_pop?: Maybe<Spatial_Ref_Sys_Var_Pop_Fields>
  var_samp?: Maybe<Spatial_Ref_Sys_Var_Samp_Fields>
  variance?: Maybe<Spatial_Ref_Sys_Variance_Fields>
}

/** aggregate fields of "spatial_ref_sys" */
export type Spatial_Ref_Sys_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Spatial_Ref_Sys_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Aggregate_Order_By = {
  avg?: Maybe<Spatial_Ref_Sys_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Spatial_Ref_Sys_Max_Order_By>
  min?: Maybe<Spatial_Ref_Sys_Min_Order_By>
  stddev?: Maybe<Spatial_Ref_Sys_Stddev_Order_By>
  stddev_pop?: Maybe<Spatial_Ref_Sys_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Spatial_Ref_Sys_Stddev_Samp_Order_By>
  sum?: Maybe<Spatial_Ref_Sys_Sum_Order_By>
  var_pop?: Maybe<Spatial_Ref_Sys_Var_Pop_Order_By>
  var_samp?: Maybe<Spatial_Ref_Sys_Var_Samp_Order_By>
  variance?: Maybe<Spatial_Ref_Sys_Variance_Order_By>
}

/** input type for inserting array relation for remote table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Arr_Rel_Insert_Input = {
  data: Array<Spatial_Ref_Sys_Insert_Input>
  on_conflict?: Maybe<Spatial_Ref_Sys_On_Conflict>
}

/** aggregate avg on columns */
export type Spatial_Ref_Sys_Avg_Fields = {
  __typename?: 'spatial_ref_sys_avg_fields'
  auth_srid?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Avg_Order_By = {
  auth_srid?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "spatial_ref_sys". All fields are combined with a logical 'AND'. */
export type Spatial_Ref_Sys_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Spatial_Ref_Sys_Bool_Exp>>>
  _not?: Maybe<Spatial_Ref_Sys_Bool_Exp>
  _or?: Maybe<Array<Maybe<Spatial_Ref_Sys_Bool_Exp>>>
  auth_name?: Maybe<String_Comparison_Exp>
  auth_srid?: Maybe<Int_Comparison_Exp>
  proj4text?: Maybe<String_Comparison_Exp>
  srid?: Maybe<Int_Comparison_Exp>
  srtext?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "spatial_ref_sys" */
export enum Spatial_Ref_Sys_Constraint {
  /** unique or primary key constraint */
  SpatialRefSysPkey = 'spatial_ref_sys_pkey'
}

/** input type for incrementing integer column in table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Inc_Input = {
  auth_srid?: Maybe<Scalars['Int']>
  srid?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Insert_Input = {
  auth_name?: Maybe<Scalars['String']>
  auth_srid?: Maybe<Scalars['Int']>
  proj4text?: Maybe<Scalars['String']>
  srid?: Maybe<Scalars['Int']>
  srtext?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Spatial_Ref_Sys_Max_Fields = {
  __typename?: 'spatial_ref_sys_max_fields'
  auth_name?: Maybe<Scalars['String']>
  auth_srid?: Maybe<Scalars['Int']>
  proj4text?: Maybe<Scalars['String']>
  srid?: Maybe<Scalars['Int']>
  srtext?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Max_Order_By = {
  auth_name?: Maybe<Order_By>
  auth_srid?: Maybe<Order_By>
  proj4text?: Maybe<Order_By>
  srid?: Maybe<Order_By>
  srtext?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Spatial_Ref_Sys_Min_Fields = {
  __typename?: 'spatial_ref_sys_min_fields'
  auth_name?: Maybe<Scalars['String']>
  auth_srid?: Maybe<Scalars['Int']>
  proj4text?: Maybe<Scalars['String']>
  srid?: Maybe<Scalars['Int']>
  srtext?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Min_Order_By = {
  auth_name?: Maybe<Order_By>
  auth_srid?: Maybe<Order_By>
  proj4text?: Maybe<Order_By>
  srid?: Maybe<Order_By>
  srtext?: Maybe<Order_By>
}

/** response of any mutation on the table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Mutation_Response = {
  __typename?: 'spatial_ref_sys_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Spatial_Ref_Sys>
}

/** input type for inserting object relation for remote table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Obj_Rel_Insert_Input = {
  data: Spatial_Ref_Sys_Insert_Input
  on_conflict?: Maybe<Spatial_Ref_Sys_On_Conflict>
}

/** on conflict condition type for table "spatial_ref_sys" */
export type Spatial_Ref_Sys_On_Conflict = {
  constraint: Spatial_Ref_Sys_Constraint
  update_columns: Array<Spatial_Ref_Sys_Update_Column>
  where?: Maybe<Spatial_Ref_Sys_Bool_Exp>
}

/** ordering options when selecting data from "spatial_ref_sys" */
export type Spatial_Ref_Sys_Order_By = {
  auth_name?: Maybe<Order_By>
  auth_srid?: Maybe<Order_By>
  proj4text?: Maybe<Order_By>
  srid?: Maybe<Order_By>
  srtext?: Maybe<Order_By>
}

/** primary key columns input for table: "spatial_ref_sys" */
export type Spatial_Ref_Sys_Pk_Columns_Input = {
  srid: Scalars['Int']
}

/** select columns of table "spatial_ref_sys" */
export enum Spatial_Ref_Sys_Select_Column {
  /** column name */
  AuthName = 'auth_name',
  /** column name */
  AuthSrid = 'auth_srid',
  /** column name */
  Proj4text = 'proj4text',
  /** column name */
  Srid = 'srid',
  /** column name */
  Srtext = 'srtext'
}

/** input type for updating data in table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Set_Input = {
  auth_name?: Maybe<Scalars['String']>
  auth_srid?: Maybe<Scalars['Int']>
  proj4text?: Maybe<Scalars['String']>
  srid?: Maybe<Scalars['Int']>
  srtext?: Maybe<Scalars['String']>
}

/** aggregate stddev on columns */
export type Spatial_Ref_Sys_Stddev_Fields = {
  __typename?: 'spatial_ref_sys_stddev_fields'
  auth_srid?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Stddev_Order_By = {
  auth_srid?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Spatial_Ref_Sys_Stddev_Pop_Fields = {
  __typename?: 'spatial_ref_sys_stddev_pop_fields'
  auth_srid?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Stddev_Pop_Order_By = {
  auth_srid?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Spatial_Ref_Sys_Stddev_Samp_Fields = {
  __typename?: 'spatial_ref_sys_stddev_samp_fields'
  auth_srid?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Stddev_Samp_Order_By = {
  auth_srid?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Spatial_Ref_Sys_Sum_Fields = {
  __typename?: 'spatial_ref_sys_sum_fields'
  auth_srid?: Maybe<Scalars['Int']>
  srid?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Sum_Order_By = {
  auth_srid?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** update columns of table "spatial_ref_sys" */
export enum Spatial_Ref_Sys_Update_Column {
  /** column name */
  AuthName = 'auth_name',
  /** column name */
  AuthSrid = 'auth_srid',
  /** column name */
  Proj4text = 'proj4text',
  /** column name */
  Srid = 'srid',
  /** column name */
  Srtext = 'srtext'
}

/** aggregate var_pop on columns */
export type Spatial_Ref_Sys_Var_Pop_Fields = {
  __typename?: 'spatial_ref_sys_var_pop_fields'
  auth_srid?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Var_Pop_Order_By = {
  auth_srid?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Spatial_Ref_Sys_Var_Samp_Fields = {
  __typename?: 'spatial_ref_sys_var_samp_fields'
  auth_srid?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Var_Samp_Order_By = {
  auth_srid?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Spatial_Ref_Sys_Variance_Fields = {
  __typename?: 'spatial_ref_sys_variance_fields'
  auth_srid?: Maybe<Scalars['Float']>
  srid?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "spatial_ref_sys" */
export type Spatial_Ref_Sys_Variance_Order_By = {
  auth_srid?: Maybe<Order_By>
  srid?: Maybe<Order_By>
}

export type St_D_Within_Geography_Input = {
  distance: Scalars['Float']
  from: Scalars['geography']
  use_spheroid?: Maybe<Scalars['Boolean']>
}

export type St_D_Within_Input = {
  distance: Scalars['Float']
  from: Scalars['geometry']
}

export type StatusOutput = {
  __typename?: 'StatusOutput'
  data?: Maybe<Scalars['jsonb']>
  status: Scalars['Boolean']
}

/** columns and relationships of "storages" */
export type Storages = {
  __typename?: 'storages'
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  updated_at: Scalars['timestamptz']
  used_bytes: Scalars['bigint']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** aggregated selection of "storages" */
export type Storages_Aggregate = {
  __typename?: 'storages_aggregate'
  aggregate?: Maybe<Storages_Aggregate_Fields>
  nodes: Array<Storages>
}

/** aggregate fields of "storages" */
export type Storages_Aggregate_Fields = {
  __typename?: 'storages_aggregate_fields'
  avg?: Maybe<Storages_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Storages_Max_Fields>
  min?: Maybe<Storages_Min_Fields>
  stddev?: Maybe<Storages_Stddev_Fields>
  stddev_pop?: Maybe<Storages_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Storages_Stddev_Samp_Fields>
  sum?: Maybe<Storages_Sum_Fields>
  var_pop?: Maybe<Storages_Var_Pop_Fields>
  var_samp?: Maybe<Storages_Var_Samp_Fields>
  variance?: Maybe<Storages_Variance_Fields>
}

/** aggregate fields of "storages" */
export type Storages_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Storages_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "storages" */
export type Storages_Aggregate_Order_By = {
  avg?: Maybe<Storages_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Storages_Max_Order_By>
  min?: Maybe<Storages_Min_Order_By>
  stddev?: Maybe<Storages_Stddev_Order_By>
  stddev_pop?: Maybe<Storages_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Storages_Stddev_Samp_Order_By>
  sum?: Maybe<Storages_Sum_Order_By>
  var_pop?: Maybe<Storages_Var_Pop_Order_By>
  var_samp?: Maybe<Storages_Var_Samp_Order_By>
  variance?: Maybe<Storages_Variance_Order_By>
}

/** input type for inserting array relation for remote table "storages" */
export type Storages_Arr_Rel_Insert_Input = {
  data: Array<Storages_Insert_Input>
  on_conflict?: Maybe<Storages_On_Conflict>
}

/** aggregate avg on columns */
export type Storages_Avg_Fields = {
  __typename?: 'storages_avg_fields'
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "storages" */
export type Storages_Avg_Order_By = {
  used_bytes?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "storages". All fields are combined with a logical 'AND'. */
export type Storages_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Storages_Bool_Exp>>>
  _not?: Maybe<Storages_Bool_Exp>
  _or?: Maybe<Array<Maybe<Storages_Bool_Exp>>>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  used_bytes?: Maybe<Bigint_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "storages" */
export enum Storages_Constraint {
  /** unique or primary key constraint */
  StoragesPkey = 'storages_pkey',
  /** unique or primary key constraint */
  StoragesUserIdKey = 'storages_user_id_key'
}

/** input type for incrementing integer column in table "storages" */
export type Storages_Inc_Input = {
  used_bytes?: Maybe<Scalars['bigint']>
}

/** input type for inserting data into table "storages" */
export type Storages_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  used_bytes?: Maybe<Scalars['bigint']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Storages_Max_Fields = {
  __typename?: 'storages_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  used_bytes?: Maybe<Scalars['bigint']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "storages" */
export type Storages_Max_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Storages_Min_Fields = {
  __typename?: 'storages_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  used_bytes?: Maybe<Scalars['bigint']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "storages" */
export type Storages_Min_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "storages" */
export type Storages_Mutation_Response = {
  __typename?: 'storages_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Storages>
}

/** input type for inserting object relation for remote table "storages" */
export type Storages_Obj_Rel_Insert_Input = {
  data: Storages_Insert_Input
  on_conflict?: Maybe<Storages_On_Conflict>
}

/** on conflict condition type for table "storages" */
export type Storages_On_Conflict = {
  constraint: Storages_Constraint
  update_columns: Array<Storages_Update_Column>
  where?: Maybe<Storages_Bool_Exp>
}

/** ordering options when selecting data from "storages" */
export type Storages_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "storages" */
export type Storages_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "storages" */
export enum Storages_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UsedBytes = 'used_bytes',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "storages" */
export type Storages_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  used_bytes?: Maybe<Scalars['bigint']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate stddev on columns */
export type Storages_Stddev_Fields = {
  __typename?: 'storages_stddev_fields'
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "storages" */
export type Storages_Stddev_Order_By = {
  used_bytes?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Storages_Stddev_Pop_Fields = {
  __typename?: 'storages_stddev_pop_fields'
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "storages" */
export type Storages_Stddev_Pop_Order_By = {
  used_bytes?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Storages_Stddev_Samp_Fields = {
  __typename?: 'storages_stddev_samp_fields'
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "storages" */
export type Storages_Stddev_Samp_Order_By = {
  used_bytes?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Storages_Sum_Fields = {
  __typename?: 'storages_sum_fields'
  used_bytes?: Maybe<Scalars['bigint']>
}

/** order by sum() on columns of table "storages" */
export type Storages_Sum_Order_By = {
  used_bytes?: Maybe<Order_By>
}

/** update columns of table "storages" */
export enum Storages_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UsedBytes = 'used_bytes',
  /** column name */
  UserId = 'user_id'
}

/** aggregate var_pop on columns */
export type Storages_Var_Pop_Fields = {
  __typename?: 'storages_var_pop_fields'
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "storages" */
export type Storages_Var_Pop_Order_By = {
  used_bytes?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Storages_Var_Samp_Fields = {
  __typename?: 'storages_var_samp_fields'
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "storages" */
export type Storages_Var_Samp_Order_By = {
  used_bytes?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Storages_Variance_Fields = {
  __typename?: 'storages_variance_fields'
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "storages" */
export type Storages_Variance_Order_By = {
  used_bytes?: Maybe<Order_By>
}

/** columns and relationships of "storages_view" */
export type Storages_View = {
  __typename?: 'storages_view'
  available_bytes?: Maybe<Scalars['bigint']>
  id?: Maybe<Scalars['uuid']>
  used_bytes?: Maybe<Scalars['bigint']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregated selection of "storages_view" */
export type Storages_View_Aggregate = {
  __typename?: 'storages_view_aggregate'
  aggregate?: Maybe<Storages_View_Aggregate_Fields>
  nodes: Array<Storages_View>
}

/** aggregate fields of "storages_view" */
export type Storages_View_Aggregate_Fields = {
  __typename?: 'storages_view_aggregate_fields'
  avg?: Maybe<Storages_View_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Storages_View_Max_Fields>
  min?: Maybe<Storages_View_Min_Fields>
  stddev?: Maybe<Storages_View_Stddev_Fields>
  stddev_pop?: Maybe<Storages_View_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Storages_View_Stddev_Samp_Fields>
  sum?: Maybe<Storages_View_Sum_Fields>
  var_pop?: Maybe<Storages_View_Var_Pop_Fields>
  var_samp?: Maybe<Storages_View_Var_Samp_Fields>
  variance?: Maybe<Storages_View_Variance_Fields>
}

/** aggregate fields of "storages_view" */
export type Storages_View_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Storages_View_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "storages_view" */
export type Storages_View_Aggregate_Order_By = {
  avg?: Maybe<Storages_View_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Storages_View_Max_Order_By>
  min?: Maybe<Storages_View_Min_Order_By>
  stddev?: Maybe<Storages_View_Stddev_Order_By>
  stddev_pop?: Maybe<Storages_View_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Storages_View_Stddev_Samp_Order_By>
  sum?: Maybe<Storages_View_Sum_Order_By>
  var_pop?: Maybe<Storages_View_Var_Pop_Order_By>
  var_samp?: Maybe<Storages_View_Var_Samp_Order_By>
  variance?: Maybe<Storages_View_Variance_Order_By>
}

/** input type for inserting array relation for remote table "storages_view" */
export type Storages_View_Arr_Rel_Insert_Input = {
  data: Array<Storages_View_Insert_Input>
}

/** aggregate avg on columns */
export type Storages_View_Avg_Fields = {
  __typename?: 'storages_view_avg_fields'
  available_bytes?: Maybe<Scalars['Float']>
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "storages_view" */
export type Storages_View_Avg_Order_By = {
  available_bytes?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "storages_view". All fields are combined with a logical 'AND'. */
export type Storages_View_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Storages_View_Bool_Exp>>>
  _not?: Maybe<Storages_View_Bool_Exp>
  _or?: Maybe<Array<Maybe<Storages_View_Bool_Exp>>>
  available_bytes?: Maybe<Bigint_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  used_bytes?: Maybe<Bigint_Comparison_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** input type for incrementing integer column in table "storages_view" */
export type Storages_View_Inc_Input = {
  available_bytes?: Maybe<Scalars['bigint']>
  used_bytes?: Maybe<Scalars['bigint']>
}

/** input type for inserting data into table "storages_view" */
export type Storages_View_Insert_Input = {
  available_bytes?: Maybe<Scalars['bigint']>
  id?: Maybe<Scalars['uuid']>
  used_bytes?: Maybe<Scalars['bigint']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Storages_View_Max_Fields = {
  __typename?: 'storages_view_max_fields'
  available_bytes?: Maybe<Scalars['bigint']>
  id?: Maybe<Scalars['uuid']>
  used_bytes?: Maybe<Scalars['bigint']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "storages_view" */
export type Storages_View_Max_Order_By = {
  available_bytes?: Maybe<Order_By>
  id?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Storages_View_Min_Fields = {
  __typename?: 'storages_view_min_fields'
  available_bytes?: Maybe<Scalars['bigint']>
  id?: Maybe<Scalars['uuid']>
  used_bytes?: Maybe<Scalars['bigint']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "storages_view" */
export type Storages_View_Min_Order_By = {
  available_bytes?: Maybe<Order_By>
  id?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "storages_view" */
export type Storages_View_Mutation_Response = {
  __typename?: 'storages_view_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Storages_View>
}

/** input type for inserting object relation for remote table "storages_view" */
export type Storages_View_Obj_Rel_Insert_Input = {
  data: Storages_View_Insert_Input
}

/** ordering options when selecting data from "storages_view" */
export type Storages_View_Order_By = {
  available_bytes?: Maybe<Order_By>
  id?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** select columns of table "storages_view" */
export enum Storages_View_Select_Column {
  /** column name */
  AvailableBytes = 'available_bytes',
  /** column name */
  Id = 'id',
  /** column name */
  UsedBytes = 'used_bytes',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "storages_view" */
export type Storages_View_Set_Input = {
  available_bytes?: Maybe<Scalars['bigint']>
  id?: Maybe<Scalars['uuid']>
  used_bytes?: Maybe<Scalars['bigint']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate stddev on columns */
export type Storages_View_Stddev_Fields = {
  __typename?: 'storages_view_stddev_fields'
  available_bytes?: Maybe<Scalars['Float']>
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "storages_view" */
export type Storages_View_Stddev_Order_By = {
  available_bytes?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Storages_View_Stddev_Pop_Fields = {
  __typename?: 'storages_view_stddev_pop_fields'
  available_bytes?: Maybe<Scalars['Float']>
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "storages_view" */
export type Storages_View_Stddev_Pop_Order_By = {
  available_bytes?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Storages_View_Stddev_Samp_Fields = {
  __typename?: 'storages_view_stddev_samp_fields'
  available_bytes?: Maybe<Scalars['Float']>
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "storages_view" */
export type Storages_View_Stddev_Samp_Order_By = {
  available_bytes?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Storages_View_Sum_Fields = {
  __typename?: 'storages_view_sum_fields'
  available_bytes?: Maybe<Scalars['bigint']>
  used_bytes?: Maybe<Scalars['bigint']>
}

/** order by sum() on columns of table "storages_view" */
export type Storages_View_Sum_Order_By = {
  available_bytes?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
}

/** aggregate var_pop on columns */
export type Storages_View_Var_Pop_Fields = {
  __typename?: 'storages_view_var_pop_fields'
  available_bytes?: Maybe<Scalars['Float']>
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "storages_view" */
export type Storages_View_Var_Pop_Order_By = {
  available_bytes?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Storages_View_Var_Samp_Fields = {
  __typename?: 'storages_view_var_samp_fields'
  available_bytes?: Maybe<Scalars['Float']>
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "storages_view" */
export type Storages_View_Var_Samp_Order_By = {
  available_bytes?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Storages_View_Variance_Fields = {
  __typename?: 'storages_view_variance_fields'
  available_bytes?: Maybe<Scalars['Float']>
  used_bytes?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "storages_view" */
export type Storages_View_Variance_Order_By = {
  available_bytes?: Maybe<Order_By>
  used_bytes?: Maybe<Order_By>
}

/** columns and relationships of "store" */
export type Store = {
  __typename?: 'store'
  category?: Maybe<Scalars['String']>
  /** An object relationship */
  cover?: Maybe<Media_Files>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at: Scalars['timestamptz']
  currency: Scalars['String']
  date_end?: Maybe<Scalars['timestamptz']>
  date_start: Scalars['timestamptz']
  description: Scalars['String']
  duration?: Maybe<Scalars['Int']>
  id: Scalars['uuid']
  /** An object relationship */
  location?: Maybe<Locations>
  location_id?: Maybe<Scalars['uuid']>
  /** An array relationship */
  media_files: Array<Media_Files>
  /** An aggregated array relationship */
  media_files_aggregate: Media_Files_Aggregate
  name: Scalars['String']
  nft: Scalars['Boolean']
  phone?: Maybe<Scalars['String']>
  price?: Maybe<Scalars['numeric']>
  quantity?: Maybe<Scalars['Int']>
  /** An object relationship */
  store_category?: Maybe<Store_Categories>
  /** An object relationship */
  store_type: Store_Types
  total_media_files: Scalars['Int']
  type: Store_Types_Enum
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** columns and relationships of "store" */
export type StoreMedia_FilesArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** columns and relationships of "store" */
export type StoreMedia_Files_AggregateArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** aggregated selection of "store" */
export type Store_Aggregate = {
  __typename?: 'store_aggregate'
  aggregate?: Maybe<Store_Aggregate_Fields>
  nodes: Array<Store>
}

/** aggregate fields of "store" */
export type Store_Aggregate_Fields = {
  __typename?: 'store_aggregate_fields'
  avg?: Maybe<Store_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Store_Max_Fields>
  min?: Maybe<Store_Min_Fields>
  stddev?: Maybe<Store_Stddev_Fields>
  stddev_pop?: Maybe<Store_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Store_Stddev_Samp_Fields>
  sum?: Maybe<Store_Sum_Fields>
  var_pop?: Maybe<Store_Var_Pop_Fields>
  var_samp?: Maybe<Store_Var_Samp_Fields>
  variance?: Maybe<Store_Variance_Fields>
}

/** aggregate fields of "store" */
export type Store_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Store_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "store" */
export type Store_Aggregate_Order_By = {
  avg?: Maybe<Store_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Store_Max_Order_By>
  min?: Maybe<Store_Min_Order_By>
  stddev?: Maybe<Store_Stddev_Order_By>
  stddev_pop?: Maybe<Store_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Store_Stddev_Samp_Order_By>
  sum?: Maybe<Store_Sum_Order_By>
  var_pop?: Maybe<Store_Var_Pop_Order_By>
  var_samp?: Maybe<Store_Var_Samp_Order_By>
  variance?: Maybe<Store_Variance_Order_By>
}

/** input type for inserting array relation for remote table "store" */
export type Store_Arr_Rel_Insert_Input = {
  data: Array<Store_Insert_Input>
  on_conflict?: Maybe<Store_On_Conflict>
}

/** aggregate avg on columns */
export type Store_Avg_Fields = {
  __typename?: 'store_avg_fields'
  duration?: Maybe<Scalars['Float']>
  price?: Maybe<Scalars['Float']>
  quantity?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "store" */
export type Store_Avg_Order_By = {
  duration?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "store". All fields are combined with a logical 'AND'. */
export type Store_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Store_Bool_Exp>>>
  _not?: Maybe<Store_Bool_Exp>
  _or?: Maybe<Array<Maybe<Store_Bool_Exp>>>
  category?: Maybe<String_Comparison_Exp>
  cover?: Maybe<Media_Files_Bool_Exp>
  cover_media_file_id?: Maybe<Uuid_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  currency?: Maybe<String_Comparison_Exp>
  date_end?: Maybe<Timestamptz_Comparison_Exp>
  date_start?: Maybe<Timestamptz_Comparison_Exp>
  description?: Maybe<String_Comparison_Exp>
  duration?: Maybe<Int_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  location?: Maybe<Locations_Bool_Exp>
  location_id?: Maybe<Uuid_Comparison_Exp>
  media_files?: Maybe<Media_Files_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
  nft?: Maybe<Boolean_Comparison_Exp>
  phone?: Maybe<String_Comparison_Exp>
  price?: Maybe<Numeric_Comparison_Exp>
  quantity?: Maybe<Int_Comparison_Exp>
  store_category?: Maybe<Store_Categories_Bool_Exp>
  store_type?: Maybe<Store_Types_Bool_Exp>
  total_media_files?: Maybe<Int_Comparison_Exp>
  type?: Maybe<Store_Types_Enum_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** columns and relationships of "store_categories" */
export type Store_Categories = {
  __typename?: 'store_categories'
  /** An array relationship */
  stores: Array<Store>
  /** An aggregated array relationship */
  stores_aggregate: Store_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "store_categories" */
export type Store_CategoriesStoresArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** columns and relationships of "store_categories" */
export type Store_CategoriesStores_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** aggregated selection of "store_categories" */
export type Store_Categories_Aggregate = {
  __typename?: 'store_categories_aggregate'
  aggregate?: Maybe<Store_Categories_Aggregate_Fields>
  nodes: Array<Store_Categories>
}

/** aggregate fields of "store_categories" */
export type Store_Categories_Aggregate_Fields = {
  __typename?: 'store_categories_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Store_Categories_Max_Fields>
  min?: Maybe<Store_Categories_Min_Fields>
}

/** aggregate fields of "store_categories" */
export type Store_Categories_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Store_Categories_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "store_categories" */
export type Store_Categories_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Store_Categories_Max_Order_By>
  min?: Maybe<Store_Categories_Min_Order_By>
}

/** input type for inserting array relation for remote table "store_categories" */
export type Store_Categories_Arr_Rel_Insert_Input = {
  data: Array<Store_Categories_Insert_Input>
  on_conflict?: Maybe<Store_Categories_On_Conflict>
}

/** Boolean expression to filter rows from the table "store_categories". All fields are combined with a logical 'AND'. */
export type Store_Categories_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Store_Categories_Bool_Exp>>>
  _not?: Maybe<Store_Categories_Bool_Exp>
  _or?: Maybe<Array<Maybe<Store_Categories_Bool_Exp>>>
  stores?: Maybe<Store_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "store_categories" */
export enum Store_Categories_Constraint {
  /** unique or primary key constraint */
  StoreCategoriesPkey = 'store_categories_pkey'
}

/** input type for inserting data into table "store_categories" */
export type Store_Categories_Insert_Input = {
  stores?: Maybe<Store_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Store_Categories_Max_Fields = {
  __typename?: 'store_categories_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "store_categories" */
export type Store_Categories_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Store_Categories_Min_Fields = {
  __typename?: 'store_categories_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "store_categories" */
export type Store_Categories_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "store_categories" */
export type Store_Categories_Mutation_Response = {
  __typename?: 'store_categories_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Store_Categories>
}

/** input type for inserting object relation for remote table "store_categories" */
export type Store_Categories_Obj_Rel_Insert_Input = {
  data: Store_Categories_Insert_Input
  on_conflict?: Maybe<Store_Categories_On_Conflict>
}

/** on conflict condition type for table "store_categories" */
export type Store_Categories_On_Conflict = {
  constraint: Store_Categories_Constraint
  update_columns: Array<Store_Categories_Update_Column>
  where?: Maybe<Store_Categories_Bool_Exp>
}

/** ordering options when selecting data from "store_categories" */
export type Store_Categories_Order_By = {
  stores_aggregate?: Maybe<Store_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "store_categories" */
export type Store_Categories_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "store_categories" */
export enum Store_Categories_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "store_categories" */
export type Store_Categories_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "store_categories" */
export enum Store_Categories_Update_Column {
  /** column name */
  Value = 'value'
}

/** unique or primary key constraints on table "store" */
export enum Store_Constraint {
  /** unique or primary key constraint */
  StorePkey = 'store_pkey'
}

/** input type for incrementing integer column in table "store" */
export type Store_Inc_Input = {
  duration?: Maybe<Scalars['Int']>
  price?: Maybe<Scalars['numeric']>
  quantity?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "store" */
export type Store_Insert_Input = {
  category?: Maybe<Scalars['String']>
  cover?: Maybe<Media_Files_Obj_Rel_Insert_Input>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  currency?: Maybe<Scalars['String']>
  date_end?: Maybe<Scalars['timestamptz']>
  date_start?: Maybe<Scalars['timestamptz']>
  description?: Maybe<Scalars['String']>
  duration?: Maybe<Scalars['Int']>
  id?: Maybe<Scalars['uuid']>
  location?: Maybe<Locations_Obj_Rel_Insert_Input>
  location_id?: Maybe<Scalars['uuid']>
  media_files?: Maybe<Media_Files_Arr_Rel_Insert_Input>
  name?: Maybe<Scalars['String']>
  nft?: Maybe<Scalars['Boolean']>
  phone?: Maybe<Scalars['String']>
  price?: Maybe<Scalars['numeric']>
  quantity?: Maybe<Scalars['Int']>
  store_category?: Maybe<Store_Categories_Obj_Rel_Insert_Input>
  store_type?: Maybe<Store_Types_Obj_Rel_Insert_Input>
  total_media_files?: Maybe<Scalars['Int']>
  type?: Maybe<Store_Types_Enum>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Store_Max_Fields = {
  __typename?: 'store_max_fields'
  category?: Maybe<Scalars['String']>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  currency?: Maybe<Scalars['String']>
  date_end?: Maybe<Scalars['timestamptz']>
  date_start?: Maybe<Scalars['timestamptz']>
  description?: Maybe<Scalars['String']>
  duration?: Maybe<Scalars['Int']>
  id?: Maybe<Scalars['uuid']>
  location_id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  phone?: Maybe<Scalars['String']>
  price?: Maybe<Scalars['numeric']>
  quantity?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "store" */
export type Store_Max_Order_By = {
  category?: Maybe<Order_By>
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  currency?: Maybe<Order_By>
  date_end?: Maybe<Order_By>
  date_start?: Maybe<Order_By>
  description?: Maybe<Order_By>
  duration?: Maybe<Order_By>
  id?: Maybe<Order_By>
  location_id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  phone?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Store_Min_Fields = {
  __typename?: 'store_min_fields'
  category?: Maybe<Scalars['String']>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  currency?: Maybe<Scalars['String']>
  date_end?: Maybe<Scalars['timestamptz']>
  date_start?: Maybe<Scalars['timestamptz']>
  description?: Maybe<Scalars['String']>
  duration?: Maybe<Scalars['Int']>
  id?: Maybe<Scalars['uuid']>
  location_id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  phone?: Maybe<Scalars['String']>
  price?: Maybe<Scalars['numeric']>
  quantity?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "store" */
export type Store_Min_Order_By = {
  category?: Maybe<Order_By>
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  currency?: Maybe<Order_By>
  date_end?: Maybe<Order_By>
  date_start?: Maybe<Order_By>
  description?: Maybe<Order_By>
  duration?: Maybe<Order_By>
  id?: Maybe<Order_By>
  location_id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  phone?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "store" */
export type Store_Mutation_Response = {
  __typename?: 'store_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Store>
}

/** input type for inserting object relation for remote table "store" */
export type Store_Obj_Rel_Insert_Input = {
  data: Store_Insert_Input
  on_conflict?: Maybe<Store_On_Conflict>
}

/** on conflict condition type for table "store" */
export type Store_On_Conflict = {
  constraint: Store_Constraint
  update_columns: Array<Store_Update_Column>
  where?: Maybe<Store_Bool_Exp>
}

/** ordering options when selecting data from "store" */
export type Store_Order_By = {
  category?: Maybe<Order_By>
  cover?: Maybe<Media_Files_Order_By>
  cover_media_file_id?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  currency?: Maybe<Order_By>
  date_end?: Maybe<Order_By>
  date_start?: Maybe<Order_By>
  description?: Maybe<Order_By>
  duration?: Maybe<Order_By>
  id?: Maybe<Order_By>
  location?: Maybe<Locations_Order_By>
  location_id?: Maybe<Order_By>
  media_files_aggregate?: Maybe<Media_Files_Aggregate_Order_By>
  name?: Maybe<Order_By>
  nft?: Maybe<Order_By>
  phone?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  store_category?: Maybe<Store_Categories_Order_By>
  store_type?: Maybe<Store_Types_Order_By>
  total_media_files?: Maybe<Order_By>
  type?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "store" */
export type Store_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "store" */
export enum Store_Select_Column {
  /** column name */
  Category = 'category',
  /** column name */
  CoverMediaFileId = 'cover_media_file_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Currency = 'currency',
  /** column name */
  DateEnd = 'date_end',
  /** column name */
  DateStart = 'date_start',
  /** column name */
  Description = 'description',
  /** column name */
  Duration = 'duration',
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Name = 'name',
  /** column name */
  Nft = 'nft',
  /** column name */
  Phone = 'phone',
  /** column name */
  Price = 'price',
  /** column name */
  Quantity = 'quantity',
  /** column name */
  TotalMediaFiles = 'total_media_files',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "store" */
export type Store_Set_Input = {
  category?: Maybe<Scalars['String']>
  cover_media_file_id?: Maybe<Scalars['uuid']>
  created_at?: Maybe<Scalars['timestamptz']>
  currency?: Maybe<Scalars['String']>
  date_end?: Maybe<Scalars['timestamptz']>
  date_start?: Maybe<Scalars['timestamptz']>
  description?: Maybe<Scalars['String']>
  duration?: Maybe<Scalars['Int']>
  id?: Maybe<Scalars['uuid']>
  location_id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  nft?: Maybe<Scalars['Boolean']>
  phone?: Maybe<Scalars['String']>
  price?: Maybe<Scalars['numeric']>
  quantity?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
  type?: Maybe<Store_Types_Enum>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate stddev on columns */
export type Store_Stddev_Fields = {
  __typename?: 'store_stddev_fields'
  duration?: Maybe<Scalars['Float']>
  price?: Maybe<Scalars['Float']>
  quantity?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "store" */
export type Store_Stddev_Order_By = {
  duration?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Store_Stddev_Pop_Fields = {
  __typename?: 'store_stddev_pop_fields'
  duration?: Maybe<Scalars['Float']>
  price?: Maybe<Scalars['Float']>
  quantity?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "store" */
export type Store_Stddev_Pop_Order_By = {
  duration?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Store_Stddev_Samp_Fields = {
  __typename?: 'store_stddev_samp_fields'
  duration?: Maybe<Scalars['Float']>
  price?: Maybe<Scalars['Float']>
  quantity?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "store" */
export type Store_Stddev_Samp_Order_By = {
  duration?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Store_Sum_Fields = {
  __typename?: 'store_sum_fields'
  duration?: Maybe<Scalars['Int']>
  price?: Maybe<Scalars['numeric']>
  quantity?: Maybe<Scalars['Int']>
  total_media_files?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "store" */
export type Store_Sum_Order_By = {
  duration?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** columns and relationships of "store_types" */
export type Store_Types = {
  __typename?: 'store_types'
  /** An array relationship */
  stores: Array<Store>
  /** An aggregated array relationship */
  stores_aggregate: Store_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "store_types" */
export type Store_TypesStoresArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** columns and relationships of "store_types" */
export type Store_TypesStores_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** aggregated selection of "store_types" */
export type Store_Types_Aggregate = {
  __typename?: 'store_types_aggregate'
  aggregate?: Maybe<Store_Types_Aggregate_Fields>
  nodes: Array<Store_Types>
}

/** aggregate fields of "store_types" */
export type Store_Types_Aggregate_Fields = {
  __typename?: 'store_types_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Store_Types_Max_Fields>
  min?: Maybe<Store_Types_Min_Fields>
}

/** aggregate fields of "store_types" */
export type Store_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Store_Types_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "store_types" */
export type Store_Types_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Store_Types_Max_Order_By>
  min?: Maybe<Store_Types_Min_Order_By>
}

/** input type for inserting array relation for remote table "store_types" */
export type Store_Types_Arr_Rel_Insert_Input = {
  data: Array<Store_Types_Insert_Input>
  on_conflict?: Maybe<Store_Types_On_Conflict>
}

/** Boolean expression to filter rows from the table "store_types". All fields are combined with a logical 'AND'. */
export type Store_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Store_Types_Bool_Exp>>>
  _not?: Maybe<Store_Types_Bool_Exp>
  _or?: Maybe<Array<Maybe<Store_Types_Bool_Exp>>>
  stores?: Maybe<Store_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "store_types" */
export enum Store_Types_Constraint {
  /** unique or primary key constraint */
  StoreTypesPkey = 'store_types_pkey'
}

export enum Store_Types_Enum {
  Event = 'EVENT',
  Nft = 'NFT',
  Product = 'PRODUCT',
  Wallpaper = 'WALLPAPER'
}

/** expression to compare columns of type store_types_enum. All fields are combined with logical 'AND'. */
export type Store_Types_Enum_Comparison_Exp = {
  _eq?: Maybe<Store_Types_Enum>
  _in?: Maybe<Array<Store_Types_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Store_Types_Enum>
  _nin?: Maybe<Array<Store_Types_Enum>>
}

/** input type for inserting data into table "store_types" */
export type Store_Types_Insert_Input = {
  stores?: Maybe<Store_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Store_Types_Max_Fields = {
  __typename?: 'store_types_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "store_types" */
export type Store_Types_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Store_Types_Min_Fields = {
  __typename?: 'store_types_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "store_types" */
export type Store_Types_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "store_types" */
export type Store_Types_Mutation_Response = {
  __typename?: 'store_types_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Store_Types>
}

/** input type for inserting object relation for remote table "store_types" */
export type Store_Types_Obj_Rel_Insert_Input = {
  data: Store_Types_Insert_Input
  on_conflict?: Maybe<Store_Types_On_Conflict>
}

/** on conflict condition type for table "store_types" */
export type Store_Types_On_Conflict = {
  constraint: Store_Types_Constraint
  update_columns: Array<Store_Types_Update_Column>
  where?: Maybe<Store_Types_Bool_Exp>
}

/** ordering options when selecting data from "store_types" */
export type Store_Types_Order_By = {
  stores_aggregate?: Maybe<Store_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "store_types" */
export type Store_Types_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "store_types" */
export enum Store_Types_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "store_types" */
export type Store_Types_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "store_types" */
export enum Store_Types_Update_Column {
  /** column name */
  Value = 'value'
}

/** update columns of table "store" */
export enum Store_Update_Column {
  /** column name */
  Category = 'category',
  /** column name */
  CoverMediaFileId = 'cover_media_file_id',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Currency = 'currency',
  /** column name */
  DateEnd = 'date_end',
  /** column name */
  DateStart = 'date_start',
  /** column name */
  Description = 'description',
  /** column name */
  Duration = 'duration',
  /** column name */
  Id = 'id',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Name = 'name',
  /** column name */
  Nft = 'nft',
  /** column name */
  Phone = 'phone',
  /** column name */
  Price = 'price',
  /** column name */
  Quantity = 'quantity',
  /** column name */
  TotalMediaFiles = 'total_media_files',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** aggregate var_pop on columns */
export type Store_Var_Pop_Fields = {
  __typename?: 'store_var_pop_fields'
  duration?: Maybe<Scalars['Float']>
  price?: Maybe<Scalars['Float']>
  quantity?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "store" */
export type Store_Var_Pop_Order_By = {
  duration?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Store_Var_Samp_Fields = {
  __typename?: 'store_var_samp_fields'
  duration?: Maybe<Scalars['Float']>
  price?: Maybe<Scalars['Float']>
  quantity?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "store" */
export type Store_Var_Samp_Order_By = {
  duration?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Store_Variance_Fields = {
  __typename?: 'store_variance_fields'
  duration?: Maybe<Scalars['Float']>
  price?: Maybe<Scalars['Float']>
  quantity?: Maybe<Scalars['Float']>
  total_media_files?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "store" */
export type Store_Variance_Order_By = {
  duration?: Maybe<Order_By>
  price?: Maybe<Order_By>
  quantity?: Maybe<Order_By>
  total_media_files?: Maybe<Order_By>
}

/** expression to compare columns of type String. All fields are combined with logical 'AND'. */
export type String_Comparison_Exp = {
  _eq?: Maybe<Scalars['String']>
  _gt?: Maybe<Scalars['String']>
  _gte?: Maybe<Scalars['String']>
  _ilike?: Maybe<Scalars['String']>
  _in?: Maybe<Array<Scalars['String']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _like?: Maybe<Scalars['String']>
  _lt?: Maybe<Scalars['String']>
  _lte?: Maybe<Scalars['String']>
  _neq?: Maybe<Scalars['String']>
  _nilike?: Maybe<Scalars['String']>
  _nin?: Maybe<Array<Scalars['String']>>
  _nlike?: Maybe<Scalars['String']>
  _nsimilar?: Maybe<Scalars['String']>
  _similar?: Maybe<Scalars['String']>
}

/** columns and relationships of "styles" */
export type Styles = {
  __typename?: 'styles'
  id: Scalars['uuid']
  /** An array relationship */
  localizations: Array<Localizations>
  /** An aggregated array relationship */
  localizations_aggregate: Localizations_Aggregate
  name: Scalars['String']
  /** An array relationship */
  users_styles: Array<Users_Styles>
  /** An aggregated array relationship */
  users_styles_aggregate: Users_Styles_Aggregate
}

/** columns and relationships of "styles" */
export type StylesLocalizationsArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** columns and relationships of "styles" */
export type StylesLocalizations_AggregateArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** columns and relationships of "styles" */
export type StylesUsers_StylesArgs = {
  distinct_on?: Maybe<Array<Users_Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Styles_Order_By>>
  where?: Maybe<Users_Styles_Bool_Exp>
}

/** columns and relationships of "styles" */
export type StylesUsers_Styles_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Styles_Order_By>>
  where?: Maybe<Users_Styles_Bool_Exp>
}

/** aggregated selection of "styles" */
export type Styles_Aggregate = {
  __typename?: 'styles_aggregate'
  aggregate?: Maybe<Styles_Aggregate_Fields>
  nodes: Array<Styles>
}

/** aggregate fields of "styles" */
export type Styles_Aggregate_Fields = {
  __typename?: 'styles_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Styles_Max_Fields>
  min?: Maybe<Styles_Min_Fields>
}

/** aggregate fields of "styles" */
export type Styles_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Styles_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "styles" */
export type Styles_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Styles_Max_Order_By>
  min?: Maybe<Styles_Min_Order_By>
}

/** input type for inserting array relation for remote table "styles" */
export type Styles_Arr_Rel_Insert_Input = {
  data: Array<Styles_Insert_Input>
  on_conflict?: Maybe<Styles_On_Conflict>
}

/** Boolean expression to filter rows from the table "styles". All fields are combined with a logical 'AND'. */
export type Styles_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Styles_Bool_Exp>>>
  _not?: Maybe<Styles_Bool_Exp>
  _or?: Maybe<Array<Maybe<Styles_Bool_Exp>>>
  id?: Maybe<Uuid_Comparison_Exp>
  localizations?: Maybe<Localizations_Bool_Exp>
  name?: Maybe<String_Comparison_Exp>
  users_styles?: Maybe<Users_Styles_Bool_Exp>
}

/** unique or primary key constraints on table "styles" */
export enum Styles_Constraint {
  /** unique or primary key constraint */
  StyleNameKey = 'style_name_key',
  /** unique or primary key constraint */
  StylePkey = 'style_pkey'
}

/** input type for inserting data into table "styles" */
export type Styles_Insert_Input = {
  id?: Maybe<Scalars['uuid']>
  localizations?: Maybe<Localizations_Arr_Rel_Insert_Input>
  name?: Maybe<Scalars['String']>
  users_styles?: Maybe<Users_Styles_Arr_Rel_Insert_Input>
}

/** aggregate max on columns */
export type Styles_Max_Fields = {
  __typename?: 'styles_max_fields'
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "styles" */
export type Styles_Max_Order_By = {
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Styles_Min_Fields = {
  __typename?: 'styles_min_fields'
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "styles" */
export type Styles_Min_Order_By = {
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
}

/** response of any mutation on the table "styles" */
export type Styles_Mutation_Response = {
  __typename?: 'styles_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Styles>
}

/** input type for inserting object relation for remote table "styles" */
export type Styles_Obj_Rel_Insert_Input = {
  data: Styles_Insert_Input
  on_conflict?: Maybe<Styles_On_Conflict>
}

/** on conflict condition type for table "styles" */
export type Styles_On_Conflict = {
  constraint: Styles_Constraint
  update_columns: Array<Styles_Update_Column>
  where?: Maybe<Styles_Bool_Exp>
}

/** ordering options when selecting data from "styles" */
export type Styles_Order_By = {
  id?: Maybe<Order_By>
  localizations_aggregate?: Maybe<Localizations_Aggregate_Order_By>
  name?: Maybe<Order_By>
  users_styles_aggregate?: Maybe<Users_Styles_Aggregate_Order_By>
}

/** primary key columns input for table: "styles" */
export type Styles_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "styles" */
export enum Styles_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** input type for updating data in table "styles" */
export type Styles_Set_Input = {
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
}

/** update columns of table "styles" */
export enum Styles_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name'
}

/** subscription root */
export type Subscription_Root = {
  __typename?: 'subscription_root'
  /** fetch data from the table: "albums" */
  albums: Array<Albums>
  /** fetch aggregated fields from the table: "albums" */
  albums_aggregate: Albums_Aggregate
  /** fetch data from the table: "albums" using primary key columns */
  albums_by_pk?: Maybe<Albums>
  /** fetch data from the table: "albums_payment_types" */
  albums_payment_types: Array<Albums_Payment_Types>
  /** fetch aggregated fields from the table: "albums_payment_types" */
  albums_payment_types_aggregate: Albums_Payment_Types_Aggregate
  /** fetch data from the table: "albums_payment_types" using primary key columns */
  albums_payment_types_by_pk?: Maybe<Albums_Payment_Types>
  /** fetch data from the table: "albums_statistics_view" */
  albums_statistics_view: Array<Albums_Statistics_View>
  /** fetch aggregated fields from the table: "albums_statistics_view" */
  albums_statistics_view_aggregate: Albums_Statistics_View_Aggregate
  /** fetch data from the table: "albums_tags" */
  albums_tags: Array<Albums_Tags>
  /** fetch aggregated fields from the table: "albums_tags" */
  albums_tags_aggregate: Albums_Tags_Aggregate
  /** fetch data from the table: "albums_tags" using primary key columns */
  albums_tags_by_pk?: Maybe<Albums_Tags>
  /** fetch data from the table: "albums_visits" */
  albums_visits: Array<Albums_Visits>
  /** fetch aggregated fields from the table: "albums_visits" */
  albums_visits_aggregate: Albums_Visits_Aggregate
  /** fetch data from the table: "albums_visits" using primary key columns */
  albums_visits_by_pk?: Maybe<Albums_Visits>
  /** fetch data from the table: "auth.account_providers" */
  auth_account_providers: Array<Auth_Account_Providers>
  /** fetch aggregated fields from the table: "auth.account_providers" */
  auth_account_providers_aggregate: Auth_Account_Providers_Aggregate
  /** fetch data from the table: "auth.account_providers" using primary key columns */
  auth_account_providers_by_pk?: Maybe<Auth_Account_Providers>
  /** fetch data from the table: "auth.account_roles" */
  auth_account_roles: Array<Auth_Account_Roles>
  /** fetch aggregated fields from the table: "auth.account_roles" */
  auth_account_roles_aggregate: Auth_Account_Roles_Aggregate
  /** fetch data from the table: "auth.account_roles" using primary key columns */
  auth_account_roles_by_pk?: Maybe<Auth_Account_Roles>
  /** fetch data from the table: "auth.accounts" */
  auth_accounts: Array<Auth_Accounts>
  /** fetch aggregated fields from the table: "auth.accounts" */
  auth_accounts_aggregate: Auth_Accounts_Aggregate
  /** fetch data from the table: "auth.accounts" using primary key columns */
  auth_accounts_by_pk?: Maybe<Auth_Accounts>
  /** fetch data from the table: "auth.providers" */
  auth_providers: Array<Auth_Providers>
  /** fetch aggregated fields from the table: "auth.providers" */
  auth_providers_aggregate: Auth_Providers_Aggregate
  /** fetch data from the table: "auth.providers" using primary key columns */
  auth_providers_by_pk?: Maybe<Auth_Providers>
  /** fetch data from the table: "auth.refresh_tokens" */
  auth_refresh_tokens: Array<Auth_Refresh_Tokens>
  /** fetch aggregated fields from the table: "auth.refresh_tokens" */
  auth_refresh_tokens_aggregate: Auth_Refresh_Tokens_Aggregate
  /** fetch data from the table: "auth.refresh_tokens" using primary key columns */
  auth_refresh_tokens_by_pk?: Maybe<Auth_Refresh_Tokens>
  /** fetch data from the table: "auth.roles" */
  auth_roles: Array<Auth_Roles>
  /** fetch aggregated fields from the table: "auth.roles" */
  auth_roles_aggregate: Auth_Roles_Aggregate
  /** fetch data from the table: "auth.roles" using primary key columns */
  auth_roles_by_pk?: Maybe<Auth_Roles>
  /** fetch data from the table: "config" */
  config: Array<Config>
  /** fetch aggregated fields from the table: "config" */
  config_aggregate: Config_Aggregate
  /** fetch data from the table: "config" using primary key columns */
  config_by_pk?: Maybe<Config>
  /** fetch data from the table: "config_keys" */
  config_keys: Array<Config_Keys>
  /** fetch aggregated fields from the table: "config_keys" */
  config_keys_aggregate: Config_Keys_Aggregate
  /** fetch data from the table: "config_keys" using primary key columns */
  config_keys_by_pk?: Maybe<Config_Keys>
  /** fetch data from the table: "contacts" */
  contacts: Array<Contacts>
  /** fetch aggregated fields from the table: "contacts" */
  contacts_aggregate: Contacts_Aggregate
  /** fetch data from the table: "contacts" using primary key columns */
  contacts_by_pk?: Maybe<Contacts>
  /** fetch data from the table: "cover_images" */
  cover_images: Array<Cover_Images>
  /** fetch aggregated fields from the table: "cover_images" */
  cover_images_aggregate: Cover_Images_Aggregate
  /** fetch data from the table: "cover_images" using primary key columns */
  cover_images_by_pk?: Maybe<Cover_Images>
  /** fetch data from the table: "credit_cards" */
  credit_cards: Array<Credit_Cards>
  /** fetch aggregated fields from the table: "credit_cards" */
  credit_cards_aggregate: Credit_Cards_Aggregate
  /** fetch data from the table: "credit_cards" using primary key columns */
  credit_cards_by_pk?: Maybe<Credit_Cards>
  /** fetch data from the table: "downloads" */
  downloads: Array<Downloads>
  /** fetch aggregated fields from the table: "downloads" */
  downloads_aggregate: Downloads_Aggregate
  /** fetch data from the table: "downloads" using primary key columns */
  downloads_by_pk?: Maybe<Downloads>
  /** fetch data from the table: "events" */
  events: Array<Events>
  /** fetch aggregated fields from the table: "events" */
  events_aggregate: Events_Aggregate
  /** fetch data from the table: "events" using primary key columns */
  events_by_pk?: Maybe<Events>
  /** fetch data from the table: "events_payment_types" */
  events_payment_types: Array<Events_Payment_Types>
  /** fetch aggregated fields from the table: "events_payment_types" */
  events_payment_types_aggregate: Events_Payment_Types_Aggregate
  /** fetch data from the table: "events_payment_types" using primary key columns */
  events_payment_types_by_pk?: Maybe<Events_Payment_Types>
  /** fetch data from the table: "events_statuses" */
  events_statuses: Array<Events_Statuses>
  /** fetch aggregated fields from the table: "events_statuses" */
  events_statuses_aggregate: Events_Statuses_Aggregate
  /** fetch data from the table: "events_statuses" using primary key columns */
  events_statuses_by_pk?: Maybe<Events_Statuses>
  /** fetch data from the table: "favorites" */
  favorites: Array<Favorites>
  /** fetch aggregated fields from the table: "favorites" */
  favorites_aggregate: Favorites_Aggregate
  /** fetch data from the table: "favorites" using primary key columns */
  favorites_by_pk?: Maybe<Favorites>
  /** fetch data from the table: "folders" */
  folders: Array<Folders>
  /** fetch aggregated fields from the table: "folders" */
  folders_aggregate: Folders_Aggregate
  /** fetch data from the table: "folders" using primary key columns */
  folders_by_pk?: Maybe<Folders>
  /** fetch data from the table: "geography_columns" */
  geography_columns: Array<Geography_Columns>
  /** fetch aggregated fields from the table: "geography_columns" */
  geography_columns_aggregate: Geography_Columns_Aggregate
  /** fetch data from the table: "geometry_columns" */
  geometry_columns: Array<Geometry_Columns>
  /** fetch aggregated fields from the table: "geometry_columns" */
  geometry_columns_aggregate: Geometry_Columns_Aggregate
  /** fetch data from the table: "locales" */
  locales: Array<Locales>
  /** fetch aggregated fields from the table: "locales" */
  locales_aggregate: Locales_Aggregate
  /** fetch data from the table: "locales" using primary key columns */
  locales_by_pk?: Maybe<Locales>
  /** fetch data from the table: "localizations" */
  localizations: Array<Localizations>
  /** fetch aggregated fields from the table: "localizations" */
  localizations_aggregate: Localizations_Aggregate
  /** fetch data from the table: "localizations" using primary key columns */
  localizations_by_pk?: Maybe<Localizations>
  /** fetch data from the table: "locations" */
  locations: Array<Locations>
  /** fetch aggregated fields from the table: "locations" */
  locations_aggregate: Locations_Aggregate
  /** fetch data from the table: "locations" using primary key columns */
  locations_by_pk?: Maybe<Locations>
  /** fetch data from the table: "maker_types" */
  maker_types: Array<Maker_Types>
  /** fetch aggregated fields from the table: "maker_types" */
  maker_types_aggregate: Maker_Types_Aggregate
  /** fetch data from the table: "maker_types" using primary key columns */
  maker_types_by_pk?: Maybe<Maker_Types>
  /** fetch data from the table: "makers" */
  makers: Array<Makers>
  /** fetch aggregated fields from the table: "makers" */
  makers_aggregate: Makers_Aggregate
  /** fetch data from the table: "makers" using primary key columns */
  makers_by_pk?: Maybe<Makers>
  /** fetch data from the table: "media_file_types" */
  media_file_types: Array<Media_File_Types>
  /** fetch aggregated fields from the table: "media_file_types" */
  media_file_types_aggregate: Media_File_Types_Aggregate
  /** fetch data from the table: "media_file_types" using primary key columns */
  media_file_types_by_pk?: Maybe<Media_File_Types>
  /** fetch data from the table: "media_files" */
  media_files: Array<Media_Files>
  /** fetch aggregated fields from the table: "media_files" */
  media_files_aggregate: Media_Files_Aggregate
  /** fetch data from the table: "media_files" using primary key columns */
  media_files_by_pk?: Maybe<Media_Files>
  /** fetch data from the table: "menu_items" */
  menu_items: Array<Menu_Items>
  /** fetch aggregated fields from the table: "menu_items" */
  menu_items_aggregate: Menu_Items_Aggregate
  /** fetch data from the table: "menu_items" using primary key columns */
  menu_items_by_pk?: Maybe<Menu_Items>
  /** fetch data from the table: "menu_pages" */
  menu_pages: Array<Menu_Pages>
  /** fetch aggregated fields from the table: "menu_pages" */
  menu_pages_aggregate: Menu_Pages_Aggregate
  /** fetch data from the table: "menu_pages" using primary key columns */
  menu_pages_by_pk?: Maybe<Menu_Pages>
  /** fetch data from the table: "menu_type" */
  menu_type: Array<Menu_Type>
  /** fetch aggregated fields from the table: "menu_type" */
  menu_type_aggregate: Menu_Type_Aggregate
  /** fetch data from the table: "menu_type" using primary key columns */
  menu_type_by_pk?: Maybe<Menu_Type>
  /** fetch data from the table: "messages" */
  messages: Array<Messages>
  /** fetch aggregated fields from the table: "messages" */
  messages_aggregate: Messages_Aggregate
  /** fetch data from the table: "messages" using primary key columns */
  messages_by_pk?: Maybe<Messages>
  /** fetch data from the table: "messages_events_updated" */
  messages_events_updated: Array<Messages_Events_Updated>
  /** fetch aggregated fields from the table: "messages_events_updated" */
  messages_events_updated_aggregate: Messages_Events_Updated_Aggregate
  /** fetch data from the table: "messages_events_updated" using primary key columns */
  messages_events_updated_by_pk?: Maybe<Messages_Events_Updated>
  /** fetch data from the table: "messages_statuses" */
  messages_statuses: Array<Messages_Statuses>
  /** fetch aggregated fields from the table: "messages_statuses" */
  messages_statuses_aggregate: Messages_Statuses_Aggregate
  /** fetch data from the table: "messages_statuses" using primary key columns */
  messages_statuses_by_pk?: Maybe<Messages_Statuses>
  /** fetch data from the table: "messages_types" */
  messages_types: Array<Messages_Types>
  /** fetch aggregated fields from the table: "messages_types" */
  messages_types_aggregate: Messages_Types_Aggregate
  /** fetch data from the table: "messages_types" using primary key columns */
  messages_types_by_pk?: Maybe<Messages_Types>
  /** fetch data from the table: "network_transactions" */
  network_transactions: Array<Network_Transactions>
  /** fetch aggregated fields from the table: "network_transactions" */
  network_transactions_aggregate: Network_Transactions_Aggregate
  /** fetch data from the table: "network_transactions" using primary key columns */
  network_transactions_by_pk?: Maybe<Network_Transactions>
  /** fetch data from the table: "new_messages_view" */
  new_messages_view: Array<New_Messages_View>
  /** fetch aggregated fields from the table: "new_messages_view" */
  new_messages_view_aggregate: New_Messages_View_Aggregate
  /** fetch data from the table: "pages" */
  pages: Array<Pages>
  /** fetch aggregated fields from the table: "pages" */
  pages_aggregate: Pages_Aggregate
  /** fetch data from the table: "pages" using primary key columns */
  pages_by_pk?: Maybe<Pages>
  /** fetch data from the table: "pages_contents" */
  pages_contents: Array<Pages_Contents>
  /** fetch aggregated fields from the table: "pages_contents" */
  pages_contents_aggregate: Pages_Contents_Aggregate
  /** fetch data from the table: "pages_contents_blocks" */
  pages_contents_blocks: Array<Pages_Contents_Blocks>
  /** fetch aggregated fields from the table: "pages_contents_blocks" */
  pages_contents_blocks_aggregate: Pages_Contents_Blocks_Aggregate
  /** fetch data from the table: "pages_contents_blocks" using primary key columns */
  pages_contents_blocks_by_pk?: Maybe<Pages_Contents_Blocks>
  /** fetch data from the table: "pages_contents" using primary key columns */
  pages_contents_by_pk?: Maybe<Pages_Contents>
  /** fetch data from the table: "payment_types" */
  payment_types: Array<Payment_Types>
  /** fetch aggregated fields from the table: "payment_types" */
  payment_types_aggregate: Payment_Types_Aggregate
  /** fetch data from the table: "payment_types" using primary key columns */
  payment_types_by_pk?: Maybe<Payment_Types>
  /** fetch data from the table: "permissions" */
  permissions: Array<Permissions>
  /** fetch aggregated fields from the table: "permissions" */
  permissions_aggregate: Permissions_Aggregate
  /** fetch data from the table: "permissions" using primary key columns */
  permissions_by_pk?: Maybe<Permissions>
  /** fetch data from the table: "purchased" */
  purchased: Array<Purchased>
  /** fetch aggregated fields from the table: "purchased" */
  purchased_aggregate: Purchased_Aggregate
  /** fetch data from the table: "purchased" using primary key columns */
  purchased_by_pk?: Maybe<Purchased>
  /** fetch data from the table: "reviews" */
  reviews: Array<Reviews>
  /** fetch aggregated fields from the table: "reviews" */
  reviews_aggregate: Reviews_Aggregate
  /** fetch data from the table: "reviews" using primary key columns */
  reviews_by_pk?: Maybe<Reviews>
  /** execute function "search_makers" which returns "makers" */
  search_makers: Array<Makers>
  /** execute function "search_makers" and query aggregates on result of table type "makers" */
  search_makers_aggregate: Makers_Aggregate
  /** execute function "search_photographers" which returns "users" */
  search_photographers: Array<Users>
  /** execute function "search_photographers" and query aggregates on result of table type "users" */
  search_photographers_aggregate: Users_Aggregate
  /** execute function "search_photographers_by_location" which returns "users" */
  search_photographers_by_location: Array<Users>
  /** execute function "search_photographers_by_location" and query aggregates on result of table type "users" */
  search_photographers_by_location_aggregate: Users_Aggregate
  /** execute function "search_photographers_by_location_box" which returns "users" */
  search_photographers_by_location_box: Array<Users>
  /** execute function "search_photographers_by_location_box" and query aggregates on result of table type "users" */
  search_photographers_by_location_box_aggregate: Users_Aggregate
  /** execute function "search_styles" which returns "styles" */
  search_styles: Array<Styles>
  /** execute function "search_styles" and query aggregates on result of table type "styles" */
  search_styles_aggregate: Styles_Aggregate
  /** fetch data from the table: "seo" */
  seo: Array<Seo>
  /** fetch aggregated fields from the table: "seo" */
  seo_aggregate: Seo_Aggregate
  /** fetch data from the table: "seo" using primary key columns */
  seo_by_pk?: Maybe<Seo>
  /** fetch data from the table: "spatial_ref_sys" */
  spatial_ref_sys: Array<Spatial_Ref_Sys>
  /** fetch aggregated fields from the table: "spatial_ref_sys" */
  spatial_ref_sys_aggregate: Spatial_Ref_Sys_Aggregate
  /** fetch data from the table: "spatial_ref_sys" using primary key columns */
  spatial_ref_sys_by_pk?: Maybe<Spatial_Ref_Sys>
  /** fetch data from the table: "storages" */
  storages: Array<Storages>
  /** fetch aggregated fields from the table: "storages" */
  storages_aggregate: Storages_Aggregate
  /** fetch data from the table: "storages" using primary key columns */
  storages_by_pk?: Maybe<Storages>
  /** fetch data from the table: "storages_view" */
  storages_view: Array<Storages_View>
  /** fetch aggregated fields from the table: "storages_view" */
  storages_view_aggregate: Storages_View_Aggregate
  /** fetch data from the table: "store" */
  store: Array<Store>
  /** fetch aggregated fields from the table: "store" */
  store_aggregate: Store_Aggregate
  /** fetch data from the table: "store" using primary key columns */
  store_by_pk?: Maybe<Store>
  /** fetch data from the table: "store_categories" */
  store_categories: Array<Store_Categories>
  /** fetch aggregated fields from the table: "store_categories" */
  store_categories_aggregate: Store_Categories_Aggregate
  /** fetch data from the table: "store_categories" using primary key columns */
  store_categories_by_pk?: Maybe<Store_Categories>
  /** fetch data from the table: "store_types" */
  store_types: Array<Store_Types>
  /** fetch aggregated fields from the table: "store_types" */
  store_types_aggregate: Store_Types_Aggregate
  /** fetch data from the table: "store_types" using primary key columns */
  store_types_by_pk?: Maybe<Store_Types>
  /** fetch data from the table: "styles" */
  styles: Array<Styles>
  /** fetch aggregated fields from the table: "styles" */
  styles_aggregate: Styles_Aggregate
  /** fetch data from the table: "styles" using primary key columns */
  styles_by_pk?: Maybe<Styles>
  /** fetch data from the table: "system_messages" */
  system_messages: Array<System_Messages>
  /** fetch aggregated fields from the table: "system_messages" */
  system_messages_aggregate: System_Messages_Aggregate
  /** fetch data from the table: "system_messages" using primary key columns */
  system_messages_by_pk?: Maybe<System_Messages>
  /** fetch data from the table: "tags" */
  tags: Array<Tags>
  /** fetch aggregated fields from the table: "tags" */
  tags_aggregate: Tags_Aggregate
  /** fetch data from the table: "tags" using primary key columns */
  tags_by_pk?: Maybe<Tags>
  /** fetch data from the table: "tariffs" */
  tariffs: Array<Tariffs>
  /** fetch aggregated fields from the table: "tariffs" */
  tariffs_aggregate: Tariffs_Aggregate
  /** fetch data from the table: "tariffs" using primary key columns */
  tariffs_by_pk?: Maybe<Tariffs>
  /** fetch data from the table: "transaction_status" */
  transaction_status: Array<Transaction_Status>
  /** fetch aggregated fields from the table: "transaction_status" */
  transaction_status_aggregate: Transaction_Status_Aggregate
  /** fetch data from the table: "transaction_status" using primary key columns */
  transaction_status_by_pk?: Maybe<Transaction_Status>
  /** fetch data from the table: "transaction_type" */
  transaction_type: Array<Transaction_Type>
  /** fetch aggregated fields from the table: "transaction_type" */
  transaction_type_aggregate: Transaction_Type_Aggregate
  /** fetch data from the table: "transaction_type" using primary key columns */
  transaction_type_by_pk?: Maybe<Transaction_Type>
  /** fetch data from the table: "transactions" */
  transactions: Array<Transactions>
  /** fetch aggregated fields from the table: "transactions" */
  transactions_aggregate: Transactions_Aggregate
  /** fetch data from the table: "transactions" using primary key columns */
  transactions_by_pk?: Maybe<Transactions>
  /** fetch data from the table: "users" */
  users: Array<Users>
  /** fetch aggregated fields from the table: "users" */
  users_aggregate: Users_Aggregate
  /** fetch data from the table: "users" using primary key columns */
  users_by_pk?: Maybe<Users>
  /** fetch data from the table: "users_maker_types" */
  users_maker_types: Array<Users_Maker_Types>
  /** fetch aggregated fields from the table: "users_maker_types" */
  users_maker_types_aggregate: Users_Maker_Types_Aggregate
  /** fetch data from the table: "users_maker_types" using primary key columns */
  users_maker_types_by_pk?: Maybe<Users_Maker_Types>
  /** fetch data from the table: "users_makers" */
  users_makers: Array<Users_Makers>
  /** fetch aggregated fields from the table: "users_makers" */
  users_makers_aggregate: Users_Makers_Aggregate
  /** fetch data from the table: "users_makers" using primary key columns */
  users_makers_by_pk?: Maybe<Users_Makers>
  /** fetch data from the table: "users_payment_types" */
  users_payment_types: Array<Users_Payment_Types>
  /** fetch aggregated fields from the table: "users_payment_types" */
  users_payment_types_aggregate: Users_Payment_Types_Aggregate
  /** fetch data from the table: "users_payment_types" using primary key columns */
  users_payment_types_by_pk?: Maybe<Users_Payment_Types>
  /** fetch data from the table: "users_permissions" */
  users_permissions: Array<Users_Permissions>
  /** fetch aggregated fields from the table: "users_permissions" */
  users_permissions_aggregate: Users_Permissions_Aggregate
  /** fetch data from the table: "users_permissions" using primary key columns */
  users_permissions_by_pk?: Maybe<Users_Permissions>
  /** fetch data from the table: "users_search_media_files" */
  users_search_media_files: Array<Users_Search_Media_Files>
  /** fetch aggregated fields from the table: "users_search_media_files" */
  users_search_media_files_aggregate: Users_Search_Media_Files_Aggregate
  /** fetch data from the table: "users_search_media_files" using primary key columns */
  users_search_media_files_by_pk?: Maybe<Users_Search_Media_Files>
  /** fetch data from the table: "users_stats" */
  users_stats: Array<Users_Stats>
  /** fetch aggregated fields from the table: "users_stats" */
  users_stats_aggregate: Users_Stats_Aggregate
  /** fetch data from the table: "users_stats" using primary key columns */
  users_stats_by_pk?: Maybe<Users_Stats>
  /** fetch data from the table: "users_styles" */
  users_styles: Array<Users_Styles>
  /** fetch aggregated fields from the table: "users_styles" */
  users_styles_aggregate: Users_Styles_Aggregate
  /** fetch data from the table: "users_styles" using primary key columns */
  users_styles_by_pk?: Maybe<Users_Styles>
  /** fetch data from the table: "users_tariffs" */
  users_tariffs: Array<Users_Tariffs>
  /** fetch aggregated fields from the table: "users_tariffs" */
  users_tariffs_aggregate: Users_Tariffs_Aggregate
  /** fetch data from the table: "users_tariffs" using primary key columns */
  users_tariffs_by_pk?: Maybe<Users_Tariffs>
  /** fetch data from the table: "wallets" */
  wallets: Array<Wallets>
  /** fetch aggregated fields from the table: "wallets" */
  wallets_aggregate: Wallets_Aggregate
  /** fetch data from the table: "wallets" using primary key columns */
  wallets_by_pk?: Maybe<Wallets>
}

/** subscription root */
export type Subscription_RootAlbumsArgs = {
  distinct_on?: Maybe<Array<Albums_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Order_By>>
  where?: Maybe<Albums_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAlbums_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Order_By>>
  where?: Maybe<Albums_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAlbums_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootAlbums_Payment_TypesArgs = {
  distinct_on?: Maybe<Array<Albums_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Payment_Types_Order_By>>
  where?: Maybe<Albums_Payment_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAlbums_Payment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Payment_Types_Order_By>>
  where?: Maybe<Albums_Payment_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAlbums_Payment_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootAlbums_Statistics_ViewArgs = {
  distinct_on?: Maybe<Array<Albums_Statistics_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Statistics_View_Order_By>>
  where?: Maybe<Albums_Statistics_View_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAlbums_Statistics_View_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Statistics_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Statistics_View_Order_By>>
  where?: Maybe<Albums_Statistics_View_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAlbums_TagsArgs = {
  distinct_on?: Maybe<Array<Albums_Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Tags_Order_By>>
  where?: Maybe<Albums_Tags_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAlbums_Tags_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Tags_Order_By>>
  where?: Maybe<Albums_Tags_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAlbums_Tags_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootAlbums_VisitsArgs = {
  distinct_on?: Maybe<Array<Albums_Visits_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Visits_Order_By>>
  where?: Maybe<Albums_Visits_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAlbums_Visits_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Visits_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Visits_Order_By>>
  where?: Maybe<Albums_Visits_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAlbums_Visits_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootAuth_Account_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>
  where?: Maybe<Auth_Account_Providers_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Account_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Providers_Order_By>>
  where?: Maybe<Auth_Account_Providers_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Account_Providers_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootAuth_Account_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>
  where?: Maybe<Auth_Account_Roles_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Account_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Account_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Account_Roles_Order_By>>
  where?: Maybe<Auth_Account_Roles_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Account_Roles_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootAuth_AccountsArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>
  where?: Maybe<Auth_Accounts_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Accounts_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Accounts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Accounts_Order_By>>
  where?: Maybe<Auth_Accounts_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Accounts_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootAuth_ProvidersArgs = {
  distinct_on?: Maybe<Array<Auth_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Providers_Order_By>>
  where?: Maybe<Auth_Providers_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Providers_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Providers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Providers_Order_By>>
  where?: Maybe<Auth_Providers_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Providers_By_PkArgs = {
  provider: Scalars['String']
}

/** subscription root */
export type Subscription_RootAuth_Refresh_TokensArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Refresh_Tokens_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Refresh_Tokens_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Refresh_Tokens_Order_By>>
  where?: Maybe<Auth_Refresh_Tokens_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Refresh_Tokens_By_PkArgs = {
  refresh_token: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootAuth_RolesArgs = {
  distinct_on?: Maybe<Array<Auth_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Roles_Order_By>>
  where?: Maybe<Auth_Roles_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Roles_AggregateArgs = {
  distinct_on?: Maybe<Array<Auth_Roles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Auth_Roles_Order_By>>
  where?: Maybe<Auth_Roles_Bool_Exp>
}

/** subscription root */
export type Subscription_RootAuth_Roles_By_PkArgs = {
  role: Scalars['String']
}

/** subscription root */
export type Subscription_RootConfigArgs = {
  distinct_on?: Maybe<Array<Config_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Config_Order_By>>
  where?: Maybe<Config_Bool_Exp>
}

/** subscription root */
export type Subscription_RootConfig_AggregateArgs = {
  distinct_on?: Maybe<Array<Config_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Config_Order_By>>
  where?: Maybe<Config_Bool_Exp>
}

/** subscription root */
export type Subscription_RootConfig_By_PkArgs = {
  key: Config_Keys_Enum
}

/** subscription root */
export type Subscription_RootConfig_KeysArgs = {
  distinct_on?: Maybe<Array<Config_Keys_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Config_Keys_Order_By>>
  where?: Maybe<Config_Keys_Bool_Exp>
}

/** subscription root */
export type Subscription_RootConfig_Keys_AggregateArgs = {
  distinct_on?: Maybe<Array<Config_Keys_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Config_Keys_Order_By>>
  where?: Maybe<Config_Keys_Bool_Exp>
}

/** subscription root */
export type Subscription_RootConfig_Keys_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootContactsArgs = {
  distinct_on?: Maybe<Array<Contacts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Contacts_Order_By>>
  where?: Maybe<Contacts_Bool_Exp>
}

/** subscription root */
export type Subscription_RootContacts_AggregateArgs = {
  distinct_on?: Maybe<Array<Contacts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Contacts_Order_By>>
  where?: Maybe<Contacts_Bool_Exp>
}

/** subscription root */
export type Subscription_RootContacts_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootCover_ImagesArgs = {
  distinct_on?: Maybe<Array<Cover_Images_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Cover_Images_Order_By>>
  where?: Maybe<Cover_Images_Bool_Exp>
}

/** subscription root */
export type Subscription_RootCover_Images_AggregateArgs = {
  distinct_on?: Maybe<Array<Cover_Images_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Cover_Images_Order_By>>
  where?: Maybe<Cover_Images_Bool_Exp>
}

/** subscription root */
export type Subscription_RootCover_Images_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootCredit_CardsArgs = {
  distinct_on?: Maybe<Array<Credit_Cards_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Credit_Cards_Order_By>>
  where?: Maybe<Credit_Cards_Bool_Exp>
}

/** subscription root */
export type Subscription_RootCredit_Cards_AggregateArgs = {
  distinct_on?: Maybe<Array<Credit_Cards_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Credit_Cards_Order_By>>
  where?: Maybe<Credit_Cards_Bool_Exp>
}

/** subscription root */
export type Subscription_RootCredit_Cards_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootDownloadsArgs = {
  distinct_on?: Maybe<Array<Downloads_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Downloads_Order_By>>
  where?: Maybe<Downloads_Bool_Exp>
}

/** subscription root */
export type Subscription_RootDownloads_AggregateArgs = {
  distinct_on?: Maybe<Array<Downloads_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Downloads_Order_By>>
  where?: Maybe<Downloads_Bool_Exp>
}

/** subscription root */
export type Subscription_RootDownloads_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootEventsArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** subscription root */
export type Subscription_RootEvents_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** subscription root */
export type Subscription_RootEvents_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootEvents_Payment_TypesArgs = {
  distinct_on?: Maybe<Array<Events_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Payment_Types_Order_By>>
  where?: Maybe<Events_Payment_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootEvents_Payment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Payment_Types_Order_By>>
  where?: Maybe<Events_Payment_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootEvents_Payment_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootEvents_StatusesArgs = {
  distinct_on?: Maybe<Array<Events_Statuses_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Statuses_Order_By>>
  where?: Maybe<Events_Statuses_Bool_Exp>
}

/** subscription root */
export type Subscription_RootEvents_Statuses_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Statuses_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Statuses_Order_By>>
  where?: Maybe<Events_Statuses_Bool_Exp>
}

/** subscription root */
export type Subscription_RootEvents_Statuses_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootFavoritesArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** subscription root */
export type Subscription_RootFavorites_AggregateArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** subscription root */
export type Subscription_RootFavorites_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootFoldersArgs = {
  distinct_on?: Maybe<Array<Folders_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Folders_Order_By>>
  where?: Maybe<Folders_Bool_Exp>
}

/** subscription root */
export type Subscription_RootFolders_AggregateArgs = {
  distinct_on?: Maybe<Array<Folders_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Folders_Order_By>>
  where?: Maybe<Folders_Bool_Exp>
}

/** subscription root */
export type Subscription_RootFolders_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootGeography_ColumnsArgs = {
  distinct_on?: Maybe<Array<Geography_Columns_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Geography_Columns_Order_By>>
  where?: Maybe<Geography_Columns_Bool_Exp>
}

/** subscription root */
export type Subscription_RootGeography_Columns_AggregateArgs = {
  distinct_on?: Maybe<Array<Geography_Columns_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Geography_Columns_Order_By>>
  where?: Maybe<Geography_Columns_Bool_Exp>
}

/** subscription root */
export type Subscription_RootGeometry_ColumnsArgs = {
  distinct_on?: Maybe<Array<Geometry_Columns_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Geometry_Columns_Order_By>>
  where?: Maybe<Geometry_Columns_Bool_Exp>
}

/** subscription root */
export type Subscription_RootGeometry_Columns_AggregateArgs = {
  distinct_on?: Maybe<Array<Geometry_Columns_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Geometry_Columns_Order_By>>
  where?: Maybe<Geometry_Columns_Bool_Exp>
}

/** subscription root */
export type Subscription_RootLocalesArgs = {
  distinct_on?: Maybe<Array<Locales_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locales_Order_By>>
  where?: Maybe<Locales_Bool_Exp>
}

/** subscription root */
export type Subscription_RootLocales_AggregateArgs = {
  distinct_on?: Maybe<Array<Locales_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locales_Order_By>>
  where?: Maybe<Locales_Bool_Exp>
}

/** subscription root */
export type Subscription_RootLocales_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootLocalizationsArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** subscription root */
export type Subscription_RootLocalizations_AggregateArgs = {
  distinct_on?: Maybe<Array<Localizations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Localizations_Order_By>>
  where?: Maybe<Localizations_Bool_Exp>
}

/** subscription root */
export type Subscription_RootLocalizations_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootLocationsArgs = {
  distinct_on?: Maybe<Array<Locations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locations_Order_By>>
  where?: Maybe<Locations_Bool_Exp>
}

/** subscription root */
export type Subscription_RootLocations_AggregateArgs = {
  distinct_on?: Maybe<Array<Locations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locations_Order_By>>
  where?: Maybe<Locations_Bool_Exp>
}

/** subscription root */
export type Subscription_RootLocations_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootMaker_TypesArgs = {
  distinct_on?: Maybe<Array<Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Maker_Types_Order_By>>
  where?: Maybe<Maker_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMaker_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Maker_Types_Order_By>>
  where?: Maybe<Maker_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMaker_Types_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootMakersArgs = {
  distinct_on?: Maybe<Array<Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Makers_Order_By>>
  where?: Maybe<Makers_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMakers_AggregateArgs = {
  distinct_on?: Maybe<Array<Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Makers_Order_By>>
  where?: Maybe<Makers_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMakers_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootMedia_File_TypesArgs = {
  distinct_on?: Maybe<Array<Media_File_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_File_Types_Order_By>>
  where?: Maybe<Media_File_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMedia_File_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Media_File_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_File_Types_Order_By>>
  where?: Maybe<Media_File_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMedia_File_Types_By_PkArgs = {
  id: Scalars['String']
}

/** subscription root */
export type Subscription_RootMedia_FilesArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMedia_Files_AggregateArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMedia_Files_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootMenu_ItemsArgs = {
  distinct_on?: Maybe<Array<Menu_Items_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Items_Order_By>>
  where?: Maybe<Menu_Items_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMenu_Items_AggregateArgs = {
  distinct_on?: Maybe<Array<Menu_Items_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Items_Order_By>>
  where?: Maybe<Menu_Items_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMenu_Items_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootMenu_PagesArgs = {
  distinct_on?: Maybe<Array<Menu_Pages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Pages_Order_By>>
  where?: Maybe<Menu_Pages_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMenu_Pages_AggregateArgs = {
  distinct_on?: Maybe<Array<Menu_Pages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Pages_Order_By>>
  where?: Maybe<Menu_Pages_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMenu_Pages_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootMenu_TypeArgs = {
  distinct_on?: Maybe<Array<Menu_Type_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Type_Order_By>>
  where?: Maybe<Menu_Type_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMenu_Type_AggregateArgs = {
  distinct_on?: Maybe<Array<Menu_Type_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Menu_Type_Order_By>>
  where?: Maybe<Menu_Type_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMenu_Type_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootMessagesArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMessages_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMessages_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootMessages_Events_UpdatedArgs = {
  distinct_on?: Maybe<Array<Messages_Events_Updated_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Events_Updated_Order_By>>
  where?: Maybe<Messages_Events_Updated_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMessages_Events_Updated_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Events_Updated_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Events_Updated_Order_By>>
  where?: Maybe<Messages_Events_Updated_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMessages_Events_Updated_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootMessages_StatusesArgs = {
  distinct_on?: Maybe<Array<Messages_Statuses_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Statuses_Order_By>>
  where?: Maybe<Messages_Statuses_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMessages_Statuses_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Statuses_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Statuses_Order_By>>
  where?: Maybe<Messages_Statuses_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMessages_Statuses_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootMessages_TypesArgs = {
  distinct_on?: Maybe<Array<Messages_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Types_Order_By>>
  where?: Maybe<Messages_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMessages_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Types_Order_By>>
  where?: Maybe<Messages_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootMessages_Types_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootNetwork_TransactionsArgs = {
  distinct_on?: Maybe<Array<Network_Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Network_Transactions_Order_By>>
  where?: Maybe<Network_Transactions_Bool_Exp>
}

/** subscription root */
export type Subscription_RootNetwork_Transactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Network_Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Network_Transactions_Order_By>>
  where?: Maybe<Network_Transactions_Bool_Exp>
}

/** subscription root */
export type Subscription_RootNetwork_Transactions_By_PkArgs = {
  network: Scalars['String']
  tx_hash: Scalars['String']
}

/** subscription root */
export type Subscription_RootNew_Messages_ViewArgs = {
  distinct_on?: Maybe<Array<New_Messages_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<New_Messages_View_Order_By>>
  where?: Maybe<New_Messages_View_Bool_Exp>
}

/** subscription root */
export type Subscription_RootNew_Messages_View_AggregateArgs = {
  distinct_on?: Maybe<Array<New_Messages_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<New_Messages_View_Order_By>>
  where?: Maybe<New_Messages_View_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPagesArgs = {
  distinct_on?: Maybe<Array<Pages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Order_By>>
  where?: Maybe<Pages_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPages_AggregateArgs = {
  distinct_on?: Maybe<Array<Pages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Order_By>>
  where?: Maybe<Pages_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPages_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootPages_ContentsArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Order_By>>
  where?: Maybe<Pages_Contents_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPages_Contents_AggregateArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Order_By>>
  where?: Maybe<Pages_Contents_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPages_Contents_BlocksArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Blocks_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Blocks_Order_By>>
  where?: Maybe<Pages_Contents_Blocks_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPages_Contents_Blocks_AggregateArgs = {
  distinct_on?: Maybe<Array<Pages_Contents_Blocks_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Pages_Contents_Blocks_Order_By>>
  where?: Maybe<Pages_Contents_Blocks_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPages_Contents_Blocks_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootPages_Contents_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootPayment_TypesArgs = {
  distinct_on?: Maybe<Array<Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Payment_Types_Order_By>>
  where?: Maybe<Payment_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPayment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Payment_Types_Order_By>>
  where?: Maybe<Payment_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPayment_Types_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootPermissionsArgs = {
  distinct_on?: Maybe<Array<Permissions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Permissions_Order_By>>
  where?: Maybe<Permissions_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPermissions_AggregateArgs = {
  distinct_on?: Maybe<Array<Permissions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Permissions_Order_By>>
  where?: Maybe<Permissions_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPermissions_By_PkArgs = {
  id: Scalars['String']
}

/** subscription root */
export type Subscription_RootPurchasedArgs = {
  distinct_on?: Maybe<Array<Purchased_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Purchased_Order_By>>
  where?: Maybe<Purchased_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPurchased_AggregateArgs = {
  distinct_on?: Maybe<Array<Purchased_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Purchased_Order_By>>
  where?: Maybe<Purchased_Bool_Exp>
}

/** subscription root */
export type Subscription_RootPurchased_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootReviewsArgs = {
  distinct_on?: Maybe<Array<Reviews_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Reviews_Order_By>>
  where?: Maybe<Reviews_Bool_Exp>
}

/** subscription root */
export type Subscription_RootReviews_AggregateArgs = {
  distinct_on?: Maybe<Array<Reviews_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Reviews_Order_By>>
  where?: Maybe<Reviews_Bool_Exp>
}

/** subscription root */
export type Subscription_RootReviews_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootSearch_MakersArgs = {
  args: Search_Makers_Args
  distinct_on?: Maybe<Array<Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Makers_Order_By>>
  where?: Maybe<Makers_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSearch_Makers_AggregateArgs = {
  args: Search_Makers_Args
  distinct_on?: Maybe<Array<Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Makers_Order_By>>
  where?: Maybe<Makers_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSearch_PhotographersArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSearch_Photographers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSearch_Photographers_By_LocationArgs = {
  args: Search_Photographers_By_Location_Args
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSearch_Photographers_By_Location_AggregateArgs = {
  args: Search_Photographers_By_Location_Args
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSearch_Photographers_By_Location_BoxArgs = {
  args: Search_Photographers_By_Location_Box_Args
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSearch_Photographers_By_Location_Box_AggregateArgs = {
  args: Search_Photographers_By_Location_Box_Args
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSearch_StylesArgs = {
  args: Search_Styles_Args
  distinct_on?: Maybe<Array<Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Styles_Order_By>>
  where?: Maybe<Styles_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSearch_Styles_AggregateArgs = {
  args: Search_Styles_Args
  distinct_on?: Maybe<Array<Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Styles_Order_By>>
  where?: Maybe<Styles_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSeoArgs = {
  distinct_on?: Maybe<Array<Seo_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Seo_Order_By>>
  where?: Maybe<Seo_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSeo_AggregateArgs = {
  distinct_on?: Maybe<Array<Seo_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Seo_Order_By>>
  where?: Maybe<Seo_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSeo_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootSpatial_Ref_SysArgs = {
  distinct_on?: Maybe<Array<Spatial_Ref_Sys_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Spatial_Ref_Sys_Order_By>>
  where?: Maybe<Spatial_Ref_Sys_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSpatial_Ref_Sys_AggregateArgs = {
  distinct_on?: Maybe<Array<Spatial_Ref_Sys_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Spatial_Ref_Sys_Order_By>>
  where?: Maybe<Spatial_Ref_Sys_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSpatial_Ref_Sys_By_PkArgs = {
  srid: Scalars['Int']
}

/** subscription root */
export type Subscription_RootStoragesArgs = {
  distinct_on?: Maybe<Array<Storages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Storages_Order_By>>
  where?: Maybe<Storages_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStorages_AggregateArgs = {
  distinct_on?: Maybe<Array<Storages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Storages_Order_By>>
  where?: Maybe<Storages_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStorages_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootStorages_ViewArgs = {
  distinct_on?: Maybe<Array<Storages_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Storages_View_Order_By>>
  where?: Maybe<Storages_View_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStorages_View_AggregateArgs = {
  distinct_on?: Maybe<Array<Storages_View_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Storages_View_Order_By>>
  where?: Maybe<Storages_View_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStoreArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStore_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStore_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootStore_CategoriesArgs = {
  distinct_on?: Maybe<Array<Store_Categories_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Categories_Order_By>>
  where?: Maybe<Store_Categories_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStore_Categories_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Categories_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Categories_Order_By>>
  where?: Maybe<Store_Categories_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStore_Categories_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootStore_TypesArgs = {
  distinct_on?: Maybe<Array<Store_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Types_Order_By>>
  where?: Maybe<Store_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStore_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Types_Order_By>>
  where?: Maybe<Store_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStore_Types_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootStylesArgs = {
  distinct_on?: Maybe<Array<Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Styles_Order_By>>
  where?: Maybe<Styles_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStyles_AggregateArgs = {
  distinct_on?: Maybe<Array<Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Styles_Order_By>>
  where?: Maybe<Styles_Bool_Exp>
}

/** subscription root */
export type Subscription_RootStyles_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootSystem_MessagesArgs = {
  distinct_on?: Maybe<Array<System_Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<System_Messages_Order_By>>
  where?: Maybe<System_Messages_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSystem_Messages_AggregateArgs = {
  distinct_on?: Maybe<Array<System_Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<System_Messages_Order_By>>
  where?: Maybe<System_Messages_Bool_Exp>
}

/** subscription root */
export type Subscription_RootSystem_Messages_By_PkArgs = {
  id: Scalars['String']
}

/** subscription root */
export type Subscription_RootTagsArgs = {
  distinct_on?: Maybe<Array<Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Tags_Order_By>>
  where?: Maybe<Tags_Bool_Exp>
}

/** subscription root */
export type Subscription_RootTags_AggregateArgs = {
  distinct_on?: Maybe<Array<Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Tags_Order_By>>
  where?: Maybe<Tags_Bool_Exp>
}

/** subscription root */
export type Subscription_RootTags_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootTariffsArgs = {
  distinct_on?: Maybe<Array<Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Tariffs_Order_By>>
  where?: Maybe<Tariffs_Bool_Exp>
}

/** subscription root */
export type Subscription_RootTariffs_AggregateArgs = {
  distinct_on?: Maybe<Array<Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Tariffs_Order_By>>
  where?: Maybe<Tariffs_Bool_Exp>
}

/** subscription root */
export type Subscription_RootTariffs_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootTransaction_StatusArgs = {
  distinct_on?: Maybe<Array<Transaction_Status_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transaction_Status_Order_By>>
  where?: Maybe<Transaction_Status_Bool_Exp>
}

/** subscription root */
export type Subscription_RootTransaction_Status_AggregateArgs = {
  distinct_on?: Maybe<Array<Transaction_Status_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transaction_Status_Order_By>>
  where?: Maybe<Transaction_Status_Bool_Exp>
}

/** subscription root */
export type Subscription_RootTransaction_Status_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootTransaction_TypeArgs = {
  distinct_on?: Maybe<Array<Transaction_Type_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transaction_Type_Order_By>>
  where?: Maybe<Transaction_Type_Bool_Exp>
}

/** subscription root */
export type Subscription_RootTransaction_Type_AggregateArgs = {
  distinct_on?: Maybe<Array<Transaction_Type_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transaction_Type_Order_By>>
  where?: Maybe<Transaction_Type_Bool_Exp>
}

/** subscription root */
export type Subscription_RootTransaction_Type_By_PkArgs = {
  value: Scalars['String']
}

/** subscription root */
export type Subscription_RootTransactionsArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** subscription root */
export type Subscription_RootTransactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** subscription root */
export type Subscription_RootTransactions_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootUsersArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Order_By>>
  where?: Maybe<Users_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootUsers_Maker_TypesArgs = {
  distinct_on?: Maybe<Array<Users_Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Maker_Types_Order_By>>
  where?: Maybe<Users_Maker_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Maker_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Maker_Types_Order_By>>
  where?: Maybe<Users_Maker_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Maker_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootUsers_MakersArgs = {
  distinct_on?: Maybe<Array<Users_Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Makers_Order_By>>
  where?: Maybe<Users_Makers_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Makers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Makers_Order_By>>
  where?: Maybe<Users_Makers_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Makers_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootUsers_Payment_TypesArgs = {
  distinct_on?: Maybe<Array<Users_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Payment_Types_Order_By>>
  where?: Maybe<Users_Payment_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Payment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Payment_Types_Order_By>>
  where?: Maybe<Users_Payment_Types_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Payment_Types_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootUsers_PermissionsArgs = {
  distinct_on?: Maybe<Array<Users_Permissions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Permissions_Order_By>>
  where?: Maybe<Users_Permissions_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Permissions_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Permissions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Permissions_Order_By>>
  where?: Maybe<Users_Permissions_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Permissions_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootUsers_Search_Media_FilesArgs = {
  distinct_on?: Maybe<Array<Users_Search_Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Search_Media_Files_Order_By>>
  where?: Maybe<Users_Search_Media_Files_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Search_Media_Files_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Search_Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Search_Media_Files_Order_By>>
  where?: Maybe<Users_Search_Media_Files_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Search_Media_Files_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootUsers_StatsArgs = {
  distinct_on?: Maybe<Array<Users_Stats_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Stats_Order_By>>
  where?: Maybe<Users_Stats_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Stats_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Stats_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Stats_Order_By>>
  where?: Maybe<Users_Stats_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Stats_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootUsers_StylesArgs = {
  distinct_on?: Maybe<Array<Users_Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Styles_Order_By>>
  where?: Maybe<Users_Styles_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Styles_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Styles_Order_By>>
  where?: Maybe<Users_Styles_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Styles_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootUsers_TariffsArgs = {
  distinct_on?: Maybe<Array<Users_Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Tariffs_Order_By>>
  where?: Maybe<Users_Tariffs_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Tariffs_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Tariffs_Order_By>>
  where?: Maybe<Users_Tariffs_Bool_Exp>
}

/** subscription root */
export type Subscription_RootUsers_Tariffs_By_PkArgs = {
  id: Scalars['uuid']
}

/** subscription root */
export type Subscription_RootWalletsArgs = {
  distinct_on?: Maybe<Array<Wallets_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Wallets_Order_By>>
  where?: Maybe<Wallets_Bool_Exp>
}

/** subscription root */
export type Subscription_RootWallets_AggregateArgs = {
  distinct_on?: Maybe<Array<Wallets_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Wallets_Order_By>>
  where?: Maybe<Wallets_Bool_Exp>
}

/** subscription root */
export type Subscription_RootWallets_By_PkArgs = {
  id: Scalars['uuid']
}

/** columns and relationships of "system_messages" */
export type System_Messages = {
  __typename?: 'system_messages'
  id: Scalars['String']
  locale: Locales_Enum
  /** An object relationship */
  localeByLocale: Locales
  message: Scalars['String']
}

/** aggregated selection of "system_messages" */
export type System_Messages_Aggregate = {
  __typename?: 'system_messages_aggregate'
  aggregate?: Maybe<System_Messages_Aggregate_Fields>
  nodes: Array<System_Messages>
}

/** aggregate fields of "system_messages" */
export type System_Messages_Aggregate_Fields = {
  __typename?: 'system_messages_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<System_Messages_Max_Fields>
  min?: Maybe<System_Messages_Min_Fields>
}

/** aggregate fields of "system_messages" */
export type System_Messages_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<System_Messages_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "system_messages" */
export type System_Messages_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<System_Messages_Max_Order_By>
  min?: Maybe<System_Messages_Min_Order_By>
}

/** input type for inserting array relation for remote table "system_messages" */
export type System_Messages_Arr_Rel_Insert_Input = {
  data: Array<System_Messages_Insert_Input>
  on_conflict?: Maybe<System_Messages_On_Conflict>
}

/** Boolean expression to filter rows from the table "system_messages". All fields are combined with a logical 'AND'. */
export type System_Messages_Bool_Exp = {
  _and?: Maybe<Array<Maybe<System_Messages_Bool_Exp>>>
  _not?: Maybe<System_Messages_Bool_Exp>
  _or?: Maybe<Array<Maybe<System_Messages_Bool_Exp>>>
  id?: Maybe<String_Comparison_Exp>
  locale?: Maybe<Locales_Enum_Comparison_Exp>
  localeByLocale?: Maybe<Locales_Bool_Exp>
  message?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "system_messages" */
export enum System_Messages_Constraint {
  /** unique or primary key constraint */
  SystemMessagesPkey = 'system_messages_pkey'
}

/** input type for inserting data into table "system_messages" */
export type System_Messages_Insert_Input = {
  id?: Maybe<Scalars['String']>
  locale?: Maybe<Locales_Enum>
  localeByLocale?: Maybe<Locales_Obj_Rel_Insert_Input>
  message?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type System_Messages_Max_Fields = {
  __typename?: 'system_messages_max_fields'
  id?: Maybe<Scalars['String']>
  message?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "system_messages" */
export type System_Messages_Max_Order_By = {
  id?: Maybe<Order_By>
  message?: Maybe<Order_By>
}

/** aggregate min on columns */
export type System_Messages_Min_Fields = {
  __typename?: 'system_messages_min_fields'
  id?: Maybe<Scalars['String']>
  message?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "system_messages" */
export type System_Messages_Min_Order_By = {
  id?: Maybe<Order_By>
  message?: Maybe<Order_By>
}

/** response of any mutation on the table "system_messages" */
export type System_Messages_Mutation_Response = {
  __typename?: 'system_messages_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<System_Messages>
}

/** input type for inserting object relation for remote table "system_messages" */
export type System_Messages_Obj_Rel_Insert_Input = {
  data: System_Messages_Insert_Input
  on_conflict?: Maybe<System_Messages_On_Conflict>
}

/** on conflict condition type for table "system_messages" */
export type System_Messages_On_Conflict = {
  constraint: System_Messages_Constraint
  update_columns: Array<System_Messages_Update_Column>
  where?: Maybe<System_Messages_Bool_Exp>
}

/** ordering options when selecting data from "system_messages" */
export type System_Messages_Order_By = {
  id?: Maybe<Order_By>
  locale?: Maybe<Order_By>
  localeByLocale?: Maybe<Locales_Order_By>
  message?: Maybe<Order_By>
}

/** primary key columns input for table: "system_messages" */
export type System_Messages_Pk_Columns_Input = {
  id: Scalars['String']
}

/** select columns of table "system_messages" */
export enum System_Messages_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Locale = 'locale',
  /** column name */
  Message = 'message'
}

/** input type for updating data in table "system_messages" */
export type System_Messages_Set_Input = {
  id?: Maybe<Scalars['String']>
  locale?: Maybe<Locales_Enum>
  message?: Maybe<Scalars['String']>
}

/** update columns of table "system_messages" */
export enum System_Messages_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Locale = 'locale',
  /** column name */
  Message = 'message'
}

/** columns and relationships of "tags" */
export type Tags = {
  __typename?: 'tags'
  /** An array relationship */
  albums_tags: Array<Albums_Tags>
  /** An aggregated array relationship */
  albums_tags_aggregate: Albums_Tags_Aggregate
  id: Scalars['uuid']
  moderated: Scalars['Boolean']
  tag: Scalars['String']
}

/** columns and relationships of "tags" */
export type TagsAlbums_TagsArgs = {
  distinct_on?: Maybe<Array<Albums_Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Tags_Order_By>>
  where?: Maybe<Albums_Tags_Bool_Exp>
}

/** columns and relationships of "tags" */
export type TagsAlbums_Tags_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Tags_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Tags_Order_By>>
  where?: Maybe<Albums_Tags_Bool_Exp>
}

/** aggregated selection of "tags" */
export type Tags_Aggregate = {
  __typename?: 'tags_aggregate'
  aggregate?: Maybe<Tags_Aggregate_Fields>
  nodes: Array<Tags>
}

/** aggregate fields of "tags" */
export type Tags_Aggregate_Fields = {
  __typename?: 'tags_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Tags_Max_Fields>
  min?: Maybe<Tags_Min_Fields>
}

/** aggregate fields of "tags" */
export type Tags_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Tags_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "tags" */
export type Tags_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Tags_Max_Order_By>
  min?: Maybe<Tags_Min_Order_By>
}

/** input type for inserting array relation for remote table "tags" */
export type Tags_Arr_Rel_Insert_Input = {
  data: Array<Tags_Insert_Input>
  on_conflict?: Maybe<Tags_On_Conflict>
}

/** Boolean expression to filter rows from the table "tags". All fields are combined with a logical 'AND'. */
export type Tags_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Tags_Bool_Exp>>>
  _not?: Maybe<Tags_Bool_Exp>
  _or?: Maybe<Array<Maybe<Tags_Bool_Exp>>>
  albums_tags?: Maybe<Albums_Tags_Bool_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  moderated?: Maybe<Boolean_Comparison_Exp>
  tag?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "tags" */
export enum Tags_Constraint {
  /** unique or primary key constraint */
  TagsPkey = 'tags_pkey',
  /** unique or primary key constraint */
  TagsTagKey = 'tags_tag_key'
}

/** input type for inserting data into table "tags" */
export type Tags_Insert_Input = {
  albums_tags?: Maybe<Albums_Tags_Arr_Rel_Insert_Input>
  id?: Maybe<Scalars['uuid']>
  moderated?: Maybe<Scalars['Boolean']>
  tag?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Tags_Max_Fields = {
  __typename?: 'tags_max_fields'
  id?: Maybe<Scalars['uuid']>
  tag?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "tags" */
export type Tags_Max_Order_By = {
  id?: Maybe<Order_By>
  tag?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Tags_Min_Fields = {
  __typename?: 'tags_min_fields'
  id?: Maybe<Scalars['uuid']>
  tag?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "tags" */
export type Tags_Min_Order_By = {
  id?: Maybe<Order_By>
  tag?: Maybe<Order_By>
}

/** response of any mutation on the table "tags" */
export type Tags_Mutation_Response = {
  __typename?: 'tags_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Tags>
}

/** input type for inserting object relation for remote table "tags" */
export type Tags_Obj_Rel_Insert_Input = {
  data: Tags_Insert_Input
  on_conflict?: Maybe<Tags_On_Conflict>
}

/** on conflict condition type for table "tags" */
export type Tags_On_Conflict = {
  constraint: Tags_Constraint
  update_columns: Array<Tags_Update_Column>
  where?: Maybe<Tags_Bool_Exp>
}

/** ordering options when selecting data from "tags" */
export type Tags_Order_By = {
  albums_tags_aggregate?: Maybe<Albums_Tags_Aggregate_Order_By>
  id?: Maybe<Order_By>
  moderated?: Maybe<Order_By>
  tag?: Maybe<Order_By>
}

/** primary key columns input for table: "tags" */
export type Tags_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "tags" */
export enum Tags_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Moderated = 'moderated',
  /** column name */
  Tag = 'tag'
}

/** input type for updating data in table "tags" */
export type Tags_Set_Input = {
  id?: Maybe<Scalars['uuid']>
  moderated?: Maybe<Scalars['Boolean']>
  tag?: Maybe<Scalars['String']>
}

/** update columns of table "tags" */
export enum Tags_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Moderated = 'moderated',
  /** column name */
  Tag = 'tag'
}

/** columns and relationships of "tariffs" */
export type Tariffs = {
  __typename?: 'tariffs'
  active: Scalars['Boolean']
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  name: Scalars['String']
  price: Scalars['numeric']
  space: Scalars['bigint']
  updated_at: Scalars['timestamptz']
  /** An array relationship */
  users_tariffs: Array<Users_Tariffs>
  /** An aggregated array relationship */
  users_tariffs_aggregate: Users_Tariffs_Aggregate
}

/** columns and relationships of "tariffs" */
export type TariffsUsers_TariffsArgs = {
  distinct_on?: Maybe<Array<Users_Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Tariffs_Order_By>>
  where?: Maybe<Users_Tariffs_Bool_Exp>
}

/** columns and relationships of "tariffs" */
export type TariffsUsers_Tariffs_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Tariffs_Order_By>>
  where?: Maybe<Users_Tariffs_Bool_Exp>
}

/** aggregated selection of "tariffs" */
export type Tariffs_Aggregate = {
  __typename?: 'tariffs_aggregate'
  aggregate?: Maybe<Tariffs_Aggregate_Fields>
  nodes: Array<Tariffs>
}

/** aggregate fields of "tariffs" */
export type Tariffs_Aggregate_Fields = {
  __typename?: 'tariffs_aggregate_fields'
  avg?: Maybe<Tariffs_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Tariffs_Max_Fields>
  min?: Maybe<Tariffs_Min_Fields>
  stddev?: Maybe<Tariffs_Stddev_Fields>
  stddev_pop?: Maybe<Tariffs_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Tariffs_Stddev_Samp_Fields>
  sum?: Maybe<Tariffs_Sum_Fields>
  var_pop?: Maybe<Tariffs_Var_Pop_Fields>
  var_samp?: Maybe<Tariffs_Var_Samp_Fields>
  variance?: Maybe<Tariffs_Variance_Fields>
}

/** aggregate fields of "tariffs" */
export type Tariffs_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Tariffs_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "tariffs" */
export type Tariffs_Aggregate_Order_By = {
  avg?: Maybe<Tariffs_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Tariffs_Max_Order_By>
  min?: Maybe<Tariffs_Min_Order_By>
  stddev?: Maybe<Tariffs_Stddev_Order_By>
  stddev_pop?: Maybe<Tariffs_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Tariffs_Stddev_Samp_Order_By>
  sum?: Maybe<Tariffs_Sum_Order_By>
  var_pop?: Maybe<Tariffs_Var_Pop_Order_By>
  var_samp?: Maybe<Tariffs_Var_Samp_Order_By>
  variance?: Maybe<Tariffs_Variance_Order_By>
}

/** input type for inserting array relation for remote table "tariffs" */
export type Tariffs_Arr_Rel_Insert_Input = {
  data: Array<Tariffs_Insert_Input>
  on_conflict?: Maybe<Tariffs_On_Conflict>
}

/** aggregate avg on columns */
export type Tariffs_Avg_Fields = {
  __typename?: 'tariffs_avg_fields'
  price?: Maybe<Scalars['Float']>
  space?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "tariffs" */
export type Tariffs_Avg_Order_By = {
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "tariffs". All fields are combined with a logical 'AND'. */
export type Tariffs_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Tariffs_Bool_Exp>>>
  _not?: Maybe<Tariffs_Bool_Exp>
  _or?: Maybe<Array<Maybe<Tariffs_Bool_Exp>>>
  active?: Maybe<Boolean_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  name?: Maybe<String_Comparison_Exp>
  price?: Maybe<Numeric_Comparison_Exp>
  space?: Maybe<Bigint_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  users_tariffs?: Maybe<Users_Tariffs_Bool_Exp>
}

/** unique or primary key constraints on table "tariffs" */
export enum Tariffs_Constraint {
  /** unique or primary key constraint */
  TariffsPkey = 'tariffs_pkey'
}

/** input type for incrementing integer column in table "tariffs" */
export type Tariffs_Inc_Input = {
  price?: Maybe<Scalars['numeric']>
  space?: Maybe<Scalars['bigint']>
}

/** input type for inserting data into table "tariffs" */
export type Tariffs_Insert_Input = {
  active?: Maybe<Scalars['Boolean']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  price?: Maybe<Scalars['numeric']>
  space?: Maybe<Scalars['bigint']>
  updated_at?: Maybe<Scalars['timestamptz']>
  users_tariffs?: Maybe<Users_Tariffs_Arr_Rel_Insert_Input>
}

/** aggregate max on columns */
export type Tariffs_Max_Fields = {
  __typename?: 'tariffs_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  price?: Maybe<Scalars['numeric']>
  space?: Maybe<Scalars['bigint']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "tariffs" */
export type Tariffs_Max_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Tariffs_Min_Fields = {
  __typename?: 'tariffs_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  price?: Maybe<Scalars['numeric']>
  space?: Maybe<Scalars['bigint']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "tariffs" */
export type Tariffs_Min_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "tariffs" */
export type Tariffs_Mutation_Response = {
  __typename?: 'tariffs_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Tariffs>
}

/** input type for inserting object relation for remote table "tariffs" */
export type Tariffs_Obj_Rel_Insert_Input = {
  data: Tariffs_Insert_Input
  on_conflict?: Maybe<Tariffs_On_Conflict>
}

/** on conflict condition type for table "tariffs" */
export type Tariffs_On_Conflict = {
  constraint: Tariffs_Constraint
  update_columns: Array<Tariffs_Update_Column>
  where?: Maybe<Tariffs_Bool_Exp>
}

/** ordering options when selecting data from "tariffs" */
export type Tariffs_Order_By = {
  active?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  name?: Maybe<Order_By>
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  users_tariffs_aggregate?: Maybe<Users_Tariffs_Aggregate_Order_By>
}

/** primary key columns input for table: "tariffs" */
export type Tariffs_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "tariffs" */
export enum Tariffs_Select_Column {
  /** column name */
  Active = 'active',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Price = 'price',
  /** column name */
  Space = 'space',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "tariffs" */
export type Tariffs_Set_Input = {
  active?: Maybe<Scalars['Boolean']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  name?: Maybe<Scalars['String']>
  price?: Maybe<Scalars['numeric']>
  space?: Maybe<Scalars['bigint']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate stddev on columns */
export type Tariffs_Stddev_Fields = {
  __typename?: 'tariffs_stddev_fields'
  price?: Maybe<Scalars['Float']>
  space?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "tariffs" */
export type Tariffs_Stddev_Order_By = {
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Tariffs_Stddev_Pop_Fields = {
  __typename?: 'tariffs_stddev_pop_fields'
  price?: Maybe<Scalars['Float']>
  space?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "tariffs" */
export type Tariffs_Stddev_Pop_Order_By = {
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Tariffs_Stddev_Samp_Fields = {
  __typename?: 'tariffs_stddev_samp_fields'
  price?: Maybe<Scalars['Float']>
  space?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "tariffs" */
export type Tariffs_Stddev_Samp_Order_By = {
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Tariffs_Sum_Fields = {
  __typename?: 'tariffs_sum_fields'
  price?: Maybe<Scalars['numeric']>
  space?: Maybe<Scalars['bigint']>
}

/** order by sum() on columns of table "tariffs" */
export type Tariffs_Sum_Order_By = {
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
}

/** update columns of table "tariffs" */
export enum Tariffs_Update_Column {
  /** column name */
  Active = 'active',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Name = 'name',
  /** column name */
  Price = 'price',
  /** column name */
  Space = 'space',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate var_pop on columns */
export type Tariffs_Var_Pop_Fields = {
  __typename?: 'tariffs_var_pop_fields'
  price?: Maybe<Scalars['Float']>
  space?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "tariffs" */
export type Tariffs_Var_Pop_Order_By = {
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Tariffs_Var_Samp_Fields = {
  __typename?: 'tariffs_var_samp_fields'
  price?: Maybe<Scalars['Float']>
  space?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "tariffs" */
export type Tariffs_Var_Samp_Order_By = {
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Tariffs_Variance_Fields = {
  __typename?: 'tariffs_variance_fields'
  price?: Maybe<Scalars['Float']>
  space?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "tariffs" */
export type Tariffs_Variance_Order_By = {
  price?: Maybe<Order_By>
  space?: Maybe<Order_By>
}

/** expression to compare columns of type timestamp. All fields are combined with logical 'AND'. */
export type Timestamp_Comparison_Exp = {
  _eq?: Maybe<Scalars['timestamp']>
  _gt?: Maybe<Scalars['timestamp']>
  _gte?: Maybe<Scalars['timestamp']>
  _in?: Maybe<Array<Scalars['timestamp']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['timestamp']>
  _lte?: Maybe<Scalars['timestamp']>
  _neq?: Maybe<Scalars['timestamp']>
  _nin?: Maybe<Array<Scalars['timestamp']>>
}

/** expression to compare columns of type timestamptz. All fields are combined with logical 'AND'. */
export type Timestamptz_Comparison_Exp = {
  _eq?: Maybe<Scalars['timestamptz']>
  _gt?: Maybe<Scalars['timestamptz']>
  _gte?: Maybe<Scalars['timestamptz']>
  _in?: Maybe<Array<Scalars['timestamptz']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['timestamptz']>
  _lte?: Maybe<Scalars['timestamptz']>
  _neq?: Maybe<Scalars['timestamptz']>
  _nin?: Maybe<Array<Scalars['timestamptz']>>
}

/** columns and relationships of "transaction_status" */
export type Transaction_Status = {
  __typename?: 'transaction_status'
  /** An array relationship */
  transactions: Array<Transactions>
  /** An aggregated array relationship */
  transactions_aggregate: Transactions_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "transaction_status" */
export type Transaction_StatusTransactionsArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** columns and relationships of "transaction_status" */
export type Transaction_StatusTransactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** aggregated selection of "transaction_status" */
export type Transaction_Status_Aggregate = {
  __typename?: 'transaction_status_aggregate'
  aggregate?: Maybe<Transaction_Status_Aggregate_Fields>
  nodes: Array<Transaction_Status>
}

/** aggregate fields of "transaction_status" */
export type Transaction_Status_Aggregate_Fields = {
  __typename?: 'transaction_status_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Transaction_Status_Max_Fields>
  min?: Maybe<Transaction_Status_Min_Fields>
}

/** aggregate fields of "transaction_status" */
export type Transaction_Status_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Transaction_Status_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "transaction_status" */
export type Transaction_Status_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Transaction_Status_Max_Order_By>
  min?: Maybe<Transaction_Status_Min_Order_By>
}

/** input type for inserting array relation for remote table "transaction_status" */
export type Transaction_Status_Arr_Rel_Insert_Input = {
  data: Array<Transaction_Status_Insert_Input>
  on_conflict?: Maybe<Transaction_Status_On_Conflict>
}

/** Boolean expression to filter rows from the table "transaction_status". All fields are combined with a logical 'AND'. */
export type Transaction_Status_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Transaction_Status_Bool_Exp>>>
  _not?: Maybe<Transaction_Status_Bool_Exp>
  _or?: Maybe<Array<Maybe<Transaction_Status_Bool_Exp>>>
  transactions?: Maybe<Transactions_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "transaction_status" */
export enum Transaction_Status_Constraint {
  /** unique or primary key constraint */
  PaymentStatusPkey = 'payment_status_pkey'
}

export enum Transaction_Status_Enum {
  Confirmed = 'CONFIRMED',
  Created = 'CREATED',
  Delayed = 'DELAYED',
  Failed = 'FAILED',
  Pending = 'PENDING',
  Resolved = 'RESOLVED'
}

/** expression to compare columns of type transaction_status_enum. All fields are combined with logical 'AND'. */
export type Transaction_Status_Enum_Comparison_Exp = {
  _eq?: Maybe<Transaction_Status_Enum>
  _in?: Maybe<Array<Transaction_Status_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Transaction_Status_Enum>
  _nin?: Maybe<Array<Transaction_Status_Enum>>
}

/** input type for inserting data into table "transaction_status" */
export type Transaction_Status_Insert_Input = {
  transactions?: Maybe<Transactions_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Transaction_Status_Max_Fields = {
  __typename?: 'transaction_status_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "transaction_status" */
export type Transaction_Status_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Transaction_Status_Min_Fields = {
  __typename?: 'transaction_status_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "transaction_status" */
export type Transaction_Status_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "transaction_status" */
export type Transaction_Status_Mutation_Response = {
  __typename?: 'transaction_status_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Transaction_Status>
}

/** input type for inserting object relation for remote table "transaction_status" */
export type Transaction_Status_Obj_Rel_Insert_Input = {
  data: Transaction_Status_Insert_Input
  on_conflict?: Maybe<Transaction_Status_On_Conflict>
}

/** on conflict condition type for table "transaction_status" */
export type Transaction_Status_On_Conflict = {
  constraint: Transaction_Status_Constraint
  update_columns: Array<Transaction_Status_Update_Column>
  where?: Maybe<Transaction_Status_Bool_Exp>
}

/** ordering options when selecting data from "transaction_status" */
export type Transaction_Status_Order_By = {
  transactions_aggregate?: Maybe<Transactions_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "transaction_status" */
export type Transaction_Status_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "transaction_status" */
export enum Transaction_Status_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "transaction_status" */
export type Transaction_Status_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "transaction_status" */
export enum Transaction_Status_Update_Column {
  /** column name */
  Value = 'value'
}

/** columns and relationships of "transaction_type" */
export type Transaction_Type = {
  __typename?: 'transaction_type'
  /** An array relationship */
  transactions: Array<Transactions>
  /** An aggregated array relationship */
  transactions_aggregate: Transactions_Aggregate
  value: Scalars['String']
}

/** columns and relationships of "transaction_type" */
export type Transaction_TypeTransactionsArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** columns and relationships of "transaction_type" */
export type Transaction_TypeTransactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** aggregated selection of "transaction_type" */
export type Transaction_Type_Aggregate = {
  __typename?: 'transaction_type_aggregate'
  aggregate?: Maybe<Transaction_Type_Aggregate_Fields>
  nodes: Array<Transaction_Type>
}

/** aggregate fields of "transaction_type" */
export type Transaction_Type_Aggregate_Fields = {
  __typename?: 'transaction_type_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Transaction_Type_Max_Fields>
  min?: Maybe<Transaction_Type_Min_Fields>
}

/** aggregate fields of "transaction_type" */
export type Transaction_Type_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Transaction_Type_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "transaction_type" */
export type Transaction_Type_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Transaction_Type_Max_Order_By>
  min?: Maybe<Transaction_Type_Min_Order_By>
}

/** input type for inserting array relation for remote table "transaction_type" */
export type Transaction_Type_Arr_Rel_Insert_Input = {
  data: Array<Transaction_Type_Insert_Input>
  on_conflict?: Maybe<Transaction_Type_On_Conflict>
}

/** Boolean expression to filter rows from the table "transaction_type". All fields are combined with a logical 'AND'. */
export type Transaction_Type_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Transaction_Type_Bool_Exp>>>
  _not?: Maybe<Transaction_Type_Bool_Exp>
  _or?: Maybe<Array<Maybe<Transaction_Type_Bool_Exp>>>
  transactions?: Maybe<Transactions_Bool_Exp>
  value?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "transaction_type" */
export enum Transaction_Type_Constraint {
  /** unique or primary key constraint */
  WalletOperationTypePkey = 'wallet_operation_type_pkey'
}

export enum Transaction_Type_Enum {
  BuyTokens = 'BUY_TOKENS',
  PaymentForTariff = 'PAYMENT_FOR_TARIFF',
  SendToUser = 'SEND_TO_USER',
  Widthdraw = 'WIDTHDRAW'
}

/** expression to compare columns of type transaction_type_enum. All fields are combined with logical 'AND'. */
export type Transaction_Type_Enum_Comparison_Exp = {
  _eq?: Maybe<Transaction_Type_Enum>
  _in?: Maybe<Array<Transaction_Type_Enum>>
  _is_null?: Maybe<Scalars['Boolean']>
  _neq?: Maybe<Transaction_Type_Enum>
  _nin?: Maybe<Array<Transaction_Type_Enum>>
}

/** input type for inserting data into table "transaction_type" */
export type Transaction_Type_Insert_Input = {
  transactions?: Maybe<Transactions_Arr_Rel_Insert_Input>
  value?: Maybe<Scalars['String']>
}

/** aggregate max on columns */
export type Transaction_Type_Max_Fields = {
  __typename?: 'transaction_type_max_fields'
  value?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "transaction_type" */
export type Transaction_Type_Max_Order_By = {
  value?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Transaction_Type_Min_Fields = {
  __typename?: 'transaction_type_min_fields'
  value?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "transaction_type" */
export type Transaction_Type_Min_Order_By = {
  value?: Maybe<Order_By>
}

/** response of any mutation on the table "transaction_type" */
export type Transaction_Type_Mutation_Response = {
  __typename?: 'transaction_type_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Transaction_Type>
}

/** input type for inserting object relation for remote table "transaction_type" */
export type Transaction_Type_Obj_Rel_Insert_Input = {
  data: Transaction_Type_Insert_Input
  on_conflict?: Maybe<Transaction_Type_On_Conflict>
}

/** on conflict condition type for table "transaction_type" */
export type Transaction_Type_On_Conflict = {
  constraint: Transaction_Type_Constraint
  update_columns: Array<Transaction_Type_Update_Column>
  where?: Maybe<Transaction_Type_Bool_Exp>
}

/** ordering options when selecting data from "transaction_type" */
export type Transaction_Type_Order_By = {
  transactions_aggregate?: Maybe<Transactions_Aggregate_Order_By>
  value?: Maybe<Order_By>
}

/** primary key columns input for table: "transaction_type" */
export type Transaction_Type_Pk_Columns_Input = {
  value: Scalars['String']
}

/** select columns of table "transaction_type" */
export enum Transaction_Type_Select_Column {
  /** column name */
  Value = 'value'
}

/** input type for updating data in table "transaction_type" */
export type Transaction_Type_Set_Input = {
  value?: Maybe<Scalars['String']>
}

/** update columns of table "transaction_type" */
export enum Transaction_Type_Update_Column {
  /** column name */
  Value = 'value'
}

/** columns and relationships of "transactions" */
export type Transactions = {
  __typename?: 'transactions'
  /** An object relationship */
  album?: Maybe<Albums>
  album_id?: Maybe<Scalars['uuid']>
  amount: Scalars['numeric']
  comment?: Maybe<Scalars['String']>
  created_at: Scalars['timestamptz']
  data?: Maybe<Scalars['jsonb']>
  /** An object relationship */
  event?: Maybe<Events>
  event_id?: Maybe<Scalars['uuid']>
  /** An object relationship */
  from_wallet?: Maybe<Wallets>
  from_wallet_id?: Maybe<Scalars['uuid']>
  id: Scalars['uuid']
  /** An array relationship */
  network_transactions: Array<Network_Transactions>
  /** An aggregated array relationship */
  network_transactions_aggregate: Network_Transactions_Aggregate
  /** An array relationship */
  purchased: Array<Purchased>
  /** An aggregated array relationship */
  purchased_aggregate: Purchased_Aggregate
  status: Transaction_Status_Enum
  /** An object relationship */
  to_wallet: Wallets
  to_wallet_id: Scalars['uuid']
  /** An object relationship */
  transaction_status: Transaction_Status
  /** An object relationship */
  transaction_type: Transaction_Type
  type: Transaction_Type_Enum
  updated_at: Scalars['timestamptz']
}

/** columns and relationships of "transactions" */
export type TransactionsDataArgs = {
  path?: Maybe<Scalars['String']>
}

/** columns and relationships of "transactions" */
export type TransactionsNetwork_TransactionsArgs = {
  distinct_on?: Maybe<Array<Network_Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Network_Transactions_Order_By>>
  where?: Maybe<Network_Transactions_Bool_Exp>
}

/** columns and relationships of "transactions" */
export type TransactionsNetwork_Transactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Network_Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Network_Transactions_Order_By>>
  where?: Maybe<Network_Transactions_Bool_Exp>
}

/** columns and relationships of "transactions" */
export type TransactionsPurchasedArgs = {
  distinct_on?: Maybe<Array<Purchased_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Purchased_Order_By>>
  where?: Maybe<Purchased_Bool_Exp>
}

/** columns and relationships of "transactions" */
export type TransactionsPurchased_AggregateArgs = {
  distinct_on?: Maybe<Array<Purchased_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Purchased_Order_By>>
  where?: Maybe<Purchased_Bool_Exp>
}

/** aggregated selection of "transactions" */
export type Transactions_Aggregate = {
  __typename?: 'transactions_aggregate'
  aggregate?: Maybe<Transactions_Aggregate_Fields>
  nodes: Array<Transactions>
}

/** aggregate fields of "transactions" */
export type Transactions_Aggregate_Fields = {
  __typename?: 'transactions_aggregate_fields'
  avg?: Maybe<Transactions_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Transactions_Max_Fields>
  min?: Maybe<Transactions_Min_Fields>
  stddev?: Maybe<Transactions_Stddev_Fields>
  stddev_pop?: Maybe<Transactions_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Transactions_Stddev_Samp_Fields>
  sum?: Maybe<Transactions_Sum_Fields>
  var_pop?: Maybe<Transactions_Var_Pop_Fields>
  var_samp?: Maybe<Transactions_Var_Samp_Fields>
  variance?: Maybe<Transactions_Variance_Fields>
}

/** aggregate fields of "transactions" */
export type Transactions_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Transactions_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "transactions" */
export type Transactions_Aggregate_Order_By = {
  avg?: Maybe<Transactions_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Transactions_Max_Order_By>
  min?: Maybe<Transactions_Min_Order_By>
  stddev?: Maybe<Transactions_Stddev_Order_By>
  stddev_pop?: Maybe<Transactions_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Transactions_Stddev_Samp_Order_By>
  sum?: Maybe<Transactions_Sum_Order_By>
  var_pop?: Maybe<Transactions_Var_Pop_Order_By>
  var_samp?: Maybe<Transactions_Var_Samp_Order_By>
  variance?: Maybe<Transactions_Variance_Order_By>
}

/** append existing jsonb value of filtered columns with new jsonb value */
export type Transactions_Append_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** input type for inserting array relation for remote table "transactions" */
export type Transactions_Arr_Rel_Insert_Input = {
  data: Array<Transactions_Insert_Input>
  on_conflict?: Maybe<Transactions_On_Conflict>
}

/** aggregate avg on columns */
export type Transactions_Avg_Fields = {
  __typename?: 'transactions_avg_fields'
  amount?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "transactions" */
export type Transactions_Avg_Order_By = {
  amount?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "transactions". All fields are combined with a logical 'AND'. */
export type Transactions_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Transactions_Bool_Exp>>>
  _not?: Maybe<Transactions_Bool_Exp>
  _or?: Maybe<Array<Maybe<Transactions_Bool_Exp>>>
  album?: Maybe<Albums_Bool_Exp>
  album_id?: Maybe<Uuid_Comparison_Exp>
  amount?: Maybe<Numeric_Comparison_Exp>
  comment?: Maybe<String_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  data?: Maybe<Jsonb_Comparison_Exp>
  event?: Maybe<Events_Bool_Exp>
  event_id?: Maybe<Uuid_Comparison_Exp>
  from_wallet?: Maybe<Wallets_Bool_Exp>
  from_wallet_id?: Maybe<Uuid_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  network_transactions?: Maybe<Network_Transactions_Bool_Exp>
  purchased?: Maybe<Purchased_Bool_Exp>
  status?: Maybe<Transaction_Status_Enum_Comparison_Exp>
  to_wallet?: Maybe<Wallets_Bool_Exp>
  to_wallet_id?: Maybe<Uuid_Comparison_Exp>
  transaction_status?: Maybe<Transaction_Status_Bool_Exp>
  transaction_type?: Maybe<Transaction_Type_Bool_Exp>
  type?: Maybe<Transaction_Type_Enum_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
}

/** unique or primary key constraints on table "transactions" */
export enum Transactions_Constraint {
  /** unique or primary key constraint */
  TransactionsPkey = 'transactions_pkey'
}

/** delete the field or element with specified path (for JSON arrays, negative integers count from the end) */
export type Transactions_Delete_At_Path_Input = {
  data?: Maybe<Array<Maybe<Scalars['String']>>>
}

/**
 * delete the array element with specified index (negative integers count from the
 * end). throws an error if top level container is not an array
 */
export type Transactions_Delete_Elem_Input = {
  data?: Maybe<Scalars['Int']>
}

/** delete key/value pair or string element. key/value pairs are matched based on their key value */
export type Transactions_Delete_Key_Input = {
  data?: Maybe<Scalars['String']>
}

/** input type for incrementing integer column in table "transactions" */
export type Transactions_Inc_Input = {
  amount?: Maybe<Scalars['numeric']>
}

/** input type for inserting data into table "transactions" */
export type Transactions_Insert_Input = {
  album?: Maybe<Albums_Obj_Rel_Insert_Input>
  album_id?: Maybe<Scalars['uuid']>
  amount?: Maybe<Scalars['numeric']>
  comment?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  data?: Maybe<Scalars['jsonb']>
  event?: Maybe<Events_Obj_Rel_Insert_Input>
  event_id?: Maybe<Scalars['uuid']>
  from_wallet?: Maybe<Wallets_Obj_Rel_Insert_Input>
  from_wallet_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  network_transactions?: Maybe<Network_Transactions_Arr_Rel_Insert_Input>
  purchased?: Maybe<Purchased_Arr_Rel_Insert_Input>
  status?: Maybe<Transaction_Status_Enum>
  to_wallet?: Maybe<Wallets_Obj_Rel_Insert_Input>
  to_wallet_id?: Maybe<Scalars['uuid']>
  transaction_status?: Maybe<Transaction_Status_Obj_Rel_Insert_Input>
  transaction_type?: Maybe<Transaction_Type_Obj_Rel_Insert_Input>
  type?: Maybe<Transaction_Type_Enum>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate max on columns */
export type Transactions_Max_Fields = {
  __typename?: 'transactions_max_fields'
  album_id?: Maybe<Scalars['uuid']>
  amount?: Maybe<Scalars['numeric']>
  comment?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  from_wallet_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  to_wallet_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by max() on columns of table "transactions" */
export type Transactions_Max_Order_By = {
  album_id?: Maybe<Order_By>
  amount?: Maybe<Order_By>
  comment?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  from_wallet_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  to_wallet_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Transactions_Min_Fields = {
  __typename?: 'transactions_min_fields'
  album_id?: Maybe<Scalars['uuid']>
  amount?: Maybe<Scalars['numeric']>
  comment?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  event_id?: Maybe<Scalars['uuid']>
  from_wallet_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  to_wallet_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** order by min() on columns of table "transactions" */
export type Transactions_Min_Order_By = {
  album_id?: Maybe<Order_By>
  amount?: Maybe<Order_By>
  comment?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  event_id?: Maybe<Order_By>
  from_wallet_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  to_wallet_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** response of any mutation on the table "transactions" */
export type Transactions_Mutation_Response = {
  __typename?: 'transactions_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Transactions>
}

/** input type for inserting object relation for remote table "transactions" */
export type Transactions_Obj_Rel_Insert_Input = {
  data: Transactions_Insert_Input
  on_conflict?: Maybe<Transactions_On_Conflict>
}

/** on conflict condition type for table "transactions" */
export type Transactions_On_Conflict = {
  constraint: Transactions_Constraint
  update_columns: Array<Transactions_Update_Column>
  where?: Maybe<Transactions_Bool_Exp>
}

/** ordering options when selecting data from "transactions" */
export type Transactions_Order_By = {
  album?: Maybe<Albums_Order_By>
  album_id?: Maybe<Order_By>
  amount?: Maybe<Order_By>
  comment?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  data?: Maybe<Order_By>
  event?: Maybe<Events_Order_By>
  event_id?: Maybe<Order_By>
  from_wallet?: Maybe<Wallets_Order_By>
  from_wallet_id?: Maybe<Order_By>
  id?: Maybe<Order_By>
  network_transactions_aggregate?: Maybe<
    Network_Transactions_Aggregate_Order_By
  >
  purchased_aggregate?: Maybe<Purchased_Aggregate_Order_By>
  status?: Maybe<Order_By>
  to_wallet?: Maybe<Wallets_Order_By>
  to_wallet_id?: Maybe<Order_By>
  transaction_status?: Maybe<Transaction_Status_Order_By>
  transaction_type?: Maybe<Transaction_Type_Order_By>
  type?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
}

/** primary key columns input for table: "transactions" */
export type Transactions_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** prepend existing jsonb value of filtered columns with new jsonb value */
export type Transactions_Prepend_Input = {
  data?: Maybe<Scalars['jsonb']>
}

/** select columns of table "transactions" */
export enum Transactions_Select_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Amount = 'amount',
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Data = 'data',
  /** column name */
  EventId = 'event_id',
  /** column name */
  FromWalletId = 'from_wallet_id',
  /** column name */
  Id = 'id',
  /** column name */
  Status = 'status',
  /** column name */
  ToWalletId = 'to_wallet_id',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** input type for updating data in table "transactions" */
export type Transactions_Set_Input = {
  album_id?: Maybe<Scalars['uuid']>
  amount?: Maybe<Scalars['numeric']>
  comment?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  data?: Maybe<Scalars['jsonb']>
  event_id?: Maybe<Scalars['uuid']>
  from_wallet_id?: Maybe<Scalars['uuid']>
  id?: Maybe<Scalars['uuid']>
  status?: Maybe<Transaction_Status_Enum>
  to_wallet_id?: Maybe<Scalars['uuid']>
  type?: Maybe<Transaction_Type_Enum>
  updated_at?: Maybe<Scalars['timestamptz']>
}

/** aggregate stddev on columns */
export type Transactions_Stddev_Fields = {
  __typename?: 'transactions_stddev_fields'
  amount?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "transactions" */
export type Transactions_Stddev_Order_By = {
  amount?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Transactions_Stddev_Pop_Fields = {
  __typename?: 'transactions_stddev_pop_fields'
  amount?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "transactions" */
export type Transactions_Stddev_Pop_Order_By = {
  amount?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Transactions_Stddev_Samp_Fields = {
  __typename?: 'transactions_stddev_samp_fields'
  amount?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "transactions" */
export type Transactions_Stddev_Samp_Order_By = {
  amount?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Transactions_Sum_Fields = {
  __typename?: 'transactions_sum_fields'
  amount?: Maybe<Scalars['numeric']>
}

/** order by sum() on columns of table "transactions" */
export type Transactions_Sum_Order_By = {
  amount?: Maybe<Order_By>
}

/** update columns of table "transactions" */
export enum Transactions_Update_Column {
  /** column name */
  AlbumId = 'album_id',
  /** column name */
  Amount = 'amount',
  /** column name */
  Comment = 'comment',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Data = 'data',
  /** column name */
  EventId = 'event_id',
  /** column name */
  FromWalletId = 'from_wallet_id',
  /** column name */
  Id = 'id',
  /** column name */
  Status = 'status',
  /** column name */
  ToWalletId = 'to_wallet_id',
  /** column name */
  Type = 'type',
  /** column name */
  UpdatedAt = 'updated_at'
}

/** aggregate var_pop on columns */
export type Transactions_Var_Pop_Fields = {
  __typename?: 'transactions_var_pop_fields'
  amount?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "transactions" */
export type Transactions_Var_Pop_Order_By = {
  amount?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Transactions_Var_Samp_Fields = {
  __typename?: 'transactions_var_samp_fields'
  amount?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "transactions" */
export type Transactions_Var_Samp_Order_By = {
  amount?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Transactions_Variance_Fields = {
  __typename?: 'transactions_variance_fields'
  amount?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "transactions" */
export type Transactions_Variance_Order_By = {
  amount?: Maybe<Order_By>
}

/** columns and relationships of "users" */
export type Users = {
  __typename?: 'users'
  /** An object relationship */
  account?: Maybe<Auth_Accounts>
  /** A computed field, executes function "get_active_tariffs_by_user" */
  active_tariffs?: Maybe<Array<Users_Tariffs>>
  /** An array relationship */
  albums: Array<Albums>
  /** An aggregated array relationship */
  albums_aggregate: Albums_Aggregate
  /** An array relationship */
  albums_visits: Array<Albums_Visits>
  /** An aggregated array relationship */
  albums_visits_aggregate: Albums_Visits_Aggregate
  /** A computed field, executes function "linza_user_available_for_search" */
  available_for_search?: Maybe<Scalars['Boolean']>
  avatar_blurhash?: Maybe<Scalars['String']>
  avatar_url?: Maybe<Scalars['String']>
  bio?: Maybe<Scalars['String']>
  blocked_for_search: Scalars['Boolean']
  /** An array relationship */
  contacts: Array<Contacts>
  /** An aggregated array relationship */
  contacts_aggregate: Contacts_Aggregate
  created_at: Scalars['timestamptz']
  /** An array relationship */
  credit_cards: Array<Credit_Cards>
  /** An aggregated array relationship */
  credit_cards_aggregate: Credit_Cards_Aggregate
  /** An array relationship */
  customer_events: Array<Events>
  /** An aggregated array relationship */
  customer_events_aggregate: Events_Aggregate
  display_name?: Maybe<Scalars['String']>
  /** An array relationship */
  downloads: Array<Downloads>
  /** An aggregated array relationship */
  downloads_aggregate: Downloads_Aggregate
  /** An array relationship */
  events: Array<Events>
  /** An aggregated array relationship */
  events_aggregate: Events_Aggregate
  /** An array relationship */
  favorites: Array<Favorites>
  /** An aggregated array relationship */
  favorites_aggregate: Favorites_Aggregate
  /** An array relationship */
  folders: Array<Folders>
  /** An aggregated array relationship */
  folders_aggregate: Folders_Aggregate
  hide_in_search: Scalars['Boolean']
  id: Scalars['uuid']
  /** An array relationship */
  in_contacts: Array<Contacts>
  /** An aggregated array relationship */
  in_contacts_aggregate: Contacts_Aggregate
  /** An array relationship */
  in_favorites: Array<Favorites>
  /** An aggregated array relationship */
  in_favorites_aggregate: Favorites_Aggregate
  /** An array relationship */
  in_messages: Array<Messages>
  /** An aggregated array relationship */
  in_messages_aggregate: Messages_Aggregate
  /** An array relationship */
  in_reviews: Array<Reviews>
  /** An aggregated array relationship */
  in_reviews_aggregate: Reviews_Aggregate
  /** A computed field, executes function "linza_user_is_favorite" */
  is_favorite?: Maybe<Scalars['Boolean']>
  is_photographer: Scalars['Boolean']
  last_session: Scalars['timestamptz']
  locale: Locales_Enum
  /** An object relationship */
  localeByLocale: Locales
  /** An object relationship */
  location?: Maybe<Locations>
  location_id?: Maybe<Scalars['uuid']>
  /** An array relationship */
  locations: Array<Locations>
  /** An aggregated array relationship */
  locations_aggregate: Locations_Aggregate
  /** An array relationship */
  maker_types: Array<Users_Maker_Types>
  /** An aggregated array relationship */
  maker_types_aggregate: Users_Maker_Types_Aggregate
  /** An array relationship */
  makers: Array<Users_Makers>
  /** An aggregated array relationship */
  makers_aggregate: Users_Makers_Aggregate
  /** An array relationship */
  media_files: Array<Media_Files>
  /** An aggregated array relationship */
  media_files_aggregate: Media_Files_Aggregate
  /** An array relationship */
  media_files_in_search: Array<Users_Search_Media_Files>
  /** An aggregated array relationship */
  media_files_in_search_aggregate: Users_Search_Media_Files_Aggregate
  /** A computed field, executes function "user_messages" */
  messages?: Maybe<Array<Messages>>
  /** An object relationship */
  new_messages?: Maybe<New_Messages_View>
  /** A computed field, executes function "linza_user_is_online" */
  online?: Maybe<Scalars['Boolean']>
  /** An array relationship */
  out_messages: Array<Messages>
  /** An aggregated array relationship */
  out_messages_aggregate: Messages_Aggregate
  /** An array relationship */
  out_reviews: Array<Reviews>
  /** An aggregated array relationship */
  out_reviews_aggregate: Reviews_Aggregate
  /** An array relationship */
  payment_types: Array<Users_Payment_Types>
  /** An aggregated array relationship */
  payment_types_aggregate: Users_Payment_Types_Aggregate
  /** An array relationship */
  permissions: Array<Users_Permissions>
  /** An aggregated array relationship */
  permissions_aggregate: Users_Permissions_Aggregate
  phone?: Maybe<Scalars['String']>
  /** An array relationship */
  purchased: Array<Purchased>
  /** An aggregated array relationship */
  purchased_aggregate: Purchased_Aggregate
  rating?: Maybe<Scalars['numeric']>
  slogan?: Maybe<Scalars['String']>
  /** An object relationship */
  stats?: Maybe<Users_Stats>
  /** An object relationship */
  storage?: Maybe<Storages_View>
  /** An array relationship */
  stores: Array<Store>
  /** An aggregated array relationship */
  stores_aggregate: Store_Aggregate
  /** An array relationship */
  styles: Array<Users_Styles>
  /** An aggregated array relationship */
  styles_aggregate: Users_Styles_Aggregate
  /** An array relationship */
  tariffs: Array<Users_Tariffs>
  /** An aggregated array relationship */
  tariffs_aggregate: Users_Tariffs_Aggregate
  telegram?: Maybe<Scalars['String']>
  updated_at: Scalars['timestamptz']
  username: Scalars['String']
  /** An object relationship */
  wallet?: Maybe<Wallets>
  waves_address?: Maybe<Scalars['String']>
  whatsapp?: Maybe<Scalars['String']>
}

/** columns and relationships of "users" */
export type UsersActive_TariffsArgs = {
  distinct_on?: Maybe<Array<Users_Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Tariffs_Order_By>>
  where?: Maybe<Users_Tariffs_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersAlbumsArgs = {
  distinct_on?: Maybe<Array<Albums_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Order_By>>
  where?: Maybe<Albums_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersAlbums_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Order_By>>
  where?: Maybe<Albums_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersAlbums_VisitsArgs = {
  distinct_on?: Maybe<Array<Albums_Visits_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Visits_Order_By>>
  where?: Maybe<Albums_Visits_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersAlbums_Visits_AggregateArgs = {
  distinct_on?: Maybe<Array<Albums_Visits_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Albums_Visits_Order_By>>
  where?: Maybe<Albums_Visits_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersContactsArgs = {
  distinct_on?: Maybe<Array<Contacts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Contacts_Order_By>>
  where?: Maybe<Contacts_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersContacts_AggregateArgs = {
  distinct_on?: Maybe<Array<Contacts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Contacts_Order_By>>
  where?: Maybe<Contacts_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersCredit_CardsArgs = {
  distinct_on?: Maybe<Array<Credit_Cards_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Credit_Cards_Order_By>>
  where?: Maybe<Credit_Cards_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersCredit_Cards_AggregateArgs = {
  distinct_on?: Maybe<Array<Credit_Cards_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Credit_Cards_Order_By>>
  where?: Maybe<Credit_Cards_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersCustomer_EventsArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersCustomer_Events_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersDownloadsArgs = {
  distinct_on?: Maybe<Array<Downloads_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Downloads_Order_By>>
  where?: Maybe<Downloads_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersDownloads_AggregateArgs = {
  distinct_on?: Maybe<Array<Downloads_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Downloads_Order_By>>
  where?: Maybe<Downloads_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersEventsArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersEvents_AggregateArgs = {
  distinct_on?: Maybe<Array<Events_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Events_Order_By>>
  where?: Maybe<Events_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersFavoritesArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersFavorites_AggregateArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersFoldersArgs = {
  distinct_on?: Maybe<Array<Folders_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Folders_Order_By>>
  where?: Maybe<Folders_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersFolders_AggregateArgs = {
  distinct_on?: Maybe<Array<Folders_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Folders_Order_By>>
  where?: Maybe<Folders_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersIn_ContactsArgs = {
  distinct_on?: Maybe<Array<Contacts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Contacts_Order_By>>
  where?: Maybe<Contacts_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersIn_Contacts_AggregateArgs = {
  distinct_on?: Maybe<Array<Contacts_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Contacts_Order_By>>
  where?: Maybe<Contacts_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersIn_FavoritesArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersIn_Favorites_AggregateArgs = {
  distinct_on?: Maybe<Array<Favorites_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Favorites_Order_By>>
  where?: Maybe<Favorites_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersIn_MessagesArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersIn_Messages_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersIn_ReviewsArgs = {
  distinct_on?: Maybe<Array<Reviews_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Reviews_Order_By>>
  where?: Maybe<Reviews_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersIn_Reviews_AggregateArgs = {
  distinct_on?: Maybe<Array<Reviews_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Reviews_Order_By>>
  where?: Maybe<Reviews_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersLocationsArgs = {
  distinct_on?: Maybe<Array<Locations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locations_Order_By>>
  where?: Maybe<Locations_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersLocations_AggregateArgs = {
  distinct_on?: Maybe<Array<Locations_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Locations_Order_By>>
  where?: Maybe<Locations_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersMaker_TypesArgs = {
  distinct_on?: Maybe<Array<Users_Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Maker_Types_Order_By>>
  where?: Maybe<Users_Maker_Types_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersMaker_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Maker_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Maker_Types_Order_By>>
  where?: Maybe<Users_Maker_Types_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersMakersArgs = {
  distinct_on?: Maybe<Array<Users_Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Makers_Order_By>>
  where?: Maybe<Users_Makers_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersMakers_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Makers_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Makers_Order_By>>
  where?: Maybe<Users_Makers_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersMedia_FilesArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersMedia_Files_AggregateArgs = {
  distinct_on?: Maybe<Array<Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Media_Files_Order_By>>
  where?: Maybe<Media_Files_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersMedia_Files_In_SearchArgs = {
  distinct_on?: Maybe<Array<Users_Search_Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Search_Media_Files_Order_By>>
  where?: Maybe<Users_Search_Media_Files_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersMedia_Files_In_Search_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Search_Media_Files_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Search_Media_Files_Order_By>>
  where?: Maybe<Users_Search_Media_Files_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersMessagesArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersOut_MessagesArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersOut_Messages_AggregateArgs = {
  distinct_on?: Maybe<Array<Messages_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Messages_Order_By>>
  where?: Maybe<Messages_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersOut_ReviewsArgs = {
  distinct_on?: Maybe<Array<Reviews_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Reviews_Order_By>>
  where?: Maybe<Reviews_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersOut_Reviews_AggregateArgs = {
  distinct_on?: Maybe<Array<Reviews_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Reviews_Order_By>>
  where?: Maybe<Reviews_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersPayment_TypesArgs = {
  distinct_on?: Maybe<Array<Users_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Payment_Types_Order_By>>
  where?: Maybe<Users_Payment_Types_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersPayment_Types_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Payment_Types_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Payment_Types_Order_By>>
  where?: Maybe<Users_Payment_Types_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersPermissionsArgs = {
  distinct_on?: Maybe<Array<Users_Permissions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Permissions_Order_By>>
  where?: Maybe<Users_Permissions_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersPermissions_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Permissions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Permissions_Order_By>>
  where?: Maybe<Users_Permissions_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersPurchasedArgs = {
  distinct_on?: Maybe<Array<Purchased_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Purchased_Order_By>>
  where?: Maybe<Purchased_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersPurchased_AggregateArgs = {
  distinct_on?: Maybe<Array<Purchased_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Purchased_Order_By>>
  where?: Maybe<Purchased_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersStoresArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersStores_AggregateArgs = {
  distinct_on?: Maybe<Array<Store_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Store_Order_By>>
  where?: Maybe<Store_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersStylesArgs = {
  distinct_on?: Maybe<Array<Users_Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Styles_Order_By>>
  where?: Maybe<Users_Styles_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersStyles_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Styles_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Styles_Order_By>>
  where?: Maybe<Users_Styles_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersTariffsArgs = {
  distinct_on?: Maybe<Array<Users_Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Tariffs_Order_By>>
  where?: Maybe<Users_Tariffs_Bool_Exp>
}

/** columns and relationships of "users" */
export type UsersTariffs_AggregateArgs = {
  distinct_on?: Maybe<Array<Users_Tariffs_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Users_Tariffs_Order_By>>
  where?: Maybe<Users_Tariffs_Bool_Exp>
}

/** aggregated selection of "users" */
export type Users_Aggregate = {
  __typename?: 'users_aggregate'
  aggregate?: Maybe<Users_Aggregate_Fields>
  nodes: Array<Users>
}

/** aggregate fields of "users" */
export type Users_Aggregate_Fields = {
  __typename?: 'users_aggregate_fields'
  avg?: Maybe<Users_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Users_Max_Fields>
  min?: Maybe<Users_Min_Fields>
  stddev?: Maybe<Users_Stddev_Fields>
  stddev_pop?: Maybe<Users_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Users_Stddev_Samp_Fields>
  sum?: Maybe<Users_Sum_Fields>
  var_pop?: Maybe<Users_Var_Pop_Fields>
  var_samp?: Maybe<Users_Var_Samp_Fields>
  variance?: Maybe<Users_Variance_Fields>
}

/** aggregate fields of "users" */
export type Users_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "users" */
export type Users_Aggregate_Order_By = {
  avg?: Maybe<Users_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Users_Max_Order_By>
  min?: Maybe<Users_Min_Order_By>
  stddev?: Maybe<Users_Stddev_Order_By>
  stddev_pop?: Maybe<Users_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Users_Stddev_Samp_Order_By>
  sum?: Maybe<Users_Sum_Order_By>
  var_pop?: Maybe<Users_Var_Pop_Order_By>
  var_samp?: Maybe<Users_Var_Samp_Order_By>
  variance?: Maybe<Users_Variance_Order_By>
}

/** input type for inserting array relation for remote table "users" */
export type Users_Arr_Rel_Insert_Input = {
  data: Array<Users_Insert_Input>
  on_conflict?: Maybe<Users_On_Conflict>
}

/** aggregate avg on columns */
export type Users_Avg_Fields = {
  __typename?: 'users_avg_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "users" */
export type Users_Avg_Order_By = {
  rating?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "users". All fields are combined with a logical 'AND'. */
export type Users_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Users_Bool_Exp>>>
  _not?: Maybe<Users_Bool_Exp>
  _or?: Maybe<Array<Maybe<Users_Bool_Exp>>>
  account?: Maybe<Auth_Accounts_Bool_Exp>
  albums?: Maybe<Albums_Bool_Exp>
  albums_visits?: Maybe<Albums_Visits_Bool_Exp>
  avatar_blurhash?: Maybe<String_Comparison_Exp>
  avatar_url?: Maybe<String_Comparison_Exp>
  bio?: Maybe<String_Comparison_Exp>
  blocked_for_search?: Maybe<Boolean_Comparison_Exp>
  contacts?: Maybe<Contacts_Bool_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  credit_cards?: Maybe<Credit_Cards_Bool_Exp>
  customer_events?: Maybe<Events_Bool_Exp>
  display_name?: Maybe<String_Comparison_Exp>
  downloads?: Maybe<Downloads_Bool_Exp>
  events?: Maybe<Events_Bool_Exp>
  favorites?: Maybe<Favorites_Bool_Exp>
  folders?: Maybe<Folders_Bool_Exp>
  hide_in_search?: Maybe<Boolean_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  in_contacts?: Maybe<Contacts_Bool_Exp>
  in_favorites?: Maybe<Favorites_Bool_Exp>
  in_messages?: Maybe<Messages_Bool_Exp>
  in_reviews?: Maybe<Reviews_Bool_Exp>
  is_photographer?: Maybe<Boolean_Comparison_Exp>
  last_session?: Maybe<Timestamptz_Comparison_Exp>
  locale?: Maybe<Locales_Enum_Comparison_Exp>
  localeByLocale?: Maybe<Locales_Bool_Exp>
  location?: Maybe<Locations_Bool_Exp>
  location_id?: Maybe<Uuid_Comparison_Exp>
  locations?: Maybe<Locations_Bool_Exp>
  maker_types?: Maybe<Users_Maker_Types_Bool_Exp>
  makers?: Maybe<Users_Makers_Bool_Exp>
  media_files?: Maybe<Media_Files_Bool_Exp>
  media_files_in_search?: Maybe<Users_Search_Media_Files_Bool_Exp>
  new_messages?: Maybe<New_Messages_View_Bool_Exp>
  out_messages?: Maybe<Messages_Bool_Exp>
  out_reviews?: Maybe<Reviews_Bool_Exp>
  payment_types?: Maybe<Users_Payment_Types_Bool_Exp>
  permissions?: Maybe<Users_Permissions_Bool_Exp>
  phone?: Maybe<String_Comparison_Exp>
  purchased?: Maybe<Purchased_Bool_Exp>
  rating?: Maybe<Numeric_Comparison_Exp>
  slogan?: Maybe<String_Comparison_Exp>
  stats?: Maybe<Users_Stats_Bool_Exp>
  storage?: Maybe<Storages_View_Bool_Exp>
  stores?: Maybe<Store_Bool_Exp>
  styles?: Maybe<Users_Styles_Bool_Exp>
  tariffs?: Maybe<Users_Tariffs_Bool_Exp>
  telegram?: Maybe<String_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  username?: Maybe<String_Comparison_Exp>
  wallet?: Maybe<Wallets_Bool_Exp>
  waves_address?: Maybe<String_Comparison_Exp>
  whatsapp?: Maybe<String_Comparison_Exp>
}

/** unique or primary key constraints on table "users" */
export enum Users_Constraint {
  /** unique or primary key constraint */
  UserUsernameKey = 'user_username_key',
  /** unique or primary key constraint */
  UsersPhoneKey = 'users_phone_key',
  /** unique or primary key constraint */
  UsersPkey = 'users_pkey',
  /** unique or primary key constraint */
  UsersTelegramKey = 'users_telegram_key',
  /** unique or primary key constraint */
  UsersWhatsappKey = 'users_whatsapp_key'
}

/** input type for incrementing integer column in table "users" */
export type Users_Inc_Input = {
  rating?: Maybe<Scalars['numeric']>
}

/** input type for inserting data into table "users" */
export type Users_Insert_Input = {
  account?: Maybe<Auth_Accounts_Obj_Rel_Insert_Input>
  albums?: Maybe<Albums_Arr_Rel_Insert_Input>
  albums_visits?: Maybe<Albums_Visits_Arr_Rel_Insert_Input>
  avatar_blurhash?: Maybe<Scalars['String']>
  avatar_url?: Maybe<Scalars['String']>
  bio?: Maybe<Scalars['String']>
  blocked_for_search?: Maybe<Scalars['Boolean']>
  contacts?: Maybe<Contacts_Arr_Rel_Insert_Input>
  created_at?: Maybe<Scalars['timestamptz']>
  credit_cards?: Maybe<Credit_Cards_Arr_Rel_Insert_Input>
  customer_events?: Maybe<Events_Arr_Rel_Insert_Input>
  display_name?: Maybe<Scalars['String']>
  downloads?: Maybe<Downloads_Arr_Rel_Insert_Input>
  events?: Maybe<Events_Arr_Rel_Insert_Input>
  favorites?: Maybe<Favorites_Arr_Rel_Insert_Input>
  folders?: Maybe<Folders_Arr_Rel_Insert_Input>
  hide_in_search?: Maybe<Scalars['Boolean']>
  id?: Maybe<Scalars['uuid']>
  in_contacts?: Maybe<Contacts_Arr_Rel_Insert_Input>
  in_favorites?: Maybe<Favorites_Arr_Rel_Insert_Input>
  in_messages?: Maybe<Messages_Arr_Rel_Insert_Input>
  in_reviews?: Maybe<Reviews_Arr_Rel_Insert_Input>
  is_photographer?: Maybe<Scalars['Boolean']>
  last_session?: Maybe<Scalars['timestamptz']>
  locale?: Maybe<Locales_Enum>
  localeByLocale?: Maybe<Locales_Obj_Rel_Insert_Input>
  location?: Maybe<Locations_Obj_Rel_Insert_Input>
  location_id?: Maybe<Scalars['uuid']>
  locations?: Maybe<Locations_Arr_Rel_Insert_Input>
  maker_types?: Maybe<Users_Maker_Types_Arr_Rel_Insert_Input>
  makers?: Maybe<Users_Makers_Arr_Rel_Insert_Input>
  media_files?: Maybe<Media_Files_Arr_Rel_Insert_Input>
  media_files_in_search?: Maybe<Users_Search_Media_Files_Arr_Rel_Insert_Input>
  out_messages?: Maybe<Messages_Arr_Rel_Insert_Input>
  out_reviews?: Maybe<Reviews_Arr_Rel_Insert_Input>
  payment_types?: Maybe<Users_Payment_Types_Arr_Rel_Insert_Input>
  permissions?: Maybe<Users_Permissions_Arr_Rel_Insert_Input>
  phone?: Maybe<Scalars['String']>
  purchased?: Maybe<Purchased_Arr_Rel_Insert_Input>
  rating?: Maybe<Scalars['numeric']>
  slogan?: Maybe<Scalars['String']>
  stats?: Maybe<Users_Stats_Obj_Rel_Insert_Input>
  storage?: Maybe<Storages_View_Obj_Rel_Insert_Input>
  stores?: Maybe<Store_Arr_Rel_Insert_Input>
  styles?: Maybe<Users_Styles_Arr_Rel_Insert_Input>
  tariffs?: Maybe<Users_Tariffs_Arr_Rel_Insert_Input>
  telegram?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  username?: Maybe<Scalars['String']>
  wallet?: Maybe<Wallets_Obj_Rel_Insert_Input>
  waves_address?: Maybe<Scalars['String']>
  whatsapp?: Maybe<Scalars['String']>
}

/** columns and relationships of "users_maker_types" */
export type Users_Maker_Types = {
  __typename?: 'users_maker_types'
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  maker_type: Maker_Types_Enum
  /** An object relationship */
  type?: Maybe<Maker_Types>
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user?: Maybe<Users>
  user_id: Scalars['uuid']
}

/** aggregated selection of "users_maker_types" */
export type Users_Maker_Types_Aggregate = {
  __typename?: 'users_maker_types_aggregate'
  aggregate?: Maybe<Users_Maker_Types_Aggregate_Fields>
  nodes: Array<Users_Maker_Types>
}

/** aggregate fields of "users_maker_types" */
export type Users_Maker_Types_Aggregate_Fields = {
  __typename?: 'users_maker_types_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Users_Maker_Types_Max_Fields>
  min?: Maybe<Users_Maker_Types_Min_Fields>
}

/** aggregate fields of "users_maker_types" */
export type Users_Maker_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Maker_Types_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "users_maker_types" */
export type Users_Maker_Types_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Users_Maker_Types_Max_Order_By>
  min?: Maybe<Users_Maker_Types_Min_Order_By>
}

/** input type for inserting array relation for remote table "users_maker_types" */
export type Users_Maker_Types_Arr_Rel_Insert_Input = {
  data: Array<Users_Maker_Types_Insert_Input>
  on_conflict?: Maybe<Users_Maker_Types_On_Conflict>
}

/** Boolean expression to filter rows from the table "users_maker_types". All fields are combined with a logical 'AND'. */
export type Users_Maker_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Users_Maker_Types_Bool_Exp>>>
  _not?: Maybe<Users_Maker_Types_Bool_Exp>
  _or?: Maybe<Array<Maybe<Users_Maker_Types_Bool_Exp>>>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  maker_type?: Maybe<Maker_Types_Enum_Comparison_Exp>
  type?: Maybe<Maker_Types_Bool_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "users_maker_types" */
export enum Users_Maker_Types_Constraint {
  /** unique or primary key constraint */
  UserMakerTypePkey = 'user_maker_type_pkey',
  /** unique or primary key constraint */
  UsersMakerTypesUserIdMakerTypeKey = 'users_maker_types_user_id_maker_type_key'
}

/** input type for inserting data into table "users_maker_types" */
export type Users_Maker_Types_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  maker_type?: Maybe<Maker_Types_Enum>
  type?: Maybe<Maker_Types_Obj_Rel_Insert_Input>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Users_Maker_Types_Max_Fields = {
  __typename?: 'users_maker_types_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "users_maker_types" */
export type Users_Maker_Types_Max_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Users_Maker_Types_Min_Fields = {
  __typename?: 'users_maker_types_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "users_maker_types" */
export type Users_Maker_Types_Min_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "users_maker_types" */
export type Users_Maker_Types_Mutation_Response = {
  __typename?: 'users_maker_types_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Users_Maker_Types>
}

/** input type for inserting object relation for remote table "users_maker_types" */
export type Users_Maker_Types_Obj_Rel_Insert_Input = {
  data: Users_Maker_Types_Insert_Input
  on_conflict?: Maybe<Users_Maker_Types_On_Conflict>
}

/** on conflict condition type for table "users_maker_types" */
export type Users_Maker_Types_On_Conflict = {
  constraint: Users_Maker_Types_Constraint
  update_columns: Array<Users_Maker_Types_Update_Column>
  where?: Maybe<Users_Maker_Types_Bool_Exp>
}

/** ordering options when selecting data from "users_maker_types" */
export type Users_Maker_Types_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  maker_type?: Maybe<Order_By>
  type?: Maybe<Maker_Types_Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "users_maker_types" */
export type Users_Maker_Types_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "users_maker_types" */
export enum Users_Maker_Types_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  MakerType = 'maker_type',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "users_maker_types" */
export type Users_Maker_Types_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  maker_type?: Maybe<Maker_Types_Enum>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "users_maker_types" */
export enum Users_Maker_Types_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  MakerType = 'maker_type',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** columns and relationships of "users_makers" */
export type Users_Makers = {
  __typename?: 'users_makers'
  id: Scalars['uuid']
  /** An object relationship */
  maker: Makers
  maker_id: Scalars['uuid']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** aggregated selection of "users_makers" */
export type Users_Makers_Aggregate = {
  __typename?: 'users_makers_aggregate'
  aggregate?: Maybe<Users_Makers_Aggregate_Fields>
  nodes: Array<Users_Makers>
}

/** aggregate fields of "users_makers" */
export type Users_Makers_Aggregate_Fields = {
  __typename?: 'users_makers_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Users_Makers_Max_Fields>
  min?: Maybe<Users_Makers_Min_Fields>
}

/** aggregate fields of "users_makers" */
export type Users_Makers_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Makers_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "users_makers" */
export type Users_Makers_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Users_Makers_Max_Order_By>
  min?: Maybe<Users_Makers_Min_Order_By>
}

/** input type for inserting array relation for remote table "users_makers" */
export type Users_Makers_Arr_Rel_Insert_Input = {
  data: Array<Users_Makers_Insert_Input>
  on_conflict?: Maybe<Users_Makers_On_Conflict>
}

/** Boolean expression to filter rows from the table "users_makers". All fields are combined with a logical 'AND'. */
export type Users_Makers_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Users_Makers_Bool_Exp>>>
  _not?: Maybe<Users_Makers_Bool_Exp>
  _or?: Maybe<Array<Maybe<Users_Makers_Bool_Exp>>>
  id?: Maybe<Uuid_Comparison_Exp>
  maker?: Maybe<Makers_Bool_Exp>
  maker_id?: Maybe<Uuid_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "users_makers" */
export enum Users_Makers_Constraint {
  /** unique or primary key constraint */
  UsersMakersPkey = 'users_makers_pkey',
  /** unique or primary key constraint */
  UsersMakersUserIdMakerIdKey = 'users_makers_user_id_maker_id_key'
}

/** input type for inserting data into table "users_makers" */
export type Users_Makers_Insert_Input = {
  id?: Maybe<Scalars['uuid']>
  maker?: Maybe<Makers_Obj_Rel_Insert_Input>
  maker_id?: Maybe<Scalars['uuid']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Users_Makers_Max_Fields = {
  __typename?: 'users_makers_max_fields'
  id?: Maybe<Scalars['uuid']>
  maker_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "users_makers" */
export type Users_Makers_Max_Order_By = {
  id?: Maybe<Order_By>
  maker_id?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Users_Makers_Min_Fields = {
  __typename?: 'users_makers_min_fields'
  id?: Maybe<Scalars['uuid']>
  maker_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "users_makers" */
export type Users_Makers_Min_Order_By = {
  id?: Maybe<Order_By>
  maker_id?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "users_makers" */
export type Users_Makers_Mutation_Response = {
  __typename?: 'users_makers_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Users_Makers>
}

/** input type for inserting object relation for remote table "users_makers" */
export type Users_Makers_Obj_Rel_Insert_Input = {
  data: Users_Makers_Insert_Input
  on_conflict?: Maybe<Users_Makers_On_Conflict>
}

/** on conflict condition type for table "users_makers" */
export type Users_Makers_On_Conflict = {
  constraint: Users_Makers_Constraint
  update_columns: Array<Users_Makers_Update_Column>
  where?: Maybe<Users_Makers_Bool_Exp>
}

/** ordering options when selecting data from "users_makers" */
export type Users_Makers_Order_By = {
  id?: Maybe<Order_By>
  maker?: Maybe<Makers_Order_By>
  maker_id?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "users_makers" */
export type Users_Makers_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "users_makers" */
export enum Users_Makers_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  MakerId = 'maker_id',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "users_makers" */
export type Users_Makers_Set_Input = {
  id?: Maybe<Scalars['uuid']>
  maker_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "users_makers" */
export enum Users_Makers_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  MakerId = 'maker_id',
  /** column name */
  UserId = 'user_id'
}

/** aggregate max on columns */
export type Users_Max_Fields = {
  __typename?: 'users_max_fields'
  avatar_blurhash?: Maybe<Scalars['String']>
  avatar_url?: Maybe<Scalars['String']>
  bio?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  display_name?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  last_session?: Maybe<Scalars['timestamptz']>
  location_id?: Maybe<Scalars['uuid']>
  phone?: Maybe<Scalars['String']>
  rating?: Maybe<Scalars['numeric']>
  slogan?: Maybe<Scalars['String']>
  telegram?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  username?: Maybe<Scalars['String']>
  waves_address?: Maybe<Scalars['String']>
  whatsapp?: Maybe<Scalars['String']>
}

/** order by max() on columns of table "users" */
export type Users_Max_Order_By = {
  avatar_blurhash?: Maybe<Order_By>
  avatar_url?: Maybe<Order_By>
  bio?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  display_name?: Maybe<Order_By>
  id?: Maybe<Order_By>
  last_session?: Maybe<Order_By>
  location_id?: Maybe<Order_By>
  phone?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  slogan?: Maybe<Order_By>
  telegram?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  username?: Maybe<Order_By>
  waves_address?: Maybe<Order_By>
  whatsapp?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Users_Min_Fields = {
  __typename?: 'users_min_fields'
  avatar_blurhash?: Maybe<Scalars['String']>
  avatar_url?: Maybe<Scalars['String']>
  bio?: Maybe<Scalars['String']>
  created_at?: Maybe<Scalars['timestamptz']>
  display_name?: Maybe<Scalars['String']>
  id?: Maybe<Scalars['uuid']>
  last_session?: Maybe<Scalars['timestamptz']>
  location_id?: Maybe<Scalars['uuid']>
  phone?: Maybe<Scalars['String']>
  rating?: Maybe<Scalars['numeric']>
  slogan?: Maybe<Scalars['String']>
  telegram?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  username?: Maybe<Scalars['String']>
  waves_address?: Maybe<Scalars['String']>
  whatsapp?: Maybe<Scalars['String']>
}

/** order by min() on columns of table "users" */
export type Users_Min_Order_By = {
  avatar_blurhash?: Maybe<Order_By>
  avatar_url?: Maybe<Order_By>
  bio?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  display_name?: Maybe<Order_By>
  id?: Maybe<Order_By>
  last_session?: Maybe<Order_By>
  location_id?: Maybe<Order_By>
  phone?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  slogan?: Maybe<Order_By>
  telegram?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  username?: Maybe<Order_By>
  waves_address?: Maybe<Order_By>
  whatsapp?: Maybe<Order_By>
}

/** response of any mutation on the table "users" */
export type Users_Mutation_Response = {
  __typename?: 'users_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Users>
}

/** input type for inserting object relation for remote table "users" */
export type Users_Obj_Rel_Insert_Input = {
  data: Users_Insert_Input
  on_conflict?: Maybe<Users_On_Conflict>
}

/** on conflict condition type for table "users" */
export type Users_On_Conflict = {
  constraint: Users_Constraint
  update_columns: Array<Users_Update_Column>
  where?: Maybe<Users_Bool_Exp>
}

/** ordering options when selecting data from "users" */
export type Users_Order_By = {
  account?: Maybe<Auth_Accounts_Order_By>
  albums_aggregate?: Maybe<Albums_Aggregate_Order_By>
  albums_visits_aggregate?: Maybe<Albums_Visits_Aggregate_Order_By>
  avatar_blurhash?: Maybe<Order_By>
  avatar_url?: Maybe<Order_By>
  bio?: Maybe<Order_By>
  blocked_for_search?: Maybe<Order_By>
  contacts_aggregate?: Maybe<Contacts_Aggregate_Order_By>
  created_at?: Maybe<Order_By>
  credit_cards_aggregate?: Maybe<Credit_Cards_Aggregate_Order_By>
  customer_events_aggregate?: Maybe<Events_Aggregate_Order_By>
  display_name?: Maybe<Order_By>
  downloads_aggregate?: Maybe<Downloads_Aggregate_Order_By>
  events_aggregate?: Maybe<Events_Aggregate_Order_By>
  favorites_aggregate?: Maybe<Favorites_Aggregate_Order_By>
  folders_aggregate?: Maybe<Folders_Aggregate_Order_By>
  hide_in_search?: Maybe<Order_By>
  id?: Maybe<Order_By>
  in_contacts_aggregate?: Maybe<Contacts_Aggregate_Order_By>
  in_favorites_aggregate?: Maybe<Favorites_Aggregate_Order_By>
  in_messages_aggregate?: Maybe<Messages_Aggregate_Order_By>
  in_reviews_aggregate?: Maybe<Reviews_Aggregate_Order_By>
  is_photographer?: Maybe<Order_By>
  last_session?: Maybe<Order_By>
  locale?: Maybe<Order_By>
  localeByLocale?: Maybe<Locales_Order_By>
  location?: Maybe<Locations_Order_By>
  location_id?: Maybe<Order_By>
  locations_aggregate?: Maybe<Locations_Aggregate_Order_By>
  maker_types_aggregate?: Maybe<Users_Maker_Types_Aggregate_Order_By>
  makers_aggregate?: Maybe<Users_Makers_Aggregate_Order_By>
  media_files_aggregate?: Maybe<Media_Files_Aggregate_Order_By>
  media_files_in_search_aggregate?: Maybe<
    Users_Search_Media_Files_Aggregate_Order_By
  >
  new_messages?: Maybe<New_Messages_View_Order_By>
  out_messages_aggregate?: Maybe<Messages_Aggregate_Order_By>
  out_reviews_aggregate?: Maybe<Reviews_Aggregate_Order_By>
  payment_types_aggregate?: Maybe<Users_Payment_Types_Aggregate_Order_By>
  permissions_aggregate?: Maybe<Users_Permissions_Aggregate_Order_By>
  phone?: Maybe<Order_By>
  purchased_aggregate?: Maybe<Purchased_Aggregate_Order_By>
  rating?: Maybe<Order_By>
  slogan?: Maybe<Order_By>
  stats?: Maybe<Users_Stats_Order_By>
  storage?: Maybe<Storages_View_Order_By>
  stores_aggregate?: Maybe<Store_Aggregate_Order_By>
  styles_aggregate?: Maybe<Users_Styles_Aggregate_Order_By>
  tariffs_aggregate?: Maybe<Users_Tariffs_Aggregate_Order_By>
  telegram?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  username?: Maybe<Order_By>
  wallet?: Maybe<Wallets_Order_By>
  waves_address?: Maybe<Order_By>
  whatsapp?: Maybe<Order_By>
}

/** columns and relationships of "users_payment_types" */
export type Users_Payment_Types = {
  __typename?: 'users_payment_types'
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  payment_type: Payment_Types_Enum
  price: Scalars['numeric']
  /** An object relationship */
  type: Payment_Types
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** aggregated selection of "users_payment_types" */
export type Users_Payment_Types_Aggregate = {
  __typename?: 'users_payment_types_aggregate'
  aggregate?: Maybe<Users_Payment_Types_Aggregate_Fields>
  nodes: Array<Users_Payment_Types>
}

/** aggregate fields of "users_payment_types" */
export type Users_Payment_Types_Aggregate_Fields = {
  __typename?: 'users_payment_types_aggregate_fields'
  avg?: Maybe<Users_Payment_Types_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Users_Payment_Types_Max_Fields>
  min?: Maybe<Users_Payment_Types_Min_Fields>
  stddev?: Maybe<Users_Payment_Types_Stddev_Fields>
  stddev_pop?: Maybe<Users_Payment_Types_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Users_Payment_Types_Stddev_Samp_Fields>
  sum?: Maybe<Users_Payment_Types_Sum_Fields>
  var_pop?: Maybe<Users_Payment_Types_Var_Pop_Fields>
  var_samp?: Maybe<Users_Payment_Types_Var_Samp_Fields>
  variance?: Maybe<Users_Payment_Types_Variance_Fields>
}

/** aggregate fields of "users_payment_types" */
export type Users_Payment_Types_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Payment_Types_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "users_payment_types" */
export type Users_Payment_Types_Aggregate_Order_By = {
  avg?: Maybe<Users_Payment_Types_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Users_Payment_Types_Max_Order_By>
  min?: Maybe<Users_Payment_Types_Min_Order_By>
  stddev?: Maybe<Users_Payment_Types_Stddev_Order_By>
  stddev_pop?: Maybe<Users_Payment_Types_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Users_Payment_Types_Stddev_Samp_Order_By>
  sum?: Maybe<Users_Payment_Types_Sum_Order_By>
  var_pop?: Maybe<Users_Payment_Types_Var_Pop_Order_By>
  var_samp?: Maybe<Users_Payment_Types_Var_Samp_Order_By>
  variance?: Maybe<Users_Payment_Types_Variance_Order_By>
}

/** input type for inserting array relation for remote table "users_payment_types" */
export type Users_Payment_Types_Arr_Rel_Insert_Input = {
  data: Array<Users_Payment_Types_Insert_Input>
  on_conflict?: Maybe<Users_Payment_Types_On_Conflict>
}

/** aggregate avg on columns */
export type Users_Payment_Types_Avg_Fields = {
  __typename?: 'users_payment_types_avg_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "users_payment_types" */
export type Users_Payment_Types_Avg_Order_By = {
  price?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "users_payment_types". All fields are combined with a logical 'AND'. */
export type Users_Payment_Types_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Users_Payment_Types_Bool_Exp>>>
  _not?: Maybe<Users_Payment_Types_Bool_Exp>
  _or?: Maybe<Array<Maybe<Users_Payment_Types_Bool_Exp>>>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  payment_type?: Maybe<Payment_Types_Enum_Comparison_Exp>
  price?: Maybe<Numeric_Comparison_Exp>
  type?: Maybe<Payment_Types_Bool_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "users_payment_types" */
export enum Users_Payment_Types_Constraint {
  /** unique or primary key constraint */
  UsersPaymentTypesUserIdPaymentTypeKey = 'users_payment_types_user_id_payment_type_key',
  /** unique or primary key constraint */
  UsersPaymentsTypesPkey = 'users_payments_types_pkey'
}

/** input type for incrementing integer column in table "users_payment_types" */
export type Users_Payment_Types_Inc_Input = {
  price?: Maybe<Scalars['numeric']>
}

/** input type for inserting data into table "users_payment_types" */
export type Users_Payment_Types_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  payment_type?: Maybe<Payment_Types_Enum>
  price?: Maybe<Scalars['numeric']>
  type?: Maybe<Payment_Types_Obj_Rel_Insert_Input>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Users_Payment_Types_Max_Fields = {
  __typename?: 'users_payment_types_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  price?: Maybe<Scalars['numeric']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "users_payment_types" */
export type Users_Payment_Types_Max_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  price?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Users_Payment_Types_Min_Fields = {
  __typename?: 'users_payment_types_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  price?: Maybe<Scalars['numeric']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "users_payment_types" */
export type Users_Payment_Types_Min_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  price?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "users_payment_types" */
export type Users_Payment_Types_Mutation_Response = {
  __typename?: 'users_payment_types_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Users_Payment_Types>
}

/** input type for inserting object relation for remote table "users_payment_types" */
export type Users_Payment_Types_Obj_Rel_Insert_Input = {
  data: Users_Payment_Types_Insert_Input
  on_conflict?: Maybe<Users_Payment_Types_On_Conflict>
}

/** on conflict condition type for table "users_payment_types" */
export type Users_Payment_Types_On_Conflict = {
  constraint: Users_Payment_Types_Constraint
  update_columns: Array<Users_Payment_Types_Update_Column>
  where?: Maybe<Users_Payment_Types_Bool_Exp>
}

/** ordering options when selecting data from "users_payment_types" */
export type Users_Payment_Types_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  payment_type?: Maybe<Order_By>
  price?: Maybe<Order_By>
  type?: Maybe<Payment_Types_Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "users_payment_types" */
export type Users_Payment_Types_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "users_payment_types" */
export enum Users_Payment_Types_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  PaymentType = 'payment_type',
  /** column name */
  Price = 'price',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "users_payment_types" */
export type Users_Payment_Types_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  payment_type?: Maybe<Payment_Types_Enum>
  price?: Maybe<Scalars['numeric']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate stddev on columns */
export type Users_Payment_Types_Stddev_Fields = {
  __typename?: 'users_payment_types_stddev_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "users_payment_types" */
export type Users_Payment_Types_Stddev_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Users_Payment_Types_Stddev_Pop_Fields = {
  __typename?: 'users_payment_types_stddev_pop_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "users_payment_types" */
export type Users_Payment_Types_Stddev_Pop_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Users_Payment_Types_Stddev_Samp_Fields = {
  __typename?: 'users_payment_types_stddev_samp_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "users_payment_types" */
export type Users_Payment_Types_Stddev_Samp_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Users_Payment_Types_Sum_Fields = {
  __typename?: 'users_payment_types_sum_fields'
  price?: Maybe<Scalars['numeric']>
}

/** order by sum() on columns of table "users_payment_types" */
export type Users_Payment_Types_Sum_Order_By = {
  price?: Maybe<Order_By>
}

/** update columns of table "users_payment_types" */
export enum Users_Payment_Types_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  PaymentType = 'payment_type',
  /** column name */
  Price = 'price',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** aggregate var_pop on columns */
export type Users_Payment_Types_Var_Pop_Fields = {
  __typename?: 'users_payment_types_var_pop_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "users_payment_types" */
export type Users_Payment_Types_Var_Pop_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Users_Payment_Types_Var_Samp_Fields = {
  __typename?: 'users_payment_types_var_samp_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "users_payment_types" */
export type Users_Payment_Types_Var_Samp_Order_By = {
  price?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Users_Payment_Types_Variance_Fields = {
  __typename?: 'users_payment_types_variance_fields'
  price?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "users_payment_types" */
export type Users_Payment_Types_Variance_Order_By = {
  price?: Maybe<Order_By>
}

/** columns and relationships of "users_permissions" */
export type Users_Permissions = {
  __typename?: 'users_permissions'
  created_at: Scalars['timestamptz']
  id: Scalars['uuid']
  permission: Permissions_Enum
  /** An object relationship */
  permission_value: Permissions
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** aggregated selection of "users_permissions" */
export type Users_Permissions_Aggregate = {
  __typename?: 'users_permissions_aggregate'
  aggregate?: Maybe<Users_Permissions_Aggregate_Fields>
  nodes: Array<Users_Permissions>
}

/** aggregate fields of "users_permissions" */
export type Users_Permissions_Aggregate_Fields = {
  __typename?: 'users_permissions_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Users_Permissions_Max_Fields>
  min?: Maybe<Users_Permissions_Min_Fields>
}

/** aggregate fields of "users_permissions" */
export type Users_Permissions_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Permissions_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "users_permissions" */
export type Users_Permissions_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Users_Permissions_Max_Order_By>
  min?: Maybe<Users_Permissions_Min_Order_By>
}

/** input type for inserting array relation for remote table "users_permissions" */
export type Users_Permissions_Arr_Rel_Insert_Input = {
  data: Array<Users_Permissions_Insert_Input>
  on_conflict?: Maybe<Users_Permissions_On_Conflict>
}

/** Boolean expression to filter rows from the table "users_permissions". All fields are combined with a logical 'AND'. */
export type Users_Permissions_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Users_Permissions_Bool_Exp>>>
  _not?: Maybe<Users_Permissions_Bool_Exp>
  _or?: Maybe<Array<Maybe<Users_Permissions_Bool_Exp>>>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  permission?: Maybe<Permissions_Enum_Comparison_Exp>
  permission_value?: Maybe<Permissions_Bool_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "users_permissions" */
export enum Users_Permissions_Constraint {
  /** unique or primary key constraint */
  UserPermissionsPermissionKey = 'user_permissions_permission_key',
  /** unique or primary key constraint */
  UserPermissionsPkey = 'user_permissions_pkey',
  /** unique or primary key constraint */
  UserPermissionsUserIdPermissionKey = 'user_permissions_user_id_permission_key'
}

/** input type for inserting data into table "users_permissions" */
export type Users_Permissions_Insert_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  permission?: Maybe<Permissions_Enum>
  permission_value?: Maybe<Permissions_Obj_Rel_Insert_Input>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Users_Permissions_Max_Fields = {
  __typename?: 'users_permissions_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "users_permissions" */
export type Users_Permissions_Max_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Users_Permissions_Min_Fields = {
  __typename?: 'users_permissions_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "users_permissions" */
export type Users_Permissions_Min_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "users_permissions" */
export type Users_Permissions_Mutation_Response = {
  __typename?: 'users_permissions_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Users_Permissions>
}

/** input type for inserting object relation for remote table "users_permissions" */
export type Users_Permissions_Obj_Rel_Insert_Input = {
  data: Users_Permissions_Insert_Input
  on_conflict?: Maybe<Users_Permissions_On_Conflict>
}

/** on conflict condition type for table "users_permissions" */
export type Users_Permissions_On_Conflict = {
  constraint: Users_Permissions_Constraint
  update_columns: Array<Users_Permissions_Update_Column>
  where?: Maybe<Users_Permissions_Bool_Exp>
}

/** ordering options when selecting data from "users_permissions" */
export type Users_Permissions_Order_By = {
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  permission?: Maybe<Order_By>
  permission_value?: Maybe<Permissions_Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "users_permissions" */
export type Users_Permissions_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "users_permissions" */
export enum Users_Permissions_Select_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Permission = 'permission',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "users_permissions" */
export type Users_Permissions_Set_Input = {
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  permission?: Maybe<Permissions_Enum>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "users_permissions" */
export enum Users_Permissions_Update_Column {
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  Permission = 'permission',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** primary key columns input for table: "users" */
export type Users_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** columns and relationships of "users_search_media_files" */
export type Users_Search_Media_Files = {
  __typename?: 'users_search_media_files'
  id: Scalars['uuid']
  /** An object relationship */
  media_file: Media_Files
  media_file_id: Scalars['uuid']
  order: Scalars['Int']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** aggregated selection of "users_search_media_files" */
export type Users_Search_Media_Files_Aggregate = {
  __typename?: 'users_search_media_files_aggregate'
  aggregate?: Maybe<Users_Search_Media_Files_Aggregate_Fields>
  nodes: Array<Users_Search_Media_Files>
}

/** aggregate fields of "users_search_media_files" */
export type Users_Search_Media_Files_Aggregate_Fields = {
  __typename?: 'users_search_media_files_aggregate_fields'
  avg?: Maybe<Users_Search_Media_Files_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Users_Search_Media_Files_Max_Fields>
  min?: Maybe<Users_Search_Media_Files_Min_Fields>
  stddev?: Maybe<Users_Search_Media_Files_Stddev_Fields>
  stddev_pop?: Maybe<Users_Search_Media_Files_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Users_Search_Media_Files_Stddev_Samp_Fields>
  sum?: Maybe<Users_Search_Media_Files_Sum_Fields>
  var_pop?: Maybe<Users_Search_Media_Files_Var_Pop_Fields>
  var_samp?: Maybe<Users_Search_Media_Files_Var_Samp_Fields>
  variance?: Maybe<Users_Search_Media_Files_Variance_Fields>
}

/** aggregate fields of "users_search_media_files" */
export type Users_Search_Media_Files_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Search_Media_Files_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "users_search_media_files" */
export type Users_Search_Media_Files_Aggregate_Order_By = {
  avg?: Maybe<Users_Search_Media_Files_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Users_Search_Media_Files_Max_Order_By>
  min?: Maybe<Users_Search_Media_Files_Min_Order_By>
  stddev?: Maybe<Users_Search_Media_Files_Stddev_Order_By>
  stddev_pop?: Maybe<Users_Search_Media_Files_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Users_Search_Media_Files_Stddev_Samp_Order_By>
  sum?: Maybe<Users_Search_Media_Files_Sum_Order_By>
  var_pop?: Maybe<Users_Search_Media_Files_Var_Pop_Order_By>
  var_samp?: Maybe<Users_Search_Media_Files_Var_Samp_Order_By>
  variance?: Maybe<Users_Search_Media_Files_Variance_Order_By>
}

/** input type for inserting array relation for remote table "users_search_media_files" */
export type Users_Search_Media_Files_Arr_Rel_Insert_Input = {
  data: Array<Users_Search_Media_Files_Insert_Input>
  on_conflict?: Maybe<Users_Search_Media_Files_On_Conflict>
}

/** aggregate avg on columns */
export type Users_Search_Media_Files_Avg_Fields = {
  __typename?: 'users_search_media_files_avg_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "users_search_media_files" */
export type Users_Search_Media_Files_Avg_Order_By = {
  order?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "users_search_media_files". All fields are combined with a logical 'AND'. */
export type Users_Search_Media_Files_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Users_Search_Media_Files_Bool_Exp>>>
  _not?: Maybe<Users_Search_Media_Files_Bool_Exp>
  _or?: Maybe<Array<Maybe<Users_Search_Media_Files_Bool_Exp>>>
  id?: Maybe<Uuid_Comparison_Exp>
  media_file?: Maybe<Media_Files_Bool_Exp>
  media_file_id?: Maybe<Uuid_Comparison_Exp>
  order?: Maybe<Int_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "users_search_media_files" */
export enum Users_Search_Media_Files_Constraint {
  /** unique or primary key constraint */
  SearchPhotosPkey = 'search_photos_pkey',
  /** unique or primary key constraint */
  UsersSearchPhotosUserIdOrderKey = 'users_search_photos_user_id_order_key',
  /** unique or primary key constraint */
  UsersSearchPhotosUserIdPhotoIdKey = 'users_search_photos_user_id_photo_id_key'
}

/** input type for incrementing integer column in table "users_search_media_files" */
export type Users_Search_Media_Files_Inc_Input = {
  order?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "users_search_media_files" */
export type Users_Search_Media_Files_Insert_Input = {
  id?: Maybe<Scalars['uuid']>
  media_file?: Maybe<Media_Files_Obj_Rel_Insert_Input>
  media_file_id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Users_Search_Media_Files_Max_Fields = {
  __typename?: 'users_search_media_files_max_fields'
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "users_search_media_files" */
export type Users_Search_Media_Files_Max_Order_By = {
  id?: Maybe<Order_By>
  media_file_id?: Maybe<Order_By>
  order?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Users_Search_Media_Files_Min_Fields = {
  __typename?: 'users_search_media_files_min_fields'
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "users_search_media_files" */
export type Users_Search_Media_Files_Min_Order_By = {
  id?: Maybe<Order_By>
  media_file_id?: Maybe<Order_By>
  order?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "users_search_media_files" */
export type Users_Search_Media_Files_Mutation_Response = {
  __typename?: 'users_search_media_files_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Users_Search_Media_Files>
}

/** input type for inserting object relation for remote table "users_search_media_files" */
export type Users_Search_Media_Files_Obj_Rel_Insert_Input = {
  data: Users_Search_Media_Files_Insert_Input
  on_conflict?: Maybe<Users_Search_Media_Files_On_Conflict>
}

/** on conflict condition type for table "users_search_media_files" */
export type Users_Search_Media_Files_On_Conflict = {
  constraint: Users_Search_Media_Files_Constraint
  update_columns: Array<Users_Search_Media_Files_Update_Column>
  where?: Maybe<Users_Search_Media_Files_Bool_Exp>
}

/** ordering options when selecting data from "users_search_media_files" */
export type Users_Search_Media_Files_Order_By = {
  id?: Maybe<Order_By>
  media_file?: Maybe<Media_Files_Order_By>
  media_file_id?: Maybe<Order_By>
  order?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "users_search_media_files" */
export type Users_Search_Media_Files_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "users_search_media_files" */
export enum Users_Search_Media_Files_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  MediaFileId = 'media_file_id',
  /** column name */
  Order = 'order',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "users_search_media_files" */
export type Users_Search_Media_Files_Set_Input = {
  id?: Maybe<Scalars['uuid']>
  media_file_id?: Maybe<Scalars['uuid']>
  order?: Maybe<Scalars['Int']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate stddev on columns */
export type Users_Search_Media_Files_Stddev_Fields = {
  __typename?: 'users_search_media_files_stddev_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "users_search_media_files" */
export type Users_Search_Media_Files_Stddev_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Users_Search_Media_Files_Stddev_Pop_Fields = {
  __typename?: 'users_search_media_files_stddev_pop_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "users_search_media_files" */
export type Users_Search_Media_Files_Stddev_Pop_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Users_Search_Media_Files_Stddev_Samp_Fields = {
  __typename?: 'users_search_media_files_stddev_samp_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "users_search_media_files" */
export type Users_Search_Media_Files_Stddev_Samp_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Users_Search_Media_Files_Sum_Fields = {
  __typename?: 'users_search_media_files_sum_fields'
  order?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "users_search_media_files" */
export type Users_Search_Media_Files_Sum_Order_By = {
  order?: Maybe<Order_By>
}

/** update columns of table "users_search_media_files" */
export enum Users_Search_Media_Files_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  MediaFileId = 'media_file_id',
  /** column name */
  Order = 'order',
  /** column name */
  UserId = 'user_id'
}

/** aggregate var_pop on columns */
export type Users_Search_Media_Files_Var_Pop_Fields = {
  __typename?: 'users_search_media_files_var_pop_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "users_search_media_files" */
export type Users_Search_Media_Files_Var_Pop_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Users_Search_Media_Files_Var_Samp_Fields = {
  __typename?: 'users_search_media_files_var_samp_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "users_search_media_files" */
export type Users_Search_Media_Files_Var_Samp_Order_By = {
  order?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Users_Search_Media_Files_Variance_Fields = {
  __typename?: 'users_search_media_files_variance_fields'
  order?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "users_search_media_files" */
export type Users_Search_Media_Files_Variance_Order_By = {
  order?: Maybe<Order_By>
}

/** select columns of table "users" */
export enum Users_Select_Column {
  /** column name */
  AvatarBlurhash = 'avatar_blurhash',
  /** column name */
  AvatarUrl = 'avatar_url',
  /** column name */
  Bio = 'bio',
  /** column name */
  BlockedForSearch = 'blocked_for_search',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DisplayName = 'display_name',
  /** column name */
  HideInSearch = 'hide_in_search',
  /** column name */
  Id = 'id',
  /** column name */
  IsPhotographer = 'is_photographer',
  /** column name */
  LastSession = 'last_session',
  /** column name */
  Locale = 'locale',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Phone = 'phone',
  /** column name */
  Rating = 'rating',
  /** column name */
  Slogan = 'slogan',
  /** column name */
  Telegram = 'telegram',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Username = 'username',
  /** column name */
  WavesAddress = 'waves_address',
  /** column name */
  Whatsapp = 'whatsapp'
}

/** input type for updating data in table "users" */
export type Users_Set_Input = {
  avatar_blurhash?: Maybe<Scalars['String']>
  avatar_url?: Maybe<Scalars['String']>
  bio?: Maybe<Scalars['String']>
  blocked_for_search?: Maybe<Scalars['Boolean']>
  created_at?: Maybe<Scalars['timestamptz']>
  display_name?: Maybe<Scalars['String']>
  hide_in_search?: Maybe<Scalars['Boolean']>
  id?: Maybe<Scalars['uuid']>
  is_photographer?: Maybe<Scalars['Boolean']>
  last_session?: Maybe<Scalars['timestamptz']>
  locale?: Maybe<Locales_Enum>
  location_id?: Maybe<Scalars['uuid']>
  phone?: Maybe<Scalars['String']>
  rating?: Maybe<Scalars['numeric']>
  slogan?: Maybe<Scalars['String']>
  telegram?: Maybe<Scalars['String']>
  updated_at?: Maybe<Scalars['timestamptz']>
  username?: Maybe<Scalars['String']>
  waves_address?: Maybe<Scalars['String']>
  whatsapp?: Maybe<Scalars['String']>
}

/** columns and relationships of "users_stats" */
export type Users_Stats = {
  __typename?: 'users_stats'
  id: Scalars['uuid']
  likes: Scalars['Int']
  rating: Scalars['Int']
  total_files: Scalars['Int']
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** aggregated selection of "users_stats" */
export type Users_Stats_Aggregate = {
  __typename?: 'users_stats_aggregate'
  aggregate?: Maybe<Users_Stats_Aggregate_Fields>
  nodes: Array<Users_Stats>
}

/** aggregate fields of "users_stats" */
export type Users_Stats_Aggregate_Fields = {
  __typename?: 'users_stats_aggregate_fields'
  avg?: Maybe<Users_Stats_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Users_Stats_Max_Fields>
  min?: Maybe<Users_Stats_Min_Fields>
  stddev?: Maybe<Users_Stats_Stddev_Fields>
  stddev_pop?: Maybe<Users_Stats_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Users_Stats_Stddev_Samp_Fields>
  sum?: Maybe<Users_Stats_Sum_Fields>
  var_pop?: Maybe<Users_Stats_Var_Pop_Fields>
  var_samp?: Maybe<Users_Stats_Var_Samp_Fields>
  variance?: Maybe<Users_Stats_Variance_Fields>
}

/** aggregate fields of "users_stats" */
export type Users_Stats_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Stats_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "users_stats" */
export type Users_Stats_Aggregate_Order_By = {
  avg?: Maybe<Users_Stats_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Users_Stats_Max_Order_By>
  min?: Maybe<Users_Stats_Min_Order_By>
  stddev?: Maybe<Users_Stats_Stddev_Order_By>
  stddev_pop?: Maybe<Users_Stats_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Users_Stats_Stddev_Samp_Order_By>
  sum?: Maybe<Users_Stats_Sum_Order_By>
  var_pop?: Maybe<Users_Stats_Var_Pop_Order_By>
  var_samp?: Maybe<Users_Stats_Var_Samp_Order_By>
  variance?: Maybe<Users_Stats_Variance_Order_By>
}

/** input type for inserting array relation for remote table "users_stats" */
export type Users_Stats_Arr_Rel_Insert_Input = {
  data: Array<Users_Stats_Insert_Input>
  on_conflict?: Maybe<Users_Stats_On_Conflict>
}

/** aggregate avg on columns */
export type Users_Stats_Avg_Fields = {
  __typename?: 'users_stats_avg_fields'
  likes?: Maybe<Scalars['Float']>
  rating?: Maybe<Scalars['Float']>
  total_files?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "users_stats" */
export type Users_Stats_Avg_Order_By = {
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "users_stats". All fields are combined with a logical 'AND'. */
export type Users_Stats_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Users_Stats_Bool_Exp>>>
  _not?: Maybe<Users_Stats_Bool_Exp>
  _or?: Maybe<Array<Maybe<Users_Stats_Bool_Exp>>>
  id?: Maybe<Uuid_Comparison_Exp>
  likes?: Maybe<Int_Comparison_Exp>
  rating?: Maybe<Int_Comparison_Exp>
  total_files?: Maybe<Int_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "users_stats" */
export enum Users_Stats_Constraint {
  /** unique or primary key constraint */
  UserStatsPkey = 'user_stats_pkey',
  /** unique or primary key constraint */
  UserStatsUserIdKey = 'user_stats_user_id_key'
}

/** input type for incrementing integer column in table "users_stats" */
export type Users_Stats_Inc_Input = {
  likes?: Maybe<Scalars['Int']>
  rating?: Maybe<Scalars['Int']>
  total_files?: Maybe<Scalars['Int']>
}

/** input type for inserting data into table "users_stats" */
export type Users_Stats_Insert_Input = {
  id?: Maybe<Scalars['uuid']>
  likes?: Maybe<Scalars['Int']>
  rating?: Maybe<Scalars['Int']>
  total_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Users_Stats_Max_Fields = {
  __typename?: 'users_stats_max_fields'
  id?: Maybe<Scalars['uuid']>
  likes?: Maybe<Scalars['Int']>
  rating?: Maybe<Scalars['Int']>
  total_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "users_stats" */
export type Users_Stats_Max_Order_By = {
  id?: Maybe<Order_By>
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Users_Stats_Min_Fields = {
  __typename?: 'users_stats_min_fields'
  id?: Maybe<Scalars['uuid']>
  likes?: Maybe<Scalars['Int']>
  rating?: Maybe<Scalars['Int']>
  total_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "users_stats" */
export type Users_Stats_Min_Order_By = {
  id?: Maybe<Order_By>
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "users_stats" */
export type Users_Stats_Mutation_Response = {
  __typename?: 'users_stats_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Users_Stats>
}

/** input type for inserting object relation for remote table "users_stats" */
export type Users_Stats_Obj_Rel_Insert_Input = {
  data: Users_Stats_Insert_Input
  on_conflict?: Maybe<Users_Stats_On_Conflict>
}

/** on conflict condition type for table "users_stats" */
export type Users_Stats_On_Conflict = {
  constraint: Users_Stats_Constraint
  update_columns: Array<Users_Stats_Update_Column>
  where?: Maybe<Users_Stats_Bool_Exp>
}

/** ordering options when selecting data from "users_stats" */
export type Users_Stats_Order_By = {
  id?: Maybe<Order_By>
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "users_stats" */
export type Users_Stats_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "users_stats" */
export enum Users_Stats_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Likes = 'likes',
  /** column name */
  Rating = 'rating',
  /** column name */
  TotalFiles = 'total_files',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "users_stats" */
export type Users_Stats_Set_Input = {
  id?: Maybe<Scalars['uuid']>
  likes?: Maybe<Scalars['Int']>
  rating?: Maybe<Scalars['Int']>
  total_files?: Maybe<Scalars['Int']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate stddev on columns */
export type Users_Stats_Stddev_Fields = {
  __typename?: 'users_stats_stddev_fields'
  likes?: Maybe<Scalars['Float']>
  rating?: Maybe<Scalars['Float']>
  total_files?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "users_stats" */
export type Users_Stats_Stddev_Order_By = {
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Users_Stats_Stddev_Pop_Fields = {
  __typename?: 'users_stats_stddev_pop_fields'
  likes?: Maybe<Scalars['Float']>
  rating?: Maybe<Scalars['Float']>
  total_files?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "users_stats" */
export type Users_Stats_Stddev_Pop_Order_By = {
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Users_Stats_Stddev_Samp_Fields = {
  __typename?: 'users_stats_stddev_samp_fields'
  likes?: Maybe<Scalars['Float']>
  rating?: Maybe<Scalars['Float']>
  total_files?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "users_stats" */
export type Users_Stats_Stddev_Samp_Order_By = {
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Users_Stats_Sum_Fields = {
  __typename?: 'users_stats_sum_fields'
  likes?: Maybe<Scalars['Int']>
  rating?: Maybe<Scalars['Int']>
  total_files?: Maybe<Scalars['Int']>
}

/** order by sum() on columns of table "users_stats" */
export type Users_Stats_Sum_Order_By = {
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
}

/** update columns of table "users_stats" */
export enum Users_Stats_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  Likes = 'likes',
  /** column name */
  Rating = 'rating',
  /** column name */
  TotalFiles = 'total_files',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** aggregate var_pop on columns */
export type Users_Stats_Var_Pop_Fields = {
  __typename?: 'users_stats_var_pop_fields'
  likes?: Maybe<Scalars['Float']>
  rating?: Maybe<Scalars['Float']>
  total_files?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "users_stats" */
export type Users_Stats_Var_Pop_Order_By = {
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Users_Stats_Var_Samp_Fields = {
  __typename?: 'users_stats_var_samp_fields'
  likes?: Maybe<Scalars['Float']>
  rating?: Maybe<Scalars['Float']>
  total_files?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "users_stats" */
export type Users_Stats_Var_Samp_Order_By = {
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Users_Stats_Variance_Fields = {
  __typename?: 'users_stats_variance_fields'
  likes?: Maybe<Scalars['Float']>
  rating?: Maybe<Scalars['Float']>
  total_files?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "users_stats" */
export type Users_Stats_Variance_Order_By = {
  likes?: Maybe<Order_By>
  rating?: Maybe<Order_By>
  total_files?: Maybe<Order_By>
}

/** aggregate stddev on columns */
export type Users_Stddev_Fields = {
  __typename?: 'users_stddev_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "users" */
export type Users_Stddev_Order_By = {
  rating?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Users_Stddev_Pop_Fields = {
  __typename?: 'users_stddev_pop_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "users" */
export type Users_Stddev_Pop_Order_By = {
  rating?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Users_Stddev_Samp_Fields = {
  __typename?: 'users_stddev_samp_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "users" */
export type Users_Stddev_Samp_Order_By = {
  rating?: Maybe<Order_By>
}

/** columns and relationships of "users_styles" */
export type Users_Styles = {
  __typename?: 'users_styles'
  id: Scalars['uuid']
  /** An object relationship */
  style: Styles
  style_id: Scalars['uuid']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** aggregated selection of "users_styles" */
export type Users_Styles_Aggregate = {
  __typename?: 'users_styles_aggregate'
  aggregate?: Maybe<Users_Styles_Aggregate_Fields>
  nodes: Array<Users_Styles>
}

/** aggregate fields of "users_styles" */
export type Users_Styles_Aggregate_Fields = {
  __typename?: 'users_styles_aggregate_fields'
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Users_Styles_Max_Fields>
  min?: Maybe<Users_Styles_Min_Fields>
}

/** aggregate fields of "users_styles" */
export type Users_Styles_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Styles_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "users_styles" */
export type Users_Styles_Aggregate_Order_By = {
  count?: Maybe<Order_By>
  max?: Maybe<Users_Styles_Max_Order_By>
  min?: Maybe<Users_Styles_Min_Order_By>
}

/** input type for inserting array relation for remote table "users_styles" */
export type Users_Styles_Arr_Rel_Insert_Input = {
  data: Array<Users_Styles_Insert_Input>
  on_conflict?: Maybe<Users_Styles_On_Conflict>
}

/** Boolean expression to filter rows from the table "users_styles". All fields are combined with a logical 'AND'. */
export type Users_Styles_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Users_Styles_Bool_Exp>>>
  _not?: Maybe<Users_Styles_Bool_Exp>
  _or?: Maybe<Array<Maybe<Users_Styles_Bool_Exp>>>
  id?: Maybe<Uuid_Comparison_Exp>
  style?: Maybe<Styles_Bool_Exp>
  style_id?: Maybe<Uuid_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "users_styles" */
export enum Users_Styles_Constraint {
  /** unique or primary key constraint */
  UsersStylesPkey = 'users_styles_pkey',
  /** unique or primary key constraint */
  UsersStylesUserIdStyleIdKey = 'users_styles_user_id_style_id_key'
}

/** input type for inserting data into table "users_styles" */
export type Users_Styles_Insert_Input = {
  id?: Maybe<Scalars['uuid']>
  style?: Maybe<Styles_Obj_Rel_Insert_Input>
  style_id?: Maybe<Scalars['uuid']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Users_Styles_Max_Fields = {
  __typename?: 'users_styles_max_fields'
  id?: Maybe<Scalars['uuid']>
  style_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "users_styles" */
export type Users_Styles_Max_Order_By = {
  id?: Maybe<Order_By>
  style_id?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Users_Styles_Min_Fields = {
  __typename?: 'users_styles_min_fields'
  id?: Maybe<Scalars['uuid']>
  style_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "users_styles" */
export type Users_Styles_Min_Order_By = {
  id?: Maybe<Order_By>
  style_id?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "users_styles" */
export type Users_Styles_Mutation_Response = {
  __typename?: 'users_styles_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Users_Styles>
}

/** input type for inserting object relation for remote table "users_styles" */
export type Users_Styles_Obj_Rel_Insert_Input = {
  data: Users_Styles_Insert_Input
  on_conflict?: Maybe<Users_Styles_On_Conflict>
}

/** on conflict condition type for table "users_styles" */
export type Users_Styles_On_Conflict = {
  constraint: Users_Styles_Constraint
  update_columns: Array<Users_Styles_Update_Column>
  where?: Maybe<Users_Styles_Bool_Exp>
}

/** ordering options when selecting data from "users_styles" */
export type Users_Styles_Order_By = {
  id?: Maybe<Order_By>
  style?: Maybe<Styles_Order_By>
  style_id?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "users_styles" */
export type Users_Styles_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "users_styles" */
export enum Users_Styles_Select_Column {
  /** column name */
  Id = 'id',
  /** column name */
  StyleId = 'style_id',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "users_styles" */
export type Users_Styles_Set_Input = {
  id?: Maybe<Scalars['uuid']>
  style_id?: Maybe<Scalars['uuid']>
  user_id?: Maybe<Scalars['uuid']>
}

/** update columns of table "users_styles" */
export enum Users_Styles_Update_Column {
  /** column name */
  Id = 'id',
  /** column name */
  StyleId = 'style_id',
  /** column name */
  UserId = 'user_id'
}

/** aggregate sum on columns */
export type Users_Sum_Fields = {
  __typename?: 'users_sum_fields'
  rating?: Maybe<Scalars['numeric']>
}

/** order by sum() on columns of table "users" */
export type Users_Sum_Order_By = {
  rating?: Maybe<Order_By>
}

/** columns and relationships of "users_tariffs" */
export type Users_Tariffs = {
  __typename?: 'users_tariffs'
  auto_renewal: Scalars['Boolean']
  created_at: Scalars['timestamptz']
  end: Scalars['timestamptz']
  id: Scalars['uuid']
  length: Scalars['numeric']
  start: Scalars['timestamptz']
  /** An object relationship */
  tariff: Tariffs
  tariff_id: Scalars['uuid']
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** aggregated selection of "users_tariffs" */
export type Users_Tariffs_Aggregate = {
  __typename?: 'users_tariffs_aggregate'
  aggregate?: Maybe<Users_Tariffs_Aggregate_Fields>
  nodes: Array<Users_Tariffs>
}

/** aggregate fields of "users_tariffs" */
export type Users_Tariffs_Aggregate_Fields = {
  __typename?: 'users_tariffs_aggregate_fields'
  avg?: Maybe<Users_Tariffs_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Users_Tariffs_Max_Fields>
  min?: Maybe<Users_Tariffs_Min_Fields>
  stddev?: Maybe<Users_Tariffs_Stddev_Fields>
  stddev_pop?: Maybe<Users_Tariffs_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Users_Tariffs_Stddev_Samp_Fields>
  sum?: Maybe<Users_Tariffs_Sum_Fields>
  var_pop?: Maybe<Users_Tariffs_Var_Pop_Fields>
  var_samp?: Maybe<Users_Tariffs_Var_Samp_Fields>
  variance?: Maybe<Users_Tariffs_Variance_Fields>
}

/** aggregate fields of "users_tariffs" */
export type Users_Tariffs_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Users_Tariffs_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "users_tariffs" */
export type Users_Tariffs_Aggregate_Order_By = {
  avg?: Maybe<Users_Tariffs_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Users_Tariffs_Max_Order_By>
  min?: Maybe<Users_Tariffs_Min_Order_By>
  stddev?: Maybe<Users_Tariffs_Stddev_Order_By>
  stddev_pop?: Maybe<Users_Tariffs_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Users_Tariffs_Stddev_Samp_Order_By>
  sum?: Maybe<Users_Tariffs_Sum_Order_By>
  var_pop?: Maybe<Users_Tariffs_Var_Pop_Order_By>
  var_samp?: Maybe<Users_Tariffs_Var_Samp_Order_By>
  variance?: Maybe<Users_Tariffs_Variance_Order_By>
}

/** input type for inserting array relation for remote table "users_tariffs" */
export type Users_Tariffs_Arr_Rel_Insert_Input = {
  data: Array<Users_Tariffs_Insert_Input>
  on_conflict?: Maybe<Users_Tariffs_On_Conflict>
}

/** aggregate avg on columns */
export type Users_Tariffs_Avg_Fields = {
  __typename?: 'users_tariffs_avg_fields'
  length?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "users_tariffs" */
export type Users_Tariffs_Avg_Order_By = {
  length?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "users_tariffs". All fields are combined with a logical 'AND'. */
export type Users_Tariffs_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Users_Tariffs_Bool_Exp>>>
  _not?: Maybe<Users_Tariffs_Bool_Exp>
  _or?: Maybe<Array<Maybe<Users_Tariffs_Bool_Exp>>>
  auto_renewal?: Maybe<Boolean_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  end?: Maybe<Timestamptz_Comparison_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  length?: Maybe<Numeric_Comparison_Exp>
  start?: Maybe<Timestamptz_Comparison_Exp>
  tariff?: Maybe<Tariffs_Bool_Exp>
  tariff_id?: Maybe<Uuid_Comparison_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "users_tariffs" */
export enum Users_Tariffs_Constraint {
  /** unique or primary key constraint */
  UsersTariffsPkey = 'users_tariffs_pkey'
}

/** input type for incrementing integer column in table "users_tariffs" */
export type Users_Tariffs_Inc_Input = {
  length?: Maybe<Scalars['numeric']>
}

/** input type for inserting data into table "users_tariffs" */
export type Users_Tariffs_Insert_Input = {
  auto_renewal?: Maybe<Scalars['Boolean']>
  created_at?: Maybe<Scalars['timestamptz']>
  end?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  length?: Maybe<Scalars['numeric']>
  start?: Maybe<Scalars['timestamptz']>
  tariff?: Maybe<Tariffs_Obj_Rel_Insert_Input>
  tariff_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Users_Tariffs_Max_Fields = {
  __typename?: 'users_tariffs_max_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  end?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  length?: Maybe<Scalars['numeric']>
  start?: Maybe<Scalars['timestamptz']>
  tariff_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "users_tariffs" */
export type Users_Tariffs_Max_Order_By = {
  created_at?: Maybe<Order_By>
  end?: Maybe<Order_By>
  id?: Maybe<Order_By>
  length?: Maybe<Order_By>
  start?: Maybe<Order_By>
  tariff_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Users_Tariffs_Min_Fields = {
  __typename?: 'users_tariffs_min_fields'
  created_at?: Maybe<Scalars['timestamptz']>
  end?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  length?: Maybe<Scalars['numeric']>
  start?: Maybe<Scalars['timestamptz']>
  tariff_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "users_tariffs" */
export type Users_Tariffs_Min_Order_By = {
  created_at?: Maybe<Order_By>
  end?: Maybe<Order_By>
  id?: Maybe<Order_By>
  length?: Maybe<Order_By>
  start?: Maybe<Order_By>
  tariff_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "users_tariffs" */
export type Users_Tariffs_Mutation_Response = {
  __typename?: 'users_tariffs_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Users_Tariffs>
}

/** input type for inserting object relation for remote table "users_tariffs" */
export type Users_Tariffs_Obj_Rel_Insert_Input = {
  data: Users_Tariffs_Insert_Input
  on_conflict?: Maybe<Users_Tariffs_On_Conflict>
}

/** on conflict condition type for table "users_tariffs" */
export type Users_Tariffs_On_Conflict = {
  constraint: Users_Tariffs_Constraint
  update_columns: Array<Users_Tariffs_Update_Column>
  where?: Maybe<Users_Tariffs_Bool_Exp>
}

/** ordering options when selecting data from "users_tariffs" */
export type Users_Tariffs_Order_By = {
  auto_renewal?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  end?: Maybe<Order_By>
  id?: Maybe<Order_By>
  length?: Maybe<Order_By>
  start?: Maybe<Order_By>
  tariff?: Maybe<Tariffs_Order_By>
  tariff_id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "users_tariffs" */
export type Users_Tariffs_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "users_tariffs" */
export enum Users_Tariffs_Select_Column {
  /** column name */
  AutoRenewal = 'auto_renewal',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  End = 'end',
  /** column name */
  Id = 'id',
  /** column name */
  Length = 'length',
  /** column name */
  Start = 'start',
  /** column name */
  TariffId = 'tariff_id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "users_tariffs" */
export type Users_Tariffs_Set_Input = {
  auto_renewal?: Maybe<Scalars['Boolean']>
  created_at?: Maybe<Scalars['timestamptz']>
  end?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  length?: Maybe<Scalars['numeric']>
  start?: Maybe<Scalars['timestamptz']>
  tariff_id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate stddev on columns */
export type Users_Tariffs_Stddev_Fields = {
  __typename?: 'users_tariffs_stddev_fields'
  length?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "users_tariffs" */
export type Users_Tariffs_Stddev_Order_By = {
  length?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Users_Tariffs_Stddev_Pop_Fields = {
  __typename?: 'users_tariffs_stddev_pop_fields'
  length?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "users_tariffs" */
export type Users_Tariffs_Stddev_Pop_Order_By = {
  length?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Users_Tariffs_Stddev_Samp_Fields = {
  __typename?: 'users_tariffs_stddev_samp_fields'
  length?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "users_tariffs" */
export type Users_Tariffs_Stddev_Samp_Order_By = {
  length?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Users_Tariffs_Sum_Fields = {
  __typename?: 'users_tariffs_sum_fields'
  length?: Maybe<Scalars['numeric']>
}

/** order by sum() on columns of table "users_tariffs" */
export type Users_Tariffs_Sum_Order_By = {
  length?: Maybe<Order_By>
}

/** update columns of table "users_tariffs" */
export enum Users_Tariffs_Update_Column {
  /** column name */
  AutoRenewal = 'auto_renewal',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  End = 'end',
  /** column name */
  Id = 'id',
  /** column name */
  Length = 'length',
  /** column name */
  Start = 'start',
  /** column name */
  TariffId = 'tariff_id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** aggregate var_pop on columns */
export type Users_Tariffs_Var_Pop_Fields = {
  __typename?: 'users_tariffs_var_pop_fields'
  length?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "users_tariffs" */
export type Users_Tariffs_Var_Pop_Order_By = {
  length?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Users_Tariffs_Var_Samp_Fields = {
  __typename?: 'users_tariffs_var_samp_fields'
  length?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "users_tariffs" */
export type Users_Tariffs_Var_Samp_Order_By = {
  length?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Users_Tariffs_Variance_Fields = {
  __typename?: 'users_tariffs_variance_fields'
  length?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "users_tariffs" */
export type Users_Tariffs_Variance_Order_By = {
  length?: Maybe<Order_By>
}

/** update columns of table "users" */
export enum Users_Update_Column {
  /** column name */
  AvatarBlurhash = 'avatar_blurhash',
  /** column name */
  AvatarUrl = 'avatar_url',
  /** column name */
  Bio = 'bio',
  /** column name */
  BlockedForSearch = 'blocked_for_search',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  DisplayName = 'display_name',
  /** column name */
  HideInSearch = 'hide_in_search',
  /** column name */
  Id = 'id',
  /** column name */
  IsPhotographer = 'is_photographer',
  /** column name */
  LastSession = 'last_session',
  /** column name */
  Locale = 'locale',
  /** column name */
  LocationId = 'location_id',
  /** column name */
  Phone = 'phone',
  /** column name */
  Rating = 'rating',
  /** column name */
  Slogan = 'slogan',
  /** column name */
  Telegram = 'telegram',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  Username = 'username',
  /** column name */
  WavesAddress = 'waves_address',
  /** column name */
  Whatsapp = 'whatsapp'
}

/** aggregate var_pop on columns */
export type Users_Var_Pop_Fields = {
  __typename?: 'users_var_pop_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "users" */
export type Users_Var_Pop_Order_By = {
  rating?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Users_Var_Samp_Fields = {
  __typename?: 'users_var_samp_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "users" */
export type Users_Var_Samp_Order_By = {
  rating?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Users_Variance_Fields = {
  __typename?: 'users_variance_fields'
  rating?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "users" */
export type Users_Variance_Order_By = {
  rating?: Maybe<Order_By>
}

/** expression to compare columns of type uuid. All fields are combined with logical 'AND'. */
export type Uuid_Comparison_Exp = {
  _eq?: Maybe<Scalars['uuid']>
  _gt?: Maybe<Scalars['uuid']>
  _gte?: Maybe<Scalars['uuid']>
  _in?: Maybe<Array<Scalars['uuid']>>
  _is_null?: Maybe<Scalars['Boolean']>
  _lt?: Maybe<Scalars['uuid']>
  _lte?: Maybe<Scalars['uuid']>
  _neq?: Maybe<Scalars['uuid']>
  _nin?: Maybe<Array<Scalars['uuid']>>
}

/** columns and relationships of "wallets" */
export type Wallets = {
  __typename?: 'wallets'
  balance: Scalars['numeric']
  created_at: Scalars['timestamptz']
  /** An array relationship */
  credit_cards: Array<Credit_Cards>
  /** An aggregated array relationship */
  credit_cards_aggregate: Credit_Cards_Aggregate
  id: Scalars['uuid']
  /** An array relationship */
  input_transactions: Array<Transactions>
  /** An aggregated array relationship */
  input_transactions_aggregate: Transactions_Aggregate
  /** An array relationship */
  output_transactions: Array<Transactions>
  /** An aggregated array relationship */
  output_transactions_aggregate: Transactions_Aggregate
  /** A computed field, executes function "wallet_transactions" */
  transactions?: Maybe<Array<Transactions>>
  updated_at: Scalars['timestamptz']
  /** An object relationship */
  user: Users
  user_id: Scalars['uuid']
}

/** columns and relationships of "wallets" */
export type WalletsCredit_CardsArgs = {
  distinct_on?: Maybe<Array<Credit_Cards_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Credit_Cards_Order_By>>
  where?: Maybe<Credit_Cards_Bool_Exp>
}

/** columns and relationships of "wallets" */
export type WalletsCredit_Cards_AggregateArgs = {
  distinct_on?: Maybe<Array<Credit_Cards_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Credit_Cards_Order_By>>
  where?: Maybe<Credit_Cards_Bool_Exp>
}

/** columns and relationships of "wallets" */
export type WalletsInput_TransactionsArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** columns and relationships of "wallets" */
export type WalletsInput_Transactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** columns and relationships of "wallets" */
export type WalletsOutput_TransactionsArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** columns and relationships of "wallets" */
export type WalletsOutput_Transactions_AggregateArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** columns and relationships of "wallets" */
export type WalletsTransactionsArgs = {
  distinct_on?: Maybe<Array<Transactions_Select_Column>>
  limit?: Maybe<Scalars['Int']>
  offset?: Maybe<Scalars['Int']>
  order_by?: Maybe<Array<Transactions_Order_By>>
  where?: Maybe<Transactions_Bool_Exp>
}

/** aggregated selection of "wallets" */
export type Wallets_Aggregate = {
  __typename?: 'wallets_aggregate'
  aggregate?: Maybe<Wallets_Aggregate_Fields>
  nodes: Array<Wallets>
}

/** aggregate fields of "wallets" */
export type Wallets_Aggregate_Fields = {
  __typename?: 'wallets_aggregate_fields'
  avg?: Maybe<Wallets_Avg_Fields>
  count?: Maybe<Scalars['Int']>
  max?: Maybe<Wallets_Max_Fields>
  min?: Maybe<Wallets_Min_Fields>
  stddev?: Maybe<Wallets_Stddev_Fields>
  stddev_pop?: Maybe<Wallets_Stddev_Pop_Fields>
  stddev_samp?: Maybe<Wallets_Stddev_Samp_Fields>
  sum?: Maybe<Wallets_Sum_Fields>
  var_pop?: Maybe<Wallets_Var_Pop_Fields>
  var_samp?: Maybe<Wallets_Var_Samp_Fields>
  variance?: Maybe<Wallets_Variance_Fields>
}

/** aggregate fields of "wallets" */
export type Wallets_Aggregate_FieldsCountArgs = {
  columns?: Maybe<Array<Wallets_Select_Column>>
  distinct?: Maybe<Scalars['Boolean']>
}

/** order by aggregate values of table "wallets" */
export type Wallets_Aggregate_Order_By = {
  avg?: Maybe<Wallets_Avg_Order_By>
  count?: Maybe<Order_By>
  max?: Maybe<Wallets_Max_Order_By>
  min?: Maybe<Wallets_Min_Order_By>
  stddev?: Maybe<Wallets_Stddev_Order_By>
  stddev_pop?: Maybe<Wallets_Stddev_Pop_Order_By>
  stddev_samp?: Maybe<Wallets_Stddev_Samp_Order_By>
  sum?: Maybe<Wallets_Sum_Order_By>
  var_pop?: Maybe<Wallets_Var_Pop_Order_By>
  var_samp?: Maybe<Wallets_Var_Samp_Order_By>
  variance?: Maybe<Wallets_Variance_Order_By>
}

/** input type for inserting array relation for remote table "wallets" */
export type Wallets_Arr_Rel_Insert_Input = {
  data: Array<Wallets_Insert_Input>
  on_conflict?: Maybe<Wallets_On_Conflict>
}

/** aggregate avg on columns */
export type Wallets_Avg_Fields = {
  __typename?: 'wallets_avg_fields'
  balance?: Maybe<Scalars['Float']>
}

/** order by avg() on columns of table "wallets" */
export type Wallets_Avg_Order_By = {
  balance?: Maybe<Order_By>
}

/** Boolean expression to filter rows from the table "wallets". All fields are combined with a logical 'AND'. */
export type Wallets_Bool_Exp = {
  _and?: Maybe<Array<Maybe<Wallets_Bool_Exp>>>
  _not?: Maybe<Wallets_Bool_Exp>
  _or?: Maybe<Array<Maybe<Wallets_Bool_Exp>>>
  balance?: Maybe<Numeric_Comparison_Exp>
  created_at?: Maybe<Timestamptz_Comparison_Exp>
  credit_cards?: Maybe<Credit_Cards_Bool_Exp>
  id?: Maybe<Uuid_Comparison_Exp>
  input_transactions?: Maybe<Transactions_Bool_Exp>
  output_transactions?: Maybe<Transactions_Bool_Exp>
  updated_at?: Maybe<Timestamptz_Comparison_Exp>
  user?: Maybe<Users_Bool_Exp>
  user_id?: Maybe<Uuid_Comparison_Exp>
}

/** unique or primary key constraints on table "wallets" */
export enum Wallets_Constraint {
  /** unique or primary key constraint */
  WalletsPkey = 'wallets_pkey',
  /** unique or primary key constraint */
  WalletsUserIdKey = 'wallets_user_id_key'
}

/** input type for incrementing integer column in table "wallets" */
export type Wallets_Inc_Input = {
  balance?: Maybe<Scalars['numeric']>
}

/** input type for inserting data into table "wallets" */
export type Wallets_Insert_Input = {
  balance?: Maybe<Scalars['numeric']>
  created_at?: Maybe<Scalars['timestamptz']>
  credit_cards?: Maybe<Credit_Cards_Arr_Rel_Insert_Input>
  id?: Maybe<Scalars['uuid']>
  input_transactions?: Maybe<Transactions_Arr_Rel_Insert_Input>
  output_transactions?: Maybe<Transactions_Arr_Rel_Insert_Input>
  updated_at?: Maybe<Scalars['timestamptz']>
  user?: Maybe<Users_Obj_Rel_Insert_Input>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate max on columns */
export type Wallets_Max_Fields = {
  __typename?: 'wallets_max_fields'
  balance?: Maybe<Scalars['numeric']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by max() on columns of table "wallets" */
export type Wallets_Max_Order_By = {
  balance?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** aggregate min on columns */
export type Wallets_Min_Fields = {
  __typename?: 'wallets_min_fields'
  balance?: Maybe<Scalars['numeric']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** order by min() on columns of table "wallets" */
export type Wallets_Min_Order_By = {
  balance?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  id?: Maybe<Order_By>
  updated_at?: Maybe<Order_By>
  user_id?: Maybe<Order_By>
}

/** response of any mutation on the table "wallets" */
export type Wallets_Mutation_Response = {
  __typename?: 'wallets_mutation_response'
  /** number of affected rows by the mutation */
  affected_rows: Scalars['Int']
  /** data of the affected rows by the mutation */
  returning: Array<Wallets>
}

/** input type for inserting object relation for remote table "wallets" */
export type Wallets_Obj_Rel_Insert_Input = {
  data: Wallets_Insert_Input
  on_conflict?: Maybe<Wallets_On_Conflict>
}

/** on conflict condition type for table "wallets" */
export type Wallets_On_Conflict = {
  constraint: Wallets_Constraint
  update_columns: Array<Wallets_Update_Column>
  where?: Maybe<Wallets_Bool_Exp>
}

/** ordering options when selecting data from "wallets" */
export type Wallets_Order_By = {
  balance?: Maybe<Order_By>
  created_at?: Maybe<Order_By>
  credit_cards_aggregate?: Maybe<Credit_Cards_Aggregate_Order_By>
  id?: Maybe<Order_By>
  input_transactions_aggregate?: Maybe<Transactions_Aggregate_Order_By>
  output_transactions_aggregate?: Maybe<Transactions_Aggregate_Order_By>
  updated_at?: Maybe<Order_By>
  user?: Maybe<Users_Order_By>
  user_id?: Maybe<Order_By>
}

/** primary key columns input for table: "wallets" */
export type Wallets_Pk_Columns_Input = {
  id: Scalars['uuid']
}

/** select columns of table "wallets" */
export enum Wallets_Select_Column {
  /** column name */
  Balance = 'balance',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** input type for updating data in table "wallets" */
export type Wallets_Set_Input = {
  balance?: Maybe<Scalars['numeric']>
  created_at?: Maybe<Scalars['timestamptz']>
  id?: Maybe<Scalars['uuid']>
  updated_at?: Maybe<Scalars['timestamptz']>
  user_id?: Maybe<Scalars['uuid']>
}

/** aggregate stddev on columns */
export type Wallets_Stddev_Fields = {
  __typename?: 'wallets_stddev_fields'
  balance?: Maybe<Scalars['Float']>
}

/** order by stddev() on columns of table "wallets" */
export type Wallets_Stddev_Order_By = {
  balance?: Maybe<Order_By>
}

/** aggregate stddev_pop on columns */
export type Wallets_Stddev_Pop_Fields = {
  __typename?: 'wallets_stddev_pop_fields'
  balance?: Maybe<Scalars['Float']>
}

/** order by stddev_pop() on columns of table "wallets" */
export type Wallets_Stddev_Pop_Order_By = {
  balance?: Maybe<Order_By>
}

/** aggregate stddev_samp on columns */
export type Wallets_Stddev_Samp_Fields = {
  __typename?: 'wallets_stddev_samp_fields'
  balance?: Maybe<Scalars['Float']>
}

/** order by stddev_samp() on columns of table "wallets" */
export type Wallets_Stddev_Samp_Order_By = {
  balance?: Maybe<Order_By>
}

/** aggregate sum on columns */
export type Wallets_Sum_Fields = {
  __typename?: 'wallets_sum_fields'
  balance?: Maybe<Scalars['numeric']>
}

/** order by sum() on columns of table "wallets" */
export type Wallets_Sum_Order_By = {
  balance?: Maybe<Order_By>
}

/** update columns of table "wallets" */
export enum Wallets_Update_Column {
  /** column name */
  Balance = 'balance',
  /** column name */
  CreatedAt = 'created_at',
  /** column name */
  Id = 'id',
  /** column name */
  UpdatedAt = 'updated_at',
  /** column name */
  UserId = 'user_id'
}

/** aggregate var_pop on columns */
export type Wallets_Var_Pop_Fields = {
  __typename?: 'wallets_var_pop_fields'
  balance?: Maybe<Scalars['Float']>
}

/** order by var_pop() on columns of table "wallets" */
export type Wallets_Var_Pop_Order_By = {
  balance?: Maybe<Order_By>
}

/** aggregate var_samp on columns */
export type Wallets_Var_Samp_Fields = {
  __typename?: 'wallets_var_samp_fields'
  balance?: Maybe<Scalars['Float']>
}

/** order by var_samp() on columns of table "wallets" */
export type Wallets_Var_Samp_Order_By = {
  balance?: Maybe<Order_By>
}

/** aggregate variance on columns */
export type Wallets_Variance_Fields = {
  __typename?: 'wallets_variance_fields'
  balance?: Maybe<Scalars['Float']>
}

/** order by variance() on columns of table "wallets" */
export type Wallets_Variance_Order_By = {
  balance?: Maybe<Order_By>
}

export type AdminPagesSubscriptionVariables = Exact<{ [key: string]: never }>

export type AdminPagesSubscription = { __typename?: 'subscription_root' } & {
  pages: Array<{ __typename?: 'pages' } & PageWithLocalesFragment>
}

export type AdminGetPagesQueryVariables = Exact<{ [key: string]: never }>

export type AdminGetPagesQuery = { __typename?: 'query_root' } & {
  pages: Array<{ __typename?: 'pages' } & PageWithLocalesFragment>
}

export type AdminGetPageQueryVariables = Exact<{
  id: Scalars['uuid']
}>

export type AdminGetPageQuery = { __typename?: 'query_root' } & {
  page?: Maybe<{ __typename?: 'pages' } & FullPageFragment>
}

export type AdminUpdatePageMutationVariables = Exact<{
  object: Pages_Insert_Input
  on_conflict: Pages_On_Conflict
}>

export type AdminUpdatePageMutation = { __typename?: 'mutation_root' } & {
  page?: Maybe<{ __typename?: 'pages' } & FullPageFragment>
}

export type AdminDeletePageMutationVariables = Exact<{
  id: Scalars['uuid']
}>

export type AdminDeletePageMutation = { __typename?: 'mutation_root' } & {
  delete_pages_by_pk?: Maybe<{ __typename?: 'pages' } & Pick<Pages, 'id'>>
}

export type AdminAddMenuPageMutationVariables = Exact<{
  pageId: Scalars['uuid']
  order: Scalars['Int']
}>

export type AdminAddMenuPageMutation = { __typename?: 'mutation_root' } & {
  insert_menu_pages_one?: Maybe<
    { __typename?: 'menu_pages' } & MenuPageFragment
  >
}

export type AdminDeleteMenuPageMutationVariables = Exact<{
  id: Scalars['uuid']
}>

export type AdminDeleteMenuPageMutation = { __typename?: 'mutation_root' } & {
  delete_menu_pages_by_pk?: Maybe<
    { __typename?: 'menu_pages' } & Pick<Menu_Pages, 'id'>
  >
}

export type AdminGetMenuPagesQueryVariables = Exact<{ [key: string]: never }>

export type AdminGetMenuPagesQuery = { __typename?: 'query_root' } & {
  menu: Array<
    { __typename?: 'menu_pages' } & Pick<
      Menu_Pages,
      'id' | 'order' | 'page_id'
    > & {
        page: { __typename?: 'pages' } & Pick<Pages, 'name' | 'id' | 'active'>
      }
  >
}

export type AdminUpdateOrderMenuPagesMutationVariables = Exact<{
  objects: Array<Menu_Pages_Insert_Input>
}>

export type AdminUpdateOrderMenuPagesMutation = {
  __typename?: 'mutation_root'
} & {
  insert_menu_pages?: Maybe<
    { __typename?: 'menu_pages_mutation_response' } & Pick<
      Menu_Pages_Mutation_Response,
      'affected_rows'
    >
  >
}

export type AdminMenuItemsQueryVariables = Exact<{
  type: Menu_Type_Enum
}>

export type AdminMenuItemsQuery = { __typename?: 'query_root' } & {
  menu: Array<{ __typename?: 'menu_items' } & MenuItemWithLocalizationFragment>
}

export type AdminAddMenuItemMutationVariables = Exact<{
  item: Menu_Items_Insert_Input
}>

export type AdminAddMenuItemMutation = { __typename?: 'mutation_root' } & {
  insert_menu_items_one?: Maybe<
    { __typename?: 'menu_items' } & MenuItemWithLocalizationFragment
  >
}

export type AdminUpdateOrderMenuItemsMutationVariables = Exact<{
  objects: Array<Menu_Items_Insert_Input>
}>

export type AdminUpdateOrderMenuItemsMutation = {
  __typename?: 'mutation_root'
} & {
  insert_menu_items?: Maybe<
    { __typename?: 'menu_items_mutation_response' } & Pick<
      Menu_Items_Mutation_Response,
      'affected_rows'
    >
  >
}

export type AdminDeleteMenuItemMutationVariables = Exact<{
  id: Scalars['uuid']
}>

export type AdminDeleteMenuItemMutation = { __typename?: 'mutation_root' } & {
  delete_menu_items_by_pk?: Maybe<
    { __typename?: 'menu_items' } & Pick<Menu_Items, 'id'>
  >
}

export type AdminDeletePageContentBlockMutationVariables = Exact<{
  id: Scalars['uuid']
}>

export type AdminDeletePageContentBlockMutation = {
  __typename?: 'mutation_root'
} & {
  delete_pages_contents_blocks_by_pk?: Maybe<
    { __typename?: 'pages_contents_blocks' } & Pick<Pages_Contents_Blocks, 'id'>
  >
}

export type AdminGetCoverImagesQueryVariables = Exact<{ [key: string]: never }>

export type AdminGetCoverImagesQuery = { __typename?: 'query_root' } & {
  coverImages: Array<
    { __typename?: 'cover_images' } & Pick<
      Cover_Images,
      'id' | 'url' | 'blurhash' | 'desktop_active' | 'mobile_active'
    >
  >
}

export type AdminAddCoverImagesMutationVariables = Exact<{
  item: Cover_Images_Insert_Input
}>

export type AdminAddCoverImagesMutation = { __typename?: 'mutation_root' } & {
  insert_cover_images_one?: Maybe<
    { __typename?: 'cover_images' } & CoverImageFragment
  >
}

export type AdminDeleteCoverImagesMutationVariables = Exact<{
  id: Scalars['uuid']
}>

export type AdminDeleteCoverImagesMutation = {
  __typename?: 'mutation_root'
} & {
  delete_cover_images_by_pk?: Maybe<
    { __typename?: 'cover_images' } & Pick<Cover_Images, 'id'>
  >
}

export type AdminEditUserMutationVariables = Exact<{
  id: Scalars['uuid']
  blocked: Scalars['Boolean']
}>

export type AdminEditUserMutation = { __typename?: 'mutation_root' } & {
  update_users_by_pk?: Maybe<{ __typename?: 'users' } & FullUserFragment>
}

export type SeoFragment = { __typename: 'seo' } & Pick<
  Seo,
  'id' | 'image' | 'keywords' | 'description' | 'title'
>

export type PageFragment = { __typename: 'pages' } & Pick<
  Pages,
  'id' | 'slug' | 'name' | 'active'
>

export type PageContentFragment = { __typename: 'pages_contents' } & Pick<
  Pages_Contents,
  'id' | 'locale' | 'name_in_menu' | 'page_id' | 'title' | 'content'
>

export type PageContentBlockFragment = {
  __typename: 'pages_contents_blocks'
} & Pick<
  Pages_Contents_Blocks,
  | 'id'
  | 'page_content_id'
  | 'title'
  | 'hash'
  | 'content'
  | 'order'
  | 'menu_label'
  | 'menu_link'
  | 'active'
>

export type FullPageFragment = { __typename?: 'pages' } & {
  pages_contents: Array<
    { __typename?: 'pages_contents' } & {
      seo: { __typename?: 'seo' } & SeoFragment
      pages_contents_blocks: Array<
        { __typename?: 'pages_contents_blocks' } & PageContentBlockFragment
      >
    } & PageContentFragment
  >
} & PageFragment

export type PageWithLocalesFragment = { __typename?: 'pages' } & {
  pages_contents: Array<
    { __typename: 'pages_contents' } & Pick<Pages_Contents, 'id' | 'locale'>
  >
} & PageFragment

export type MenuPageFragment = { __typename: 'menu_pages' } & Pick<
  Menu_Pages,
  'id' | 'order' | 'page_id'
>

export type MenuItemFragment = { __typename: 'menu_items' } & Pick<
  Menu_Items,
  'id' | 'order' | 'url' | 'external' | 'type'
>

export type MenuItemWithLocalizationFragment = { __typename?: 'menu_items' } & {
  localizations: Array<{ __typename?: 'localizations' } & LocalizationFragment>
} & MenuItemFragment

export type TransactionFragment = { __typename: 'transactions' } & Pick<
  Transactions,
  | 'album_id'
  | 'amount'
  | 'comment'
  | 'created_at'
  | 'data'
  | 'event_id'
  | 'from_wallet_id'
  | 'id'
  | 'status'
  | 'to_wallet_id'
  | 'type'
  | 'updated_at'
> & {
    from_wallet?: Maybe<
      { __typename?: 'wallets' } & {
        user: { __typename?: 'users' } & UserFragment
      } & WalletFragment
    >
    to_wallet: { __typename?: 'wallets' } & {
      user: { __typename?: 'users' } & UserFragment
    } & WalletFragment
  }

export type NetworkTransactionFragment = {
  __typename: 'network_transactions'
} & Pick<
  Network_Transactions,
  | 'tx_hash'
  | 'transaction_id'
  | 'created_at'
  | 'status'
  | 'crypto_amount'
  | 'usd_amount'
  | 'data'
>

export type WalletFragment = { __typename: 'wallets' } & Pick<
  Wallets,
  'user_id' | 'id' | 'created_at' | 'updated_at' | 'balance'
>

export type ConfigFragment = { __typename?: 'config' } & Pick<
  Config,
  'key' | 'value'
>

export type MediaFileFragment = { __typename: 'media_files' } & Pick<
  Media_Files,
  'id' | 'preview_url' | 'blurhash'
>

export type FavoriteFragment = { __typename: 'favorites' } & Pick<
  Favorites,
  'id' | 'owner_id' | 'user_id' | 'album_id' | 'media_file_id'
>

export type StorageFragment = { __typename: 'storages_view' } & Pick<
  Storages_View,
  'id' | 'user_id' | 'used_bytes' | 'available_bytes'
>

export type UserTariffFragment = { __typename: 'users_tariffs' } & Pick<
  Users_Tariffs,
  | 'tariff_id'
  | 'id'
  | 'auto_renewal'
  | 'created_at'
  | 'end'
  | 'length'
  | 'start'
  | 'user_id'
>

export type TariffFragment = { __typename: 'tariffs' } & Pick<
  Tariffs,
  'id' | 'active' | 'created_at' | 'name' | 'price' | 'space' | 'updated_at'
>

export type LocationFragment = { __typename: 'locations' } & Pick<
  Locations,
  'id' | 'name' | 'geo' | 'display_name' | 'city' | 'country' | 'user_id'
>

export type GeocodingFragment = { __typename?: 'GeoOutput' } & Pick<
  GeoOutput,
  | 'city'
  | 'name'
  | 'display_name'
  | 'address_line1'
  | 'address_line2'
  | 'postcode'
  | 'region'
  | 'result_type'
  | 'state'
  | 'country'
  | 'country_code'
  | 'county'
  | 'geo'
>

export type LocalizationFragment = { __typename: 'localizations' } & Pick<
  Localizations,
  'id' | 'locale' | 'value'
>

export type StyleFragment = { __typename: 'styles' } & Pick<
  Styles,
  'id' | 'name'
>

export type CoverImageFragment = { __typename: 'cover_images' } & Pick<
  Cover_Images,
  'id' | 'url' | 'blurhash' | 'desktop_active' | 'mobile_active'
>

export type AlbumFragment = { __typename: 'albums' } & Pick<
  Albums,
  | 'id'
  | 'name'
  | 'comment'
  | 'user_id'
  | 'location_id'
  | 'total_media_files'
  | 'created_at'
  | 'private'
> & {
    user: { __typename?: 'users' } & UserFragment
    statistics?: Maybe<
      { __typename?: 'albums_statistics_view' } & StatisticsFragment
    >
    location?: Maybe<
      { __typename?: 'locations' } & LocationFragment & LocationFragment
    >
    cover?: Maybe<{ __typename?: 'media_files' } & MediaFileFragment>
    tags: Array<
      { __typename?: 'albums_tags' } & {
        tag: { __typename?: 'tags' } & TagFragment
      } & AlbumTagFragment
    >
    payment_types: Array<
      { __typename?: 'albums_payment_types' } & AlbumPaymentTypeFragment
    >
  }

export type StatisticsFragment = {
  __typename: 'albums_statistics_view'
} & Pick<
  Albums_Statistics_View,
  | 'album_id'
  | 'downloads'
  | 'income'
  | 'visits'
  | 'new_visits'
  | 'new_downloads'
>

export type AlbumTagFragment = { __typename: 'albums_tags' } & Pick<
  Albums_Tags,
  'album_id' | 'tag_id'
>

export type TagFragment = { __typename: 'tags' } & Pick<Tags, 'id' | 'tag'>

export type AlbumPaymentTypeFragment = {
  __typename: 'albums_payment_types'
} & Pick<
  Albums_Payment_Types,
  'id' | 'album_id' | 'price' | 'payment_type' | 'data'
>

export type EventFragment = { __typename: 'events' } & Pick<
  Events,
  | 'id'
  | 'private'
  | 'name'
  | 'comment'
  | 'note'
  | 'created_at'
  | 'updated_at'
  | 'date'
  | 'retouching'
  | 'time_for_upload'
  | 'total_media_files'
  | 'uploaded_time'
  | 'cover_media_file_id'
  | 'location_id'
  | 'user_id'
  | 'customer_id'
> & {
    cover?: Maybe<{ __typename?: 'media_files' } & MediaFileFragment>
    location?: Maybe<{ __typename?: 'locations' } & LocationFragment>
    user: { __typename?: 'users' } & UserFragment
    customer: { __typename?: 'users' } & UserFragment
    payment_types: Array<
      { __typename?: 'events_payment_types' } & EventPaymentTypeFragment
    >
  }

export type EventPaymentTypeFragment = {
  __typename: 'events_payment_types'
} & Pick<
  Events_Payment_Types,
  'id' | 'event_id' | 'price' | 'payment_type' | 'data'
>

export type StoreFragment = { __typename: 'store' } & Pick<
  Store,
  | 'id'
  | 'category'
  | 'type'
  | 'total_media_files'
  | 'name'
  | 'description'
  | 'nft'
  | 'phone'
  | 'quantity'
  | 'duration'
  | 'date_start'
  | 'date_end'
  | 'price'
  | 'currency'
  | 'user_id'
  | 'cover_media_file_id'
  | 'location_id'
  | 'created_at'
  | 'updated_at'
> & {
    user: { __typename?: 'users' } & UserFragment
    cover?: Maybe<{ __typename?: 'media_files' } & MediaFileFragment>
    location?: Maybe<{ __typename?: 'locations' } & LocationFragment>
  }

export type AccountFragment = { __typename: 'auth_accounts' } & Pick<
  Auth_Accounts,
  'id' | 'email' | 'active' | 'default_role'
> & {
    account_roles: Array<
      { __typename?: 'auth_account_roles' } & Pick<Auth_Account_Roles, 'role'>
    >
  }

export type UserFragment = { __typename: 'users' } & Pick<
  Users,
  | 'id'
  | 'display_name'
  | 'username'
  | 'slogan'
  | 'bio'
  | 'avatar_url'
  | 'avatar_blurhash'
  | 'created_at'
  | 'is_photographer'
  | 'is_favorite'
  | 'waves_address'
>

export type FullUserFragment = { __typename?: 'users' } & Pick<
  Users,
  'blocked_for_search' | 'hide_in_search'
> &
  UserFragment

export type CurrentUserFragment = { __typename?: 'users' } & Pick<
  Users,
  'phone' | 'telegram' | 'whatsapp' | 'location_id'
> & {
    account?: Maybe<{ __typename?: 'auth_accounts' } & AccountFragment>
    location?: Maybe<{ __typename?: 'locations' } & LocationFragment>
    styles: Array<
      { __typename?: 'users_styles' } & {
        style: { __typename?: 'styles' } & StyleFragment
      } & UserStyleFragment
    >
    payment_types: Array<
      { __typename?: 'users_payment_types' } & UserPaymentTypeFragment
    >
    storage?: Maybe<
      { __typename?: 'storages_view' } & Pick<
        Storages_View,
        'available_bytes' | 'used_bytes'
      >
    >
    favorites_aggregate: { __typename?: 'favorites_aggregate' } & {
      aggregate?: Maybe<
        { __typename?: 'favorites_aggregate_fields' } & Pick<
          Favorites_Aggregate_Fields,
          'count'
        >
      >
    }
    in_favorites_aggregate: { __typename?: 'favorites_aggregate' } & {
      aggregate?: Maybe<
        { __typename?: 'favorites_aggregate_fields' } & Pick<
          Favorites_Aggregate_Fields,
          'count'
        >
      >
    }
    media_search: Array<
      { __typename?: 'users_search_media_files' } & UserSearchMediaFileFragment
    >
    media_search_aggregate: {
      __typename?: 'users_search_media_files_aggregate'
    } & {
      aggregate?: Maybe<
        { __typename?: 'users_search_media_files_aggregate_fields' } & Pick<
          Users_Search_Media_Files_Aggregate_Fields,
          'count'
        >
      >
    }
  } & FullUserFragment

export type SearchUserFragment = { __typename?: 'users' } & {
  location?: Maybe<{ __typename?: 'locations' } & LocationFragment>
  in_favorites_aggregate: { __typename?: 'favorites_aggregate' } & {
    aggregate?: Maybe<
      { __typename?: 'favorites_aggregate_fields' } & Pick<
        Favorites_Aggregate_Fields,
        'count'
      >
    >
  }
  media_files_in_search: Array<
    { __typename?: 'users_search_media_files' } & {
      media_file: { __typename?: 'media_files' } & MediaFileFragment
    }
  >
} & FullUserFragment

export type UserLocationFragment = { __typename?: 'users' } & Pick<
  Users,
  'location_id'
> & { location?: Maybe<{ __typename?: 'locations' } & LocationFragment> }

export type UserPaymentTypeFragment = {
  __typename: 'users_payment_types'
} & Pick<Users_Payment_Types, 'id' | 'payment_type' | 'price'>

export type UserSearchMediaFileFragment = {
  __typename: 'users_search_media_files'
} & Pick<Users_Search_Media_Files, 'order' | 'id' | 'media_file_id'> & {
    media_file: { __typename?: 'media_files' } & MediaFileFragment
  }

export type UserSearchMediaFilesFragment = { __typename?: 'users' } & {
  media_files_in_search: Array<
    { __typename?: 'users_search_media_files' } & UserSearchMediaFileFragment
  >
}

export type UserStyleFragment = { __typename: 'users_styles' } & Pick<
  Users_Styles,
  'id' | 'style_id' | 'user_id'
>

export type AddToFavoriteMutationVariables = Exact<{
  object: Favorites_Insert_Input
}>

export type AddToFavoriteMutation = { __typename?: 'mutation_root' } & {
  insert_favorites_one?: Maybe<
    { __typename?: 'favorites' } & {
      user?: Maybe<{ __typename: 'users' } & Pick<Users, 'id' | 'is_favorite'>>
      media_file?: Maybe<
        { __typename: 'media_files' } & Pick<Media_Files, 'id' | 'is_favorite'>
      >
      album?: Maybe<
        { __typename: 'albums' } & Pick<Albums, 'id' | 'is_favorite'>
      >
    }
  >
}

export type CreateLocationMutationVariables = Exact<{
  object: Locations_Insert_Input
}>

export type CreateLocationMutation = { __typename?: 'mutation_root' } & {
  location?: Maybe<{ __typename?: 'locations' } & LocationFragment>
}

export type CreateStoreMutationVariables = Exact<{
  object: Store_Insert_Input
}>

export type CreateStoreMutation = { __typename?: 'mutation_root' } & {
  store?: Maybe<{ __typename?: 'store' } & StoreFragment>
}

export type CreateTagMutationVariables = Exact<{
  objects: Array<Tags_Insert_Input>
}>

export type CreateTagMutation = { __typename?: 'mutation_root' } & {
  insert_tags?: Maybe<
    { __typename?: 'tags_mutation_response' } & Pick<
      Tags_Mutation_Response,
      'affected_rows'
    > & { returning: Array<{ __typename?: 'tags' } & Pick<Tags, 'tag' | 'id'>> }
  >
}

export type DeleteMediaMutationVariables = Exact<{
  id: Scalars['uuid']
}>

export type DeleteMediaMutation = { __typename?: 'mutation_root' } & {
  delete_media_files?: Maybe<
    { __typename?: 'media_files_mutation_response' } & {
      returning: Array<{ __typename?: 'media_files' } & MediaFileFragment>
    }
  >
}

export type CreateAlbumMutationVariables = Exact<{
  object: Albums_Insert_Input
  payment: Array<Albums_Payment_Types_Insert_Input>
}>

export type CreateAlbumMutation = { __typename?: 'mutation_root' } & {
  album?: Maybe<{ __typename?: 'albums' } & AlbumFragment>
  payment?: Maybe<
    { __typename?: 'albums_payment_types_mutation_response' } & {
      returning: Array<
        { __typename?: 'albums_payment_types' } & AlbumPaymentTypeFragment
      >
    }
  >
}

export type CreateEventMutationVariables = Exact<{
  object: Events_Insert_Input
  payment: Array<Events_Payment_Types_Insert_Input>
}>

export type CreateEventMutation = { __typename?: 'mutation_root' } & {
  event?: Maybe<{ __typename?: 'events' } & EventFragment>
  payment?: Maybe<
    { __typename?: 'events_payment_types_mutation_response' } & {
      returning: Array<
        { __typename?: 'events_payment_types' } & EventPaymentTypeFragment
      >
    }
  >
}

export type DeleteAlbumMutationVariables = Exact<{
  id: Scalars['uuid']
}>

export type DeleteAlbumMutation = { __typename?: 'mutation_root' } & {
  delete_albums_by_pk?: Maybe<{ __typename?: 'albums' } & Pick<Albums, 'id'>>
}

export type DeleteEventMutationVariables = Exact<{
  id: Scalars['uuid']
}>

export type DeleteEventMutation = { __typename?: 'mutation_root' } & {
  delete_events_by_pk?: Maybe<{ __typename?: 'events' } & Pick<Events, 'id'>>
}

export type UpdateAlbumMutationVariables = Exact<{
  id: Scalars['uuid']
  object: Albums_Set_Input
  payment: Array<Albums_Payment_Types_Insert_Input>
  tags: Array<Albums_Tags_Insert_Input>
}>

export type UpdateAlbumMutation = { __typename?: 'mutation_root' } & {
  album?: Maybe<{ __typename?: 'albums' } & AlbumFragment>
  delete_albums_payment_types?: Maybe<
    { __typename?: 'albums_payment_types_mutation_response' } & Pick<
      Albums_Payment_Types_Mutation_Response,
      'affected_rows'
    >
  >
  insert_albums_payment_types?: Maybe<
    { __typename?: 'albums_payment_types_mutation_response' } & {
      returning: Array<
        { __typename?: 'albums_payment_types' } & AlbumPaymentTypeFragment
      >
    }
  >
}

export type UpdateCoverEventMutationVariables = Exact<{
  id: Scalars['uuid']
  object: Events_Set_Input
}>

export type UpdateCoverEventMutation = { __typename?: 'mutation_root' } & {
  event?: Maybe<{ __typename?: 'events' } & EventFragment>
}

export type UpdateCoverAlbumMutationVariables = Exact<{
  id: Scalars['uuid']
  object: Albums_Set_Input
}>

export type UpdateCoverAlbumMutation = { __typename?: 'mutation_root' } & {
  album?: Maybe<{ __typename?: 'albums' } & AlbumFragment>
}

export type UpdateEventMutationVariables = Exact<{
  id: Scalars['uuid']
  object: Events_Set_Input
  payment: Array<Events_Payment_Types_Insert_Input>
}>

export type UpdateEventMutation = { __typename?: 'mutation_root' } & {
  event?: Maybe<{ __typename?: 'events' } & EventFragment>
  delete_events_payment_types?: Maybe<
    { __typename?: 'events_payment_types_mutation_response' } & Pick<
      Events_Payment_Types_Mutation_Response,
      'affected_rows'
    >
  >
  insert_events_payment_types?: Maybe<
    { __typename?: 'events_payment_types_mutation_response' } & {
      returning: Array<
        { __typename?: 'events_payment_types' } & EventPaymentTypeFragment
      >
    }
  >
}

export type RemoveFromFavoriteMutationVariables = Exact<{
  where: Favorites_Bool_Exp
}>

export type RemoveFromFavoriteMutation = { __typename?: 'mutation_root' } & {
  delete_favorites?: Maybe<
    { __typename?: 'favorites_mutation_response' } & Pick<
      Favorites_Mutation_Response,
      'affected_rows'
    > & {
        returning: Array<
          { __typename?: 'favorites' } & {
            user?: Maybe<
              { __typename: 'users' } & Pick<Users, 'id' | 'is_favorite'>
            >
            media_file?: Maybe<
              { __typename: 'media_files' } & Pick<
                Media_Files,
                'id' | 'is_favorite'
              >
            >
            album?: Maybe<
              { __typename: 'albums' } & Pick<Albums, 'id' | 'is_favorite'>
            >
          }
        >
      }
  >
}

export type UpdateSettingsPaymentTypesMutationVariables = Exact<{
  objects: Array<Users_Payment_Types_Insert_Input>
  forDelete: Array<Scalars['uuid']>
  delete: Scalars['Boolean']
}>

export type UpdateSettingsPaymentTypesMutation = {
  __typename?: 'mutation_root'
} & {
  insert_users_payment_types?: Maybe<
    { __typename?: 'users_payment_types_mutation_response' } & Pick<
      Users_Payment_Types_Mutation_Response,
      'affected_rows'
    >
  >
  delete_users_payment_types?: Maybe<
    { __typename?: 'users_payment_types_mutation_response' } & Pick<
      Users_Payment_Types_Mutation_Response,
      'affected_rows'
    >
  >
}

export type UpdateSettingsSearchPhotosMutationVariables = Exact<{
  userId: Scalars['uuid']
  objects: Array<Users_Search_Media_Files_Insert_Input>
}>

export type UpdateSettingsSearchPhotosMutation = {
  __typename?: 'mutation_root'
} & {
  delete_users_search_media_files?: Maybe<
    { __typename?: 'users_search_media_files_mutation_response' } & Pick<
      Users_Search_Media_Files_Mutation_Response,
      'affected_rows'
    >
  >
  insert_users_search_media_files?: Maybe<
    { __typename?: 'users_search_media_files_mutation_response' } & {
      returning: Array<
        {
          __typename?: 'users_search_media_files'
        } & UserSearchMediaFileFragment
      >
    }
  >
}

export type UpdateSettingsStylesMutationVariables = Exact<{
  objects: Array<Users_Styles_Insert_Input>
  forDelete: Array<Scalars['uuid']>
  insert: Scalars['Boolean']
  delete: Scalars['Boolean']
}>

export type UpdateSettingsStylesMutation = { __typename?: 'mutation_root' } & {
  insert_users_styles?: Maybe<
    { __typename?: 'users_styles_mutation_response' } & Pick<
      Users_Styles_Mutation_Response,
      'affected_rows'
    >
  >
  delete_users_styles?: Maybe<
    { __typename?: 'users_styles_mutation_response' } & Pick<
      Users_Styles_Mutation_Response,
      'affected_rows'
    >
  >
}

export type UpdateCurrentUserMutationVariables = Exact<{
  userId: Scalars['uuid']
  user: Users_Set_Input
  email: Scalars['citext']
  styles: Array<Users_Styles_Insert_Input>
}>

export type UpdateCurrentUserMutation = { __typename?: 'mutation_root' } & {
  update_users_by_pk?: Maybe<{ __typename?: 'users' } & CurrentUserFragment>
  update_auth_accounts?: Maybe<
    { __typename?: 'auth_accounts_mutation_response' } & {
      returning: Array<{ __typename?: 'auth_accounts' } & AccountFragment>
    }
  >
  delete_users_styles?: Maybe<
    { __typename?: 'users_styles_mutation_response' } & Pick<
      Users_Styles_Mutation_Response,
      'affected_rows'
    >
  >
  insert_users_styles?: Maybe<
    { __typename?: 'users_styles_mutation_response' } & {
      returning: Array<
        { __typename?: 'users_styles' } & {
          style: { __typename?: 'styles' } & StyleFragment & StyleFragment
        } & UserStyleFragment &
          UserStyleFragment
      >
    }
  >
}

export type UpdateUserWavesTokenMutationVariables = Exact<{
  userId: Scalars['uuid']
  waves_address: Scalars['String']
}>

export type UpdateUserWavesTokenMutation = { __typename?: 'mutation_root' } & {
  update_users_by_pk?: Maybe<{ __typename?: 'users' } & CurrentUserFragment>
}

export type GetWalletQueryVariables = Exact<{
  userId: Scalars['uuid']
}>

export type GetWalletQuery = { __typename?: 'query_root' } & {
  wallets: Array<{ __typename?: 'wallets' } & WalletFragment>
}

export type GetCurrentUserQueryVariables = Exact<{
  userId: Scalars['uuid']
}>

export type GetCurrentUserQuery = { __typename?: 'query_root' } & {
  user?: Maybe<{ __typename?: 'users' } & CurrentUserFragment>
}

export type GetPageQueryVariables = Exact<{
  locales: Array<Locales_Enum>
  slug: Scalars['String']
}>

export type GetPageQuery = { __typename?: 'query_root' } & {
  pages: Array<
    { __typename?: 'pages' } & {
      pages_contents: Array<
        { __typename?: 'pages_contents' } & {
          seo: { __typename?: 'seo' } & SeoFragment
          pages_contents_blocks: Array<
            { __typename?: 'pages_contents_blocks' } & PageContentBlockFragment
          >
        } & PageContentFragment
      >
    } & PageFragment
  >
}

export type GetMenuPagesQueryVariables = Exact<{
  locales: Array<Locales_Enum>
}>

export type GetMenuPagesQuery = { __typename?: 'query_root' } & {
  menu: Array<
    { __typename?: 'menu_pages' } & {
      page: { __typename?: 'pages' } & {
        pages_contents: Array<
          { __typename: 'pages_contents' } & Pick<
            Pages_Contents,
            'id' | 'name_in_menu'
          > & {
              pages_contents_blocks: Array<
                { __typename: 'pages_contents_blocks' } & Pick<
                  Pages_Contents_Blocks,
                  'id' | 'hash' | 'menu_label' | 'menu_link' | 'active'
                >
              >
            }
        >
      } & PageFragment
    } & MenuPageFragment
  >
}

export type GetMenuItemsQueryVariables = Exact<{
  types: Array<Menu_Type_Enum>
  locales: Array<Locales_Enum>
}>

export type GetMenuItemsQuery = { __typename?: 'query_root' } & {
  menu: Array<
    { __typename?: 'menu_items' } & {
      localizations: Array<
        { __typename?: 'localizations' } & LocalizationFragment
      >
    } & MenuItemFragment
  >
}

export type GetTransactionsQueryVariables = Exact<{
  where?: Maybe<Transactions_Bool_Exp>
  order_by?: Maybe<Array<Transactions_Order_By>>
  limit: Scalars['Int']
  offset: Scalars['Int']
}>

export type GetTransactionsQuery = { __typename?: 'query_root' } & {
  transactions: Array<{ __typename?: 'transactions' } & TransactionFragment>
}

export type GetConfigsQueryVariables = Exact<{
  in: Array<Config_Keys_Enum>
}>

export type GetConfigsQuery = { __typename?: 'query_root' } & {
  config: Array<{ __typename?: 'config' } & ConfigFragment>
}

export type GetDefaultSeoQueryVariables = Exact<{
  locales: Array<Locales_Enum>
}>

export type GetDefaultSeoQuery = { __typename?: 'query_root' } & {
  pages: Array<
    { __typename?: 'pages' } & {
      pages_contents: Array<
        { __typename?: 'pages_contents' } & {
          seo: { __typename?: 'seo' } & SeoFragment
        }
      >
    }
  >
}

export type CoverImagesQueryVariables = Exact<{ [key: string]: never }>

export type CoverImagesQuery = { __typename?: 'query_root' } & {
  cover_images: Array<{ __typename?: 'cover_images' } & CoverImageFragment>
}

export type GetUsersListQueryVariables = Exact<{ [key: string]: never }>

export type GetUsersListQuery = { __typename?: 'query_root' } & {
  users: Array<{ __typename?: 'users' } & Pick<Users, 'id' | 'username'>>
}

export type GetAlbumQueryVariables = Exact<{
  id: Scalars['uuid']
}>

export type GetAlbumQuery = { __typename?: 'query_root' } & {
  album?: Maybe<{ __typename?: 'albums' } & AlbumFragment>
}

export type GetEventQueryVariables = Exact<{
  id: Scalars['uuid']
}>

export type GetEventQuery = { __typename?: 'query_root' } & {
  event?: Maybe<{ __typename?: 'events' } & EventFragment>
}

export type GetUserFavoritesQueryVariables = Exact<{
  userId: Scalars['uuid']
}>

export type GetUserFavoritesQuery = { __typename?: 'query_root' } & {
  favorites: Array<
    { __typename?: 'favorites' } & {
      user?: Maybe<
        { __typename?: 'users' } & UserFragment &
          UserLocationFragment &
          UserSearchMediaFilesFragment
      >
    }
  >
}

export type GeoReverseQueryVariables = Exact<{
  lat: Scalars['Float']
  lng: Scalars['Float']
  lang: Scalars['String']
  type?: Maybe<Scalars['String']>
}>

export type GeoReverseQuery = { __typename?: 'query_root' } & {
  geo_reverse?: Maybe<Array<{ __typename?: 'GeoOutput' } & GeocodingFragment>>
}

export type GeoAutocompleteQueryVariables = Exact<{
  text: Scalars['String']
  lang: Scalars['String']
  type?: Maybe<Scalars['String']>
}>

export type GeoAutocompleteQuery = { __typename?: 'query_root' } & {
  geo_autocomplete?: Maybe<
    Array<{ __typename?: 'GeoOutput' } & GeocodingFragment>
  >
}

export type GetMediaQueryVariables = Exact<{
  where: Media_Files_Bool_Exp
  limit: Scalars['Int']
  offset: Scalars['Int']
  order_by?: Maybe<Array<Media_Files_Order_By>>
}>

export type GetMediaQuery = { __typename?: 'query_root' } & {
  media_files: Array<{ __typename?: 'media_files' } & MediaFileFragment>
  count: { __typename?: 'media_files_aggregate' } & {
    aggregate?: Maybe<
      { __typename?: 'media_files_aggregate_fields' } & Pick<
        Media_Files_Aggregate_Fields,
        'count'
      >
    >
  }
}

export type GetUserPaymentQueryVariables = Exact<{
  id: Scalars['uuid']
}>

export type GetUserPaymentQuery = { __typename?: 'query_root' } & {
  users_payment_types: Array<
    { __typename?: 'users_payment_types' } & Pick<
      Users_Payment_Types,
      'price' | 'payment_type' | 'id'
    >
  >
}

export type GetProfileQueryVariables = Exact<{
  username?: Scalars['String']
}>

export type GetProfileQuery = { __typename?: 'query_root' } & {
  user: Array<
    { __typename?: 'users' } & UserFragment &
      UserLocationFragment &
      UserSearchMediaFilesFragment
  >
}

export type GetSearchPhotographersQueryVariables = Exact<{
  where: Users_Bool_Exp
  limit: Scalars['Int']
  offset: Scalars['Int']
  orderBy?: Maybe<Array<Users_Order_By>>
}>

export type GetSearchPhotographersQuery = { __typename?: 'query_root' } & {
  search_photographers: Array<{ __typename?: 'users' } & SearchUserFragment>
  count: { __typename?: 'users_aggregate' } & {
    aggregate?: Maybe<
      { __typename?: 'users_aggregate_fields' } & Pick<
        Users_Aggregate_Fields,
        'count'
      >
    >
  }
}

export type GetSearchPhotographersLocationQueryVariables = Exact<{
  args: Search_Photographers_By_Location_Args
}>

export type GetSearchPhotographersLocationQuery = {
  __typename?: 'query_root'
} & {
  search_photographers_by_location: Array<
    { __typename?: 'users' } & Pick<Users, 'id' | 'display_name'>
  >
}

export type GetStoreQueryVariables = Exact<{
  id: Scalars['uuid']
}>

export type GetStoreQuery = { __typename?: 'query_root' } & {
  store?: Maybe<{ __typename?: 'store' } & StoreFragment>
}

export type GetUserAlbumsQueryVariables = Exact<{
  userId: Scalars['uuid']
}>

export type GetUserAlbumsQuery = { __typename?: 'query_root' } & {
  albums: Array<{ __typename?: 'albums' } & AlbumFragment>
}

export type GetUserStyleQueryVariables = Exact<{
  userId: Scalars['uuid']
}>

export type GetUserStyleQuery = { __typename?: 'query_root' } & {
  users_styles: Array<
    { __typename?: 'users_styles' } & {
      style: { __typename?: 'styles' } & StyleFragment
    } & UserStyleFragment
  >
}

export type GetStyleQueryVariables = Exact<{ [key: string]: never }>

export type GetStyleQuery = { __typename?: 'query_root' } & {
  styles: Array<{ __typename?: 'styles' } & StyleFragment>
}

export type GetClientsQueryVariables = Exact<{
  userId: Scalars['uuid']
}>

export type GetClientsQuery = { __typename?: 'query_root' } & {
  users: Array<{ __typename?: 'users' } & UserFragment>
}

export type GetUsersQueryVariables = Exact<{
  where?: Users_Bool_Exp
  offset: Scalars['Int']
  limit: Scalars['Int']
}>

export type GetUsersQuery = { __typename?: 'query_root' } & {
  users: Array<{ __typename?: 'users' } & CurrentUserFragment>
  users_aggregate: { __typename?: 'users_aggregate' } & {
    aggregate?: Maybe<
      { __typename?: 'users_aggregate_fields' } & Pick<
        Users_Aggregate_Fields,
        'count'
      >
    >
  }
}

export type GetUserPublicQueryVariables = Exact<{
  where?: Users_Bool_Exp
}>

export type GetUserPublicQuery = { __typename?: 'query_root' } & {
  users: Array<{ __typename?: 'users' } & SearchUserFragment>
}

export type AlbumsSubscriptionVariables = Exact<{
  limit: Scalars['Int']
  offset: Scalars['Int']
  userId: Scalars['uuid']
  order_by?: Maybe<Array<Albums_Order_By>>
}>

export type AlbumsSubscription = { __typename?: 'subscription_root' } & {
  albums: Array<{ __typename?: 'albums' } & AlbumFragment>
}

export type CurrentUserSubscriptionVariables = Exact<{
  userId: Scalars['uuid']
}>

export type CurrentUserSubscription = { __typename?: 'subscription_root' } & {
  user?: Maybe<{ __typename?: 'users' } & CurrentUserFragment>
}

export type EventsSubscriptionVariables = Exact<{
  limit: Scalars['Int']
  offset: Scalars['Int']
  userId: Scalars['uuid']
  order_by?: Maybe<Array<Events_Order_By>>
}>

export type EventsSubscription = { __typename?: 'subscription_root' } & {
  events: Array<{ __typename?: 'events' } & EventFragment>
}

export type MediaSubscriptionVariables = Exact<{
  where: Media_Files_Bool_Exp
  limit: Scalars['Int']
  offset: Scalars['Int']
  order_by?: Maybe<Array<Media_Files_Order_By>>
}>

export type MediaSubscription = { __typename?: 'subscription_root' } & {
  media_files: Array<
    { __typename?: 'media_files' } & {
      users_search_photos: Array<
        { __typename?: 'users_search_media_files' } & Pick<
          Users_Search_Media_Files,
          'id' | 'order'
        >
      >
    } & MediaFileFragment
  >
}

export type StoreSubscriptionVariables = Exact<{
  limit: Scalars['Int']
  offset: Scalars['Int']
  userId: Scalars['uuid']
  type?: Maybe<Store_Types_Enum_Comparison_Exp>
  order_by?: Maybe<Array<Store_Order_By>>
}>

export type StoreSubscription = { __typename?: 'subscription_root' } & {
  store: Array<{ __typename?: 'store' } & StoreFragment>
}

export type TransactionsSubscriptionVariables = Exact<{
  offset: Scalars['Int']
  where: Transactions_Bool_Exp
}>

export type TransactionsSubscription = { __typename?: 'subscription_root' } & {
  transactions: Array<
    { __typename?: 'transactions' } & {
      network_transactions: Array<
        { __typename?: 'network_transactions' } & NetworkTransactionFragment
      >
    } & TransactionFragment
  >
}

export type WalletSubscriptionVariables = Exact<{
  userId: Scalars['uuid']
}>

export type WalletSubscription = { __typename?: 'subscription_root' } & {
  wallets: Array<{ __typename?: 'wallets' } & WalletFragment>
}
