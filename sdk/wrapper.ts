import * as Types from './types'

import { GraphQLClient } from 'graphql-request'
import { print } from 'graphql'
import * as Operations from './operations'

export type SdkFunctionWrapper = <T>(action: () => Promise<T>) => Promise<T>

const defaultWrapper: SdkFunctionWrapper = sdkFunction => sdkFunction()
export function getSdk(
  client: GraphQLClient,
  withWrapper: SdkFunctionWrapper = defaultWrapper
) {
  return {
    adminPages(
      variables?: Types.AdminPagesSubscriptionVariables
    ): Promise<Types.AdminPagesSubscription> {
      return withWrapper(() =>
        client.request<Types.AdminPagesSubscription>(
          print(Operations.AdminPagesDocument),
          variables
        )
      )
    },
    adminGetPages(
      variables?: Types.AdminGetPagesQueryVariables
    ): Promise<Types.AdminGetPagesQuery> {
      return withWrapper(() =>
        client.request<Types.AdminGetPagesQuery>(
          print(Operations.AdminGetPagesDocument),
          variables
        )
      )
    },
    adminGetPage(
      variables: Types.AdminGetPageQueryVariables
    ): Promise<Types.AdminGetPageQuery> {
      return withWrapper(() =>
        client.request<Types.AdminGetPageQuery>(
          print(Operations.AdminGetPageDocument),
          variables
        )
      )
    },
    adminUpdatePage(
      variables: Types.AdminUpdatePageMutationVariables
    ): Promise<Types.AdminUpdatePageMutation> {
      return withWrapper(() =>
        client.request<Types.AdminUpdatePageMutation>(
          print(Operations.AdminUpdatePageDocument),
          variables
        )
      )
    },
    adminDeletePage(
      variables: Types.AdminDeletePageMutationVariables
    ): Promise<Types.AdminDeletePageMutation> {
      return withWrapper(() =>
        client.request<Types.AdminDeletePageMutation>(
          print(Operations.AdminDeletePageDocument),
          variables
        )
      )
    },
    adminAddMenuPage(
      variables: Types.AdminAddMenuPageMutationVariables
    ): Promise<Types.AdminAddMenuPageMutation> {
      return withWrapper(() =>
        client.request<Types.AdminAddMenuPageMutation>(
          print(Operations.AdminAddMenuPageDocument),
          variables
        )
      )
    },
    adminDeleteMenuPage(
      variables: Types.AdminDeleteMenuPageMutationVariables
    ): Promise<Types.AdminDeleteMenuPageMutation> {
      return withWrapper(() =>
        client.request<Types.AdminDeleteMenuPageMutation>(
          print(Operations.AdminDeleteMenuPageDocument),
          variables
        )
      )
    },
    adminGetMenuPages(
      variables?: Types.AdminGetMenuPagesQueryVariables
    ): Promise<Types.AdminGetMenuPagesQuery> {
      return withWrapper(() =>
        client.request<Types.AdminGetMenuPagesQuery>(
          print(Operations.AdminGetMenuPagesDocument),
          variables
        )
      )
    },
    adminUpdateOrderMenuPages(
      variables: Types.AdminUpdateOrderMenuPagesMutationVariables
    ): Promise<Types.AdminUpdateOrderMenuPagesMutation> {
      return withWrapper(() =>
        client.request<Types.AdminUpdateOrderMenuPagesMutation>(
          print(Operations.AdminUpdateOrderMenuPagesDocument),
          variables
        )
      )
    },
    adminMenuItems(
      variables: Types.AdminMenuItemsQueryVariables
    ): Promise<Types.AdminMenuItemsQuery> {
      return withWrapper(() =>
        client.request<Types.AdminMenuItemsQuery>(
          print(Operations.AdminMenuItemsDocument),
          variables
        )
      )
    },
    adminAddMenuItem(
      variables: Types.AdminAddMenuItemMutationVariables
    ): Promise<Types.AdminAddMenuItemMutation> {
      return withWrapper(() =>
        client.request<Types.AdminAddMenuItemMutation>(
          print(Operations.AdminAddMenuItemDocument),
          variables
        )
      )
    },
    adminUpdateOrderMenuItems(
      variables: Types.AdminUpdateOrderMenuItemsMutationVariables
    ): Promise<Types.AdminUpdateOrderMenuItemsMutation> {
      return withWrapper(() =>
        client.request<Types.AdminUpdateOrderMenuItemsMutation>(
          print(Operations.AdminUpdateOrderMenuItemsDocument),
          variables
        )
      )
    },
    adminDeleteMenuItem(
      variables: Types.AdminDeleteMenuItemMutationVariables
    ): Promise<Types.AdminDeleteMenuItemMutation> {
      return withWrapper(() =>
        client.request<Types.AdminDeleteMenuItemMutation>(
          print(Operations.AdminDeleteMenuItemDocument),
          variables
        )
      )
    },
    adminDeletePageContentBlock(
      variables: Types.AdminDeletePageContentBlockMutationVariables
    ): Promise<Types.AdminDeletePageContentBlockMutation> {
      return withWrapper(() =>
        client.request<Types.AdminDeletePageContentBlockMutation>(
          print(Operations.AdminDeletePageContentBlockDocument),
          variables
        )
      )
    },
    adminGetCoverImages(
      variables?: Types.AdminGetCoverImagesQueryVariables
    ): Promise<Types.AdminGetCoverImagesQuery> {
      return withWrapper(() =>
        client.request<Types.AdminGetCoverImagesQuery>(
          print(Operations.AdminGetCoverImagesDocument),
          variables
        )
      )
    },
    adminAddCoverImages(
      variables: Types.AdminAddCoverImagesMutationVariables
    ): Promise<Types.AdminAddCoverImagesMutation> {
      return withWrapper(() =>
        client.request<Types.AdminAddCoverImagesMutation>(
          print(Operations.AdminAddCoverImagesDocument),
          variables
        )
      )
    },
    adminDeleteCoverImages(
      variables: Types.AdminDeleteCoverImagesMutationVariables
    ): Promise<Types.AdminDeleteCoverImagesMutation> {
      return withWrapper(() =>
        client.request<Types.AdminDeleteCoverImagesMutation>(
          print(Operations.AdminDeleteCoverImagesDocument),
          variables
        )
      )
    },
    adminEditUser(
      variables: Types.AdminEditUserMutationVariables
    ): Promise<Types.AdminEditUserMutation> {
      return withWrapper(() =>
        client.request<Types.AdminEditUserMutation>(
          print(Operations.AdminEditUserDocument),
          variables
        )
      )
    },
    addToFavorite(
      variables: Types.AddToFavoriteMutationVariables
    ): Promise<Types.AddToFavoriteMutation> {
      return withWrapper(() =>
        client.request<Types.AddToFavoriteMutation>(
          print(Operations.AddToFavoriteDocument),
          variables
        )
      )
    },
    CreateLocation(
      variables: Types.CreateLocationMutationVariables
    ): Promise<Types.CreateLocationMutation> {
      return withWrapper(() =>
        client.request<Types.CreateLocationMutation>(
          print(Operations.CreateLocationDocument),
          variables
        )
      )
    },
    createStore(
      variables: Types.CreateStoreMutationVariables
    ): Promise<Types.CreateStoreMutation> {
      return withWrapper(() =>
        client.request<Types.CreateStoreMutation>(
          print(Operations.CreateStoreDocument),
          variables
        )
      )
    },
    createTag(
      variables: Types.CreateTagMutationVariables
    ): Promise<Types.CreateTagMutation> {
      return withWrapper(() =>
        client.request<Types.CreateTagMutation>(
          print(Operations.CreateTagDocument),
          variables
        )
      )
    },
    deleteMedia(
      variables: Types.DeleteMediaMutationVariables
    ): Promise<Types.DeleteMediaMutation> {
      return withWrapper(() =>
        client.request<Types.DeleteMediaMutation>(
          print(Operations.DeleteMediaDocument),
          variables
        )
      )
    },
    createAlbum(
      variables: Types.CreateAlbumMutationVariables
    ): Promise<Types.CreateAlbumMutation> {
      return withWrapper(() =>
        client.request<Types.CreateAlbumMutation>(
          print(Operations.CreateAlbumDocument),
          variables
        )
      )
    },
    createEvent(
      variables: Types.CreateEventMutationVariables
    ): Promise<Types.CreateEventMutation> {
      return withWrapper(() =>
        client.request<Types.CreateEventMutation>(
          print(Operations.CreateEventDocument),
          variables
        )
      )
    },
    deleteAlbum(
      variables: Types.DeleteAlbumMutationVariables
    ): Promise<Types.DeleteAlbumMutation> {
      return withWrapper(() =>
        client.request<Types.DeleteAlbumMutation>(
          print(Operations.DeleteAlbumDocument),
          variables
        )
      )
    },
    deleteEvent(
      variables: Types.DeleteEventMutationVariables
    ): Promise<Types.DeleteEventMutation> {
      return withWrapper(() =>
        client.request<Types.DeleteEventMutation>(
          print(Operations.DeleteEventDocument),
          variables
        )
      )
    },
    updateAlbum(
      variables: Types.UpdateAlbumMutationVariables
    ): Promise<Types.UpdateAlbumMutation> {
      return withWrapper(() =>
        client.request<Types.UpdateAlbumMutation>(
          print(Operations.UpdateAlbumDocument),
          variables
        )
      )
    },
    updateCoverEvent(
      variables: Types.UpdateCoverEventMutationVariables
    ): Promise<Types.UpdateCoverEventMutation> {
      return withWrapper(() =>
        client.request<Types.UpdateCoverEventMutation>(
          print(Operations.UpdateCoverEventDocument),
          variables
        )
      )
    },
    updateCoverAlbum(
      variables: Types.UpdateCoverAlbumMutationVariables
    ): Promise<Types.UpdateCoverAlbumMutation> {
      return withWrapper(() =>
        client.request<Types.UpdateCoverAlbumMutation>(
          print(Operations.UpdateCoverAlbumDocument),
          variables
        )
      )
    },
    updateEvent(
      variables: Types.UpdateEventMutationVariables
    ): Promise<Types.UpdateEventMutation> {
      return withWrapper(() =>
        client.request<Types.UpdateEventMutation>(
          print(Operations.UpdateEventDocument),
          variables
        )
      )
    },
    removeFromFavorite(
      variables: Types.RemoveFromFavoriteMutationVariables
    ): Promise<Types.RemoveFromFavoriteMutation> {
      return withWrapper(() =>
        client.request<Types.RemoveFromFavoriteMutation>(
          print(Operations.RemoveFromFavoriteDocument),
          variables
        )
      )
    },
    UpdateSettingsPaymentTypes(
      variables: Types.UpdateSettingsPaymentTypesMutationVariables
    ): Promise<Types.UpdateSettingsPaymentTypesMutation> {
      return withWrapper(() =>
        client.request<Types.UpdateSettingsPaymentTypesMutation>(
          print(Operations.UpdateSettingsPaymentTypesDocument),
          variables
        )
      )
    },
    UpdateSettingsSearchPhotos(
      variables: Types.UpdateSettingsSearchPhotosMutationVariables
    ): Promise<Types.UpdateSettingsSearchPhotosMutation> {
      return withWrapper(() =>
        client.request<Types.UpdateSettingsSearchPhotosMutation>(
          print(Operations.UpdateSettingsSearchPhotosDocument),
          variables
        )
      )
    },
    UpdateSettingsStyles(
      variables: Types.UpdateSettingsStylesMutationVariables
    ): Promise<Types.UpdateSettingsStylesMutation> {
      return withWrapper(() =>
        client.request<Types.UpdateSettingsStylesMutation>(
          print(Operations.UpdateSettingsStylesDocument),
          variables
        )
      )
    },
    updateCurrentUser(
      variables: Types.UpdateCurrentUserMutationVariables
    ): Promise<Types.UpdateCurrentUserMutation> {
      return withWrapper(() =>
        client.request<Types.UpdateCurrentUserMutation>(
          print(Operations.UpdateCurrentUserDocument),
          variables
        )
      )
    },
    updateUserWavesToken(
      variables: Types.UpdateUserWavesTokenMutationVariables
    ): Promise<Types.UpdateUserWavesTokenMutation> {
      return withWrapper(() =>
        client.request<Types.UpdateUserWavesTokenMutation>(
          print(Operations.UpdateUserWavesTokenDocument),
          variables
        )
      )
    },
    getWallet(
      variables: Types.GetWalletQueryVariables
    ): Promise<Types.GetWalletQuery> {
      return withWrapper(() =>
        client.request<Types.GetWalletQuery>(
          print(Operations.GetWalletDocument),
          variables
        )
      )
    },
    getCurrentUser(
      variables: Types.GetCurrentUserQueryVariables
    ): Promise<Types.GetCurrentUserQuery> {
      return withWrapper(() =>
        client.request<Types.GetCurrentUserQuery>(
          print(Operations.GetCurrentUserDocument),
          variables
        )
      )
    },
    getPage(
      variables: Types.GetPageQueryVariables
    ): Promise<Types.GetPageQuery> {
      return withWrapper(() =>
        client.request<Types.GetPageQuery>(
          print(Operations.GetPageDocument),
          variables
        )
      )
    },
    getMenuPages(
      variables: Types.GetMenuPagesQueryVariables
    ): Promise<Types.GetMenuPagesQuery> {
      return withWrapper(() =>
        client.request<Types.GetMenuPagesQuery>(
          print(Operations.GetMenuPagesDocument),
          variables
        )
      )
    },
    getMenuItems(
      variables: Types.GetMenuItemsQueryVariables
    ): Promise<Types.GetMenuItemsQuery> {
      return withWrapper(() =>
        client.request<Types.GetMenuItemsQuery>(
          print(Operations.GetMenuItemsDocument),
          variables
        )
      )
    },
    getTransactions(
      variables: Types.GetTransactionsQueryVariables
    ): Promise<Types.GetTransactionsQuery> {
      return withWrapper(() =>
        client.request<Types.GetTransactionsQuery>(
          print(Operations.GetTransactionsDocument),
          variables
        )
      )
    },
    getConfigs(
      variables: Types.GetConfigsQueryVariables
    ): Promise<Types.GetConfigsQuery> {
      return withWrapper(() =>
        client.request<Types.GetConfigsQuery>(
          print(Operations.GetConfigsDocument),
          variables
        )
      )
    },
    getDefaultSeo(
      variables: Types.GetDefaultSeoQueryVariables
    ): Promise<Types.GetDefaultSeoQuery> {
      return withWrapper(() =>
        client.request<Types.GetDefaultSeoQuery>(
          print(Operations.GetDefaultSeoDocument),
          variables
        )
      )
    },
    coverImages(
      variables?: Types.CoverImagesQueryVariables
    ): Promise<Types.CoverImagesQuery> {
      return withWrapper(() =>
        client.request<Types.CoverImagesQuery>(
          print(Operations.CoverImagesDocument),
          variables
        )
      )
    },
    getUsersList(
      variables?: Types.GetUsersListQueryVariables
    ): Promise<Types.GetUsersListQuery> {
      return withWrapper(() =>
        client.request<Types.GetUsersListQuery>(
          print(Operations.GetUsersListDocument),
          variables
        )
      )
    },
    getAlbum(
      variables: Types.GetAlbumQueryVariables
    ): Promise<Types.GetAlbumQuery> {
      return withWrapper(() =>
        client.request<Types.GetAlbumQuery>(
          print(Operations.GetAlbumDocument),
          variables
        )
      )
    },
    getEvent(
      variables: Types.GetEventQueryVariables
    ): Promise<Types.GetEventQuery> {
      return withWrapper(() =>
        client.request<Types.GetEventQuery>(
          print(Operations.GetEventDocument),
          variables
        )
      )
    },
    getUserFavorites(
      variables: Types.GetUserFavoritesQueryVariables
    ): Promise<Types.GetUserFavoritesQuery> {
      return withWrapper(() =>
        client.request<Types.GetUserFavoritesQuery>(
          print(Operations.GetUserFavoritesDocument),
          variables
        )
      )
    },
    GeoReverse(
      variables: Types.GeoReverseQueryVariables
    ): Promise<Types.GeoReverseQuery> {
      return withWrapper(() =>
        client.request<Types.GeoReverseQuery>(
          print(Operations.GeoReverseDocument),
          variables
        )
      )
    },
    GeoAutocomplete(
      variables: Types.GeoAutocompleteQueryVariables
    ): Promise<Types.GeoAutocompleteQuery> {
      return withWrapper(() =>
        client.request<Types.GeoAutocompleteQuery>(
          print(Operations.GeoAutocompleteDocument),
          variables
        )
      )
    },
    getMedia(
      variables: Types.GetMediaQueryVariables
    ): Promise<Types.GetMediaQuery> {
      return withWrapper(() =>
        client.request<Types.GetMediaQuery>(
          print(Operations.GetMediaDocument),
          variables
        )
      )
    },
    getUserPayment(
      variables: Types.GetUserPaymentQueryVariables
    ): Promise<Types.GetUserPaymentQuery> {
      return withWrapper(() =>
        client.request<Types.GetUserPaymentQuery>(
          print(Operations.GetUserPaymentDocument),
          variables
        )
      )
    },
    GetProfile(
      variables?: Types.GetProfileQueryVariables
    ): Promise<Types.GetProfileQuery> {
      return withWrapper(() =>
        client.request<Types.GetProfileQuery>(
          print(Operations.GetProfileDocument),
          variables
        )
      )
    },
    getSearchPhotographers(
      variables: Types.GetSearchPhotographersQueryVariables
    ): Promise<Types.GetSearchPhotographersQuery> {
      return withWrapper(() =>
        client.request<Types.GetSearchPhotographersQuery>(
          print(Operations.GetSearchPhotographersDocument),
          variables
        )
      )
    },
    getSearchPhotographersLocation(
      variables: Types.GetSearchPhotographersLocationQueryVariables
    ): Promise<Types.GetSearchPhotographersLocationQuery> {
      return withWrapper(() =>
        client.request<Types.GetSearchPhotographersLocationQuery>(
          print(Operations.GetSearchPhotographersLocationDocument),
          variables
        )
      )
    },
    GetStore(
      variables: Types.GetStoreQueryVariables
    ): Promise<Types.GetStoreQuery> {
      return withWrapper(() =>
        client.request<Types.GetStoreQuery>(
          print(Operations.GetStoreDocument),
          variables
        )
      )
    },
    getUserAlbums(
      variables: Types.GetUserAlbumsQueryVariables
    ): Promise<Types.GetUserAlbumsQuery> {
      return withWrapper(() =>
        client.request<Types.GetUserAlbumsQuery>(
          print(Operations.GetUserAlbumsDocument),
          variables
        )
      )
    },
    getUserStyle(
      variables: Types.GetUserStyleQueryVariables
    ): Promise<Types.GetUserStyleQuery> {
      return withWrapper(() =>
        client.request<Types.GetUserStyleQuery>(
          print(Operations.GetUserStyleDocument),
          variables
        )
      )
    },
    getStyle(
      variables?: Types.GetStyleQueryVariables
    ): Promise<Types.GetStyleQuery> {
      return withWrapper(() =>
        client.request<Types.GetStyleQuery>(
          print(Operations.GetStyleDocument),
          variables
        )
      )
    },
    getClients(
      variables: Types.GetClientsQueryVariables
    ): Promise<Types.GetClientsQuery> {
      return withWrapper(() =>
        client.request<Types.GetClientsQuery>(
          print(Operations.GetClientsDocument),
          variables
        )
      )
    },
    getUsers(
      variables: Types.GetUsersQueryVariables
    ): Promise<Types.GetUsersQuery> {
      return withWrapper(() =>
        client.request<Types.GetUsersQuery>(
          print(Operations.GetUsersDocument),
          variables
        )
      )
    },
    getUserPublic(
      variables?: Types.GetUserPublicQueryVariables
    ): Promise<Types.GetUserPublicQuery> {
      return withWrapper(() =>
        client.request<Types.GetUserPublicQuery>(
          print(Operations.GetUserPublicDocument),
          variables
        )
      )
    },
    albums(
      variables: Types.AlbumsSubscriptionVariables
    ): Promise<Types.AlbumsSubscription> {
      return withWrapper(() =>
        client.request<Types.AlbumsSubscription>(
          print(Operations.AlbumsDocument),
          variables
        )
      )
    },
    currentUser(
      variables: Types.CurrentUserSubscriptionVariables
    ): Promise<Types.CurrentUserSubscription> {
      return withWrapper(() =>
        client.request<Types.CurrentUserSubscription>(
          print(Operations.CurrentUserDocument),
          variables
        )
      )
    },
    events(
      variables: Types.EventsSubscriptionVariables
    ): Promise<Types.EventsSubscription> {
      return withWrapper(() =>
        client.request<Types.EventsSubscription>(
          print(Operations.EventsDocument),
          variables
        )
      )
    },
    media(
      variables: Types.MediaSubscriptionVariables
    ): Promise<Types.MediaSubscription> {
      return withWrapper(() =>
        client.request<Types.MediaSubscription>(
          print(Operations.MediaDocument),
          variables
        )
      )
    },
    store(
      variables: Types.StoreSubscriptionVariables
    ): Promise<Types.StoreSubscription> {
      return withWrapper(() =>
        client.request<Types.StoreSubscription>(
          print(Operations.StoreDocument),
          variables
        )
      )
    },
    transactions(
      variables: Types.TransactionsSubscriptionVariables
    ): Promise<Types.TransactionsSubscription> {
      return withWrapper(() =>
        client.request<Types.TransactionsSubscription>(
          print(Operations.TransactionsDocument),
          variables
        )
      )
    },
    wallet(
      variables: Types.WalletSubscriptionVariables
    ): Promise<Types.WalletSubscription> {
      return withWrapper(() =>
        client.request<Types.WalletSubscription>(
          print(Operations.WalletDocument),
          variables
        )
      )
    }
  }
}
export type Sdk = ReturnType<typeof getSdk>
