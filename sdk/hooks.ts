import * as Types from './types'

import * as Operations from './operations'
import * as VueApolloComposable from '@vue/apollo-composable'
import * as VueCompositionApi from '@vue/composition-api'
export type ReactiveFunction<TParam> = () => TParam

/**
 * __useAdminPagesSubscription__
 *
 * To run a query within a Vue component, call `useAdminPagesSubscription` and pass it any options that fit your needs.
 * When your component renders, `useAdminPagesSubscription` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the subscription, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/subscription.html#options;
 *
 * @example
 * const { result, loading, error } = useAdminPagesSubscription(
 *   {
 *   }
 * );
 */
export function useAdminPagesSubscription(
  options:
    | VueApolloComposable.UseSubscriptionOptions<
        Types.AdminPagesSubscription,
        Types.AdminPagesSubscriptionVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseSubscriptionOptions<
          Types.AdminPagesSubscription,
          Types.AdminPagesSubscriptionVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseSubscriptionOptions<
          Types.AdminPagesSubscription,
          Types.AdminPagesSubscriptionVariables
        >
      > = {}
) {
  return VueApolloComposable.useSubscription<
    Types.AdminPagesSubscription,
    undefined
  >(Operations.AdminPagesDocument, undefined, options)
}
export type AdminPagesSubscriptionCompositionFunctionResult = VueApolloComposable.UseSubscriptionReturn<
  Types.AdminPagesSubscription,
  Types.AdminPagesSubscriptionVariables
>

/**
 * __useAdminGetPagesQuery__
 *
 * To run a query within a Vue component, call `useAdminGetPagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAdminGetPagesQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useAdminGetPagesQuery(
 *   {
 *   }
 * );
 */
export function useAdminGetPagesQuery(
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.AdminGetPagesQuery,
        Types.AdminGetPagesQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.AdminGetPagesQuery,
          Types.AdminGetPagesQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.AdminGetPagesQuery,
          Types.AdminGetPagesQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<Types.AdminGetPagesQuery, undefined>(
    Operations.AdminGetPagesDocument,
    undefined,
    options
  )
}
export type AdminGetPagesQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.AdminGetPagesQuery,
  Types.AdminGetPagesQueryVariables
>

/**
 * __useAdminGetPageQuery__
 *
 * To run a query within a Vue component, call `useAdminGetPageQuery` and pass it any options that fit your needs.
 * When your component renders, `useAdminGetPageQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useAdminGetPageQuery(
 *   {
 *      id: // value for 'id'
 *   }
 * );
 */
export function useAdminGetPageQuery(
  variables:
    | Types.AdminGetPageQueryVariables
    | VueCompositionApi.Ref<Types.AdminGetPageQueryVariables>
    | ReactiveFunction<Types.AdminGetPageQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.AdminGetPageQuery,
        Types.AdminGetPageQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.AdminGetPageQuery,
          Types.AdminGetPageQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.AdminGetPageQuery,
          Types.AdminGetPageQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.AdminGetPageQuery,
    Types.AdminGetPageQueryVariables
  >(Operations.AdminGetPageDocument, variables, options)
}
export type AdminGetPageQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.AdminGetPageQuery,
  Types.AdminGetPageQueryVariables
>

/**
 * __useAdminUpdatePageMutation__
 *
 * To run a mutation, you first call `useAdminUpdatePageMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminUpdatePageMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminUpdatePageMutation({
 *   variables: {
 *      object: // value for 'object'
 *      on_conflict: // value for 'on_conflict'
 *   },
 * });
 */
export function useAdminUpdatePageMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminUpdatePageMutation,
        Types.AdminUpdatePageMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminUpdatePageMutation,
          Types.AdminUpdatePageMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminUpdatePageMutation,
    Types.AdminUpdatePageMutationVariables
  >(Operations.AdminUpdatePageDocument, options)
}
export type AdminUpdatePageMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminUpdatePageMutation,
  Types.AdminUpdatePageMutationVariables
>

/**
 * __useAdminDeletePageMutation__
 *
 * To run a mutation, you first call `useAdminDeletePageMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminDeletePageMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminDeletePageMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useAdminDeletePageMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminDeletePageMutation,
        Types.AdminDeletePageMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminDeletePageMutation,
          Types.AdminDeletePageMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminDeletePageMutation,
    Types.AdminDeletePageMutationVariables
  >(Operations.AdminDeletePageDocument, options)
}
export type AdminDeletePageMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminDeletePageMutation,
  Types.AdminDeletePageMutationVariables
>

/**
 * __useAdminAddMenuPageMutation__
 *
 * To run a mutation, you first call `useAdminAddMenuPageMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminAddMenuPageMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminAddMenuPageMutation({
 *   variables: {
 *      pageId: // value for 'pageId'
 *      order: // value for 'order'
 *   },
 * });
 */
export function useAdminAddMenuPageMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminAddMenuPageMutation,
        Types.AdminAddMenuPageMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminAddMenuPageMutation,
          Types.AdminAddMenuPageMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminAddMenuPageMutation,
    Types.AdminAddMenuPageMutationVariables
  >(Operations.AdminAddMenuPageDocument, options)
}
export type AdminAddMenuPageMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminAddMenuPageMutation,
  Types.AdminAddMenuPageMutationVariables
>

/**
 * __useAdminDeleteMenuPageMutation__
 *
 * To run a mutation, you first call `useAdminDeleteMenuPageMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminDeleteMenuPageMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminDeleteMenuPageMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useAdminDeleteMenuPageMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminDeleteMenuPageMutation,
        Types.AdminDeleteMenuPageMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminDeleteMenuPageMutation,
          Types.AdminDeleteMenuPageMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminDeleteMenuPageMutation,
    Types.AdminDeleteMenuPageMutationVariables
  >(Operations.AdminDeleteMenuPageDocument, options)
}
export type AdminDeleteMenuPageMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminDeleteMenuPageMutation,
  Types.AdminDeleteMenuPageMutationVariables
>

/**
 * __useAdminGetMenuPagesQuery__
 *
 * To run a query within a Vue component, call `useAdminGetMenuPagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAdminGetMenuPagesQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useAdminGetMenuPagesQuery(
 *   {
 *   }
 * );
 */
export function useAdminGetMenuPagesQuery(
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.AdminGetMenuPagesQuery,
        Types.AdminGetMenuPagesQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.AdminGetMenuPagesQuery,
          Types.AdminGetMenuPagesQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.AdminGetMenuPagesQuery,
          Types.AdminGetMenuPagesQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<Types.AdminGetMenuPagesQuery, undefined>(
    Operations.AdminGetMenuPagesDocument,
    undefined,
    options
  )
}
export type AdminGetMenuPagesQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.AdminGetMenuPagesQuery,
  Types.AdminGetMenuPagesQueryVariables
>

/**
 * __useAdminUpdateOrderMenuPagesMutation__
 *
 * To run a mutation, you first call `useAdminUpdateOrderMenuPagesMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminUpdateOrderMenuPagesMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminUpdateOrderMenuPagesMutation({
 *   variables: {
 *      objects: // value for 'objects'
 *   },
 * });
 */
export function useAdminUpdateOrderMenuPagesMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminUpdateOrderMenuPagesMutation,
        Types.AdminUpdateOrderMenuPagesMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminUpdateOrderMenuPagesMutation,
          Types.AdminUpdateOrderMenuPagesMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminUpdateOrderMenuPagesMutation,
    Types.AdminUpdateOrderMenuPagesMutationVariables
  >(Operations.AdminUpdateOrderMenuPagesDocument, options)
}
export type AdminUpdateOrderMenuPagesMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminUpdateOrderMenuPagesMutation,
  Types.AdminUpdateOrderMenuPagesMutationVariables
>

/**
 * __useAdminMenuItemsQuery__
 *
 * To run a query within a Vue component, call `useAdminMenuItemsQuery` and pass it any options that fit your needs.
 * When your component renders, `useAdminMenuItemsQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useAdminMenuItemsQuery(
 *   {
 *      type: // value for 'type'
 *   }
 * );
 */
export function useAdminMenuItemsQuery(
  variables:
    | Types.AdminMenuItemsQueryVariables
    | VueCompositionApi.Ref<Types.AdminMenuItemsQueryVariables>
    | ReactiveFunction<Types.AdminMenuItemsQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.AdminMenuItemsQuery,
        Types.AdminMenuItemsQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.AdminMenuItemsQuery,
          Types.AdminMenuItemsQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.AdminMenuItemsQuery,
          Types.AdminMenuItemsQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.AdminMenuItemsQuery,
    Types.AdminMenuItemsQueryVariables
  >(Operations.AdminMenuItemsDocument, variables, options)
}
export type AdminMenuItemsQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.AdminMenuItemsQuery,
  Types.AdminMenuItemsQueryVariables
>

/**
 * __useAdminAddMenuItemMutation__
 *
 * To run a mutation, you first call `useAdminAddMenuItemMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminAddMenuItemMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminAddMenuItemMutation({
 *   variables: {
 *      item: // value for 'item'
 *   },
 * });
 */
export function useAdminAddMenuItemMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminAddMenuItemMutation,
        Types.AdminAddMenuItemMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminAddMenuItemMutation,
          Types.AdminAddMenuItemMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminAddMenuItemMutation,
    Types.AdminAddMenuItemMutationVariables
  >(Operations.AdminAddMenuItemDocument, options)
}
export type AdminAddMenuItemMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminAddMenuItemMutation,
  Types.AdminAddMenuItemMutationVariables
>

/**
 * __useAdminUpdateOrderMenuItemsMutation__
 *
 * To run a mutation, you first call `useAdminUpdateOrderMenuItemsMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminUpdateOrderMenuItemsMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminUpdateOrderMenuItemsMutation({
 *   variables: {
 *      objects: // value for 'objects'
 *   },
 * });
 */
export function useAdminUpdateOrderMenuItemsMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminUpdateOrderMenuItemsMutation,
        Types.AdminUpdateOrderMenuItemsMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminUpdateOrderMenuItemsMutation,
          Types.AdminUpdateOrderMenuItemsMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminUpdateOrderMenuItemsMutation,
    Types.AdminUpdateOrderMenuItemsMutationVariables
  >(Operations.AdminUpdateOrderMenuItemsDocument, options)
}
export type AdminUpdateOrderMenuItemsMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminUpdateOrderMenuItemsMutation,
  Types.AdminUpdateOrderMenuItemsMutationVariables
>

/**
 * __useAdminDeleteMenuItemMutation__
 *
 * To run a mutation, you first call `useAdminDeleteMenuItemMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminDeleteMenuItemMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminDeleteMenuItemMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useAdminDeleteMenuItemMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminDeleteMenuItemMutation,
        Types.AdminDeleteMenuItemMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminDeleteMenuItemMutation,
          Types.AdminDeleteMenuItemMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminDeleteMenuItemMutation,
    Types.AdminDeleteMenuItemMutationVariables
  >(Operations.AdminDeleteMenuItemDocument, options)
}
export type AdminDeleteMenuItemMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminDeleteMenuItemMutation,
  Types.AdminDeleteMenuItemMutationVariables
>

/**
 * __useAdminDeletePageContentBlockMutation__
 *
 * To run a mutation, you first call `useAdminDeletePageContentBlockMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminDeletePageContentBlockMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminDeletePageContentBlockMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useAdminDeletePageContentBlockMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminDeletePageContentBlockMutation,
        Types.AdminDeletePageContentBlockMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminDeletePageContentBlockMutation,
          Types.AdminDeletePageContentBlockMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminDeletePageContentBlockMutation,
    Types.AdminDeletePageContentBlockMutationVariables
  >(Operations.AdminDeletePageContentBlockDocument, options)
}
export type AdminDeletePageContentBlockMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminDeletePageContentBlockMutation,
  Types.AdminDeletePageContentBlockMutationVariables
>

/**
 * __useAdminGetCoverImagesQuery__
 *
 * To run a query within a Vue component, call `useAdminGetCoverImagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useAdminGetCoverImagesQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useAdminGetCoverImagesQuery(
 *   {
 *   }
 * );
 */
export function useAdminGetCoverImagesQuery(
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.AdminGetCoverImagesQuery,
        Types.AdminGetCoverImagesQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.AdminGetCoverImagesQuery,
          Types.AdminGetCoverImagesQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.AdminGetCoverImagesQuery,
          Types.AdminGetCoverImagesQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.AdminGetCoverImagesQuery,
    undefined
  >(Operations.AdminGetCoverImagesDocument, undefined, options)
}
export type AdminGetCoverImagesQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.AdminGetCoverImagesQuery,
  Types.AdminGetCoverImagesQueryVariables
>

/**
 * __useAdminAddCoverImagesMutation__
 *
 * To run a mutation, you first call `useAdminAddCoverImagesMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminAddCoverImagesMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminAddCoverImagesMutation({
 *   variables: {
 *      item: // value for 'item'
 *   },
 * });
 */
export function useAdminAddCoverImagesMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminAddCoverImagesMutation,
        Types.AdminAddCoverImagesMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminAddCoverImagesMutation,
          Types.AdminAddCoverImagesMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminAddCoverImagesMutation,
    Types.AdminAddCoverImagesMutationVariables
  >(Operations.AdminAddCoverImagesDocument, options)
}
export type AdminAddCoverImagesMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminAddCoverImagesMutation,
  Types.AdminAddCoverImagesMutationVariables
>

/**
 * __useAdminDeleteCoverImagesMutation__
 *
 * To run a mutation, you first call `useAdminDeleteCoverImagesMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminDeleteCoverImagesMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminDeleteCoverImagesMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useAdminDeleteCoverImagesMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminDeleteCoverImagesMutation,
        Types.AdminDeleteCoverImagesMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminDeleteCoverImagesMutation,
          Types.AdminDeleteCoverImagesMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminDeleteCoverImagesMutation,
    Types.AdminDeleteCoverImagesMutationVariables
  >(Operations.AdminDeleteCoverImagesDocument, options)
}
export type AdminDeleteCoverImagesMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminDeleteCoverImagesMutation,
  Types.AdminDeleteCoverImagesMutationVariables
>

/**
 * __useAdminEditUserMutation__
 *
 * To run a mutation, you first call `useAdminEditUserMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAdminEditUserMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAdminEditUserMutation({
 *   variables: {
 *      id: // value for 'id'
 *      blocked: // value for 'blocked'
 *   },
 * });
 */
export function useAdminEditUserMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AdminEditUserMutation,
        Types.AdminEditUserMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AdminEditUserMutation,
          Types.AdminEditUserMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AdminEditUserMutation,
    Types.AdminEditUserMutationVariables
  >(Operations.AdminEditUserDocument, options)
}
export type AdminEditUserMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AdminEditUserMutation,
  Types.AdminEditUserMutationVariables
>

/**
 * __useAddToFavoriteMutation__
 *
 * To run a mutation, you first call `useAddToFavoriteMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useAddToFavoriteMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useAddToFavoriteMutation({
 *   variables: {
 *      object: // value for 'object'
 *   },
 * });
 */
export function useAddToFavoriteMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.AddToFavoriteMutation,
        Types.AddToFavoriteMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.AddToFavoriteMutation,
          Types.AddToFavoriteMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.AddToFavoriteMutation,
    Types.AddToFavoriteMutationVariables
  >(Operations.AddToFavoriteDocument, options)
}
export type AddToFavoriteMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.AddToFavoriteMutation,
  Types.AddToFavoriteMutationVariables
>

/**
 * __useCreateLocationMutation__
 *
 * To run a mutation, you first call `useCreateLocationMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useCreateLocationMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useCreateLocationMutation({
 *   variables: {
 *      object: // value for 'object'
 *   },
 * });
 */
export function useCreateLocationMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.CreateLocationMutation,
        Types.CreateLocationMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.CreateLocationMutation,
          Types.CreateLocationMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.CreateLocationMutation,
    Types.CreateLocationMutationVariables
  >(Operations.CreateLocationDocument, options)
}
export type CreateLocationMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.CreateLocationMutation,
  Types.CreateLocationMutationVariables
>

/**
 * __useCreateStoreMutation__
 *
 * To run a mutation, you first call `useCreateStoreMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useCreateStoreMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useCreateStoreMutation({
 *   variables: {
 *      object: // value for 'object'
 *   },
 * });
 */
export function useCreateStoreMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.CreateStoreMutation,
        Types.CreateStoreMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.CreateStoreMutation,
          Types.CreateStoreMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.CreateStoreMutation,
    Types.CreateStoreMutationVariables
  >(Operations.CreateStoreDocument, options)
}
export type CreateStoreMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.CreateStoreMutation,
  Types.CreateStoreMutationVariables
>

/**
 * __useCreateTagMutation__
 *
 * To run a mutation, you first call `useCreateTagMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useCreateTagMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useCreateTagMutation({
 *   variables: {
 *      objects: // value for 'objects'
 *   },
 * });
 */
export function useCreateTagMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.CreateTagMutation,
        Types.CreateTagMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.CreateTagMutation,
          Types.CreateTagMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.CreateTagMutation,
    Types.CreateTagMutationVariables
  >(Operations.CreateTagDocument, options)
}
export type CreateTagMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.CreateTagMutation,
  Types.CreateTagMutationVariables
>

/**
 * __useDeleteMediaMutation__
 *
 * To run a mutation, you first call `useDeleteMediaMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useDeleteMediaMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useDeleteMediaMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteMediaMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.DeleteMediaMutation,
        Types.DeleteMediaMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.DeleteMediaMutation,
          Types.DeleteMediaMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.DeleteMediaMutation,
    Types.DeleteMediaMutationVariables
  >(Operations.DeleteMediaDocument, options)
}
export type DeleteMediaMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.DeleteMediaMutation,
  Types.DeleteMediaMutationVariables
>

/**
 * __useCreateAlbumMutation__
 *
 * To run a mutation, you first call `useCreateAlbumMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useCreateAlbumMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useCreateAlbumMutation({
 *   variables: {
 *      object: // value for 'object'
 *      payment: // value for 'payment'
 *   },
 * });
 */
export function useCreateAlbumMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.CreateAlbumMutation,
        Types.CreateAlbumMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.CreateAlbumMutation,
          Types.CreateAlbumMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.CreateAlbumMutation,
    Types.CreateAlbumMutationVariables
  >(Operations.CreateAlbumDocument, options)
}
export type CreateAlbumMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.CreateAlbumMutation,
  Types.CreateAlbumMutationVariables
>

/**
 * __useCreateEventMutation__
 *
 * To run a mutation, you first call `useCreateEventMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useCreateEventMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useCreateEventMutation({
 *   variables: {
 *      object: // value for 'object'
 *      payment: // value for 'payment'
 *   },
 * });
 */
export function useCreateEventMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.CreateEventMutation,
        Types.CreateEventMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.CreateEventMutation,
          Types.CreateEventMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.CreateEventMutation,
    Types.CreateEventMutationVariables
  >(Operations.CreateEventDocument, options)
}
export type CreateEventMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.CreateEventMutation,
  Types.CreateEventMutationVariables
>

/**
 * __useDeleteAlbumMutation__
 *
 * To run a mutation, you first call `useDeleteAlbumMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useDeleteAlbumMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useDeleteAlbumMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteAlbumMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.DeleteAlbumMutation,
        Types.DeleteAlbumMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.DeleteAlbumMutation,
          Types.DeleteAlbumMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.DeleteAlbumMutation,
    Types.DeleteAlbumMutationVariables
  >(Operations.DeleteAlbumDocument, options)
}
export type DeleteAlbumMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.DeleteAlbumMutation,
  Types.DeleteAlbumMutationVariables
>

/**
 * __useDeleteEventMutation__
 *
 * To run a mutation, you first call `useDeleteEventMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useDeleteEventMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useDeleteEventMutation({
 *   variables: {
 *      id: // value for 'id'
 *   },
 * });
 */
export function useDeleteEventMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.DeleteEventMutation,
        Types.DeleteEventMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.DeleteEventMutation,
          Types.DeleteEventMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.DeleteEventMutation,
    Types.DeleteEventMutationVariables
  >(Operations.DeleteEventDocument, options)
}
export type DeleteEventMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.DeleteEventMutation,
  Types.DeleteEventMutationVariables
>

/**
 * __useUpdateAlbumMutation__
 *
 * To run a mutation, you first call `useUpdateAlbumMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useUpdateAlbumMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useUpdateAlbumMutation({
 *   variables: {
 *      id: // value for 'id'
 *      object: // value for 'object'
 *      payment: // value for 'payment'
 *      tags: // value for 'tags'
 *   },
 * });
 */
export function useUpdateAlbumMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.UpdateAlbumMutation,
        Types.UpdateAlbumMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.UpdateAlbumMutation,
          Types.UpdateAlbumMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.UpdateAlbumMutation,
    Types.UpdateAlbumMutationVariables
  >(Operations.UpdateAlbumDocument, options)
}
export type UpdateAlbumMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.UpdateAlbumMutation,
  Types.UpdateAlbumMutationVariables
>

/**
 * __useUpdateCoverEventMutation__
 *
 * To run a mutation, you first call `useUpdateCoverEventMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useUpdateCoverEventMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useUpdateCoverEventMutation({
 *   variables: {
 *      id: // value for 'id'
 *      object: // value for 'object'
 *   },
 * });
 */
export function useUpdateCoverEventMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.UpdateCoverEventMutation,
        Types.UpdateCoverEventMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.UpdateCoverEventMutation,
          Types.UpdateCoverEventMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.UpdateCoverEventMutation,
    Types.UpdateCoverEventMutationVariables
  >(Operations.UpdateCoverEventDocument, options)
}
export type UpdateCoverEventMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.UpdateCoverEventMutation,
  Types.UpdateCoverEventMutationVariables
>

/**
 * __useUpdateCoverAlbumMutation__
 *
 * To run a mutation, you first call `useUpdateCoverAlbumMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useUpdateCoverAlbumMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useUpdateCoverAlbumMutation({
 *   variables: {
 *      id: // value for 'id'
 *      object: // value for 'object'
 *   },
 * });
 */
export function useUpdateCoverAlbumMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.UpdateCoverAlbumMutation,
        Types.UpdateCoverAlbumMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.UpdateCoverAlbumMutation,
          Types.UpdateCoverAlbumMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.UpdateCoverAlbumMutation,
    Types.UpdateCoverAlbumMutationVariables
  >(Operations.UpdateCoverAlbumDocument, options)
}
export type UpdateCoverAlbumMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.UpdateCoverAlbumMutation,
  Types.UpdateCoverAlbumMutationVariables
>

/**
 * __useUpdateEventMutation__
 *
 * To run a mutation, you first call `useUpdateEventMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useUpdateEventMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useUpdateEventMutation({
 *   variables: {
 *      id: // value for 'id'
 *      object: // value for 'object'
 *      payment: // value for 'payment'
 *   },
 * });
 */
export function useUpdateEventMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.UpdateEventMutation,
        Types.UpdateEventMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.UpdateEventMutation,
          Types.UpdateEventMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.UpdateEventMutation,
    Types.UpdateEventMutationVariables
  >(Operations.UpdateEventDocument, options)
}
export type UpdateEventMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.UpdateEventMutation,
  Types.UpdateEventMutationVariables
>

/**
 * __useRemoveFromFavoriteMutation__
 *
 * To run a mutation, you first call `useRemoveFromFavoriteMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useRemoveFromFavoriteMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useRemoveFromFavoriteMutation({
 *   variables: {
 *      where: // value for 'where'
 *   },
 * });
 */
export function useRemoveFromFavoriteMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.RemoveFromFavoriteMutation,
        Types.RemoveFromFavoriteMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.RemoveFromFavoriteMutation,
          Types.RemoveFromFavoriteMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.RemoveFromFavoriteMutation,
    Types.RemoveFromFavoriteMutationVariables
  >(Operations.RemoveFromFavoriteDocument, options)
}
export type RemoveFromFavoriteMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.RemoveFromFavoriteMutation,
  Types.RemoveFromFavoriteMutationVariables
>

/**
 * __useUpdateSettingsPaymentTypesMutation__
 *
 * To run a mutation, you first call `useUpdateSettingsPaymentTypesMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSettingsPaymentTypesMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useUpdateSettingsPaymentTypesMutation({
 *   variables: {
 *      objects: // value for 'objects'
 *      forDelete: // value for 'forDelete'
 *      delete: // value for 'delete'
 *   },
 * });
 */
export function useUpdateSettingsPaymentTypesMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.UpdateSettingsPaymentTypesMutation,
        Types.UpdateSettingsPaymentTypesMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.UpdateSettingsPaymentTypesMutation,
          Types.UpdateSettingsPaymentTypesMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.UpdateSettingsPaymentTypesMutation,
    Types.UpdateSettingsPaymentTypesMutationVariables
  >(Operations.UpdateSettingsPaymentTypesDocument, options)
}
export type UpdateSettingsPaymentTypesMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.UpdateSettingsPaymentTypesMutation,
  Types.UpdateSettingsPaymentTypesMutationVariables
>

/**
 * __useUpdateSettingsSearchPhotosMutation__
 *
 * To run a mutation, you first call `useUpdateSettingsSearchPhotosMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSettingsSearchPhotosMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useUpdateSettingsSearchPhotosMutation({
 *   variables: {
 *      userId: // value for 'userId'
 *      objects: // value for 'objects'
 *   },
 * });
 */
export function useUpdateSettingsSearchPhotosMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.UpdateSettingsSearchPhotosMutation,
        Types.UpdateSettingsSearchPhotosMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.UpdateSettingsSearchPhotosMutation,
          Types.UpdateSettingsSearchPhotosMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.UpdateSettingsSearchPhotosMutation,
    Types.UpdateSettingsSearchPhotosMutationVariables
  >(Operations.UpdateSettingsSearchPhotosDocument, options)
}
export type UpdateSettingsSearchPhotosMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.UpdateSettingsSearchPhotosMutation,
  Types.UpdateSettingsSearchPhotosMutationVariables
>

/**
 * __useUpdateSettingsStylesMutation__
 *
 * To run a mutation, you first call `useUpdateSettingsStylesMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useUpdateSettingsStylesMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useUpdateSettingsStylesMutation({
 *   variables: {
 *      objects: // value for 'objects'
 *      forDelete: // value for 'forDelete'
 *      insert: // value for 'insert'
 *      delete: // value for 'delete'
 *   },
 * });
 */
export function useUpdateSettingsStylesMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.UpdateSettingsStylesMutation,
        Types.UpdateSettingsStylesMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.UpdateSettingsStylesMutation,
          Types.UpdateSettingsStylesMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.UpdateSettingsStylesMutation,
    Types.UpdateSettingsStylesMutationVariables
  >(Operations.UpdateSettingsStylesDocument, options)
}
export type UpdateSettingsStylesMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.UpdateSettingsStylesMutation,
  Types.UpdateSettingsStylesMutationVariables
>

/**
 * __useUpdateCurrentUserMutation__
 *
 * To run a mutation, you first call `useUpdateCurrentUserMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useUpdateCurrentUserMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useUpdateCurrentUserMutation({
 *   variables: {
 *      userId: // value for 'userId'
 *      user: // value for 'user'
 *      email: // value for 'email'
 *      styles: // value for 'styles'
 *   },
 * });
 */
export function useUpdateCurrentUserMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.UpdateCurrentUserMutation,
        Types.UpdateCurrentUserMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.UpdateCurrentUserMutation,
          Types.UpdateCurrentUserMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.UpdateCurrentUserMutation,
    Types.UpdateCurrentUserMutationVariables
  >(Operations.UpdateCurrentUserDocument, options)
}
export type UpdateCurrentUserMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.UpdateCurrentUserMutation,
  Types.UpdateCurrentUserMutationVariables
>

/**
 * __useUpdateUserWavesTokenMutation__
 *
 * To run a mutation, you first call `useUpdateUserWavesTokenMutation` within a Vue component and pass it any options that fit your needs.
 * When your component renders, `useUpdateUserWavesTokenMutation` returns an object that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - Several other properties: https://v4.apollo.vuejs.org/api/use-mutation.html#return
 *
 * @param options that will be passed into the mutation, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/mutation.html#options;
 *
 * @example
 * const { mutate, loading, error, onDone } = useUpdateUserWavesTokenMutation({
 *   variables: {
 *      userId: // value for 'userId'
 *      waves_address: // value for 'waves_address'
 *   },
 * });
 */
export function useUpdateUserWavesTokenMutation(
  options:
    | VueApolloComposable.UseMutationOptions<
        Types.UpdateUserWavesTokenMutation,
        Types.UpdateUserWavesTokenMutationVariables
      >
    | ReactiveFunction<
        VueApolloComposable.UseMutationOptions<
          Types.UpdateUserWavesTokenMutation,
          Types.UpdateUserWavesTokenMutationVariables
        >
      >
) {
  return VueApolloComposable.useMutation<
    Types.UpdateUserWavesTokenMutation,
    Types.UpdateUserWavesTokenMutationVariables
  >(Operations.UpdateUserWavesTokenDocument, options)
}
export type UpdateUserWavesTokenMutationCompositionFunctionResult = VueApolloComposable.UseMutationReturn<
  Types.UpdateUserWavesTokenMutation,
  Types.UpdateUserWavesTokenMutationVariables
>

/**
 * __useGetWalletQuery__
 *
 * To run a query within a Vue component, call `useGetWalletQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetWalletQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetWalletQuery(
 *   {
 *      userId: // value for 'userId'
 *   }
 * );
 */
export function useGetWalletQuery(
  variables:
    | Types.GetWalletQueryVariables
    | VueCompositionApi.Ref<Types.GetWalletQueryVariables>
    | ReactiveFunction<Types.GetWalletQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetWalletQuery,
        Types.GetWalletQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetWalletQuery,
          Types.GetWalletQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetWalletQuery,
          Types.GetWalletQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetWalletQuery,
    Types.GetWalletQueryVariables
  >(Operations.GetWalletDocument, variables, options)
}
export type GetWalletQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetWalletQuery,
  Types.GetWalletQueryVariables
>

/**
 * __useGetCurrentUserQuery__
 *
 * To run a query within a Vue component, call `useGetCurrentUserQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetCurrentUserQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetCurrentUserQuery(
 *   {
 *      userId: // value for 'userId'
 *   }
 * );
 */
export function useGetCurrentUserQuery(
  variables:
    | Types.GetCurrentUserQueryVariables
    | VueCompositionApi.Ref<Types.GetCurrentUserQueryVariables>
    | ReactiveFunction<Types.GetCurrentUserQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetCurrentUserQuery,
        Types.GetCurrentUserQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetCurrentUserQuery,
          Types.GetCurrentUserQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetCurrentUserQuery,
          Types.GetCurrentUserQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetCurrentUserQuery,
    Types.GetCurrentUserQueryVariables
  >(Operations.GetCurrentUserDocument, variables, options)
}
export type GetCurrentUserQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetCurrentUserQuery,
  Types.GetCurrentUserQueryVariables
>

/**
 * __useGetPageQuery__
 *
 * To run a query within a Vue component, call `useGetPageQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetPageQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetPageQuery(
 *   {
 *      locales: // value for 'locales'
 *      slug: // value for 'slug'
 *   }
 * );
 */
export function useGetPageQuery(
  variables:
    | Types.GetPageQueryVariables
    | VueCompositionApi.Ref<Types.GetPageQueryVariables>
    | ReactiveFunction<Types.GetPageQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetPageQuery,
        Types.GetPageQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetPageQuery,
          Types.GetPageQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetPageQuery,
          Types.GetPageQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetPageQuery,
    Types.GetPageQueryVariables
  >(Operations.GetPageDocument, variables, options)
}
export type GetPageQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetPageQuery,
  Types.GetPageQueryVariables
>

/**
 * __useGetMenuPagesQuery__
 *
 * To run a query within a Vue component, call `useGetMenuPagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMenuPagesQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetMenuPagesQuery(
 *   {
 *      locales: // value for 'locales'
 *   }
 * );
 */
export function useGetMenuPagesQuery(
  variables:
    | Types.GetMenuPagesQueryVariables
    | VueCompositionApi.Ref<Types.GetMenuPagesQueryVariables>
    | ReactiveFunction<Types.GetMenuPagesQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetMenuPagesQuery,
        Types.GetMenuPagesQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetMenuPagesQuery,
          Types.GetMenuPagesQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetMenuPagesQuery,
          Types.GetMenuPagesQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetMenuPagesQuery,
    Types.GetMenuPagesQueryVariables
  >(Operations.GetMenuPagesDocument, variables, options)
}
export type GetMenuPagesQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetMenuPagesQuery,
  Types.GetMenuPagesQueryVariables
>

/**
 * __useGetMenuItemsQuery__
 *
 * To run a query within a Vue component, call `useGetMenuItemsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMenuItemsQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetMenuItemsQuery(
 *   {
 *      types: // value for 'types'
 *      locales: // value for 'locales'
 *   }
 * );
 */
export function useGetMenuItemsQuery(
  variables:
    | Types.GetMenuItemsQueryVariables
    | VueCompositionApi.Ref<Types.GetMenuItemsQueryVariables>
    | ReactiveFunction<Types.GetMenuItemsQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetMenuItemsQuery,
        Types.GetMenuItemsQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetMenuItemsQuery,
          Types.GetMenuItemsQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetMenuItemsQuery,
          Types.GetMenuItemsQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetMenuItemsQuery,
    Types.GetMenuItemsQueryVariables
  >(Operations.GetMenuItemsDocument, variables, options)
}
export type GetMenuItemsQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetMenuItemsQuery,
  Types.GetMenuItemsQueryVariables
>

/**
 * __useGetTransactionsQuery__
 *
 * To run a query within a Vue component, call `useGetTransactionsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetTransactionsQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetTransactionsQuery(
 *   {
 *      where: // value for 'where'
 *      order_by: // value for 'order_by'
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *   }
 * );
 */
export function useGetTransactionsQuery(
  variables:
    | Types.GetTransactionsQueryVariables
    | VueCompositionApi.Ref<Types.GetTransactionsQueryVariables>
    | ReactiveFunction<Types.GetTransactionsQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetTransactionsQuery,
        Types.GetTransactionsQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetTransactionsQuery,
          Types.GetTransactionsQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetTransactionsQuery,
          Types.GetTransactionsQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetTransactionsQuery,
    Types.GetTransactionsQueryVariables
  >(Operations.GetTransactionsDocument, variables, options)
}
export type GetTransactionsQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetTransactionsQuery,
  Types.GetTransactionsQueryVariables
>

/**
 * __useGetConfigsQuery__
 *
 * To run a query within a Vue component, call `useGetConfigsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetConfigsQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetConfigsQuery(
 *   {
 *      in: // value for 'in'
 *   }
 * );
 */
export function useGetConfigsQuery(
  variables:
    | Types.GetConfigsQueryVariables
    | VueCompositionApi.Ref<Types.GetConfigsQueryVariables>
    | ReactiveFunction<Types.GetConfigsQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetConfigsQuery,
        Types.GetConfigsQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetConfigsQuery,
          Types.GetConfigsQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetConfigsQuery,
          Types.GetConfigsQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetConfigsQuery,
    Types.GetConfigsQueryVariables
  >(Operations.GetConfigsDocument, variables, options)
}
export type GetConfigsQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetConfigsQuery,
  Types.GetConfigsQueryVariables
>

/**
 * __useGetDefaultSeoQuery__
 *
 * To run a query within a Vue component, call `useGetDefaultSeoQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetDefaultSeoQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetDefaultSeoQuery(
 *   {
 *      locales: // value for 'locales'
 *   }
 * );
 */
export function useGetDefaultSeoQuery(
  variables:
    | Types.GetDefaultSeoQueryVariables
    | VueCompositionApi.Ref<Types.GetDefaultSeoQueryVariables>
    | ReactiveFunction<Types.GetDefaultSeoQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetDefaultSeoQuery,
        Types.GetDefaultSeoQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetDefaultSeoQuery,
          Types.GetDefaultSeoQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetDefaultSeoQuery,
          Types.GetDefaultSeoQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetDefaultSeoQuery,
    Types.GetDefaultSeoQueryVariables
  >(Operations.GetDefaultSeoDocument, variables, options)
}
export type GetDefaultSeoQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetDefaultSeoQuery,
  Types.GetDefaultSeoQueryVariables
>

/**
 * __useCoverImagesQuery__
 *
 * To run a query within a Vue component, call `useCoverImagesQuery` and pass it any options that fit your needs.
 * When your component renders, `useCoverImagesQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useCoverImagesQuery(
 *   {
 *   }
 * );
 */
export function useCoverImagesQuery(
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.CoverImagesQuery,
        Types.CoverImagesQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.CoverImagesQuery,
          Types.CoverImagesQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.CoverImagesQuery,
          Types.CoverImagesQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<Types.CoverImagesQuery, undefined>(
    Operations.CoverImagesDocument,
    undefined,
    options
  )
}
export type CoverImagesQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.CoverImagesQuery,
  Types.CoverImagesQueryVariables
>

/**
 * __useGetUsersListQuery__
 *
 * To run a query within a Vue component, call `useGetUsersListQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUsersListQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetUsersListQuery(
 *   {
 *   }
 * );
 */
export function useGetUsersListQuery(
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetUsersListQuery,
        Types.GetUsersListQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetUsersListQuery,
          Types.GetUsersListQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetUsersListQuery,
          Types.GetUsersListQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<Types.GetUsersListQuery, undefined>(
    Operations.GetUsersListDocument,
    undefined,
    options
  )
}
export type GetUsersListQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetUsersListQuery,
  Types.GetUsersListQueryVariables
>

/**
 * __useGetAlbumQuery__
 *
 * To run a query within a Vue component, call `useGetAlbumQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetAlbumQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetAlbumQuery(
 *   {
 *      id: // value for 'id'
 *   }
 * );
 */
export function useGetAlbumQuery(
  variables:
    | Types.GetAlbumQueryVariables
    | VueCompositionApi.Ref<Types.GetAlbumQueryVariables>
    | ReactiveFunction<Types.GetAlbumQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetAlbumQuery,
        Types.GetAlbumQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetAlbumQuery,
          Types.GetAlbumQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetAlbumQuery,
          Types.GetAlbumQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetAlbumQuery,
    Types.GetAlbumQueryVariables
  >(Operations.GetAlbumDocument, variables, options)
}
export type GetAlbumQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetAlbumQuery,
  Types.GetAlbumQueryVariables
>

/**
 * __useGetEventQuery__
 *
 * To run a query within a Vue component, call `useGetEventQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetEventQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetEventQuery(
 *   {
 *      id: // value for 'id'
 *   }
 * );
 */
export function useGetEventQuery(
  variables:
    | Types.GetEventQueryVariables
    | VueCompositionApi.Ref<Types.GetEventQueryVariables>
    | ReactiveFunction<Types.GetEventQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetEventQuery,
        Types.GetEventQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetEventQuery,
          Types.GetEventQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetEventQuery,
          Types.GetEventQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetEventQuery,
    Types.GetEventQueryVariables
  >(Operations.GetEventDocument, variables, options)
}
export type GetEventQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetEventQuery,
  Types.GetEventQueryVariables
>

/**
 * __useGetUserFavoritesQuery__
 *
 * To run a query within a Vue component, call `useGetUserFavoritesQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserFavoritesQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetUserFavoritesQuery(
 *   {
 *      userId: // value for 'userId'
 *   }
 * );
 */
export function useGetUserFavoritesQuery(
  variables:
    | Types.GetUserFavoritesQueryVariables
    | VueCompositionApi.Ref<Types.GetUserFavoritesQueryVariables>
    | ReactiveFunction<Types.GetUserFavoritesQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetUserFavoritesQuery,
        Types.GetUserFavoritesQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetUserFavoritesQuery,
          Types.GetUserFavoritesQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetUserFavoritesQuery,
          Types.GetUserFavoritesQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetUserFavoritesQuery,
    Types.GetUserFavoritesQueryVariables
  >(Operations.GetUserFavoritesDocument, variables, options)
}
export type GetUserFavoritesQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetUserFavoritesQuery,
  Types.GetUserFavoritesQueryVariables
>

/**
 * __useGeoReverseQuery__
 *
 * To run a query within a Vue component, call `useGeoReverseQuery` and pass it any options that fit your needs.
 * When your component renders, `useGeoReverseQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGeoReverseQuery(
 *   {
 *      lat: // value for 'lat'
 *      lng: // value for 'lng'
 *      lang: // value for 'lang'
 *      type: // value for 'type'
 *   }
 * );
 */
export function useGeoReverseQuery(
  variables:
    | Types.GeoReverseQueryVariables
    | VueCompositionApi.Ref<Types.GeoReverseQueryVariables>
    | ReactiveFunction<Types.GeoReverseQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GeoReverseQuery,
        Types.GeoReverseQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GeoReverseQuery,
          Types.GeoReverseQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GeoReverseQuery,
          Types.GeoReverseQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GeoReverseQuery,
    Types.GeoReverseQueryVariables
  >(Operations.GeoReverseDocument, variables, options)
}
export type GeoReverseQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GeoReverseQuery,
  Types.GeoReverseQueryVariables
>

/**
 * __useGeoAutocompleteQuery__
 *
 * To run a query within a Vue component, call `useGeoAutocompleteQuery` and pass it any options that fit your needs.
 * When your component renders, `useGeoAutocompleteQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGeoAutocompleteQuery(
 *   {
 *      text: // value for 'text'
 *      lang: // value for 'lang'
 *      type: // value for 'type'
 *   }
 * );
 */
export function useGeoAutocompleteQuery(
  variables:
    | Types.GeoAutocompleteQueryVariables
    | VueCompositionApi.Ref<Types.GeoAutocompleteQueryVariables>
    | ReactiveFunction<Types.GeoAutocompleteQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GeoAutocompleteQuery,
        Types.GeoAutocompleteQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GeoAutocompleteQuery,
          Types.GeoAutocompleteQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GeoAutocompleteQuery,
          Types.GeoAutocompleteQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GeoAutocompleteQuery,
    Types.GeoAutocompleteQueryVariables
  >(Operations.GeoAutocompleteDocument, variables, options)
}
export type GeoAutocompleteQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GeoAutocompleteQuery,
  Types.GeoAutocompleteQueryVariables
>

/**
 * __useGetMediaQuery__
 *
 * To run a query within a Vue component, call `useGetMediaQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetMediaQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetMediaQuery(
 *   {
 *      where: // value for 'where'
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      order_by: // value for 'order_by'
 *   }
 * );
 */
export function useGetMediaQuery(
  variables:
    | Types.GetMediaQueryVariables
    | VueCompositionApi.Ref<Types.GetMediaQueryVariables>
    | ReactiveFunction<Types.GetMediaQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetMediaQuery,
        Types.GetMediaQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetMediaQuery,
          Types.GetMediaQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetMediaQuery,
          Types.GetMediaQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetMediaQuery,
    Types.GetMediaQueryVariables
  >(Operations.GetMediaDocument, variables, options)
}
export type GetMediaQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetMediaQuery,
  Types.GetMediaQueryVariables
>

/**
 * __useGetUserPaymentQuery__
 *
 * To run a query within a Vue component, call `useGetUserPaymentQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserPaymentQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetUserPaymentQuery(
 *   {
 *      id: // value for 'id'
 *   }
 * );
 */
export function useGetUserPaymentQuery(
  variables:
    | Types.GetUserPaymentQueryVariables
    | VueCompositionApi.Ref<Types.GetUserPaymentQueryVariables>
    | ReactiveFunction<Types.GetUserPaymentQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetUserPaymentQuery,
        Types.GetUserPaymentQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetUserPaymentQuery,
          Types.GetUserPaymentQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetUserPaymentQuery,
          Types.GetUserPaymentQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetUserPaymentQuery,
    Types.GetUserPaymentQueryVariables
  >(Operations.GetUserPaymentDocument, variables, options)
}
export type GetUserPaymentQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetUserPaymentQuery,
  Types.GetUserPaymentQueryVariables
>

/**
 * __useGetProfileQuery__
 *
 * To run a query within a Vue component, call `useGetProfileQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetProfileQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetProfileQuery(
 *   {
 *      username: // value for 'username'
 *   }
 * );
 */
export function useGetProfileQuery(
  variables:
    | Types.GetProfileQueryVariables
    | VueCompositionApi.Ref<Types.GetProfileQueryVariables>
    | ReactiveFunction<Types.GetProfileQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetProfileQuery,
        Types.GetProfileQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetProfileQuery,
          Types.GetProfileQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetProfileQuery,
          Types.GetProfileQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetProfileQuery,
    Types.GetProfileQueryVariables
  >(Operations.GetProfileDocument, variables, options)
}
export type GetProfileQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetProfileQuery,
  Types.GetProfileQueryVariables
>

/**
 * __useGetSearchPhotographersQuery__
 *
 * To run a query within a Vue component, call `useGetSearchPhotographersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSearchPhotographersQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetSearchPhotographersQuery(
 *   {
 *      where: // value for 'where'
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      orderBy: // value for 'orderBy'
 *   }
 * );
 */
export function useGetSearchPhotographersQuery(
  variables:
    | Types.GetSearchPhotographersQueryVariables
    | VueCompositionApi.Ref<Types.GetSearchPhotographersQueryVariables>
    | ReactiveFunction<Types.GetSearchPhotographersQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetSearchPhotographersQuery,
        Types.GetSearchPhotographersQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetSearchPhotographersQuery,
          Types.GetSearchPhotographersQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetSearchPhotographersQuery,
          Types.GetSearchPhotographersQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetSearchPhotographersQuery,
    Types.GetSearchPhotographersQueryVariables
  >(Operations.GetSearchPhotographersDocument, variables, options)
}
export type GetSearchPhotographersQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetSearchPhotographersQuery,
  Types.GetSearchPhotographersQueryVariables
>

/**
 * __useGetSearchPhotographersLocationQuery__
 *
 * To run a query within a Vue component, call `useGetSearchPhotographersLocationQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetSearchPhotographersLocationQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetSearchPhotographersLocationQuery(
 *   {
 *      args: // value for 'args'
 *   }
 * );
 */
export function useGetSearchPhotographersLocationQuery(
  variables:
    | Types.GetSearchPhotographersLocationQueryVariables
    | VueCompositionApi.Ref<Types.GetSearchPhotographersLocationQueryVariables>
    | ReactiveFunction<Types.GetSearchPhotographersLocationQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetSearchPhotographersLocationQuery,
        Types.GetSearchPhotographersLocationQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetSearchPhotographersLocationQuery,
          Types.GetSearchPhotographersLocationQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetSearchPhotographersLocationQuery,
          Types.GetSearchPhotographersLocationQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetSearchPhotographersLocationQuery,
    Types.GetSearchPhotographersLocationQueryVariables
  >(Operations.GetSearchPhotographersLocationDocument, variables, options)
}
export type GetSearchPhotographersLocationQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetSearchPhotographersLocationQuery,
  Types.GetSearchPhotographersLocationQueryVariables
>

/**
 * __useGetStoreQuery__
 *
 * To run a query within a Vue component, call `useGetStoreQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetStoreQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetStoreQuery(
 *   {
 *      id: // value for 'id'
 *   }
 * );
 */
export function useGetStoreQuery(
  variables:
    | Types.GetStoreQueryVariables
    | VueCompositionApi.Ref<Types.GetStoreQueryVariables>
    | ReactiveFunction<Types.GetStoreQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetStoreQuery,
        Types.GetStoreQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetStoreQuery,
          Types.GetStoreQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetStoreQuery,
          Types.GetStoreQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetStoreQuery,
    Types.GetStoreQueryVariables
  >(Operations.GetStoreDocument, variables, options)
}
export type GetStoreQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetStoreQuery,
  Types.GetStoreQueryVariables
>

/**
 * __useGetUserAlbumsQuery__
 *
 * To run a query within a Vue component, call `useGetUserAlbumsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserAlbumsQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetUserAlbumsQuery(
 *   {
 *      userId: // value for 'userId'
 *   }
 * );
 */
export function useGetUserAlbumsQuery(
  variables:
    | Types.GetUserAlbumsQueryVariables
    | VueCompositionApi.Ref<Types.GetUserAlbumsQueryVariables>
    | ReactiveFunction<Types.GetUserAlbumsQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetUserAlbumsQuery,
        Types.GetUserAlbumsQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetUserAlbumsQuery,
          Types.GetUserAlbumsQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetUserAlbumsQuery,
          Types.GetUserAlbumsQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetUserAlbumsQuery,
    Types.GetUserAlbumsQueryVariables
  >(Operations.GetUserAlbumsDocument, variables, options)
}
export type GetUserAlbumsQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetUserAlbumsQuery,
  Types.GetUserAlbumsQueryVariables
>

/**
 * __useGetUserStyleQuery__
 *
 * To run a query within a Vue component, call `useGetUserStyleQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserStyleQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetUserStyleQuery(
 *   {
 *      userId: // value for 'userId'
 *   }
 * );
 */
export function useGetUserStyleQuery(
  variables:
    | Types.GetUserStyleQueryVariables
    | VueCompositionApi.Ref<Types.GetUserStyleQueryVariables>
    | ReactiveFunction<Types.GetUserStyleQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetUserStyleQuery,
        Types.GetUserStyleQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetUserStyleQuery,
          Types.GetUserStyleQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetUserStyleQuery,
          Types.GetUserStyleQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetUserStyleQuery,
    Types.GetUserStyleQueryVariables
  >(Operations.GetUserStyleDocument, variables, options)
}
export type GetUserStyleQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetUserStyleQuery,
  Types.GetUserStyleQueryVariables
>

/**
 * __useGetStyleQuery__
 *
 * To run a query within a Vue component, call `useGetStyleQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetStyleQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetStyleQuery(
 *   {
 *   }
 * );
 */
export function useGetStyleQuery(
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetStyleQuery,
        Types.GetStyleQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetStyleQuery,
          Types.GetStyleQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetStyleQuery,
          Types.GetStyleQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<Types.GetStyleQuery, undefined>(
    Operations.GetStyleDocument,
    undefined,
    options
  )
}
export type GetStyleQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetStyleQuery,
  Types.GetStyleQueryVariables
>

/**
 * __useGetClientsQuery__
 *
 * To run a query within a Vue component, call `useGetClientsQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetClientsQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetClientsQuery(
 *   {
 *      userId: // value for 'userId'
 *   }
 * );
 */
export function useGetClientsQuery(
  variables:
    | Types.GetClientsQueryVariables
    | VueCompositionApi.Ref<Types.GetClientsQueryVariables>
    | ReactiveFunction<Types.GetClientsQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetClientsQuery,
        Types.GetClientsQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetClientsQuery,
          Types.GetClientsQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetClientsQuery,
          Types.GetClientsQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetClientsQuery,
    Types.GetClientsQueryVariables
  >(Operations.GetClientsDocument, variables, options)
}
export type GetClientsQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetClientsQuery,
  Types.GetClientsQueryVariables
>

/**
 * __useGetUsersQuery__
 *
 * To run a query within a Vue component, call `useGetUsersQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUsersQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetUsersQuery(
 *   {
 *      where: // value for 'where'
 *      offset: // value for 'offset'
 *      limit: // value for 'limit'
 *   }
 * );
 */
export function useGetUsersQuery(
  variables:
    | Types.GetUsersQueryVariables
    | VueCompositionApi.Ref<Types.GetUsersQueryVariables>
    | ReactiveFunction<Types.GetUsersQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetUsersQuery,
        Types.GetUsersQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetUsersQuery,
          Types.GetUsersQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetUsersQuery,
          Types.GetUsersQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetUsersQuery,
    Types.GetUsersQueryVariables
  >(Operations.GetUsersDocument, variables, options)
}
export type GetUsersQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetUsersQuery,
  Types.GetUsersQueryVariables
>

/**
 * __useGetUserPublicQuery__
 *
 * To run a query within a Vue component, call `useGetUserPublicQuery` and pass it any options that fit your needs.
 * When your component renders, `useGetUserPublicQuery` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the query, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/query.html#options;
 *
 * @example
 * const { result, loading, error } = useGetUserPublicQuery(
 *   {
 *      where: // value for 'where'
 *   }
 * );
 */
export function useGetUserPublicQuery(
  variables:
    | Types.GetUserPublicQueryVariables
    | VueCompositionApi.Ref<Types.GetUserPublicQueryVariables>
    | ReactiveFunction<Types.GetUserPublicQueryVariables>,
  options:
    | VueApolloComposable.UseQueryOptions<
        Types.GetUserPublicQuery,
        Types.GetUserPublicQueryVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseQueryOptions<
          Types.GetUserPublicQuery,
          Types.GetUserPublicQueryVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseQueryOptions<
          Types.GetUserPublicQuery,
          Types.GetUserPublicQueryVariables
        >
      > = {}
) {
  return VueApolloComposable.useQuery<
    Types.GetUserPublicQuery,
    Types.GetUserPublicQueryVariables
  >(Operations.GetUserPublicDocument, variables, options)
}
export type GetUserPublicQueryCompositionFunctionResult = VueApolloComposable.UseQueryReturn<
  Types.GetUserPublicQuery,
  Types.GetUserPublicQueryVariables
>

/**
 * __useAlbumsSubscription__
 *
 * To run a query within a Vue component, call `useAlbumsSubscription` and pass it any options that fit your needs.
 * When your component renders, `useAlbumsSubscription` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the subscription, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/subscription.html#options;
 *
 * @example
 * const { result, loading, error } = useAlbumsSubscription(
 *   {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      userId: // value for 'userId'
 *      order_by: // value for 'order_by'
 *   }
 * );
 */
export function useAlbumsSubscription(
  variables:
    | Types.AlbumsSubscriptionVariables
    | VueCompositionApi.Ref<Types.AlbumsSubscriptionVariables>
    | ReactiveFunction<Types.AlbumsSubscriptionVariables>,
  options:
    | VueApolloComposable.UseSubscriptionOptions<
        Types.AlbumsSubscription,
        Types.AlbumsSubscriptionVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseSubscriptionOptions<
          Types.AlbumsSubscription,
          Types.AlbumsSubscriptionVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseSubscriptionOptions<
          Types.AlbumsSubscription,
          Types.AlbumsSubscriptionVariables
        >
      > = {}
) {
  return VueApolloComposable.useSubscription<
    Types.AlbumsSubscription,
    Types.AlbumsSubscriptionVariables
  >(Operations.AlbumsDocument, variables, options)
}
export type AlbumsSubscriptionCompositionFunctionResult = VueApolloComposable.UseSubscriptionReturn<
  Types.AlbumsSubscription,
  Types.AlbumsSubscriptionVariables
>

/**
 * __useCurrentUserSubscription__
 *
 * To run a query within a Vue component, call `useCurrentUserSubscription` and pass it any options that fit your needs.
 * When your component renders, `useCurrentUserSubscription` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the subscription, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/subscription.html#options;
 *
 * @example
 * const { result, loading, error } = useCurrentUserSubscription(
 *   {
 *      userId: // value for 'userId'
 *   }
 * );
 */
export function useCurrentUserSubscription(
  variables:
    | Types.CurrentUserSubscriptionVariables
    | VueCompositionApi.Ref<Types.CurrentUserSubscriptionVariables>
    | ReactiveFunction<Types.CurrentUserSubscriptionVariables>,
  options:
    | VueApolloComposable.UseSubscriptionOptions<
        Types.CurrentUserSubscription,
        Types.CurrentUserSubscriptionVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseSubscriptionOptions<
          Types.CurrentUserSubscription,
          Types.CurrentUserSubscriptionVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseSubscriptionOptions<
          Types.CurrentUserSubscription,
          Types.CurrentUserSubscriptionVariables
        >
      > = {}
) {
  return VueApolloComposable.useSubscription<
    Types.CurrentUserSubscription,
    Types.CurrentUserSubscriptionVariables
  >(Operations.CurrentUserDocument, variables, options)
}
export type CurrentUserSubscriptionCompositionFunctionResult = VueApolloComposable.UseSubscriptionReturn<
  Types.CurrentUserSubscription,
  Types.CurrentUserSubscriptionVariables
>

/**
 * __useEventsSubscription__
 *
 * To run a query within a Vue component, call `useEventsSubscription` and pass it any options that fit your needs.
 * When your component renders, `useEventsSubscription` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the subscription, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/subscription.html#options;
 *
 * @example
 * const { result, loading, error } = useEventsSubscription(
 *   {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      userId: // value for 'userId'
 *      order_by: // value for 'order_by'
 *   }
 * );
 */
export function useEventsSubscription(
  variables:
    | Types.EventsSubscriptionVariables
    | VueCompositionApi.Ref<Types.EventsSubscriptionVariables>
    | ReactiveFunction<Types.EventsSubscriptionVariables>,
  options:
    | VueApolloComposable.UseSubscriptionOptions<
        Types.EventsSubscription,
        Types.EventsSubscriptionVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseSubscriptionOptions<
          Types.EventsSubscription,
          Types.EventsSubscriptionVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseSubscriptionOptions<
          Types.EventsSubscription,
          Types.EventsSubscriptionVariables
        >
      > = {}
) {
  return VueApolloComposable.useSubscription<
    Types.EventsSubscription,
    Types.EventsSubscriptionVariables
  >(Operations.EventsDocument, variables, options)
}
export type EventsSubscriptionCompositionFunctionResult = VueApolloComposable.UseSubscriptionReturn<
  Types.EventsSubscription,
  Types.EventsSubscriptionVariables
>

/**
 * __useMediaSubscription__
 *
 * To run a query within a Vue component, call `useMediaSubscription` and pass it any options that fit your needs.
 * When your component renders, `useMediaSubscription` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the subscription, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/subscription.html#options;
 *
 * @example
 * const { result, loading, error } = useMediaSubscription(
 *   {
 *      where: // value for 'where'
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      order_by: // value for 'order_by'
 *   }
 * );
 */
export function useMediaSubscription(
  variables:
    | Types.MediaSubscriptionVariables
    | VueCompositionApi.Ref<Types.MediaSubscriptionVariables>
    | ReactiveFunction<Types.MediaSubscriptionVariables>,
  options:
    | VueApolloComposable.UseSubscriptionOptions<
        Types.MediaSubscription,
        Types.MediaSubscriptionVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseSubscriptionOptions<
          Types.MediaSubscription,
          Types.MediaSubscriptionVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseSubscriptionOptions<
          Types.MediaSubscription,
          Types.MediaSubscriptionVariables
        >
      > = {}
) {
  return VueApolloComposable.useSubscription<
    Types.MediaSubscription,
    Types.MediaSubscriptionVariables
  >(Operations.MediaDocument, variables, options)
}
export type MediaSubscriptionCompositionFunctionResult = VueApolloComposable.UseSubscriptionReturn<
  Types.MediaSubscription,
  Types.MediaSubscriptionVariables
>

/**
 * __useStoreSubscription__
 *
 * To run a query within a Vue component, call `useStoreSubscription` and pass it any options that fit your needs.
 * When your component renders, `useStoreSubscription` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the subscription, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/subscription.html#options;
 *
 * @example
 * const { result, loading, error } = useStoreSubscription(
 *   {
 *      limit: // value for 'limit'
 *      offset: // value for 'offset'
 *      userId: // value for 'userId'
 *      type: // value for 'type'
 *      order_by: // value for 'order_by'
 *   }
 * );
 */
export function useStoreSubscription(
  variables:
    | Types.StoreSubscriptionVariables
    | VueCompositionApi.Ref<Types.StoreSubscriptionVariables>
    | ReactiveFunction<Types.StoreSubscriptionVariables>,
  options:
    | VueApolloComposable.UseSubscriptionOptions<
        Types.StoreSubscription,
        Types.StoreSubscriptionVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseSubscriptionOptions<
          Types.StoreSubscription,
          Types.StoreSubscriptionVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseSubscriptionOptions<
          Types.StoreSubscription,
          Types.StoreSubscriptionVariables
        >
      > = {}
) {
  return VueApolloComposable.useSubscription<
    Types.StoreSubscription,
    Types.StoreSubscriptionVariables
  >(Operations.StoreDocument, variables, options)
}
export type StoreSubscriptionCompositionFunctionResult = VueApolloComposable.UseSubscriptionReturn<
  Types.StoreSubscription,
  Types.StoreSubscriptionVariables
>

/**
 * __useTransactionsSubscription__
 *
 * To run a query within a Vue component, call `useTransactionsSubscription` and pass it any options that fit your needs.
 * When your component renders, `useTransactionsSubscription` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the subscription, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/subscription.html#options;
 *
 * @example
 * const { result, loading, error } = useTransactionsSubscription(
 *   {
 *      offset: // value for 'offset'
 *      where: // value for 'where'
 *   }
 * );
 */
export function useTransactionsSubscription(
  variables:
    | Types.TransactionsSubscriptionVariables
    | VueCompositionApi.Ref<Types.TransactionsSubscriptionVariables>
    | ReactiveFunction<Types.TransactionsSubscriptionVariables>,
  options:
    | VueApolloComposable.UseSubscriptionOptions<
        Types.TransactionsSubscription,
        Types.TransactionsSubscriptionVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseSubscriptionOptions<
          Types.TransactionsSubscription,
          Types.TransactionsSubscriptionVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseSubscriptionOptions<
          Types.TransactionsSubscription,
          Types.TransactionsSubscriptionVariables
        >
      > = {}
) {
  return VueApolloComposable.useSubscription<
    Types.TransactionsSubscription,
    Types.TransactionsSubscriptionVariables
  >(Operations.TransactionsDocument, variables, options)
}
export type TransactionsSubscriptionCompositionFunctionResult = VueApolloComposable.UseSubscriptionReturn<
  Types.TransactionsSubscription,
  Types.TransactionsSubscriptionVariables
>

/**
 * __useWalletSubscription__
 *
 * To run a query within a Vue component, call `useWalletSubscription` and pass it any options that fit your needs.
 * When your component renders, `useWalletSubscription` returns an object from Apollo Client that contains result, loading and error properties
 * you can use to render your UI.
 *
 * @param options that will be passed into the subscription, supported options are listed on: https://v4.apollo.vuejs.org/guide-composable/subscription.html#options;
 *
 * @example
 * const { result, loading, error } = useWalletSubscription(
 *   {
 *      userId: // value for 'userId'
 *   }
 * );
 */
export function useWalletSubscription(
  variables:
    | Types.WalletSubscriptionVariables
    | VueCompositionApi.Ref<Types.WalletSubscriptionVariables>
    | ReactiveFunction<Types.WalletSubscriptionVariables>,
  options:
    | VueApolloComposable.UseSubscriptionOptions<
        Types.WalletSubscription,
        Types.WalletSubscriptionVariables
      >
    | VueCompositionApi.Ref<
        VueApolloComposable.UseSubscriptionOptions<
          Types.WalletSubscription,
          Types.WalletSubscriptionVariables
        >
      >
    | ReactiveFunction<
        VueApolloComposable.UseSubscriptionOptions<
          Types.WalletSubscription,
          Types.WalletSubscriptionVariables
        >
      > = {}
) {
  return VueApolloComposable.useSubscription<
    Types.WalletSubscription,
    Types.WalletSubscriptionVariables
  >(Operations.WalletDocument, variables, options)
}
export type WalletSubscriptionCompositionFunctionResult = VueApolloComposable.UseSubscriptionReturn<
  Types.WalletSubscription,
  Types.WalletSubscriptionVariables
>
